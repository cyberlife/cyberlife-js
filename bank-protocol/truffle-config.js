var HDWalletProvider = require("truffle-hdwallet-provider");

require('dotenv').config()

module.exports = {

  networks: {

    development: {
      host: "localhost",
      port: 8545,
      network_id: "*",
      from: "0x6259ac218eed8caf47e26246d7e13c1df70165f2",
      gas: 8000000,
      gasPrice: 20000000000
    },

    ropsten: {

      provider: function() {
        return new HDWalletProvider(process.env.mnemonic, "https://ropsten.infura.io/" + process.env.INFURA_API.toString())
      },
      from: "0x5581364f1350B82Ed4E25874f3727395BF6Ce490",
      network_id: 3,
      gas: 7500000

    },

    rinkeby: {
        provider: function() {
          return new HDWalletProvider(process.env.mnemonic, "https://rinkeby.infura.io/" + process.env.INFURA_API.toString())
        },
        from: "0x5581364f1350B82Ed4E25874f3727395BF6Ce490",
        network_id: 4,
        gas: 6500000
    }

  },

  mocha: {
    enableTimeouts: false
  },

  compilers: {
    solc: {
      version: "0.5.3",
    },
  },

  solc: {
    optimizer: { // Turning on compiler optimization that removes some local variables during compilation
      enabled: true,
      runs: 200
    }
  }

};
