//BUSINESS
var DAO = artifacts.require("contracts/cyber/DAO.sol");

//SAVINGS
var SavingsRebalancerData = artifacts.require("contracts/defi/savings/SavingsRebalancerData.sol");
var SavingsRebalancer = artifacts.require("contracts/defi/savings/SavingsRebalancer.sol");

//KYBER-LOCAL
var KyberNetworkProxy = artifacts.require("contracts/defi/kyber-core/KyberNetworkProxy.sol");
var MockMANA = artifacts.require("contracts/defi/kyber-core/MockMANA.sol");
var MockZILL = artifacts.require("contracts/defi/kyber-core/MockZILL.sol");
var MockOMG = artifacts.require("contracts/defi/kyber-core/MockOMG.sol");
var MockKNC = artifacts.require("contracts/defi/kyber-core/MockKNC.sol");

//LENDING
var LendingManager = artifacts.require("contracts/defi/lending/LendingManager.sol");
var LendingTreasury = artifacts.require("contracts/defi/lending/LendingTreasury.sol");
var NonFungibleCollateral = artifacts.require("contracts/token/NonFungibleCollateral.sol");
var Principal = artifacts.require("contracts/token/Principal.sol");
var MockDAI = artifacts.require("contracts/token/MockDAI.sol");

var Web3 = require('web3');

require('dotenv').config();

module.exports = async function (deployer, network, accounts) {

  //Main address
  var mainAddress = accounts[0]

  //REAL KNC, OMG, MANA ON RINKEBY

  var kyberRinkebyTokens = [

    "0x55080ac40700bde5725d8a87f48a01e192f660af",
    "0x659c4206b2ee8cc00af837cdda132eb30fa58df8",
    "0xfea1141e988c1919a9b04b262b92f24adcdd8e9d"

  ];

  //Kyber mocked minting amount

  var mintingAmount = "10000000000000000000000000000000000000000000000"

  //Rebalancer data params
  var rebalancingPause = 600000; //seconds
  var baseFee = 50; //wei
  var minServiceDuration = 140; //seconds
  var initialCustomerDeposit = "16000000000000000000" //9 ETH

  //Loan tokens params

  var daiInitialMint = 20;

  //LendingTreasury params

  var initialLendingDeposit = "4000000000000000000" //4 ETH

  if (network == 'development') {

    //Deploying savings branch

    deployer.deploy(SavingsRebalancerData, {from: mainAddress}).then(function() {
    return deployer.deploy(KyberNetworkProxy, mainAddress, {from: mainAddress, value: "4000000000000000000"}).then(function() {
    return deployer.deploy(DAO, {from: mainAddress}).then(function() {
    return deployer.deploy(SavingsRebalancer, {from: mainAddress}).then(async function() {
    return deployer.deploy(MockKNC, "Kyber Network Token", "KNC", 18, {from: mainAddress}).then(function() {
    return deployer.deploy(MockOMG, "Omise Go Token", "OMG", 18, {from: mainAddress}).then(function() {
    return deployer.deploy(MockMANA, "Mana Token", "MANA", 18, {from: mainAddress}).then(function() {
    return deployer.deploy(MockZILL, "Zilliqa", "ZILL", 18, {from: mainAddress}).then(function() {

    //Deploying lending branch

    return deployer.deploy(LendingManager, {from: mainAddress}).then(function() {
    return deployer.deploy(LendingTreasury, {from: mainAddress}).then(function() {
    return deployer.deploy(Principal, "PRINCIPAL", "PRI", 18, {from: mainAddress}).then(function() {
    return deployer.deploy(MockDAI, "DAI", "DAI", 0, {from: mainAddress}).then(function() {
    return deployer.deploy(NonFungibleCollateral, "COLLATERAL", "COLL", {from: mainAddress}).then(async function() {

    //Setup all contracts

    var rebalancer = await SavingsRebalancer.deployed();
    var rebalancerData = await SavingsRebalancerData.deployed();
    var kyberProxy = await KyberNetworkProxy.deployed();
    var omgToken = await MockOMG.deployed();
    var zillToken = await MockZILL.deployed();
    var manaToken = await MockMANA.deployed();
    var kncToken = await MockKNC.deployed();
    var daiToken = await MockDAI.deployed();
    var loanPrincipal = await Principal.deployed();
    var nonFungibleCollateral = await NonFungibleCollateral.deployed();
    var lendingManager = await LendingManager.deployed();
    var lendingTreasury = await LendingTreasury.deployed();
    var dao = await DAO.deployed();

    var web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
    var coinsPreference = await web3.utils.utf8ToHex("");

    //Setup loan tokens

    await daiToken.mint(mainAddress, daiInitialMint, {from: mainAddress})
    await daiToken.mint(accounts[1], daiInitialMint, {from: mainAddress})
    await daiToken.mint(accounts[2], daiInitialMint, {from: mainAddress})

    await loanPrincipal.mint(mainAddress, mintingAmount, {from: mainAddress})
    await loanPrincipal.mint(accounts[1], mintingAmount, {from: mainAddress})
    await loanPrincipal.mint(accounts[2], mintingAmount, {from: mainAddress})

    await nonFungibleCollateral.mint(mainAddress, 0, {from: mainAddress});
    await nonFungibleCollateral.mint(accounts[1], 1, {from: mainAddress});
    await nonFungibleCollateral.mint(accounts[2], 2, {from: mainAddress});

    //Give permissions to LendingManager

    await daiToken.approve(lendingManager.address, daiInitialMint, {from: mainAddress})
    await daiToken.approve(lendingManager.address, daiInitialMint, {from: accounts[1]})
    await daiToken.approve(lendingManager.address, daiInitialMint, {from: accounts[2]})

    await loanPrincipal.approve(lendingManager.address, mintingAmount, {from: mainAddress})
    await loanPrincipal.approve(lendingManager.address, mintingAmount, {from: accounts[1]})
    await loanPrincipal.approve(lendingManager.address, mintingAmount, {from: accounts[2]})

    //Setup LendingManager

    await lendingManager.setLendingTreasury(lendingTreasury.address, {from: mainAddress});

    //Setup LendingTreasury

    await lendingTreasury.toggleRelayer(mainAddress, {from: mainAddress})
    await lendingTreasury.toggleRelayer(accounts[1], {from: mainAddress})
    await lendingTreasury.toggleRelayer(accounts[2], {from: mainAddress})

    await lendingTreasury.setLoanManager(lendingManager.address, {from: mainAddress})

    await lendingTreasury.depositLendingETH({from: mainAddress, value: initialLendingDeposit})
    await lendingTreasury.depositLendingETH({from: accounts[1], value: initialLendingDeposit})
    await lendingTreasury.depositLendingETH({from: accounts[2], value: initialLendingDeposit})
    await lendingTreasury.depositLendingETH({from: accounts[3], value: initialLendingDeposit})

    //Create test loan and fill it

    /*var serializedLoan =
      await getExampleLoanData(web3, "0xEeeeeEeeeEeEeeEeEeEeeEEEeeeeEeeeeeeeEEeE", daiToken.address);

    await lendingManager.requestLoan(serializedLoan);

    await lendingManager.fillLoan(0, {from: mainAddress, value: '500000000000000'})

    console.log("Requested loan and filled it!")*/

    //Provide ETH liquidity to Kyber

    await kyberProxy.provideETHLiquidity({from: mainAddress, value: "3000000000000000000"});

    //Setup mocked Kyber coins

    await omgToken.mint(kyberProxy.address, mintingAmount, {from: mainAddress});
    await zillToken.mint(kyberProxy.address, mintingAmount, {from: mainAddress});
    await manaToken.mint(kyberProxy.address, mintingAmount, {from: mainAddress});
    await kncToken.mint(kyberProxy.address, mintingAmount, {from: mainAddress});

    //Signal Kyber contract about minted coins

    await kyberProxy
    .recordTokenMinting(omgToken.address, mintingAmount, {from: mainAddress})

    await kyberProxy
    .recordTokenMinting(zillToken.address, mintingAmount, {from: mainAddress})

    await kyberProxy
    .recordTokenMinting(manaToken.address, mintingAmount, {from: mainAddress})

    await kyberProxy
    .recordTokenMinting(kncToken.address, mintingAmount, {from: mainAddress})

    //Setup SavingsRebalancerData

    await rebalancerData.changeRebalancerAddress(rebalancer.address, {from: mainAddress});
    await rebalancerData.changeMinRebalancingPause(rebalancingPause, {from: mainAddress});
    await rebalancerData.changeCyberFund(dao.address, {from: mainAddress});
    await rebalancerData.changeBaseFee(baseFee, {from: mainAddress});

    await rebalancerData.changeDiversificationMultiplier(0, 100, {from: mainAddress});
    await rebalancerData.changeDiversificationMultiplier(1, 125, {from: mainAddress});
    await rebalancerData.changeDiversificationMultiplier(2, 150, {from: mainAddress});

    await rebalancerData.changeMinServiceDuration(minServiceDuration, {from: mainAddress});

    await rebalancerData.initializeCustomer(0, {from: mainAddress})

    //Store fee money

    await rebalancerData.depositFees({from: mainAddress, value: initialCustomerDeposit})

    //Setup SavingsRebalancer

    await rebalancer.setKyberProxy(kyberProxy.address, {from: mainAddress});
    await rebalancer.changeRebalancerData(rebalancerData.address, {from: mainAddress});

    //Put money for customer
    await rebalancer.depositETH({from: mainAddress, value: initialCustomerDeposit});

    //Start service for customer

    await rebalancerData.startService({from: mainAddress});

    //Exchange some tokens

    await rebalancer.swapEtherToToken(kncToken.address,
        "100000", "900000000000000", mainAddress, {from: mainAddress});

    await rebalancer.swapTokenToToken(kncToken.address,
        "1", omgToken.address, "90000000000000000", mainAddress, {from: mainAddress});

    await rebalancer.swapTokenToToken(kncToken.address,
        "1", zillToken.address, "90000000000000000", mainAddress, {from: mainAddress});

    await rebalancer.swapTokenToToken(kncToken.address,
        "1", manaToken.address, "90000000000000000", mainAddress, {from: mainAddress});

    //Checks values

    var totalBalance = await rebalancer.getTotalBalance(mainAddress);

    var ethAddress = await rebalancer.getETHAddress();

    var ethBalance = await rebalancer.getCustomerBalance(mainAddress, ethAddress);

    var blockedRequests = await rebalancerData.customerBlockedRequests(mainAddress);

    var feesPerSecond = await rebalancerData.customerPricePerSecond(mainAddress);

    var serviceTime = await rebalancerData.serviceTimeForCurrentFunds(mainAddress);

    var enoughFeeMoney = await rebalancerData.enoughMoneyForMinimumService(mainAddress);

    var mainRelayerApproved = await rebalancerData.isRelayerApproved(mainAddress);

    var stoppedService = await rebalancerData.customerStoppedService(mainAddress);

    var serviceStartTime = await rebalancerData.customerServiceStart(mainAddress);

    var diversificationLevel = await rebalancerData.customerDiversificationLevel(mainAddress);

    var isCustomerInitialized = await rebalancerData.isCustomerInitialized(mainAddress);

    //Log checks values:

    console.log("Total customer balance: " + totalBalance.toString());
    console.log("Customer ETH balance: " + ethBalance.toString());
    console.log("Customer blocks billing requests: " + blockedRequests.toString());
    console.log("Customer fees per second: " + feesPerSecond.toString());
    console.log("Customer max service time: " + serviceTime.toString());
    console.log("Customer has enough fee money: " + enoughFeeMoney.toString());
    console.log("The main relayer is approved: " + mainRelayerApproved.toString());
    console.log("The customer did request a service: " + (!stoppedService).toString());
    console.log("The service for the customer started at: " + serviceStartTime.toString());
    console.log("The diversification level for the customer is: " + diversificationLevel.toString());
    console.log("The customer is initialized: " + isCustomerInitialized.toString());

    console.log("Deployed savings contracts locally!")

  }) }) }) }) }) }) }) }) }) }) }) }) })

  }

}

async function getExampleLoanData(web3Instance, principalAddr, collAddr) {

  var collateralDetails = await web3Instance.eth.abi.encodeParameter('uint256', '2');

  var secondsUntilMaturity = 600;

  var collType = 0; //erc20

  var thresholds = 10; //need to pay after every minute

  var interest = 500; //5%

  var principalAmount = '500000000000000'

  var serializedLoan =
  await
  web3Instance.eth.abi
  .encodeParameters(

    ['uint','uint', 'bytes', 'address', 'address', 'uint', 'uint', 'uint'],
    [secondsUntilMaturity.toString(),
     collType.toString(),
     collateralDetails.toString(),
     principalAddr.toString(),
     collAddr.toString(),
     thresholds.toString(),
     interest.toString(),
     principalAmount.toString()]

  );

  return serializedLoan;

}

async function increaseTime(duration) {
  const id = Date.now()

  return new Promise((resolve, reject) => {
    web3.currentProvider.sendAsync({
      jsonrpc: '2.0',
      method: 'evm_increaseTime',
      params: [duration],
      id: id,
    }, err1 => {
      if (err1) return reject(err1)

      web3.currentProvider.sendAsync({
        jsonrpc: '2.0',
        method: 'evm_mine',
        id: id+1,
      }, (err2, res) => {
        return err2 ? reject(err2) : resolve(res)
      })
    })
  })
}
