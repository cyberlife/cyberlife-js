pragma solidity 0.5.3;

import "contracts/zeppelin/ERC721/ERC721Token.sol";
import "contracts/zeppelin/ownership/Ownable.sol";

contract NonFungibleCollateral is ERC721Token, Ownable {

  constructor(string memory _name, string memory _symbol)
  ERC721Token(_name, _symbol)
  public {}

  function mint(address target, uint256 id) public onlyOwner {

    super._mint(target, id);

  }

}
