pragma solidity 0.5.3;

contract ILendingTreasury {

  event ToggledRelayer(address relayer, bool state);

  event SetLoanManager(address manager);

  event DepositedETH(address customer, uint256 amount);

  event WithdrewETH(address customer, uint256 amount);

  event RegisteredLoanIncome(address customer, uint256 amount);

  function registerLoanIncome(uint id) public payable;

}
