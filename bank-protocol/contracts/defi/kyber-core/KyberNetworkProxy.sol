pragma solidity 0.5.3;

import "contracts/defi/kyber-core/KyberNetworkProxyInterface.sol";
import "contracts/zeppelin/ERC20/DetailedMintableToken.sol";

////////////////////////////////////////////////////////////////////////////////////////////////////////
/// @title Kyber Network proxy for main contract
contract KyberNetworkProxy is KyberNetworkProxyInterface {

    uint256 MAX_QTY = 10 ** 20;

    address admin;

    mapping(address => uint256) tokenAmounts;

    address ETH_TOKEN_ADDRESS = address(0x00eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee);

    modifier onlyAdmin() {

      require(msg.sender == admin, "The caller is not the admin");
      _;

    }

    constructor(address _admin) public payable {

        require(_admin != address(0));
        admin = _admin;

        if (msg.value > 0) {

          tokenAmounts[ETH_TOKEN_ADDRESS] =
            tokenAmounts[ETH_TOKEN_ADDRESS] + msg.value;

        }

    }

    function provideETHLiquidity() public payable onlyAdmin {

      require(msg.value > 0,
        "You should send more than 0 wei when providing liquidity");

      tokenAmounts[ETH_TOKEN_ADDRESS] =
        tokenAmounts[ETH_TOKEN_ADDRESS] + msg.value;

    }

    function recordTokenMinting(address tkn, uint256 amount) public onlyAdmin {

      tokenAmounts[tkn] = tokenAmounts[tkn] + amount;

    }

    function () external payable {

      tokenAmounts[ETH_TOKEN_ADDRESS] = tokenAmounts[ETH_TOKEN_ADDRESS] + msg.value;

    }

    function swapTokenToToken(
        address src,
        uint srcAmount,
        address dest,
        uint destAmount
    )
        public
    {
        bytes memory hint;

        this.tradeWithHint(
            src,
            srcAmount,
            dest,
            msg.sender,
            MAX_QTY,
            destAmount,
            address(0),
            hint
        );
    }

    function swapEtherToToken(address token, uint destAmount)
      public payable {
        bytes memory hint;

        this.tradeWithHint.value(msg.value)
        (
            ETH_TOKEN_ADDRESS,
            msg.value,
            token,
            msg.sender,
            MAX_QTY,
            destAmount,
            address(0),
            hint
        );
    }

    function swapTokenToEther(address token, uint srcAmount, uint destAmount)
      public {
        bytes memory hint;

        this.tradeWithHint(
            token,
            srcAmount,
            ETH_TOKEN_ADDRESS,
            msg.sender,
            MAX_QTY,
            destAmount,
            address(0),
            hint
        );
    }

    struct UserBalance {
        uint srcBalance;
        uint destBalance;
    }

    event ExecuteTrade(address indexed trader, address src, address dest);

    function tradeWithHint(
        address src,
        uint srcAmount,
        address dest,
        address payable destAddress,
        uint maxDestAmount,
        uint destAmount,
        address walletId,
        bytes memory hint
    )
        public
        payable
    {

        if (msg.value > 0) {

          require(tokenAmounts[dest] >= destAmount,
              "The Kyber exchange does not have that many dest tokens");

          tokenAmounts[ETH_TOKEN_ADDRESS] = tokenAmounts[ETH_TOKEN_ADDRESS] + msg.value;

          tokenAmounts[dest] = tokenAmounts[dest] - destAmount;

          DetailedMintableToken destToken =
            DetailedMintableToken(dest);

          destToken.transfer(destAddress, destAmount);

        } else if (dest == ETH_TOKEN_ADDRESS) {

          require(msg.value == 0, "Cannot send ETH when the destination is ETH");

          require(src != ETH_TOKEN_ADDRESS, "The source cannot be ETH if you exchange to ETH");

          require(tokenAmounts[ETH_TOKEN_ADDRESS] >= destAmount,
              "The Kyber exchange does not have that much ETH");

          tokenAmounts[ETH_TOKEN_ADDRESS] = tokenAmounts[ETH_TOKEN_ADDRESS] - destAmount;

          tokenAmounts[src] = tokenAmounts[src] + srcAmount;

          destAddress.transfer(destAmount);

        } else if (dest != ETH_TOKEN_ADDRESS &&
                   src != ETH_TOKEN_ADDRESS) {

          require(msg.value == 0, "Cannot send ETH when no token is ETH");

          require(tokenAmounts[dest] >= destAmount,
              "The Kyber exchange does not have many dest tokens for TOKEN-TOKEN exchange");

          tokenAmounts[src] = tokenAmounts[src] + srcAmount;

          tokenAmounts[dest] = tokenAmounts[dest] - destAmount;

          DetailedMintableToken destToken =
            DetailedMintableToken(dest);

          destToken.transfer(destAddress, destAmount);

        }

        emit ExecuteTrade(destAddress, address(src), address(dest));

    }

    function getCoinAmount(address coin) public view returns (uint256) {

      return tokenAmounts[coin];

    }

    function getEthAddress() public view returns (address) {

      return ETH_TOKEN_ADDRESS;

    }

}
