pragma solidity 0.5.3;

import "contracts/zeppelin/ownership/Ownable.sol";
import "contracts/zeppelin/ContractDetector.sol";

import "contracts/zeppelin/ERC20/DetailedMintableToken.sol";
import "contracts/zeppelin/ERC721/ERC721Token.sol";

import "contracts/interfaces/ILendingManager.sol";
import "contracts/interfaces/ILendingTreasury.sol";

contract LendingManager is ILendingManager, Ownable, ContractDetector {

  enum LoanState {
    CREATED,
    CANCELLED,
    STARTED,
    SUCCESSFUL,
    FAILED
  }

  enum CollateralType {

    ERC20,
    ERC721,
    ETH

  }

  struct Loan {

    uint id;

    LoanState state;

    address payable lender;
    address payable borrower;

    uint startDate;
    uint timeUntilMaturity;

    address principal;
    address collateral;

    CollateralType collateralType;
    bytes collateralDetails;

    uint thresholds;
    uint interest;

    uint amountLent;
    uint collected;

    uint latestRepaymentTimestamp;

  }

  //It is possible to take minRate from kyber contract, but best to get it as an input from the user.
  ERC20 constant internal ETH_TOKEN_ADDRESS = ERC20(0x00eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee);

  uint nonce;

  Loan[] loans;

  mapping(address => uint[]) lentTo;
  mapping(address => uint[]) debtOwed;

  mapping(uint => address) swapCollateral;

  ILendingTreasury treasury;

  constructor() public {

    nonce = 0;

  }

  //OWNER

  function setLendingTreasury(address _treasury) public onlyOwner {

    treasury = ILendingTreasury(_treasury);

    emit SetLendingTreasury(_treasury);

  }

  function requestLoan(

    bytes memory loanData

  ) public payable {

    uint secondsUntilMaturity;
    uint _collateralType;
    bytes memory collateralDetails; //collateral amount for erc20 & eth, ids for erc721
    address _principal;
    address _collateral;
    uint _thresholds;
    uint _interest;
    uint _principalAmount;

    (secondsUntilMaturity, _collateralType, collateralDetails,
     _principal, _collateral, _thresholds, _interest, _principalAmount)
     = abi.decode(loanData, (uint, uint, bytes, address, address, uint, uint, uint));

    require(secondsUntilMaturity > 0,
        "The maturity date needs to be in the future");

    require(_collateralType <= 2,
        "Collateral type needs to be either 0 (ERC20), 1 (ERC721) or 2 (ETH)");

    require(_thresholds >= 1, "You need at least one threshold");

    require(_interest > 0, "You need a positive interest percentage");

    require(_principalAmount > 0,
        "You need the principal amount to be bigger than 0");

    require(_principal == address(ETH_TOKEN_ADDRESS) ||
            isContract(_principal) == true,
            "The principal address needs to be from a contract or to be ETH");

    require(_collateral == address(ETH_TOKEN_ADDRESS) ||
            isContract(_collateral) == true,
            "The collateral address needs to be from a contract or to be ETH");

    nonce++;

    if (_collateralType == 0) {

      uint256 erc20Amount =
        abi.decode(collateralDetails, (uint256));

      DetailedMintableToken fungible =
        DetailedMintableToken(_collateral);

      fungible.transferFrom(msg.sender, address(this), erc20Amount);

    } else if (_collateralType == 1) {

      uint256[] memory ids =
        abi.decode(collateralDetails, (uint256[]));

      require(ids.length > 0,
          "You need to specify at least one id in the collateral array");

      ERC721Token nonFungible = ERC721Token(_collateral);

      for (uint i = 0; i < ids.length; i++) {

        nonFungible.transferFrom(msg.sender, address(this), ids[i]);

      }

    } else if (_collateralType == 2) {

      require(msg.value > 0, "You specified ETH as collateral but sent none");

      uint256 ethAmount =
        abi.decode(collateralDetails, (uint256));

      require(msg.value == ethAmount,
          "The collateral details for ETH were not correctly specified");

    }

    Loan memory newLoan = Loan(

      nonce - 1,
      LoanState.CREATED,
      address(0),
      address(uint160(msg.sender)),
      0,
      secondsUntilMaturity,
      _principal,
      _collateral,
      CollateralType(_collateralType),
      collateralDetails,
      _thresholds,
      _interest,
      _principalAmount,
      0,
      now

    );

    loans.push(newLoan);

    emit RequestedLoan(newLoan.id, loanData);

  }

  function cancelLoan(uint id) public {

    require(id < loans.length + 1, "The id needs to be between bounds");

    require(msg.sender == loans[id].borrower,
        "Only the borrower of a loan can cancel it");

    require(loans[id].state == LoanState.CREATED,
        "The loan must not be already cancelled or ongoing");

    loans[id].state = LoanState.CANCELLED;

    if (loans[id].collateralType == CollateralType.ERC20) {

      uint256 erc20Amount =
        abi.decode(loans[id].collateralDetails, (uint256));

      DetailedMintableToken fungible =
        DetailedMintableToken(loans[id].collateral);

      fungible.transfer(loans[id].borrower, erc20Amount);

    } else if (loans[id].collateralType == CollateralType.ERC721) {

      uint256[] memory ids =
        abi.decode(loans[id].collateralDetails, (uint256[]));

      ERC721Token nonFungible = ERC721Token(loans[id].collateral);

      for (uint i = 0; i < ids.length; i++) {

        nonFungible.transferFrom(address(this), loans[id].borrower, ids[i]);

      }

    } else {

      uint256 ethAmount =
        abi.decode(loans[id].collateralDetails, (uint256));

      loans[id].borrower.transfer(ethAmount);

    }

    emit CancelledLoan(id);

  }

  function fillLoan(uint id) public payable {

    if (loans[id].principal == address(ETH_TOKEN_ADDRESS))
      require(msg.value == loans[id].amountLent,
        "You did not send the ETH amount to fill the loan");

    require(loans[id].state == LoanState.CREATED,
        "The loan must not be already cancelled or ongoing");

    require(loans[id].lender == address(0),
      "Nobody must have already filled the loan");

    loans[id].state = LoanState.STARTED;

    if (loans[id].principal != address(ETH_TOKEN_ADDRESS)) {

      ERC20 principalERC20 = ERC20(loans[id].principal);

      require(
        principalERC20.transferFrom(msg.sender, loans[id].borrower, loans[id].amountLent) == true,
        "Did not manage to transferFrom the needed principal"
      );

    } else {

      loans[id].borrower.transfer(msg.value);

    }

    loans[id].lender = msg.sender;

    loans[id].startDate = now;

    emit FilledLoan(msg.sender, id);

  }

  function payLoan(uint id, uint amount) public payable {

    require(uint(loans[id].state) == 2, "The loan needs to be ongoing");

    uint totalPayback = getAmountToPay(id);

    ERC20 paybackToken;

    if (loans[id].principal != address(ETH_TOKEN_ADDRESS)) {

      require(totalPayback - loans[id].collected >= amount,
          "You do not need to pay back more than principal + interest");

      paybackToken = ERC20(loans[id].principal);

      require(
        paybackToken.transferFrom(msg.sender, loans[id].lender, amount) == true,
        "Did not manage to transferFrom the needed principal"
      );

      loans[id].latestRepaymentTimestamp = now;

      loans[id].collected = loans[id].collected + amount;

      emit PaidLoan(id, loans[id].lender, loans[id].principal, amount);

    } else {

      require(msg.value > 0,
          "If you want to pay back the loan you need to send more than 0 wei");

      require(totalPayback - loans[id].collected >= msg.value,
          "You do not need to pay back more than principal + interest");

      loans[id].lender.transfer(msg.value);

      loans[id].latestRepaymentTimestamp = now;

      loans[id].collected = loans[id].collected + msg.value;

      treasury.registerLoanIncome.value(msg.value)(id);

      emit PaidLoan(id, loans[id].lender, loans[id].principal, msg.value);

    }

    if (loans[id].collected == totalPayback) {

      loans[id].state = LoanState.SUCCESSFUL;

      transferCollateral(id, loans[id].borrower);

      emit SuccessfulLoan(id);

    }

  }

  function declareDefault(uint id) public returns (address, uint, bytes memory) {

    require(canDeclareDefault(id) == true,
      "The loan repayment is on time, cannot force default");

    require(uint(loans[id].state) == 2,
      "A loan needs to be ongoing for it to be defaulted");

    loans[id].state = LoanState.FAILED;

    uint collateralType = transferCollateral(id, loans[id].lender);

    //TODO: swap collateral with Kyber if so indicated

    emit DefaultedLoan(id);

    return (loans[id].collateral, collateralType, loans[id].collateralDetails);

  }

  function enableSwappedCollateral(uint id, address destToken) public {

    require(loans[id].state == LoanState.STARTED,
        "The loan must be ongoing");

    require(isContract(destToken) == true,
      "The destination token address must come from a  contract");

    require(loans[id].lender == msg.sender,
        "The sender must be the lender");

    require(uint(loans[id].collateralType) == 0 ||
            uint(loans[id].collateralType) == 2,
      "The collateral type needs to be ERC20 or ETH");

    swapCollateral[id] = destToken;

    emit EnabledSwapping(id, destToken);

  }

  //PRIVATE

  function transferCollateral(uint id, address payable target) internal returns (uint) {

    if (uint(loans[id].collateralType) == 0) {

      uint256 erc20Amount =
        abi.decode(loans[id].collateralDetails, (uint256));

      ERC20 fungibleColl = ERC20(loans[id].collateral);

      fungibleColl.transfer(target, erc20Amount);

      return 0;

    } else if (uint(loans[id].collateralType) == 1) {

      uint256[] memory ids =
        abi.decode(loans[id].collateralDetails, (uint256[]));

      ERC721Token nonFungibleColl = ERC721Token(loans[id].collateral);

      for (uint i = 0; i < ids.length; i++) {

        nonFungibleColl.transferFrom(address(this), target, ids[i]);

      }

      return 1;

    } else {

      uint256 ethAmount =
        abi.decode(loans[id].collateralDetails, (uint256));

      target.transfer(ethAmount);

      return 2;

    }

  }

  //TODO: Kyber swapping

  //GETTERS

  function getAmountToPay(uint id) public view returns (uint256) {

    return (

      loans[id].amountLent + loans[id].amountLent * loans[id].interest / 10**4

    );

  }

  function canDeclareDefault(uint id) public view returns (bool) {

    uint totalAmountToPay = getAmountToPay(id);

    uint thresholdTime = loans[id].timeUntilMaturity / loans[id].thresholds;

    uint thresholdsPassed = (now - loans[id].startDate) / thresholdTime;

    uint moneyPerThreshold = totalAmountToPay / loans[id].thresholds;

    return loans[id].collected < thresholdsPassed * moneyPerThreshold;

  }

  function getAmountLent(uint id) public view returns (uint) {

    return loans[id].amountLent;

  }

  function getLoanState(uint id) public view returns (uint) {

    return uint(loans[id].state);

  }

  function getLender(uint id) public view returns (address) {

    return loans[id].lender;

  }

  function getBorrower(uint id) public view returns (address) {

    return loans[id].borrower;

  }

  function getETHAddress() public view returns (address) {

    return address(ETH_TOKEN_ADDRESS);

  }

}
