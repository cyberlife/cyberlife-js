pragma solidity 0.5.3;

import "contracts/defi/kyber-core/KyberNetworkProxyInterface.sol";
import "contracts/zeppelin/ownership/Ownable.sol";

import "contracts/zeppelin/SafeMath.sol";

import "contracts/interfaces/ISavingsRebalancer.sol";
import "contracts/interfaces/ISavingsRebalancerData.sol";

import "contracts/zeppelin/ContractDetector.sol";

import "contracts/zeppelin/ERC20/DetailedMintableToken.sol";

contract SavingsRebalancer is ISavingsRebalancer, Ownable, ContractDetector {

    using SafeMath for uint256;
    using SafeMath for int256;

    //It is possible to take minRate from kyber contract, but best to get it as an input from the user.

    mapping(address => mapping(address => uint256)) tokenBalances;
    mapping(address => uint256) totalBalances;
    mapping(address => bool) portfolioIsBeingModified;

    address ETH_TOKEN_ADDRESS = address(0x00eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee);

    KyberNetworkProxyInterface _kyberNetworkProxy;
    ISavingsRebalancerData rebalancerData;

    modifier canTradeForCustomer(address customer) {

      require(

        totalBalances[customer] > 0 &&
        portfolioIsBeingModified[customer] == false,

        "Cannot trade for the customer"

      );

      _;

    }

    modifier customerStartedService(address customer) {

      require(rebalancerData.isCustomerInitialized(customer) == true &&
              rebalancerData.customerStoppedService(customer) == false,

            "The customer did not request the rebalancing service"

            );

      _;

    }

    modifier onlyAllowedRelayer() {

      require(

        rebalancerData.isRelayerApproved(msg.sender) == true,

        "This relayer is not approved"

      );

      _;

    }

    modifier portfolioIsNotModified(address customer) {

      require(
        portfolioIsBeingModified[customer] == false,
        "The customer has their portfolio under modification"
      );

      _;

    }

    function() external payable {}

    function setKyberProxy(address _proxy) public onlyOwner {

      _kyberNetworkProxy = KyberNetworkProxyInterface(_proxy);

      emit ChangedKyberProxy(_proxy);

    }

    function changeRebalancerData(address _data) public onlyOwner {

      rebalancerData = ISavingsRebalancerData(_data);

      emit ChangedRebalancerData(_data);

    }

    function depositETH() public payable portfolioIsNotModified(msg.sender)
    {

      require(msg.value > 0, "You need to send more than 0 wei");

      tokenBalances[msg.sender][ETH_TOKEN_ADDRESS] =
        tokenBalances[msg.sender][ETH_TOKEN_ADDRESS].add(msg.value);

      totalBalances[msg.sender] = totalBalances[msg.sender].add(msg.value);

      emit DepositedETH(msg.sender, msg.value);

    }

    function withdrawETH(uint256 amount)
      public portfolioIsNotModified(msg.sender)
    {

      require(tokenBalances[msg.sender][address(ETH_TOKEN_ADDRESS)] >= amount);

      tokenBalances[msg.sender][ETH_TOKEN_ADDRESS] =
        tokenBalances[msg.sender][ETH_TOKEN_ADDRESS].sub(amount);

      totalBalances[msg.sender] = totalBalances[msg.sender].sub(amount);

      msg.sender.transfer(amount);

      emit WithdrewETH(msg.sender, amount);

    }

    //@param srcToken source token contract address
    //@param srcQty in token wei
    //@param destToken destination token contract address
    //@param destAddress address to send swapped tokens to
    function swapTokenToToken (
      address srcToken,
      uint srcQty,
      address destToken,
      uint destQty,
      address customer
    ) public
      portfolioIsNotModified(customer)
      customerStartedService(customer)
      onlyAllowedRelayer {

        require(tokenBalances[customer][srcToken] >= srcQty,
          "The customer does not have that many source tokens");

        //Block other portfolio modifications until this one is done

        portfolioIsBeingModified[customer] = true;

        //Decrease customer's balance for source token

        tokenBalances[customer][srcToken] =
          tokenBalances[customer][srcToken].sub(srcQty);

        totalBalances[customer] = totalBalances[customer].sub(srcQty);

        DetailedMintableToken detailedSource =
          DetailedMintableToken(srcToken);

        detailedSource.transfer(address(_kyberNetworkProxy), srcQty);

        _kyberNetworkProxy
        .swapTokenToToken(srcToken, srcQty, destToken, destQty);

        //Increment balance of customer for traded token

        tokenBalances[customer][destToken] =
          tokenBalances[customer][destToken].add(destQty);

        totalBalances[customer] = totalBalances[customer].add(destQty);

        //Unlock for modifications

        portfolioIsBeingModified[customer] = false;

        //Emit event

        emit ExchangedTokens(customer, srcToken,
          destToken, srcQty, destQty);

    }

    function swapEtherToToken(
      address token,
      uint amount,
      uint destAmount,
      address customer)
      public
      portfolioIsNotModified(customer)
      customerStartedService(customer)
      onlyAllowedRelayer
      {

        require(amount > 0, "The amount is not greater than zero");

        require(tokenBalances[customer][ETH_TOKEN_ADDRESS] >= amount,
          "The customer does not have that much ETH in the contract");

        //Block other portfolio modifications until this one is done

        portfolioIsBeingModified[customer] = true;

        //Decrease customer's balance for source token

        tokenBalances[customer][ETH_TOKEN_ADDRESS] =
          tokenBalances[customer][ETH_TOKEN_ADDRESS].sub(amount);

        totalBalances[customer] = totalBalances[customer].sub(amount);

        _kyberNetworkProxy.swapEtherToToken.value(amount)(token, destAmount);

        //Increment balance of customer for traded token

        tokenBalances[customer][token] =
          tokenBalances[customer][token].add(destAmount);

        totalBalances[customer] = totalBalances[customer].add(destAmount);

        //Unlock for modifications

        portfolioIsBeingModified[customer] = false;

        //Emit event

        emit ExchangedTokens(customer, ETH_TOKEN_ADDRESS,
          token, amount, destAmount);

    }

    function swapTokenToEther (
      address token,
      uint tokenQty,
      uint destQty,
      address customer)
      public
      portfolioIsNotModified(customer)
      customerStartedService(customer)
      onlyAllowedRelayer {

        require(tokenBalances[customer][token] >= tokenQty,
          "The customer that does have that many source tokens");

        //Block other portfolio modifications until this one is done

        portfolioIsBeingModified[customer] = true;

        //Decrease customer's balance for source token

        tokenBalances[customer][token] =
          tokenBalances[customer][token].sub(tokenQty);

        totalBalances[customer] = totalBalances[customer].sub(tokenQty);

        DetailedMintableToken detailedSource =
          DetailedMintableToken(token);

        detailedSource.transfer(address(_kyberNetworkProxy), tokenQty);

        _kyberNetworkProxy.swapTokenToEther(token, tokenQty, destQty);

        //Increment balance of customer for traded token

        tokenBalances[customer][ETH_TOKEN_ADDRESS] =
          tokenBalances[customer][ETH_TOKEN_ADDRESS].add(destQty);

        totalBalances[customer] = totalBalances[customer].add(destQty);

        //Unlock for modifications

        portfolioIsBeingModified[customer] = false;

        //Emit event

        emit ExchangedTokens(customer, token, ETH_TOKEN_ADDRESS,
          tokenQty, destQty);

    }

    //GETTERS

    function getCustomerBalance(address customer, address token) public view returns (uint256) {

      return tokenBalances[customer][token];

    }

    function getTotalBalance(address customer) public view returns (uint256) {

      return totalBalances[customer];

    }

    function getETHAddress() public view returns (address) {

      return address(ETH_TOKEN_ADDRESS);

    }

}
