var lendingTreasury = {
  "contractName": "LendingTreasury",
  "abi": [
    {
      "constant": false,
      "inputs": [],
      "name": "renounceOwnership",
      "outputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function",
      "signature": "0x715018a6"
    },
    {
      "constant": true,
      "inputs": [],
      "name": "owner",
      "outputs": [
        {
          "name": "",
          "type": "address"
        }
      ],
      "payable": false,
      "stateMutability": "view",
      "type": "function",
      "signature": "0x8da5cb5b"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "_newOwner",
          "type": "address"
        }
      ],
      "name": "transferOwnership",
      "outputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function",
      "signature": "0xf2fde38b"
    },
    {
      "inputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "constructor",
      "signature": "constructor"
    },
    {
      "payable": true,
      "stateMutability": "payable",
      "type": "fallback"
    },
    {
      "anonymous": false,
      "inputs": [
        {
          "indexed": true,
          "name": "previousOwner",
          "type": "address"
        }
      ],
      "name": "OwnershipRenounced",
      "type": "event",
      "signature": "0xf8df31144d9c2f0f6b59d69b8b98abd5459d07f2742c4df920b25aae33c64820"
    },
    {
      "anonymous": false,
      "inputs": [
        {
          "indexed": true,
          "name": "previousOwner",
          "type": "address"
        },
        {
          "indexed": true,
          "name": "newOwner",
          "type": "address"
        }
      ],
      "name": "OwnershipTransferred",
      "type": "event",
      "signature": "0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0"
    },
    {
      "anonymous": false,
      "inputs": [
        {
          "indexed": false,
          "name": "relayer",
          "type": "address"
        },
        {
          "indexed": false,
          "name": "state",
          "type": "bool"
        }
      ],
      "name": "ToggledRelayer",
      "type": "event",
      "signature": "0x5d7c494485d85004eb95dd2894261ef8fc876bef99eec3e3577a6869bda29b6d"
    },
    {
      "anonymous": false,
      "inputs": [
        {
          "indexed": false,
          "name": "manager",
          "type": "address"
        }
      ],
      "name": "SetLoanManager",
      "type": "event",
      "signature": "0xe001325effb580d8529fcf6c04532575ac739b436078041adae30263300be484"
    },
    {
      "anonymous": false,
      "inputs": [
        {
          "indexed": false,
          "name": "customer",
          "type": "address"
        },
        {
          "indexed": false,
          "name": "amount",
          "type": "uint256"
        }
      ],
      "name": "DepositedETH",
      "type": "event",
      "signature": "0x81ad7215cbec0a1d55a48f38b34525d2b677147ee7ceb872ac23ecca5ec966e5"
    },
    {
      "anonymous": false,
      "inputs": [
        {
          "indexed": false,
          "name": "customer",
          "type": "address"
        },
        {
          "indexed": false,
          "name": "amount",
          "type": "uint256"
        }
      ],
      "name": "WithdrewETH",
      "type": "event",
      "signature": "0xaa21a47bc8f0df1a253be34a945b1d0a8f458e4da029c0ab59c1fee1e488c066"
    },
    {
      "anonymous": false,
      "inputs": [
        {
          "indexed": false,
          "name": "customer",
          "type": "address"
        },
        {
          "indexed": false,
          "name": "amount",
          "type": "uint256"
        }
      ],
      "name": "RegisteredLoanIncome",
      "type": "event",
      "signature": "0xa3e140a1160e0a4f93ae8cf2fe3e2df38a2ec3856f00a0f7bf4ff5e24d744797"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "relayer",
          "type": "address"
        }
      ],
      "name": "toggleRelayer",
      "outputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function",
      "signature": "0x7512d365"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "_manager",
          "type": "address"
        }
      ],
      "name": "setLoanManager",
      "outputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function",
      "signature": "0x5772ae70"
    },
    {
      "constant": false,
      "inputs": [],
      "name": "depositLendingETH",
      "outputs": [],
      "payable": true,
      "stateMutability": "payable",
      "type": "function",
      "signature": "0xcade50ec"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "amount",
          "type": "uint256"
        }
      ],
      "name": "withdrawLendingETH",
      "outputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function",
      "signature": "0x74815586"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "customer",
          "type": "address"
        },
        {
          "name": "loanId",
          "type": "uint256"
        }
      ],
      "name": "fillLoan",
      "outputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function",
      "signature": "0x1ccc8aca"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "id",
          "type": "uint256"
        },
        {
          "name": "destToken",
          "type": "address"
        }
      ],
      "name": "enableKyberSwap",
      "outputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function",
      "signature": "0x2d2c8308"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "id",
          "type": "uint256"
        },
        {
          "name": "customer",
          "type": "address"
        }
      ],
      "name": "seizeCollateral",
      "outputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function",
      "signature": "0xac9cf4db"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "id",
          "type": "uint256"
        }
      ],
      "name": "registerLoanIncome",
      "outputs": [],
      "payable": true,
      "stateMutability": "payable",
      "type": "function",
      "signature": "0x0efc85ea"
    }
  ],
  "bytecode": "0x608060405234801561001057600080fd5b5060008054600160a060020a03191633179055610e9e806100326000396000f3fe6080604052600436106100b9576000357c0100000000000000000000000000000000000000000000000000000000900480637481558611610081578063748155861461025d5780637512d365146102875780638da5cb5b146102ba578063ac9cf4db146102eb578063cade50ec14610324578063f2fde38b1461032c576100b9565b80630efc85ea146101845780631ccc8aca146101a35780632d2c8308146101dc5780635772ae7014610215578063715018a614610248575b60003411610111576040805160e560020a62461bcd02815260206004820181905260248201527f596f75206e65656420746f2073656e64206d6f7265207468616e203020776569604482015290519081900360640190fd5b33600090815260016020526040902054610131903463ffffffff61035f16565b33600081815260016020908152604091829020939093558051918252349282019290925281517f81ad7215cbec0a1d55a48f38b34525d2b677147ee7ceb872ac23ecca5ec966e5929181900390910190a1005b6101a16004803603602081101561019a57600080fd5b5035610372565b005b3480156101af57600080fd5b506101a1600480360360408110156101c657600080fd5b50600160a060020a03813516906020013561048d565b3480156101e857600080fd5b506101a1600480360360408110156101ff57600080fd5b5080359060200135600160a060020a03166106ca565b34801561022157600080fd5b506101a16004803603602081101561023857600080fd5b5035600160a060020a03166107aa565b34801561025457600080fd5b506101a1610822565b34801561026957600080fd5b506101a16004803603602081101561028057600080fd5b503561088e565b34801561029357600080fd5b506101a1600480360360208110156102aa57600080fd5b5035600160a060020a031661096a565b3480156102c657600080fd5b506102cf6109ea565b60408051600160a060020a039092168252519081900360200190f35b3480156102f757600080fd5b506101a16004803603604081101561030e57600080fd5b5080359060200135600160a060020a03166109f9565b6101a1610c21565b34801561033857600080fd5b506101a16004803603602081101561034f57600080fd5b5035600160a060020a0316610cd6565b8181018281101561036c57fe5b92915050565b600654600160a060020a031633146103be5760405160e560020a62461bcd028152600401808060200182810382526025815260200180610d996025913960400191505060405180910390fd5b600081815260046020908152604080832054600160a060020a031680845260059092529091205434111561042657600160a060020a03811660009081526005602090815260408083208054908490556001909252909120805434929092039091019055610446565b600160a060020a0381166000908152600560205260409020805434900390555b60408051600160a060020a038316815234602082015281517fa3e140a1160e0a4f93ae8cf2fe3e2df38a2ec3856f00a0f7bf4ff5e24d744797929181900390910190a15050565b3360009081526002602052604090205460ff1615156001146104e35760405160e560020a62461bcd028152600401808060200182810382526022815260200180610d776022913960400191505060405180910390fd5b600654604080517f7891878f000000000000000000000000000000000000000000000000000000008152600481018490529051600092600160a060020a031691637891878f916024808301926020929190829003018186803b15801561054857600080fd5b505afa15801561055c573d6000803e3d6000fd5b505050506040513d602081101561057257600080fd5b5051600160a060020a03841660009081526001602090815260408083205460059092529091205491925090820111156105df5760405160e560020a62461bcd028152600401808060200182810382526037815260200180610e3c6037913960400191505060405180910390fd5b600160a060020a0380841660009081526005602052604080822080548501905560065481517f27b743f10000000000000000000000000000000000000000000000000000000081526004810187905291519316926327b743f19285926024808201939182900301818588803b15801561065757600080fd5b505af115801561066b573d6000803e3d6000fd5b505050600160a060020a039094166000818152600360209081526040808320968352958152858220805460ff191660011790556004905293909320805473ffffffffffffffffffffffffffffffffffffffff1916909317909255505050565b3360009081526002602052604090205460ff1615156001146107205760405160e560020a62461bcd028152600401808060200182810382526022815260200180610d776022913960400191505060405180910390fd5b600654604080517f429ed14400000000000000000000000000000000000000000000000000000000815260048101859052600160a060020a0384811660248301529151919092169163429ed14491604480830192600092919082900301818387803b15801561078e57600080fd5b505af11580156107a2573d6000803e3d6000fd5b505050505050565b600054600160a060020a031633146107c157600080fd5b60068054600160a060020a03831673ffffffffffffffffffffffffffffffffffffffff19909116811790915560408051918252517fe001325effb580d8529fcf6c04532575ac739b436078041adae30263300be4849181900360200190a150565b600054600160a060020a0316331461083957600080fd5b60008054604051600160a060020a03909116917ff8df31144d9c2f0f6b59d69b8b98abd5459d07f2742c4df920b25aae33c6482091a26000805473ffffffffffffffffffffffffffffffffffffffff19169055565b33600090815260056020908152604080832054600190925290912054038111156108ec5760405160e560020a62461bcd028152600401808060200182810382526053815260200180610dbe6053913960600191505060405180910390fd5b33600081815260016020526040808220805485900390555183156108fc0291849190818181858888f1935050505015801561092b573d6000803e3d6000fd5b50604080513381526020810183905281517faa21a47bc8f0df1a253be34a945b1d0a8f458e4da029c0ab59c1fee1e488c066929181900390910190a150565b600054600160a060020a0316331461098157600080fd5b600160a060020a038116600081815260026020908152604091829020805460ff19811660ff91821615179182905583519485521615159083015280517f5d7c494485d85004eb95dd2894261ef8fc876bef99eec3e3577a6869bda29b6d9281900390910190a150565b600054600160a060020a031681565b3360009081526002602052604090205460ff161515600114610a4f5760405160e560020a62461bcd028152600401808060200182810382526022815260200180610d776022913960400191505060405180910390fd5b60008060606000600660009054906101000a9004600160a060020a0316600160a060020a031663b07cb0666040518163ffffffff167c010000000000000000000000000000000000000000000000000000000002815260040160206040518083038186803b158015610ac057600080fd5b505afa158015610ad4573d6000803e3d6000fd5b505050506040513d6020811015610aea57600080fd5b5051600654604080517f55e1172d000000000000000000000000000000000000000000000000000000008152600481018a90529051929350600160a060020a03909116916355e1172d9160248082019260009290919082900301818387803b158015610b5557600080fd5b505af1158015610b69573d6000803e3d6000fd5b505050506040513d6000823e601f3d908101601f191682016040526060811015610b9257600080fd5b8151602083015160408401805192949193820192640100000000811115610bb857600080fd5b82016020810184811115610bcb57600080fd5b8151640100000000811182820187101715610be557600080fd5b505050600160a060020a0390991660009081526003602090815260408083209c83529b905299909920805460ff19169055505050505050505050565b60003411610c635760405160e560020a62461bcd02815260040180806020018281038252602b815260200180610e11602b913960400191505060405180910390fd5b33600090815260016020526040902054610c83903463ffffffff61035f16565b33600081815260016020908152604091829020939093558051918252349282019290925281517f81ad7215cbec0a1d55a48f38b34525d2b677147ee7ceb872ac23ecca5ec966e5929181900390910190a1565b600054600160a060020a03163314610ced57600080fd5b610cf681610cf9565b50565b600160a060020a0381161515610d0e57600080fd5b60008054604051600160a060020a03808516939216917f8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e091a36000805473ffffffffffffffffffffffffffffffffffffffff1916600160a060020a039290921691909117905556fe546869732073656e646572206973206e6f7420612076616c69642072656c617965725468652063616c6c6572206973206e6f7420746865206c656e64696e67206d616e61676572596f752063616e6e6f74207769746864726177206d6f7265207468616e20796f757220746f74616c2062616c616e6365206d696e757320776861742069732063757272656e746c79206265696e67206c656e74596f75206e65656420746f2073656e64206120706f73697469766520616d6f756e74206f66206d6f6e657943616e6e6f74206c656e64206d6f7265207468616e2074686520637573746f6d657220617070726f76656420666f72206c656e64696e67a165627a7a72305820a4f2682a90827e658cf1051e481875434f67904c9d74a9ba20bfc055a50eb7940029",
  "deployedBytecode": "0x6080604052600436106100b9576000357c0100000000000000000000000000000000000000000000000000000000900480637481558611610081578063748155861461025d5780637512d365146102875780638da5cb5b146102ba578063ac9cf4db146102eb578063cade50ec14610324578063f2fde38b1461032c576100b9565b80630efc85ea146101845780631ccc8aca146101a35780632d2c8308146101dc5780635772ae7014610215578063715018a614610248575b60003411610111576040805160e560020a62461bcd02815260206004820181905260248201527f596f75206e65656420746f2073656e64206d6f7265207468616e203020776569604482015290519081900360640190fd5b33600090815260016020526040902054610131903463ffffffff61035f16565b33600081815260016020908152604091829020939093558051918252349282019290925281517f81ad7215cbec0a1d55a48f38b34525d2b677147ee7ceb872ac23ecca5ec966e5929181900390910190a1005b6101a16004803603602081101561019a57600080fd5b5035610372565b005b3480156101af57600080fd5b506101a1600480360360408110156101c657600080fd5b50600160a060020a03813516906020013561048d565b3480156101e857600080fd5b506101a1600480360360408110156101ff57600080fd5b5080359060200135600160a060020a03166106ca565b34801561022157600080fd5b506101a16004803603602081101561023857600080fd5b5035600160a060020a03166107aa565b34801561025457600080fd5b506101a1610822565b34801561026957600080fd5b506101a16004803603602081101561028057600080fd5b503561088e565b34801561029357600080fd5b506101a1600480360360208110156102aa57600080fd5b5035600160a060020a031661096a565b3480156102c657600080fd5b506102cf6109ea565b60408051600160a060020a039092168252519081900360200190f35b3480156102f757600080fd5b506101a16004803603604081101561030e57600080fd5b5080359060200135600160a060020a03166109f9565b6101a1610c21565b34801561033857600080fd5b506101a16004803603602081101561034f57600080fd5b5035600160a060020a0316610cd6565b8181018281101561036c57fe5b92915050565b600654600160a060020a031633146103be5760405160e560020a62461bcd028152600401808060200182810382526025815260200180610d996025913960400191505060405180910390fd5b600081815260046020908152604080832054600160a060020a031680845260059092529091205434111561042657600160a060020a03811660009081526005602090815260408083208054908490556001909252909120805434929092039091019055610446565b600160a060020a0381166000908152600560205260409020805434900390555b60408051600160a060020a038316815234602082015281517fa3e140a1160e0a4f93ae8cf2fe3e2df38a2ec3856f00a0f7bf4ff5e24d744797929181900390910190a15050565b3360009081526002602052604090205460ff1615156001146104e35760405160e560020a62461bcd028152600401808060200182810382526022815260200180610d776022913960400191505060405180910390fd5b600654604080517f7891878f000000000000000000000000000000000000000000000000000000008152600481018490529051600092600160a060020a031691637891878f916024808301926020929190829003018186803b15801561054857600080fd5b505afa15801561055c573d6000803e3d6000fd5b505050506040513d602081101561057257600080fd5b5051600160a060020a03841660009081526001602090815260408083205460059092529091205491925090820111156105df5760405160e560020a62461bcd028152600401808060200182810382526037815260200180610e3c6037913960400191505060405180910390fd5b600160a060020a0380841660009081526005602052604080822080548501905560065481517f27b743f10000000000000000000000000000000000000000000000000000000081526004810187905291519316926327b743f19285926024808201939182900301818588803b15801561065757600080fd5b505af115801561066b573d6000803e3d6000fd5b505050600160a060020a039094166000818152600360209081526040808320968352958152858220805460ff191660011790556004905293909320805473ffffffffffffffffffffffffffffffffffffffff1916909317909255505050565b3360009081526002602052604090205460ff1615156001146107205760405160e560020a62461bcd028152600401808060200182810382526022815260200180610d776022913960400191505060405180910390fd5b600654604080517f429ed14400000000000000000000000000000000000000000000000000000000815260048101859052600160a060020a0384811660248301529151919092169163429ed14491604480830192600092919082900301818387803b15801561078e57600080fd5b505af11580156107a2573d6000803e3d6000fd5b505050505050565b600054600160a060020a031633146107c157600080fd5b60068054600160a060020a03831673ffffffffffffffffffffffffffffffffffffffff19909116811790915560408051918252517fe001325effb580d8529fcf6c04532575ac739b436078041adae30263300be4849181900360200190a150565b600054600160a060020a0316331461083957600080fd5b60008054604051600160a060020a03909116917ff8df31144d9c2f0f6b59d69b8b98abd5459d07f2742c4df920b25aae33c6482091a26000805473ffffffffffffffffffffffffffffffffffffffff19169055565b33600090815260056020908152604080832054600190925290912054038111156108ec5760405160e560020a62461bcd028152600401808060200182810382526053815260200180610dbe6053913960600191505060405180910390fd5b33600081815260016020526040808220805485900390555183156108fc0291849190818181858888f1935050505015801561092b573d6000803e3d6000fd5b50604080513381526020810183905281517faa21a47bc8f0df1a253be34a945b1d0a8f458e4da029c0ab59c1fee1e488c066929181900390910190a150565b600054600160a060020a0316331461098157600080fd5b600160a060020a038116600081815260026020908152604091829020805460ff19811660ff91821615179182905583519485521615159083015280517f5d7c494485d85004eb95dd2894261ef8fc876bef99eec3e3577a6869bda29b6d9281900390910190a150565b600054600160a060020a031681565b3360009081526002602052604090205460ff161515600114610a4f5760405160e560020a62461bcd028152600401808060200182810382526022815260200180610d776022913960400191505060405180910390fd5b60008060606000600660009054906101000a9004600160a060020a0316600160a060020a031663b07cb0666040518163ffffffff167c010000000000000000000000000000000000000000000000000000000002815260040160206040518083038186803b158015610ac057600080fd5b505afa158015610ad4573d6000803e3d6000fd5b505050506040513d6020811015610aea57600080fd5b5051600654604080517f55e1172d000000000000000000000000000000000000000000000000000000008152600481018a90529051929350600160a060020a03909116916355e1172d9160248082019260009290919082900301818387803b158015610b5557600080fd5b505af1158015610b69573d6000803e3d6000fd5b505050506040513d6000823e601f3d908101601f191682016040526060811015610b9257600080fd5b8151602083015160408401805192949193820192640100000000811115610bb857600080fd5b82016020810184811115610bcb57600080fd5b8151640100000000811182820187101715610be557600080fd5b505050600160a060020a0390991660009081526003602090815260408083209c83529b905299909920805460ff19169055505050505050505050565b60003411610c635760405160e560020a62461bcd02815260040180806020018281038252602b815260200180610e11602b913960400191505060405180910390fd5b33600090815260016020526040902054610c83903463ffffffff61035f16565b33600081815260016020908152604091829020939093558051918252349282019290925281517f81ad7215cbec0a1d55a48f38b34525d2b677147ee7ceb872ac23ecca5ec966e5929181900390910190a1565b600054600160a060020a03163314610ced57600080fd5b610cf681610cf9565b50565b600160a060020a0381161515610d0e57600080fd5b60008054604051600160a060020a03808516939216917f8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e091a36000805473ffffffffffffffffffffffffffffffffffffffff1916600160a060020a039290921691909117905556fe546869732073656e646572206973206e6f7420612076616c69642072656c617965725468652063616c6c6572206973206e6f7420746865206c656e64696e67206d616e61676572596f752063616e6e6f74207769746864726177206d6f7265207468616e20796f757220746f74616c2062616c616e6365206d696e757320776861742069732063757272656e746c79206265696e67206c656e74596f75206e65656420746f2073656e64206120706f73697469766520616d6f756e74206f66206d6f6e657943616e6e6f74206c656e64206d6f7265207468616e2074686520637573746f6d657220617070726f76656420666f72206c656e64696e67a165627a7a72305820a4f2682a90827e658cf1051e481875434f67904c9d74a9ba20bfc055a50eb7940029",
  "sourceMap": "222:3797:9:-;;;859:23;8:9:-1;5:2;;;30:1;27;20:12;5:2;-1:-1;565:5:68;:18;;-1:-1:-1;;;;;;565:18:68;573:10;565:18;;;222:3797:9;;;;;;",
  "deployedSourceMap": "222:3797:9:-;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;941:1;929:9;:13;921:58;;;;;-1:-1:-1;;;;;921:58:9;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1032:10;1016:27;;;;:15;:27;;;;;;:42;;1048:9;1016:42;:31;:42;:::i;:::-;1002:10;986:27;;;;:15;:27;;;;;;;;;:72;;;;1070:35;;;;;1095:9;1070:35;;;;;;;;;;;;;;;;;;;;222:3797;3489:527;;;;;;13:2:-1;8:3;5:11;2:2;;;29:1;26;19:12;2:2;-1:-1;3489:527:9;;:::i;:::-;;2102:526;;8:9:-1;5:2;;;30:1;27;20:12;5:2;2102:526:9;;;;;;13:2:-1;8:3;5:11;2:2;;;29:1;26;19:12;2:2;-1:-1;;;;;;2102:526:9;;;;;;;;:::i;2632:138::-;;8:9:-1;5:2;;;30:1;27;20:12;5:2;2632:138:9;;;;;;13:2:-1;8:3;5:11;2:2;;;29:1;26;19:12;2:2;-1:-1;2632:138:9;;;;;;-1:-1:-1;;;;;2632:138:9;;:::i;1307:143::-;;8:9:-1;5:2;;;30:1;27;20:12;5:2;1307:143:9;;;;;;13:2:-1;8:3;5:11;2:2;;;29:1;26;19:12;2:2;-1:-1;1307:143:9;-1:-1:-1;;;;;1307:143:9;;:::i;999:111:68:-;;8:9:-1;5:2;;;30:1;27;20:12;5:2;999:111:68;;;:::i;1710:388:9:-;;8:9:-1;5:2;;;30:1;27;20:12;5:2;1710:388:9;;;;;;13:2:-1;8:3;5:11;2:2;;;29:1;26;19:12;2:2;-1:-1;1710:388:9;;:::i;1126:177::-;;8:9:-1;5:2;;;30:1;27;20:12;5:2;1126:177:9;;;;;;13:2:-1;8:3;5:11;2:2;;;29:1;26;19:12;2:2;-1:-1;1126:177:9;-1:-1:-1;;;;;1126:177:9;;:::i;236:20:68:-;;8:9:-1;5:2;;;30:1;27;20:12;5:2;236:20:68;;;:::i;:::-;;;;-1:-1:-1;;;;;236:20:68;;;;;;;;;;;;;;2774:711:9;;8:9:-1;5:2;;;30:1;27;20:12;5:2;2774:711:9;;;;;;13:2:-1;8:3;5:11;2:2;;;29:1;26;19:12;2:2;-1:-1;2774:711:9;;;;;;-1:-1:-1;;;;;2774:711:9;;:::i;1454:252::-;;;:::i;1272:103:68:-;;8:9:-1;5:2;;;30:1;27;20:12;5:2;1272:103:68;;;;;;13:2:-1;8:3;5:11;2:2;;;29:1;26;19:12;2:2;-1:-1;1272:103:68;-1:-1:-1;;;;;1272:103:68;;:::i;1236:128:67:-;1317:7;;;1337;;;;1330:15;;;;1236:128;;;;:::o;3489:527:9:-;791:7;;-1:-1:-1;;;;;791:7:9;769:10;:30;761:80;;;;-1:-1:-1;;;;;761:80:9;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;3567:16;3586:19;;;:15;:19;;;;;;;;;-1:-1:-1;;;;;3586:19:9;3628:30;;;:20;:30;;;;;;;3616:9;:42;3612:346;;;-1:-1:-1;;;;;3703:30:9;;3669:19;3703:30;;;:20;:30;;;;;;;;;;3742:34;;;;-1:-1:-1;3813:25:9;;;;;;;;3691:9;:42;;;;3813:39;;;3785:67;;3612:346;;;-1:-1:-1;;;;;3908:30:9;;;;;;:20;:30;;;;;;;3941:9;3908:42;;3875:75;;3612:346;3969:41;;;-1:-1:-1;;;;;3969:41:9;;;;4000:9;3969:41;;;;;;;;;;;;;;;;;848:1;3489:527;:::o;2102:526::-;648:10;634:25;;;;:13;:25;;;;;;;;:33;;:25;:33;626:80;;;;-1:-1:-1;;;;;626:80:9;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;2204:7;;:29;;;;;;;;;;;;;;2184:17;;-1:-1:-1;;;;;2204:7:9;;:21;;:29;;;;;;;;;;;;;;:7;:29;;;5:2:-1;;;;30:1;27;20:12;5:2;2204:29:9;;;;8:9:-1;5:2;;;45:16;42:1;39;24:38;77:16;74:1;67:27;5:2;2204:29:9;;;;;;;13:2:-1;8:3;5:11;2:2;;;29:1;26;19:12;2:2;-1:-1;2204:29:9;-1:-1:-1;;;;;2305:25:9;;;;;;:15;2204:29;2305:25;;;;;;;;2248:20;:30;;;;;;;2204:29;;-1:-1:-1;2248:53:9;;;:82;;2240:158;;;;-1:-1:-1;;;;;2240:158:9;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;-1:-1:-1;;;;;2438:30:9;;;;;;;:20;:30;;;;;;;;:45;;2405:78;;2490:7;;:44;;;;;;;;;;;;;:7;;;:16;;2471:12;;2490:44;;;;;;;;;;;2471:12;2490:7;:44;;;5:2:-1;;;;30:1;27;20:12;5:2;2490:44:9;;;;8:9:-1;5:2;;;45:16;42:1;39;24:38;77:16;74:1;67:27;5:2;-1:-1;;;;;;;;2541:25:9;;;;;;;:15;:25;;;;;;;;:33;;;;;;;;;:40;;-1:-1:-1;;2541:40:9;2577:4;2541:40;;;2588:15;:23;;;;;;:34;;-1:-1:-1;;2588:34:9;;;;;;;-1:-1:-1;;;2102:526:9:o;2632:138::-;648:10;634:25;;;;:13;:25;;;;;;;;:33;;:25;:33;626:80;;;;-1:-1:-1;;;;;626:80:9;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;2718:7;;:46;;;;;;;;;;;;-1:-1:-1;;;;;2718:46:9;;;;;;;;;:7;;;;;:31;;:46;;;;;:7;;:46;;;;;;;:7;;:46;;;5:2:-1;;;;30:1;27;20:12;5:2;2718:46:9;;;;8:9:-1;5:2;;;45:16;42:1;39;24:38;77:16;74:1;67:27;5:2;2718:46:9;;;;2632:138;;:::o;1307:143::-;717:5:68;;-1:-1:-1;;;;;717:5:68;703:10;:19;695:28;;;;;;1373:7:9;:35;;-1:-1:-1;;;;;1373:35:9;;-1:-1:-1;;1373:35:9;;;;;;;;1420:24;;;;;;;;;;;;;;;;1307:143;:::o;999:111:68:-;717:5;;-1:-1:-1;;;;;717:5:68;703:10;:19;695:28;;;;;;1075:5;;;1056:25;;-1:-1:-1;;;;;1075:5:68;;;;1056:25;;;1103:1;1087:18;;-1:-1:-1;;1087:18:68;;;999:111::o;1710:388:9:-;1837:10;1816:32;;;;:20;:32;;;;;;;;;1786:15;:27;;;;;;;:62;1776:72;;;1768:174;;;;-1:-1:-1;;;;;1768:174:9;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1995:10;1979:27;;;;:15;:27;;;;;;;;:36;;;1949:66;;2022:27;;;;;;2009:6;;2022:27;;1979;2022;2009:6;1995:10;2022:27;;;;;;;;8:9:-1;5:2;;;45:16;42:1;39;24:38;77:16;74:1;67:27;5:2;-1:-1;2061:31:9;;;2073:10;2061:31;;;;;;;;;;;;;;;;;;;;;1710:388;:::o;1126:177::-;717:5:68;;-1:-1:-1;;;;;717:5:68;703:10;:19;695:28;;;;;;-1:-1:-1;;;;;1216:22:9;;;;;;:13;:22;;;;;;;;;;;-1:-1:-1;;1190:48:9;;1216:22;;;;1215:23;1190:48;;;;;1250:47;;;;;1274:22;1250:47;;;;;;;;;;;;;;;;;;1126:177;:::o;236:20:68:-;;;-1:-1:-1;;;;;236:20:68;;:::o;2774:711:9:-;648:10;634:25;;;;:13;:25;;;;;;;;:33;;:25;:33;626:80;;;;-1:-1:-1;;;;;626:80:9;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;2859:26;2891:19;2916:30;2953:18;2974:7;;;;;;;;;-1:-1:-1;;;;;2974:7:9;-1:-1:-1;;;;;2974:21:9;;:23;;;;;;;;;;;;;;;;;;;;;;8:9:-1;5:2;;;30:1;27;20:12;5:2;2974:23:9;;;;8:9:-1;5:2;;;45:16;42:1;39;24:38;77:16;74:1;67:27;5:2;2974:23:9;;;;;;;13:2:-1;8:3;5:11;2:2;;;29:1;26;19:12;2:2;-1:-1;2974:23:9;3068:7;;:26;;;;;;;;;;;;;;2974:23;;-1:-1:-1;;;;;;3068:7:9;;;;:22;;:26;;;;;:7;;:26;;;;;;;;:7;;:26;;;5:2:-1;;;;30:1;27;20:12;5:2;3068:26:9;;;;8:9:-1;5:2;;;45:16;42:1;39;24:38;77:16;74:1;67:27;5:2;3068:26:9;;;;;;39:16:-1;36:1;17:17;2:54;101:4;3068:26:9;80:15:-1;;;-1:-1;;76:31;65:43;;120:4;113:20;13:2;5:11;;2:2;;;29:1;26;19:12;2:2;3068:26:9;;;;;;;;;;;;;;;;;;19:11:-1;11:20;;8:2;;;44:1;41;34:12;8:2;62:21;;123:4;114:14;;138:31;;;135:2;;;182:1;179;172:12;135:2;213:10;;261:11;244:29;;285:43;;;282:58;-1:-1;233:115;230:2;;;361:1;358;351:12;230:2;-1:-1;;;;;;;;3442:25:9;;;3474:5;3442:25;;;:15;:25;;;;;;;;:29;;;;;;;;;;:37;;-1:-1:-1;;3442:37:9;;;-1:-1:-1;;;;;;;;;2774:711:9:o;1454:252::-;1525:1;1513:9;:13;1505:69;;;;-1:-1:-1;;;;;1505:69:9;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1627:10;1611:27;;;;:15;:27;;;;;;:42;;1643:9;1611:42;:31;:42;:::i;:::-;1597:10;1581:27;;;;:15;:27;;;;;;;;;:72;;;;1665:35;;;;;1690:9;1665:35;;;;;;;;;;;;;;;;;;;;1454:252::o;1272:103:68:-;717:5;;-1:-1:-1;;;;;717:5:68;703:10;:19;695:28;;;;;;1341:29;1360:9;1341:18;:29::i;:::-;1272:103;:::o;1510:171::-;-1:-1:-1;;;;;1580:23:68;;;;1572:32;;;;;;1636:5;;;1615:38;;-1:-1:-1;;;;;1615:38:68;;;;1636:5;;;1615:38;;;1659:5;:17;;-1:-1:-1;;1659:17:68;-1:-1:-1;;;;;1659:17:68;;;;;;;;;;1510:171::o",
  "source": "pragma solidity 0.5.3;\n\nimport \"contracts/zeppelin/ownership/Ownable.sol\";\nimport \"contracts/zeppelin/SafeMath.sol\";\n\nimport \"contracts/interfaces/ILendingManager.sol\";\nimport \"contracts/interfaces/ILendingTreasury.sol\";\n\ncontract LendingTreasury is ILendingTreasury, Ownable {\n\n  using SafeMath for uint256;\n\n  mapping(address => uint256) lendingBalances;\n\n  mapping(address => bool) validRelayers;\n\n  mapping(address => mapping(uint => bool)) customerLoanTie;\n\n  mapping(uint => address) loanCustomerTie;\n\n  mapping(address => uint256) totalLentPerCustomer;\n\n  ILendingManager manager;\n\n  modifier onlyValidRelayer() {\n\n    require(validRelayers[msg.sender] == true, \"This sender is not a valid relayer\");\n\n    _;\n\n  }\n\n  modifier onlyLendingManager() {\n\n    require(msg.sender == address(manager), \"The caller is not the lending manager\");\n\n    _;\n\n  }\n\n  constructor() public {}\n\n  function() external payable {\n\n    require(msg.value > 0, \"You need to send more than 0 wei\");\n\n    lendingBalances[msg.sender] = lendingBalances[msg.sender].add(msg.value);\n\n    emit DepositedETH(msg.sender, msg.value);\n\n  }\n\n  //OWNER\n\n  function toggleRelayer(address relayer) public onlyOwner {\n\n    validRelayers[relayer] = !validRelayers[relayer];\n\n    emit ToggledRelayer(relayer, validRelayers[relayer]);\n\n  }\n\n  function setLoanManager(address _manager) public onlyOwner {\n\n    manager = ILendingManager(_manager);\n\n    emit SetLoanManager(_manager);\n\n  }\n\n  function depositLendingETH() public payable {\n\n    require(msg.value > 0, \"You need to send a positive amount of money\");\n\n    lendingBalances[msg.sender] = lendingBalances[msg.sender].add(msg.value);\n\n    emit DepositedETH(msg.sender, msg.value);\n\n  }\n\n  function withdrawLendingETH(uint256 amount) public {\n\n    require(amount <= lendingBalances[msg.sender] - totalLentPerCustomer[msg.sender],\n      \"You cannot withdraw more than your total balance minus what is currently being lent\");\n\n    lendingBalances[msg.sender] = lendingBalances[msg.sender] - amount;\n\n    msg.sender.transfer(amount);\n\n    emit WithdrewETH(msg.sender, amount);\n\n  }\n\n  function fillLoan(address customer, uint loanId) public onlyValidRelayer() {\n\n    uint amountToLend = manager.getAmountLent(loanId);\n\n    require(totalLentPerCustomer[customer]\n        + amountToLend <= lendingBalances[customer],\n        \"Cannot lend more than the customer approved for lending\");\n\n    totalLentPerCustomer[customer] = totalLentPerCustomer[customer] + amountToLend;\n\n    manager.fillLoan.value(amountToLend)(loanId);\n\n    customerLoanTie[customer][loanId] = true;\n\n    loanCustomerTie[loanId] = customer;\n\n  }\n\n  function enableKyberSwap(uint id, address destToken) public onlyValidRelayer() {\n\n    manager.enableSwappedCollateral(id, destToken);\n\n  }\n\n  function seizeCollateral(uint id, address customer) public onlyValidRelayer() {\n\n    address collateralReceived;\n    uint collateralType;\n    bytes memory collateralDetails;\n\n    address ethAddress = manager.getETHAddress();\n\n    (collateralReceived, collateralType, collateralDetails)\n      = manager.declareDefault(id);\n\n    //For the demo we have multiple token types, for production we swap with Kyber\n\n    /*require(ethAddress == collateralReceived,\n      \"The treasury only accepts ETH as collateral\");\n\n    uint ethAmount = abi.decode(collateralDetails, (uint256));\n\n    totalLentPerCustomer[customer] =\n      totalLentPerCustomer[customer] - ethAmount;*/\n\n    customerLoanTie[customer][id] = false;\n\n  }\n\n  function registerLoanIncome(uint id) public payable onlyLendingManager {\n\n    address customer = loanCustomerTie[id];\n\n    if (msg.value > totalLentPerCustomer[customer]) {\n\n      uint256 extraIncome = msg.value - totalLentPerCustomer[customer];\n\n      totalLentPerCustomer[customer] = 0;\n\n      lendingBalances[customer] = lendingBalances[customer] + extraIncome;\n\n    } else {\n\n      totalLentPerCustomer[customer] = totalLentPerCustomer[customer] - msg.value;\n\n    }\n\n    emit RegisteredLoanIncome(customer, msg.value);\n\n  }\n\n}\n",
  "sourcePath": "/home/stefan/Work/cyberlife-js/bank-protocol/contracts/defi/lending/LendingTreasury.sol",
  "ast": {
    "absolutePath": "/home/stefan/Work/cyberlife-js/bank-protocol/contracts/defi/lending/LendingTreasury.sol",
    "exportedSymbols": {
      "LendingTreasury": [
        2386
      ]
    },
    "id": 2387,
    "nodeType": "SourceUnit",
    "nodes": [
      {
        "id": 1974,
        "literals": [
          "solidity",
          "0.5",
          ".3"
        ],
        "nodeType": "PragmaDirective",
        "src": "0:22:9"
      },
      {
        "absolutePath": "contracts/zeppelin/ownership/Ownable.sol",
        "file": "contracts/zeppelin/ownership/Ownable.sol",
        "id": 1975,
        "nodeType": "ImportDirective",
        "scope": 2387,
        "sourceUnit": 11837,
        "src": "24:50:9",
        "symbolAliases": [],
        "unitAlias": ""
      },
      {
        "absolutePath": "contracts/zeppelin/SafeMath.sol",
        "file": "contracts/zeppelin/SafeMath.sol",
        "id": 1976,
        "nodeType": "ImportDirective",
        "scope": 2387,
        "sourceUnit": 11751,
        "src": "75:41:9",
        "symbolAliases": [],
        "unitAlias": ""
      },
      {
        "absolutePath": "contracts/interfaces/ILendingManager.sol",
        "file": "contracts/interfaces/ILendingManager.sol",
        "id": 1977,
        "nodeType": "ImportDirective",
        "scope": 2387,
        "sourceUnit": 9137,
        "src": "118:50:9",
        "symbolAliases": [],
        "unitAlias": ""
      },
      {
        "absolutePath": "contracts/interfaces/ILendingTreasury.sol",
        "file": "contracts/interfaces/ILendingTreasury.sol",
        "id": 1978,
        "nodeType": "ImportDirective",
        "scope": 2387,
        "sourceUnit": 9173,
        "src": "169:51:9",
        "symbolAliases": [],
        "unitAlias": ""
      },
      {
        "baseContracts": [
          {
            "arguments": null,
            "baseName": {
              "contractScope": null,
              "id": 1979,
              "name": "ILendingTreasury",
              "nodeType": "UserDefinedTypeName",
              "referencedDeclaration": 9172,
              "src": "250:16:9",
              "typeDescriptions": {
                "typeIdentifier": "t_contract$_ILendingTreasury_$9172",
                "typeString": "contract ILendingTreasury"
              }
            },
            "id": 1980,
            "nodeType": "InheritanceSpecifier",
            "src": "250:16:9"
          },
          {
            "arguments": null,
            "baseName": {
              "contractScope": null,
              "id": 1981,
              "name": "Ownable",
              "nodeType": "UserDefinedTypeName",
              "referencedDeclaration": 11836,
              "src": "268:7:9",
              "typeDescriptions": {
                "typeIdentifier": "t_contract$_Ownable_$11836",
                "typeString": "contract Ownable"
              }
            },
            "id": 1982,
            "nodeType": "InheritanceSpecifier",
            "src": "268:7:9"
          }
        ],
        "contractDependencies": [
          9172,
          11836
        ],
        "contractKind": "contract",
        "documentation": null,
        "fullyImplemented": true,
        "id": 2386,
        "linearizedBaseContracts": [
          2386,
          11836,
          9172
        ],
        "name": "LendingTreasury",
        "nodeType": "ContractDefinition",
        "nodes": [
          {
            "id": 1985,
            "libraryName": {
              "contractScope": null,
              "id": 1983,
              "name": "SafeMath",
              "nodeType": "UserDefinedTypeName",
              "referencedDeclaration": 11750,
              "src": "287:8:9",
              "typeDescriptions": {
                "typeIdentifier": "t_contract$_SafeMath_$11750",
                "typeString": "library SafeMath"
              }
            },
            "nodeType": "UsingForDirective",
            "src": "281:27:9",
            "typeName": {
              "id": 1984,
              "name": "uint256",
              "nodeType": "ElementaryTypeName",
              "src": "300:7:9",
              "typeDescriptions": {
                "typeIdentifier": "t_uint256",
                "typeString": "uint256"
              }
            }
          },
          {
            "constant": false,
            "id": 1989,
            "name": "lendingBalances",
            "nodeType": "VariableDeclaration",
            "scope": 2386,
            "src": "312:43:9",
            "stateVariable": true,
            "storageLocation": "default",
            "typeDescriptions": {
              "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
              "typeString": "mapping(address => uint256)"
            },
            "typeName": {
              "id": 1988,
              "keyType": {
                "id": 1986,
                "name": "address",
                "nodeType": "ElementaryTypeName",
                "src": "320:7:9",
                "typeDescriptions": {
                  "typeIdentifier": "t_address",
                  "typeString": "address"
                }
              },
              "nodeType": "Mapping",
              "src": "312:27:9",
              "typeDescriptions": {
                "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                "typeString": "mapping(address => uint256)"
              },
              "valueType": {
                "id": 1987,
                "name": "uint256",
                "nodeType": "ElementaryTypeName",
                "src": "331:7:9",
                "typeDescriptions": {
                  "typeIdentifier": "t_uint256",
                  "typeString": "uint256"
                }
              }
            },
            "value": null,
            "visibility": "internal"
          },
          {
            "constant": false,
            "id": 1993,
            "name": "validRelayers",
            "nodeType": "VariableDeclaration",
            "scope": 2386,
            "src": "360:38:9",
            "stateVariable": true,
            "storageLocation": "default",
            "typeDescriptions": {
              "typeIdentifier": "t_mapping$_t_address_$_t_bool_$",
              "typeString": "mapping(address => bool)"
            },
            "typeName": {
              "id": 1992,
              "keyType": {
                "id": 1990,
                "name": "address",
                "nodeType": "ElementaryTypeName",
                "src": "368:7:9",
                "typeDescriptions": {
                  "typeIdentifier": "t_address",
                  "typeString": "address"
                }
              },
              "nodeType": "Mapping",
              "src": "360:24:9",
              "typeDescriptions": {
                "typeIdentifier": "t_mapping$_t_address_$_t_bool_$",
                "typeString": "mapping(address => bool)"
              },
              "valueType": {
                "id": 1991,
                "name": "bool",
                "nodeType": "ElementaryTypeName",
                "src": "379:4:9",
                "typeDescriptions": {
                  "typeIdentifier": "t_bool",
                  "typeString": "bool"
                }
              }
            },
            "value": null,
            "visibility": "internal"
          },
          {
            "constant": false,
            "id": 1999,
            "name": "customerLoanTie",
            "nodeType": "VariableDeclaration",
            "scope": 2386,
            "src": "403:57:9",
            "stateVariable": true,
            "storageLocation": "default",
            "typeDescriptions": {
              "typeIdentifier": "t_mapping$_t_address_$_t_mapping$_t_uint256_$_t_bool_$_$",
              "typeString": "mapping(address => mapping(uint256 => bool))"
            },
            "typeName": {
              "id": 1998,
              "keyType": {
                "id": 1994,
                "name": "address",
                "nodeType": "ElementaryTypeName",
                "src": "411:7:9",
                "typeDescriptions": {
                  "typeIdentifier": "t_address",
                  "typeString": "address"
                }
              },
              "nodeType": "Mapping",
              "src": "403:41:9",
              "typeDescriptions": {
                "typeIdentifier": "t_mapping$_t_address_$_t_mapping$_t_uint256_$_t_bool_$_$",
                "typeString": "mapping(address => mapping(uint256 => bool))"
              },
              "valueType": {
                "id": 1997,
                "keyType": {
                  "id": 1995,
                  "name": "uint",
                  "nodeType": "ElementaryTypeName",
                  "src": "430:4:9",
                  "typeDescriptions": {
                    "typeIdentifier": "t_uint256",
                    "typeString": "uint256"
                  }
                },
                "nodeType": "Mapping",
                "src": "422:21:9",
                "typeDescriptions": {
                  "typeIdentifier": "t_mapping$_t_uint256_$_t_bool_$",
                  "typeString": "mapping(uint256 => bool)"
                },
                "valueType": {
                  "id": 1996,
                  "name": "bool",
                  "nodeType": "ElementaryTypeName",
                  "src": "438:4:9",
                  "typeDescriptions": {
                    "typeIdentifier": "t_bool",
                    "typeString": "bool"
                  }
                }
              }
            },
            "value": null,
            "visibility": "internal"
          },
          {
            "constant": false,
            "id": 2003,
            "name": "loanCustomerTie",
            "nodeType": "VariableDeclaration",
            "scope": 2386,
            "src": "465:40:9",
            "stateVariable": true,
            "storageLocation": "default",
            "typeDescriptions": {
              "typeIdentifier": "t_mapping$_t_uint256_$_t_address_$",
              "typeString": "mapping(uint256 => address)"
            },
            "typeName": {
              "id": 2002,
              "keyType": {
                "id": 2000,
                "name": "uint",
                "nodeType": "ElementaryTypeName",
                "src": "473:4:9",
                "typeDescriptions": {
                  "typeIdentifier": "t_uint256",
                  "typeString": "uint256"
                }
              },
              "nodeType": "Mapping",
              "src": "465:24:9",
              "typeDescriptions": {
                "typeIdentifier": "t_mapping$_t_uint256_$_t_address_$",
                "typeString": "mapping(uint256 => address)"
              },
              "valueType": {
                "id": 2001,
                "name": "address",
                "nodeType": "ElementaryTypeName",
                "src": "481:7:9",
                "stateMutability": "nonpayable",
                "typeDescriptions": {
                  "typeIdentifier": "t_address",
                  "typeString": "address"
                }
              }
            },
            "value": null,
            "visibility": "internal"
          },
          {
            "constant": false,
            "id": 2007,
            "name": "totalLentPerCustomer",
            "nodeType": "VariableDeclaration",
            "scope": 2386,
            "src": "510:48:9",
            "stateVariable": true,
            "storageLocation": "default",
            "typeDescriptions": {
              "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
              "typeString": "mapping(address => uint256)"
            },
            "typeName": {
              "id": 2006,
              "keyType": {
                "id": 2004,
                "name": "address",
                "nodeType": "ElementaryTypeName",
                "src": "518:7:9",
                "typeDescriptions": {
                  "typeIdentifier": "t_address",
                  "typeString": "address"
                }
              },
              "nodeType": "Mapping",
              "src": "510:27:9",
              "typeDescriptions": {
                "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                "typeString": "mapping(address => uint256)"
              },
              "valueType": {
                "id": 2005,
                "name": "uint256",
                "nodeType": "ElementaryTypeName",
                "src": "529:7:9",
                "typeDescriptions": {
                  "typeIdentifier": "t_uint256",
                  "typeString": "uint256"
                }
              }
            },
            "value": null,
            "visibility": "internal"
          },
          {
            "constant": false,
            "id": 2009,
            "name": "manager",
            "nodeType": "VariableDeclaration",
            "scope": 2386,
            "src": "563:23:9",
            "stateVariable": true,
            "storageLocation": "default",
            "typeDescriptions": {
              "typeIdentifier": "t_contract$_ILendingManager_$9136",
              "typeString": "contract ILendingManager"
            },
            "typeName": {
              "contractScope": null,
              "id": 2008,
              "name": "ILendingManager",
              "nodeType": "UserDefinedTypeName",
              "referencedDeclaration": 9136,
              "src": "563:15:9",
              "typeDescriptions": {
                "typeIdentifier": "t_contract$_ILendingManager_$9136",
                "typeString": "contract ILendingManager"
              }
            },
            "value": null,
            "visibility": "internal"
          },
          {
            "body": {
              "id": 2022,
              "nodeType": "Block",
              "src": "619:101:9",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "commonType": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        },
                        "id": 2017,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "leftExpression": {
                          "argumentTypes": null,
                          "baseExpression": {
                            "argumentTypes": null,
                            "id": 2012,
                            "name": "validRelayers",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 1993,
                            "src": "634:13:9",
                            "typeDescriptions": {
                              "typeIdentifier": "t_mapping$_t_address_$_t_bool_$",
                              "typeString": "mapping(address => bool)"
                            }
                          },
                          "id": 2015,
                          "indexExpression": {
                            "argumentTypes": null,
                            "expression": {
                              "argumentTypes": null,
                              "id": 2013,
                              "name": "msg",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 11851,
                              "src": "648:3:9",
                              "typeDescriptions": {
                                "typeIdentifier": "t_magic_message",
                                "typeString": "msg"
                              }
                            },
                            "id": 2014,
                            "isConstant": false,
                            "isLValue": false,
                            "isPure": false,
                            "lValueRequested": false,
                            "memberName": "sender",
                            "nodeType": "MemberAccess",
                            "referencedDeclaration": null,
                            "src": "648:10:9",
                            "typeDescriptions": {
                              "typeIdentifier": "t_address_payable",
                              "typeString": "address payable"
                            }
                          },
                          "isConstant": false,
                          "isLValue": true,
                          "isPure": false,
                          "lValueRequested": false,
                          "nodeType": "IndexAccess",
                          "src": "634:25:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_bool",
                            "typeString": "bool"
                          }
                        },
                        "nodeType": "BinaryOperation",
                        "operator": "==",
                        "rightExpression": {
                          "argumentTypes": null,
                          "hexValue": "74727565",
                          "id": 2016,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": true,
                          "kind": "bool",
                          "lValueRequested": false,
                          "nodeType": "Literal",
                          "src": "663:4:9",
                          "subdenomination": null,
                          "typeDescriptions": {
                            "typeIdentifier": "t_bool",
                            "typeString": "bool"
                          },
                          "value": "true"
                        },
                        "src": "634:33:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "hexValue": "546869732073656e646572206973206e6f7420612076616c69642072656c61796572",
                        "id": 2018,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": true,
                        "kind": "string",
                        "lValueRequested": false,
                        "nodeType": "Literal",
                        "src": "669:36:9",
                        "subdenomination": null,
                        "typeDescriptions": {
                          "typeIdentifier": "t_stringliteral_011a7c50fa3a342edbf028d9772540c2f71073c1afb9f56c7a2cf31ca4b02e12",
                          "typeString": "literal_string \"This sender is not a valid relayer\""
                        },
                        "value": "This sender is not a valid relayer"
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        },
                        {
                          "typeIdentifier": "t_stringliteral_011a7c50fa3a342edbf028d9772540c2f71073c1afb9f56c7a2cf31ca4b02e12",
                          "typeString": "literal_string \"This sender is not a valid relayer\""
                        }
                      ],
                      "id": 2011,
                      "name": "require",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [
                        11854,
                        11855
                      ],
                      "referencedDeclaration": 11855,
                      "src": "626:7:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_require_pure$_t_bool_$_t_string_memory_ptr_$returns$__$",
                        "typeString": "function (bool,string memory) pure"
                      }
                    },
                    "id": 2019,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "626:80:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 2020,
                  "nodeType": "ExpressionStatement",
                  "src": "626:80:9"
                },
                {
                  "id": 2021,
                  "nodeType": "PlaceholderStatement",
                  "src": "713:1:9"
                }
              ]
            },
            "documentation": null,
            "id": 2023,
            "name": "onlyValidRelayer",
            "nodeType": "ModifierDefinition",
            "parameters": {
              "id": 2010,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "616:2:9"
            },
            "src": "591:129:9",
            "visibility": "internal"
          },
          {
            "body": {
              "id": 2036,
              "nodeType": "Block",
              "src": "754:101:9",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "commonType": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        },
                        "id": 2031,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "leftExpression": {
                          "argumentTypes": null,
                          "expression": {
                            "argumentTypes": null,
                            "id": 2026,
                            "name": "msg",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 11851,
                            "src": "769:3:9",
                            "typeDescriptions": {
                              "typeIdentifier": "t_magic_message",
                              "typeString": "msg"
                            }
                          },
                          "id": 2027,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "memberName": "sender",
                          "nodeType": "MemberAccess",
                          "referencedDeclaration": null,
                          "src": "769:10:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address_payable",
                            "typeString": "address payable"
                          }
                        },
                        "nodeType": "BinaryOperation",
                        "operator": "==",
                        "rightExpression": {
                          "argumentTypes": null,
                          "arguments": [
                            {
                              "argumentTypes": null,
                              "id": 2029,
                              "name": "manager",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 2009,
                              "src": "791:7:9",
                              "typeDescriptions": {
                                "typeIdentifier": "t_contract$_ILendingManager_$9136",
                                "typeString": "contract ILendingManager"
                              }
                            }
                          ],
                          "expression": {
                            "argumentTypes": [
                              {
                                "typeIdentifier": "t_contract$_ILendingManager_$9136",
                                "typeString": "contract ILendingManager"
                              }
                            ],
                            "id": 2028,
                            "isConstant": false,
                            "isLValue": false,
                            "isPure": true,
                            "lValueRequested": false,
                            "nodeType": "ElementaryTypeNameExpression",
                            "src": "783:7:9",
                            "typeDescriptions": {
                              "typeIdentifier": "t_type$_t_address_$",
                              "typeString": "type(address)"
                            },
                            "typeName": "address"
                          },
                          "id": 2030,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "kind": "typeConversion",
                          "lValueRequested": false,
                          "names": [],
                          "nodeType": "FunctionCall",
                          "src": "783:16:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "src": "769:30:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "hexValue": "5468652063616c6c6572206973206e6f7420746865206c656e64696e67206d616e61676572",
                        "id": 2032,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": true,
                        "kind": "string",
                        "lValueRequested": false,
                        "nodeType": "Literal",
                        "src": "801:39:9",
                        "subdenomination": null,
                        "typeDescriptions": {
                          "typeIdentifier": "t_stringliteral_27e3839b182de27b41afb4105c65813b427eb2bfea02e249df35c3534ae43682",
                          "typeString": "literal_string \"The caller is not the lending manager\""
                        },
                        "value": "The caller is not the lending manager"
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        },
                        {
                          "typeIdentifier": "t_stringliteral_27e3839b182de27b41afb4105c65813b427eb2bfea02e249df35c3534ae43682",
                          "typeString": "literal_string \"The caller is not the lending manager\""
                        }
                      ],
                      "id": 2025,
                      "name": "require",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [
                        11854,
                        11855
                      ],
                      "referencedDeclaration": 11855,
                      "src": "761:7:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_require_pure$_t_bool_$_t_string_memory_ptr_$returns$__$",
                        "typeString": "function (bool,string memory) pure"
                      }
                    },
                    "id": 2033,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "761:80:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 2034,
                  "nodeType": "ExpressionStatement",
                  "src": "761:80:9"
                },
                {
                  "id": 2035,
                  "nodeType": "PlaceholderStatement",
                  "src": "848:1:9"
                }
              ]
            },
            "documentation": null,
            "id": 2037,
            "name": "onlyLendingManager",
            "nodeType": "ModifierDefinition",
            "parameters": {
              "id": 2024,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "751:2:9"
            },
            "src": "724:131:9",
            "visibility": "internal"
          },
          {
            "body": {
              "id": 2040,
              "nodeType": "Block",
              "src": "880:2:9",
              "statements": []
            },
            "documentation": null,
            "id": 2041,
            "implemented": true,
            "kind": "constructor",
            "modifiers": [],
            "name": "",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 2038,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "870:2:9"
            },
            "returnParameters": {
              "id": 2039,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "880:0:9"
            },
            "scope": 2386,
            "src": "859:23:9",
            "stateMutability": "nonpayable",
            "superFunction": null,
            "visibility": "public"
          },
          {
            "body": {
              "id": 2073,
              "nodeType": "Block",
              "src": "914:197:9",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "commonType": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        },
                        "id": 2048,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "leftExpression": {
                          "argumentTypes": null,
                          "expression": {
                            "argumentTypes": null,
                            "id": 2045,
                            "name": "msg",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 11851,
                            "src": "929:3:9",
                            "typeDescriptions": {
                              "typeIdentifier": "t_magic_message",
                              "typeString": "msg"
                            }
                          },
                          "id": 2046,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "memberName": "value",
                          "nodeType": "MemberAccess",
                          "referencedDeclaration": null,
                          "src": "929:9:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        },
                        "nodeType": "BinaryOperation",
                        "operator": ">",
                        "rightExpression": {
                          "argumentTypes": null,
                          "hexValue": "30",
                          "id": 2047,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": true,
                          "kind": "number",
                          "lValueRequested": false,
                          "nodeType": "Literal",
                          "src": "941:1:9",
                          "subdenomination": null,
                          "typeDescriptions": {
                            "typeIdentifier": "t_rational_0_by_1",
                            "typeString": "int_const 0"
                          },
                          "value": "0"
                        },
                        "src": "929:13:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "hexValue": "596f75206e65656420746f2073656e64206d6f7265207468616e203020776569",
                        "id": 2049,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": true,
                        "kind": "string",
                        "lValueRequested": false,
                        "nodeType": "Literal",
                        "src": "944:34:9",
                        "subdenomination": null,
                        "typeDescriptions": {
                          "typeIdentifier": "t_stringliteral_3cbcdb32609fc2b20478afb11c0146a76c2025d17208fd6973e7f3f08d317dfb",
                          "typeString": "literal_string \"You need to send more than 0 wei\""
                        },
                        "value": "You need to send more than 0 wei"
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        },
                        {
                          "typeIdentifier": "t_stringliteral_3cbcdb32609fc2b20478afb11c0146a76c2025d17208fd6973e7f3f08d317dfb",
                          "typeString": "literal_string \"You need to send more than 0 wei\""
                        }
                      ],
                      "id": 2044,
                      "name": "require",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [
                        11854,
                        11855
                      ],
                      "referencedDeclaration": 11855,
                      "src": "921:7:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_require_pure$_t_bool_$_t_string_memory_ptr_$returns$__$",
                        "typeString": "function (bool,string memory) pure"
                      }
                    },
                    "id": 2050,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "921:58:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 2051,
                  "nodeType": "ExpressionStatement",
                  "src": "921:58:9"
                },
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 2064,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "baseExpression": {
                        "argumentTypes": null,
                        "id": 2052,
                        "name": "lendingBalances",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1989,
                        "src": "986:15:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                          "typeString": "mapping(address => uint256)"
                        }
                      },
                      "id": 2055,
                      "indexExpression": {
                        "argumentTypes": null,
                        "expression": {
                          "argumentTypes": null,
                          "id": 2053,
                          "name": "msg",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 11851,
                          "src": "1002:3:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_magic_message",
                            "typeString": "msg"
                          }
                        },
                        "id": 2054,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "memberName": "sender",
                        "nodeType": "MemberAccess",
                        "referencedDeclaration": null,
                        "src": "1002:10:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address_payable",
                          "typeString": "address payable"
                        }
                      },
                      "isConstant": false,
                      "isLValue": true,
                      "isPure": false,
                      "lValueRequested": true,
                      "nodeType": "IndexAccess",
                      "src": "986:27:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "arguments": [
                        {
                          "argumentTypes": null,
                          "expression": {
                            "argumentTypes": null,
                            "id": 2061,
                            "name": "msg",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 11851,
                            "src": "1048:3:9",
                            "typeDescriptions": {
                              "typeIdentifier": "t_magic_message",
                              "typeString": "msg"
                            }
                          },
                          "id": 2062,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "memberName": "value",
                          "nodeType": "MemberAccess",
                          "referencedDeclaration": null,
                          "src": "1048:9:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        }
                      ],
                      "expression": {
                        "argumentTypes": [
                          {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        ],
                        "expression": {
                          "argumentTypes": null,
                          "baseExpression": {
                            "argumentTypes": null,
                            "id": 2056,
                            "name": "lendingBalances",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 1989,
                            "src": "1016:15:9",
                            "typeDescriptions": {
                              "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                              "typeString": "mapping(address => uint256)"
                            }
                          },
                          "id": 2059,
                          "indexExpression": {
                            "argumentTypes": null,
                            "expression": {
                              "argumentTypes": null,
                              "id": 2057,
                              "name": "msg",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 11851,
                              "src": "1032:3:9",
                              "typeDescriptions": {
                                "typeIdentifier": "t_magic_message",
                                "typeString": "msg"
                              }
                            },
                            "id": 2058,
                            "isConstant": false,
                            "isLValue": false,
                            "isPure": false,
                            "lValueRequested": false,
                            "memberName": "sender",
                            "nodeType": "MemberAccess",
                            "referencedDeclaration": null,
                            "src": "1032:10:9",
                            "typeDescriptions": {
                              "typeIdentifier": "t_address_payable",
                              "typeString": "address payable"
                            }
                          },
                          "isConstant": false,
                          "isLValue": true,
                          "isPure": false,
                          "lValueRequested": false,
                          "nodeType": "IndexAccess",
                          "src": "1016:27:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        },
                        "id": 2060,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "memberName": "add",
                        "nodeType": "MemberAccess",
                        "referencedDeclaration": 11749,
                        "src": "1016:31:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_function_internal_pure$_t_uint256_$_t_uint256_$returns$_t_uint256_$bound_to$_t_uint256_$",
                          "typeString": "function (uint256,uint256) pure returns (uint256)"
                        }
                      },
                      "id": 2063,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "kind": "functionCall",
                      "lValueRequested": false,
                      "names": [],
                      "nodeType": "FunctionCall",
                      "src": "1016:42:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "src": "986:72:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "id": 2065,
                  "nodeType": "ExpressionStatement",
                  "src": "986:72:9"
                },
                {
                  "eventCall": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "expression": {
                          "argumentTypes": null,
                          "id": 2067,
                          "name": "msg",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 11851,
                          "src": "1083:3:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_magic_message",
                            "typeString": "msg"
                          }
                        },
                        "id": 2068,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "memberName": "sender",
                        "nodeType": "MemberAccess",
                        "referencedDeclaration": null,
                        "src": "1083:10:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address_payable",
                          "typeString": "address payable"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "expression": {
                          "argumentTypes": null,
                          "id": 2069,
                          "name": "msg",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 11851,
                          "src": "1095:3:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_magic_message",
                            "typeString": "msg"
                          }
                        },
                        "id": 2070,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "memberName": "value",
                        "nodeType": "MemberAccess",
                        "referencedDeclaration": null,
                        "src": "1095:9:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_address_payable",
                          "typeString": "address payable"
                        },
                        {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      ],
                      "id": 2066,
                      "name": "DepositedETH",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 9154,
                      "src": "1070:12:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_event_nonpayable$_t_address_$_t_uint256_$returns$__$",
                        "typeString": "function (address,uint256)"
                      }
                    },
                    "id": 2071,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "1070:35:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 2072,
                  "nodeType": "EmitStatement",
                  "src": "1065:40:9"
                }
              ]
            },
            "documentation": null,
            "id": 2074,
            "implemented": true,
            "kind": "fallback",
            "modifiers": [],
            "name": "",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 2042,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "894:2:9"
            },
            "returnParameters": {
              "id": 2043,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "914:0:9"
            },
            "scope": 2386,
            "src": "886:225:9",
            "stateMutability": "payable",
            "superFunction": null,
            "visibility": "external"
          },
          {
            "body": {
              "id": 2097,
              "nodeType": "Block",
              "src": "1183:120:9",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 2088,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "baseExpression": {
                        "argumentTypes": null,
                        "id": 2081,
                        "name": "validRelayers",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1993,
                        "src": "1190:13:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_mapping$_t_address_$_t_bool_$",
                          "typeString": "mapping(address => bool)"
                        }
                      },
                      "id": 2083,
                      "indexExpression": {
                        "argumentTypes": null,
                        "id": 2082,
                        "name": "relayer",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 2076,
                        "src": "1204:7:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      "isConstant": false,
                      "isLValue": true,
                      "isPure": false,
                      "lValueRequested": true,
                      "nodeType": "IndexAccess",
                      "src": "1190:22:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_bool",
                        "typeString": "bool"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "id": 2087,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "lValueRequested": false,
                      "nodeType": "UnaryOperation",
                      "operator": "!",
                      "prefix": true,
                      "src": "1215:23:9",
                      "subExpression": {
                        "argumentTypes": null,
                        "baseExpression": {
                          "argumentTypes": null,
                          "id": 2084,
                          "name": "validRelayers",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 1993,
                          "src": "1216:13:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_mapping$_t_address_$_t_bool_$",
                            "typeString": "mapping(address => bool)"
                          }
                        },
                        "id": 2086,
                        "indexExpression": {
                          "argumentTypes": null,
                          "id": 2085,
                          "name": "relayer",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 2076,
                          "src": "1230:7:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "isConstant": false,
                        "isLValue": true,
                        "isPure": false,
                        "lValueRequested": false,
                        "nodeType": "IndexAccess",
                        "src": "1216:22:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      },
                      "typeDescriptions": {
                        "typeIdentifier": "t_bool",
                        "typeString": "bool"
                      }
                    },
                    "src": "1190:48:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_bool",
                      "typeString": "bool"
                    }
                  },
                  "id": 2089,
                  "nodeType": "ExpressionStatement",
                  "src": "1190:48:9"
                },
                {
                  "eventCall": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "id": 2091,
                        "name": "relayer",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 2076,
                        "src": "1265:7:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "baseExpression": {
                          "argumentTypes": null,
                          "id": 2092,
                          "name": "validRelayers",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 1993,
                          "src": "1274:13:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_mapping$_t_address_$_t_bool_$",
                            "typeString": "mapping(address => bool)"
                          }
                        },
                        "id": 2094,
                        "indexExpression": {
                          "argumentTypes": null,
                          "id": 2093,
                          "name": "relayer",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 2076,
                          "src": "1288:7:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "isConstant": false,
                        "isLValue": true,
                        "isPure": false,
                        "lValueRequested": false,
                        "nodeType": "IndexAccess",
                        "src": "1274:22:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        },
                        {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      ],
                      "id": 2090,
                      "name": "ToggledRelayer",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 9144,
                      "src": "1250:14:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_event_nonpayable$_t_address_$_t_bool_$returns$__$",
                        "typeString": "function (address,bool)"
                      }
                    },
                    "id": 2095,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "1250:47:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 2096,
                  "nodeType": "EmitStatement",
                  "src": "1245:52:9"
                }
              ]
            },
            "documentation": null,
            "id": 2098,
            "implemented": true,
            "kind": "function",
            "modifiers": [
              {
                "arguments": null,
                "id": 2079,
                "modifierName": {
                  "argumentTypes": null,
                  "id": 2078,
                  "name": "onlyOwner",
                  "nodeType": "Identifier",
                  "overloadedDeclarations": [],
                  "referencedDeclaration": 11784,
                  "src": "1173:9:9",
                  "typeDescriptions": {
                    "typeIdentifier": "t_modifier$__$",
                    "typeString": "modifier ()"
                  }
                },
                "nodeType": "ModifierInvocation",
                "src": "1173:9:9"
              }
            ],
            "name": "toggleRelayer",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 2077,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 2076,
                  "name": "relayer",
                  "nodeType": "VariableDeclaration",
                  "scope": 2098,
                  "src": "1149:15:9",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 2075,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "1149:7:9",
                    "stateMutability": "nonpayable",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "1148:17:9"
            },
            "returnParameters": {
              "id": 2080,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "1183:0:9"
            },
            "scope": 2386,
            "src": "1126:177:9",
            "stateMutability": "nonpayable",
            "superFunction": null,
            "visibility": "public"
          },
          {
            "body": {
              "id": 2115,
              "nodeType": "Block",
              "src": "1366:84:9",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 2109,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "id": 2105,
                      "name": "manager",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 2009,
                      "src": "1373:7:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_contract$_ILendingManager_$9136",
                        "typeString": "contract ILendingManager"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "arguments": [
                        {
                          "argumentTypes": null,
                          "id": 2107,
                          "name": "_manager",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 2100,
                          "src": "1399:8:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        }
                      ],
                      "expression": {
                        "argumentTypes": [
                          {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        ],
                        "id": 2106,
                        "name": "ILendingManager",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 9136,
                        "src": "1383:15:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_type$_t_contract$_ILendingManager_$9136_$",
                          "typeString": "type(contract ILendingManager)"
                        }
                      },
                      "id": 2108,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "kind": "typeConversion",
                      "lValueRequested": false,
                      "names": [],
                      "nodeType": "FunctionCall",
                      "src": "1383:25:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_contract$_ILendingManager_$9136",
                        "typeString": "contract ILendingManager"
                      }
                    },
                    "src": "1373:35:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_contract$_ILendingManager_$9136",
                      "typeString": "contract ILendingManager"
                    }
                  },
                  "id": 2110,
                  "nodeType": "ExpressionStatement",
                  "src": "1373:35:9"
                },
                {
                  "eventCall": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "id": 2112,
                        "name": "_manager",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 2100,
                        "src": "1435:8:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      ],
                      "id": 2111,
                      "name": "SetLoanManager",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 9148,
                      "src": "1420:14:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_event_nonpayable$_t_address_$returns$__$",
                        "typeString": "function (address)"
                      }
                    },
                    "id": 2113,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "1420:24:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 2114,
                  "nodeType": "EmitStatement",
                  "src": "1415:29:9"
                }
              ]
            },
            "documentation": null,
            "id": 2116,
            "implemented": true,
            "kind": "function",
            "modifiers": [
              {
                "arguments": null,
                "id": 2103,
                "modifierName": {
                  "argumentTypes": null,
                  "id": 2102,
                  "name": "onlyOwner",
                  "nodeType": "Identifier",
                  "overloadedDeclarations": [],
                  "referencedDeclaration": 11784,
                  "src": "1356:9:9",
                  "typeDescriptions": {
                    "typeIdentifier": "t_modifier$__$",
                    "typeString": "modifier ()"
                  }
                },
                "nodeType": "ModifierInvocation",
                "src": "1356:9:9"
              }
            ],
            "name": "setLoanManager",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 2101,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 2100,
                  "name": "_manager",
                  "nodeType": "VariableDeclaration",
                  "scope": 2116,
                  "src": "1331:16:9",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 2099,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "1331:7:9",
                    "stateMutability": "nonpayable",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "1330:18:9"
            },
            "returnParameters": {
              "id": 2104,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "1366:0:9"
            },
            "scope": 2386,
            "src": "1307:143:9",
            "stateMutability": "nonpayable",
            "superFunction": null,
            "visibility": "public"
          },
          {
            "body": {
              "id": 2148,
              "nodeType": "Block",
              "src": "1498:208:9",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "commonType": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        },
                        "id": 2123,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "leftExpression": {
                          "argumentTypes": null,
                          "expression": {
                            "argumentTypes": null,
                            "id": 2120,
                            "name": "msg",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 11851,
                            "src": "1513:3:9",
                            "typeDescriptions": {
                              "typeIdentifier": "t_magic_message",
                              "typeString": "msg"
                            }
                          },
                          "id": 2121,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "memberName": "value",
                          "nodeType": "MemberAccess",
                          "referencedDeclaration": null,
                          "src": "1513:9:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        },
                        "nodeType": "BinaryOperation",
                        "operator": ">",
                        "rightExpression": {
                          "argumentTypes": null,
                          "hexValue": "30",
                          "id": 2122,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": true,
                          "kind": "number",
                          "lValueRequested": false,
                          "nodeType": "Literal",
                          "src": "1525:1:9",
                          "subdenomination": null,
                          "typeDescriptions": {
                            "typeIdentifier": "t_rational_0_by_1",
                            "typeString": "int_const 0"
                          },
                          "value": "0"
                        },
                        "src": "1513:13:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "hexValue": "596f75206e65656420746f2073656e64206120706f73697469766520616d6f756e74206f66206d6f6e6579",
                        "id": 2124,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": true,
                        "kind": "string",
                        "lValueRequested": false,
                        "nodeType": "Literal",
                        "src": "1528:45:9",
                        "subdenomination": null,
                        "typeDescriptions": {
                          "typeIdentifier": "t_stringliteral_d70d2a8c12d989eb36b3b63feb7ad989e7585e5e5f58a19a6d5ef0dd415c9dc4",
                          "typeString": "literal_string \"You need to send a positive amount of money\""
                        },
                        "value": "You need to send a positive amount of money"
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        },
                        {
                          "typeIdentifier": "t_stringliteral_d70d2a8c12d989eb36b3b63feb7ad989e7585e5e5f58a19a6d5ef0dd415c9dc4",
                          "typeString": "literal_string \"You need to send a positive amount of money\""
                        }
                      ],
                      "id": 2119,
                      "name": "require",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [
                        11854,
                        11855
                      ],
                      "referencedDeclaration": 11855,
                      "src": "1505:7:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_require_pure$_t_bool_$_t_string_memory_ptr_$returns$__$",
                        "typeString": "function (bool,string memory) pure"
                      }
                    },
                    "id": 2125,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "1505:69:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 2126,
                  "nodeType": "ExpressionStatement",
                  "src": "1505:69:9"
                },
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 2139,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "baseExpression": {
                        "argumentTypes": null,
                        "id": 2127,
                        "name": "lendingBalances",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1989,
                        "src": "1581:15:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                          "typeString": "mapping(address => uint256)"
                        }
                      },
                      "id": 2130,
                      "indexExpression": {
                        "argumentTypes": null,
                        "expression": {
                          "argumentTypes": null,
                          "id": 2128,
                          "name": "msg",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 11851,
                          "src": "1597:3:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_magic_message",
                            "typeString": "msg"
                          }
                        },
                        "id": 2129,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "memberName": "sender",
                        "nodeType": "MemberAccess",
                        "referencedDeclaration": null,
                        "src": "1597:10:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address_payable",
                          "typeString": "address payable"
                        }
                      },
                      "isConstant": false,
                      "isLValue": true,
                      "isPure": false,
                      "lValueRequested": true,
                      "nodeType": "IndexAccess",
                      "src": "1581:27:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "arguments": [
                        {
                          "argumentTypes": null,
                          "expression": {
                            "argumentTypes": null,
                            "id": 2136,
                            "name": "msg",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 11851,
                            "src": "1643:3:9",
                            "typeDescriptions": {
                              "typeIdentifier": "t_magic_message",
                              "typeString": "msg"
                            }
                          },
                          "id": 2137,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "memberName": "value",
                          "nodeType": "MemberAccess",
                          "referencedDeclaration": null,
                          "src": "1643:9:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        }
                      ],
                      "expression": {
                        "argumentTypes": [
                          {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        ],
                        "expression": {
                          "argumentTypes": null,
                          "baseExpression": {
                            "argumentTypes": null,
                            "id": 2131,
                            "name": "lendingBalances",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 1989,
                            "src": "1611:15:9",
                            "typeDescriptions": {
                              "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                              "typeString": "mapping(address => uint256)"
                            }
                          },
                          "id": 2134,
                          "indexExpression": {
                            "argumentTypes": null,
                            "expression": {
                              "argumentTypes": null,
                              "id": 2132,
                              "name": "msg",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 11851,
                              "src": "1627:3:9",
                              "typeDescriptions": {
                                "typeIdentifier": "t_magic_message",
                                "typeString": "msg"
                              }
                            },
                            "id": 2133,
                            "isConstant": false,
                            "isLValue": false,
                            "isPure": false,
                            "lValueRequested": false,
                            "memberName": "sender",
                            "nodeType": "MemberAccess",
                            "referencedDeclaration": null,
                            "src": "1627:10:9",
                            "typeDescriptions": {
                              "typeIdentifier": "t_address_payable",
                              "typeString": "address payable"
                            }
                          },
                          "isConstant": false,
                          "isLValue": true,
                          "isPure": false,
                          "lValueRequested": false,
                          "nodeType": "IndexAccess",
                          "src": "1611:27:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        },
                        "id": 2135,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "memberName": "add",
                        "nodeType": "MemberAccess",
                        "referencedDeclaration": 11749,
                        "src": "1611:31:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_function_internal_pure$_t_uint256_$_t_uint256_$returns$_t_uint256_$bound_to$_t_uint256_$",
                          "typeString": "function (uint256,uint256) pure returns (uint256)"
                        }
                      },
                      "id": 2138,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "kind": "functionCall",
                      "lValueRequested": false,
                      "names": [],
                      "nodeType": "FunctionCall",
                      "src": "1611:42:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "src": "1581:72:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "id": 2140,
                  "nodeType": "ExpressionStatement",
                  "src": "1581:72:9"
                },
                {
                  "eventCall": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "expression": {
                          "argumentTypes": null,
                          "id": 2142,
                          "name": "msg",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 11851,
                          "src": "1678:3:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_magic_message",
                            "typeString": "msg"
                          }
                        },
                        "id": 2143,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "memberName": "sender",
                        "nodeType": "MemberAccess",
                        "referencedDeclaration": null,
                        "src": "1678:10:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address_payable",
                          "typeString": "address payable"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "expression": {
                          "argumentTypes": null,
                          "id": 2144,
                          "name": "msg",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 11851,
                          "src": "1690:3:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_magic_message",
                            "typeString": "msg"
                          }
                        },
                        "id": 2145,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "memberName": "value",
                        "nodeType": "MemberAccess",
                        "referencedDeclaration": null,
                        "src": "1690:9:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_address_payable",
                          "typeString": "address payable"
                        },
                        {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      ],
                      "id": 2141,
                      "name": "DepositedETH",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 9154,
                      "src": "1665:12:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_event_nonpayable$_t_address_$_t_uint256_$returns$__$",
                        "typeString": "function (address,uint256)"
                      }
                    },
                    "id": 2146,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "1665:35:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 2147,
                  "nodeType": "EmitStatement",
                  "src": "1660:40:9"
                }
              ]
            },
            "documentation": null,
            "id": 2149,
            "implemented": true,
            "kind": "function",
            "modifiers": [],
            "name": "depositLendingETH",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 2117,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "1480:2:9"
            },
            "returnParameters": {
              "id": 2118,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "1498:0:9"
            },
            "scope": 2386,
            "src": "1454:252:9",
            "stateMutability": "payable",
            "superFunction": null,
            "visibility": "public"
          },
          {
            "body": {
              "id": 2195,
              "nodeType": "Block",
              "src": "1761:337:9",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "commonType": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        },
                        "id": 2165,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "leftExpression": {
                          "argumentTypes": null,
                          "id": 2155,
                          "name": "amount",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 2151,
                          "src": "1776:6:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        },
                        "nodeType": "BinaryOperation",
                        "operator": "<=",
                        "rightExpression": {
                          "argumentTypes": null,
                          "commonType": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          },
                          "id": 2164,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "leftExpression": {
                            "argumentTypes": null,
                            "baseExpression": {
                              "argumentTypes": null,
                              "id": 2156,
                              "name": "lendingBalances",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 1989,
                              "src": "1786:15:9",
                              "typeDescriptions": {
                                "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                                "typeString": "mapping(address => uint256)"
                              }
                            },
                            "id": 2159,
                            "indexExpression": {
                              "argumentTypes": null,
                              "expression": {
                                "argumentTypes": null,
                                "id": 2157,
                                "name": "msg",
                                "nodeType": "Identifier",
                                "overloadedDeclarations": [],
                                "referencedDeclaration": 11851,
                                "src": "1802:3:9",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_magic_message",
                                  "typeString": "msg"
                                }
                              },
                              "id": 2158,
                              "isConstant": false,
                              "isLValue": false,
                              "isPure": false,
                              "lValueRequested": false,
                              "memberName": "sender",
                              "nodeType": "MemberAccess",
                              "referencedDeclaration": null,
                              "src": "1802:10:9",
                              "typeDescriptions": {
                                "typeIdentifier": "t_address_payable",
                                "typeString": "address payable"
                              }
                            },
                            "isConstant": false,
                            "isLValue": true,
                            "isPure": false,
                            "lValueRequested": false,
                            "nodeType": "IndexAccess",
                            "src": "1786:27:9",
                            "typeDescriptions": {
                              "typeIdentifier": "t_uint256",
                              "typeString": "uint256"
                            }
                          },
                          "nodeType": "BinaryOperation",
                          "operator": "-",
                          "rightExpression": {
                            "argumentTypes": null,
                            "baseExpression": {
                              "argumentTypes": null,
                              "id": 2160,
                              "name": "totalLentPerCustomer",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 2007,
                              "src": "1816:20:9",
                              "typeDescriptions": {
                                "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                                "typeString": "mapping(address => uint256)"
                              }
                            },
                            "id": 2163,
                            "indexExpression": {
                              "argumentTypes": null,
                              "expression": {
                                "argumentTypes": null,
                                "id": 2161,
                                "name": "msg",
                                "nodeType": "Identifier",
                                "overloadedDeclarations": [],
                                "referencedDeclaration": 11851,
                                "src": "1837:3:9",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_magic_message",
                                  "typeString": "msg"
                                }
                              },
                              "id": 2162,
                              "isConstant": false,
                              "isLValue": false,
                              "isPure": false,
                              "lValueRequested": false,
                              "memberName": "sender",
                              "nodeType": "MemberAccess",
                              "referencedDeclaration": null,
                              "src": "1837:10:9",
                              "typeDescriptions": {
                                "typeIdentifier": "t_address_payable",
                                "typeString": "address payable"
                              }
                            },
                            "isConstant": false,
                            "isLValue": true,
                            "isPure": false,
                            "lValueRequested": false,
                            "nodeType": "IndexAccess",
                            "src": "1816:32:9",
                            "typeDescriptions": {
                              "typeIdentifier": "t_uint256",
                              "typeString": "uint256"
                            }
                          },
                          "src": "1786:62:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        },
                        "src": "1776:72:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "hexValue": "596f752063616e6e6f74207769746864726177206d6f7265207468616e20796f757220746f74616c2062616c616e6365206d696e757320776861742069732063757272656e746c79206265696e67206c656e74",
                        "id": 2166,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": true,
                        "kind": "string",
                        "lValueRequested": false,
                        "nodeType": "Literal",
                        "src": "1856:85:9",
                        "subdenomination": null,
                        "typeDescriptions": {
                          "typeIdentifier": "t_stringliteral_9b95227b372575c4d3540b25a2e007a20a3bdaffdc7e2a041b9c36ef3add4e50",
                          "typeString": "literal_string \"You cannot withdraw more than your total balance minus what is currently being lent\""
                        },
                        "value": "You cannot withdraw more than your total balance minus what is currently being lent"
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        },
                        {
                          "typeIdentifier": "t_stringliteral_9b95227b372575c4d3540b25a2e007a20a3bdaffdc7e2a041b9c36ef3add4e50",
                          "typeString": "literal_string \"You cannot withdraw more than your total balance minus what is currently being lent\""
                        }
                      ],
                      "id": 2154,
                      "name": "require",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [
                        11854,
                        11855
                      ],
                      "referencedDeclaration": 11855,
                      "src": "1768:7:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_require_pure$_t_bool_$_t_string_memory_ptr_$returns$__$",
                        "typeString": "function (bool,string memory) pure"
                      }
                    },
                    "id": 2167,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "1768:174:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 2168,
                  "nodeType": "ExpressionStatement",
                  "src": "1768:174:9"
                },
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 2179,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "baseExpression": {
                        "argumentTypes": null,
                        "id": 2169,
                        "name": "lendingBalances",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1989,
                        "src": "1949:15:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                          "typeString": "mapping(address => uint256)"
                        }
                      },
                      "id": 2172,
                      "indexExpression": {
                        "argumentTypes": null,
                        "expression": {
                          "argumentTypes": null,
                          "id": 2170,
                          "name": "msg",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 11851,
                          "src": "1965:3:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_magic_message",
                            "typeString": "msg"
                          }
                        },
                        "id": 2171,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "memberName": "sender",
                        "nodeType": "MemberAccess",
                        "referencedDeclaration": null,
                        "src": "1965:10:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address_payable",
                          "typeString": "address payable"
                        }
                      },
                      "isConstant": false,
                      "isLValue": true,
                      "isPure": false,
                      "lValueRequested": true,
                      "nodeType": "IndexAccess",
                      "src": "1949:27:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "commonType": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      },
                      "id": 2178,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "lValueRequested": false,
                      "leftExpression": {
                        "argumentTypes": null,
                        "baseExpression": {
                          "argumentTypes": null,
                          "id": 2173,
                          "name": "lendingBalances",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 1989,
                          "src": "1979:15:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                            "typeString": "mapping(address => uint256)"
                          }
                        },
                        "id": 2176,
                        "indexExpression": {
                          "argumentTypes": null,
                          "expression": {
                            "argumentTypes": null,
                            "id": 2174,
                            "name": "msg",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 11851,
                            "src": "1995:3:9",
                            "typeDescriptions": {
                              "typeIdentifier": "t_magic_message",
                              "typeString": "msg"
                            }
                          },
                          "id": 2175,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "memberName": "sender",
                          "nodeType": "MemberAccess",
                          "referencedDeclaration": null,
                          "src": "1995:10:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address_payable",
                            "typeString": "address payable"
                          }
                        },
                        "isConstant": false,
                        "isLValue": true,
                        "isPure": false,
                        "lValueRequested": false,
                        "nodeType": "IndexAccess",
                        "src": "1979:27:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      "nodeType": "BinaryOperation",
                      "operator": "-",
                      "rightExpression": {
                        "argumentTypes": null,
                        "id": 2177,
                        "name": "amount",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 2151,
                        "src": "2009:6:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      "src": "1979:36:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "src": "1949:66:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "id": 2180,
                  "nodeType": "ExpressionStatement",
                  "src": "1949:66:9"
                },
                {
                  "expression": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "id": 2186,
                        "name": "amount",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 2151,
                        "src": "2042:6:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      ],
                      "expression": {
                        "argumentTypes": null,
                        "expression": {
                          "argumentTypes": null,
                          "id": 2181,
                          "name": "msg",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 11851,
                          "src": "2022:3:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_magic_message",
                            "typeString": "msg"
                          }
                        },
                        "id": 2184,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "memberName": "sender",
                        "nodeType": "MemberAccess",
                        "referencedDeclaration": null,
                        "src": "2022:10:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address_payable",
                          "typeString": "address payable"
                        }
                      },
                      "id": 2185,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "lValueRequested": false,
                      "memberName": "transfer",
                      "nodeType": "MemberAccess",
                      "referencedDeclaration": null,
                      "src": "2022:19:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_transfer_nonpayable$_t_uint256_$returns$__$",
                        "typeString": "function (uint256)"
                      }
                    },
                    "id": 2187,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "2022:27:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 2188,
                  "nodeType": "ExpressionStatement",
                  "src": "2022:27:9"
                },
                {
                  "eventCall": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "expression": {
                          "argumentTypes": null,
                          "id": 2190,
                          "name": "msg",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 11851,
                          "src": "2073:3:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_magic_message",
                            "typeString": "msg"
                          }
                        },
                        "id": 2191,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "memberName": "sender",
                        "nodeType": "MemberAccess",
                        "referencedDeclaration": null,
                        "src": "2073:10:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address_payable",
                          "typeString": "address payable"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "id": 2192,
                        "name": "amount",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 2151,
                        "src": "2085:6:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_address_payable",
                          "typeString": "address payable"
                        },
                        {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      ],
                      "id": 2189,
                      "name": "WithdrewETH",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 9160,
                      "src": "2061:11:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_event_nonpayable$_t_address_$_t_uint256_$returns$__$",
                        "typeString": "function (address,uint256)"
                      }
                    },
                    "id": 2193,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "2061:31:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 2194,
                  "nodeType": "EmitStatement",
                  "src": "2056:36:9"
                }
              ]
            },
            "documentation": null,
            "id": 2196,
            "implemented": true,
            "kind": "function",
            "modifiers": [],
            "name": "withdrawLendingETH",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 2152,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 2151,
                  "name": "amount",
                  "nodeType": "VariableDeclaration",
                  "scope": 2196,
                  "src": "1738:14:9",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_uint256",
                    "typeString": "uint256"
                  },
                  "typeName": {
                    "id": 2150,
                    "name": "uint256",
                    "nodeType": "ElementaryTypeName",
                    "src": "1738:7:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "1737:16:9"
            },
            "returnParameters": {
              "id": 2153,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "1761:0:9"
            },
            "scope": 2386,
            "src": "1710:388:9",
            "stateMutability": "nonpayable",
            "superFunction": null,
            "visibility": "public"
          },
          {
            "body": {
              "id": 2259,
              "nodeType": "Block",
              "src": "2177:451:9",
              "statements": [
                {
                  "assignments": [
                    2206
                  ],
                  "declarations": [
                    {
                      "constant": false,
                      "id": 2206,
                      "name": "amountToLend",
                      "nodeType": "VariableDeclaration",
                      "scope": 2259,
                      "src": "2184:17:9",
                      "stateVariable": false,
                      "storageLocation": "default",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      },
                      "typeName": {
                        "id": 2205,
                        "name": "uint",
                        "nodeType": "ElementaryTypeName",
                        "src": "2184:4:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      "value": null,
                      "visibility": "internal"
                    }
                  ],
                  "id": 2211,
                  "initialValue": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "id": 2209,
                        "name": "loanId",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 2200,
                        "src": "2226:6:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      ],
                      "expression": {
                        "argumentTypes": null,
                        "id": 2207,
                        "name": "manager",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 2009,
                        "src": "2204:7:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_contract$_ILendingManager_$9136",
                          "typeString": "contract ILendingManager"
                        }
                      },
                      "id": 2208,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "lValueRequested": false,
                      "memberName": "getAmountLent",
                      "nodeType": "MemberAccess",
                      "referencedDeclaration": 9109,
                      "src": "2204:21:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_external_view$_t_uint256_$returns$_t_uint256_$",
                        "typeString": "function (uint256) view external returns (uint256)"
                      }
                    },
                    "id": 2210,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "2204:29:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "nodeType": "VariableDeclarationStatement",
                  "src": "2184:49:9"
                },
                {
                  "expression": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "commonType": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        },
                        "id": 2221,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "leftExpression": {
                          "argumentTypes": null,
                          "commonType": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          },
                          "id": 2217,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "leftExpression": {
                            "argumentTypes": null,
                            "baseExpression": {
                              "argumentTypes": null,
                              "id": 2213,
                              "name": "totalLentPerCustomer",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 2007,
                              "src": "2248:20:9",
                              "typeDescriptions": {
                                "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                                "typeString": "mapping(address => uint256)"
                              }
                            },
                            "id": 2215,
                            "indexExpression": {
                              "argumentTypes": null,
                              "id": 2214,
                              "name": "customer",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 2198,
                              "src": "2269:8:9",
                              "typeDescriptions": {
                                "typeIdentifier": "t_address",
                                "typeString": "address"
                              }
                            },
                            "isConstant": false,
                            "isLValue": true,
                            "isPure": false,
                            "lValueRequested": false,
                            "nodeType": "IndexAccess",
                            "src": "2248:30:9",
                            "typeDescriptions": {
                              "typeIdentifier": "t_uint256",
                              "typeString": "uint256"
                            }
                          },
                          "nodeType": "BinaryOperation",
                          "operator": "+",
                          "rightExpression": {
                            "argumentTypes": null,
                            "id": 2216,
                            "name": "amountToLend",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 2206,
                            "src": "2289:12:9",
                            "typeDescriptions": {
                              "typeIdentifier": "t_uint256",
                              "typeString": "uint256"
                            }
                          },
                          "src": "2248:53:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        },
                        "nodeType": "BinaryOperation",
                        "operator": "<=",
                        "rightExpression": {
                          "argumentTypes": null,
                          "baseExpression": {
                            "argumentTypes": null,
                            "id": 2218,
                            "name": "lendingBalances",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 1989,
                            "src": "2305:15:9",
                            "typeDescriptions": {
                              "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                              "typeString": "mapping(address => uint256)"
                            }
                          },
                          "id": 2220,
                          "indexExpression": {
                            "argumentTypes": null,
                            "id": 2219,
                            "name": "customer",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 2198,
                            "src": "2321:8:9",
                            "typeDescriptions": {
                              "typeIdentifier": "t_address",
                              "typeString": "address"
                            }
                          },
                          "isConstant": false,
                          "isLValue": true,
                          "isPure": false,
                          "lValueRequested": false,
                          "nodeType": "IndexAccess",
                          "src": "2305:25:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        },
                        "src": "2248:82:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "hexValue": "43616e6e6f74206c656e64206d6f7265207468616e2074686520637573746f6d657220617070726f76656420666f72206c656e64696e67",
                        "id": 2222,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": true,
                        "kind": "string",
                        "lValueRequested": false,
                        "nodeType": "Literal",
                        "src": "2340:57:9",
                        "subdenomination": null,
                        "typeDescriptions": {
                          "typeIdentifier": "t_stringliteral_dbcdd8a7afd244d1ef485b4244e6f588c584997d458f835376392c0228bb97c4",
                          "typeString": "literal_string \"Cannot lend more than the customer approved for lending\""
                        },
                        "value": "Cannot lend more than the customer approved for lending"
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        },
                        {
                          "typeIdentifier": "t_stringliteral_dbcdd8a7afd244d1ef485b4244e6f588c584997d458f835376392c0228bb97c4",
                          "typeString": "literal_string \"Cannot lend more than the customer approved for lending\""
                        }
                      ],
                      "id": 2212,
                      "name": "require",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [
                        11854,
                        11855
                      ],
                      "referencedDeclaration": 11855,
                      "src": "2240:7:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_require_pure$_t_bool_$_t_string_memory_ptr_$returns$__$",
                        "typeString": "function (bool,string memory) pure"
                      }
                    },
                    "id": 2223,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "2240:158:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 2224,
                  "nodeType": "ExpressionStatement",
                  "src": "2240:158:9"
                },
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 2233,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "baseExpression": {
                        "argumentTypes": null,
                        "id": 2225,
                        "name": "totalLentPerCustomer",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 2007,
                        "src": "2405:20:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                          "typeString": "mapping(address => uint256)"
                        }
                      },
                      "id": 2227,
                      "indexExpression": {
                        "argumentTypes": null,
                        "id": 2226,
                        "name": "customer",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 2198,
                        "src": "2426:8:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      "isConstant": false,
                      "isLValue": true,
                      "isPure": false,
                      "lValueRequested": true,
                      "nodeType": "IndexAccess",
                      "src": "2405:30:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "commonType": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      },
                      "id": 2232,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "lValueRequested": false,
                      "leftExpression": {
                        "argumentTypes": null,
                        "baseExpression": {
                          "argumentTypes": null,
                          "id": 2228,
                          "name": "totalLentPerCustomer",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 2007,
                          "src": "2438:20:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                            "typeString": "mapping(address => uint256)"
                          }
                        },
                        "id": 2230,
                        "indexExpression": {
                          "argumentTypes": null,
                          "id": 2229,
                          "name": "customer",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 2198,
                          "src": "2459:8:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "isConstant": false,
                        "isLValue": true,
                        "isPure": false,
                        "lValueRequested": false,
                        "nodeType": "IndexAccess",
                        "src": "2438:30:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      "nodeType": "BinaryOperation",
                      "operator": "+",
                      "rightExpression": {
                        "argumentTypes": null,
                        "id": 2231,
                        "name": "amountToLend",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 2206,
                        "src": "2471:12:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      "src": "2438:45:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "src": "2405:78:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "id": 2234,
                  "nodeType": "ExpressionStatement",
                  "src": "2405:78:9"
                },
                {
                  "expression": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "id": 2242,
                        "name": "loanId",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 2200,
                        "src": "2527:6:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      ],
                      "arguments": [
                        {
                          "argumentTypes": null,
                          "id": 2240,
                          "name": "amountToLend",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 2206,
                          "src": "2513:12:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        }
                      ],
                      "expression": {
                        "argumentTypes": [
                          {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        ],
                        "expression": {
                          "argumentTypes": null,
                          "expression": {
                            "argumentTypes": null,
                            "id": 2235,
                            "name": "manager",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 2009,
                            "src": "2490:7:9",
                            "typeDescriptions": {
                              "typeIdentifier": "t_contract$_ILendingManager_$9136",
                              "typeString": "contract ILendingManager"
                            }
                          },
                          "id": 2238,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "memberName": "fillLoan",
                          "nodeType": "MemberAccess",
                          "referencedDeclaration": 9054,
                          "src": "2490:16:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_function_external_payable$_t_uint256_$returns$__$",
                            "typeString": "function (uint256) payable external"
                          }
                        },
                        "id": 2239,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "memberName": "value",
                        "nodeType": "MemberAccess",
                        "referencedDeclaration": null,
                        "src": "2490:22:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_function_setvalue_nonpayable$_t_uint256_$returns$_t_function_external_payable$_t_uint256_$returns$__$value_$",
                          "typeString": "function (uint256) returns (function (uint256) payable external)"
                        }
                      },
                      "id": 2241,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "kind": "functionCall",
                      "lValueRequested": false,
                      "names": [],
                      "nodeType": "FunctionCall",
                      "src": "2490:36:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_external_payable$_t_uint256_$returns$__$value",
                        "typeString": "function (uint256) payable external"
                      }
                    },
                    "id": 2243,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "2490:44:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 2244,
                  "nodeType": "ExpressionStatement",
                  "src": "2490:44:9"
                },
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 2251,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "baseExpression": {
                        "argumentTypes": null,
                        "baseExpression": {
                          "argumentTypes": null,
                          "id": 2245,
                          "name": "customerLoanTie",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 1999,
                          "src": "2541:15:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_mapping$_t_address_$_t_mapping$_t_uint256_$_t_bool_$_$",
                            "typeString": "mapping(address => mapping(uint256 => bool))"
                          }
                        },
                        "id": 2248,
                        "indexExpression": {
                          "argumentTypes": null,
                          "id": 2246,
                          "name": "customer",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 2198,
                          "src": "2557:8:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "isConstant": false,
                        "isLValue": true,
                        "isPure": false,
                        "lValueRequested": false,
                        "nodeType": "IndexAccess",
                        "src": "2541:25:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_mapping$_t_uint256_$_t_bool_$",
                          "typeString": "mapping(uint256 => bool)"
                        }
                      },
                      "id": 2249,
                      "indexExpression": {
                        "argumentTypes": null,
                        "id": 2247,
                        "name": "loanId",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 2200,
                        "src": "2567:6:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      "isConstant": false,
                      "isLValue": true,
                      "isPure": false,
                      "lValueRequested": true,
                      "nodeType": "IndexAccess",
                      "src": "2541:33:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_bool",
                        "typeString": "bool"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "hexValue": "74727565",
                      "id": 2250,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": true,
                      "kind": "bool",
                      "lValueRequested": false,
                      "nodeType": "Literal",
                      "src": "2577:4:9",
                      "subdenomination": null,
                      "typeDescriptions": {
                        "typeIdentifier": "t_bool",
                        "typeString": "bool"
                      },
                      "value": "true"
                    },
                    "src": "2541:40:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_bool",
                      "typeString": "bool"
                    }
                  },
                  "id": 2252,
                  "nodeType": "ExpressionStatement",
                  "src": "2541:40:9"
                },
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 2257,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "baseExpression": {
                        "argumentTypes": null,
                        "id": 2253,
                        "name": "loanCustomerTie",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 2003,
                        "src": "2588:15:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_mapping$_t_uint256_$_t_address_$",
                          "typeString": "mapping(uint256 => address)"
                        }
                      },
                      "id": 2255,
                      "indexExpression": {
                        "argumentTypes": null,
                        "id": 2254,
                        "name": "loanId",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 2200,
                        "src": "2604:6:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      "isConstant": false,
                      "isLValue": true,
                      "isPure": false,
                      "lValueRequested": true,
                      "nodeType": "IndexAccess",
                      "src": "2588:23:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_address",
                        "typeString": "address"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "id": 2256,
                      "name": "customer",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 2198,
                      "src": "2614:8:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_address",
                        "typeString": "address"
                      }
                    },
                    "src": "2588:34:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "id": 2258,
                  "nodeType": "ExpressionStatement",
                  "src": "2588:34:9"
                }
              ]
            },
            "documentation": null,
            "id": 2260,
            "implemented": true,
            "kind": "function",
            "modifiers": [
              {
                "arguments": [],
                "id": 2203,
                "modifierName": {
                  "argumentTypes": null,
                  "id": 2202,
                  "name": "onlyValidRelayer",
                  "nodeType": "Identifier",
                  "overloadedDeclarations": [],
                  "referencedDeclaration": 2023,
                  "src": "2158:16:9",
                  "typeDescriptions": {
                    "typeIdentifier": "t_modifier$__$",
                    "typeString": "modifier ()"
                  }
                },
                "nodeType": "ModifierInvocation",
                "src": "2158:18:9"
              }
            ],
            "name": "fillLoan",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 2201,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 2198,
                  "name": "customer",
                  "nodeType": "VariableDeclaration",
                  "scope": 2260,
                  "src": "2120:16:9",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 2197,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "2120:7:9",
                    "stateMutability": "nonpayable",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                },
                {
                  "constant": false,
                  "id": 2200,
                  "name": "loanId",
                  "nodeType": "VariableDeclaration",
                  "scope": 2260,
                  "src": "2138:11:9",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_uint256",
                    "typeString": "uint256"
                  },
                  "typeName": {
                    "id": 2199,
                    "name": "uint",
                    "nodeType": "ElementaryTypeName",
                    "src": "2138:4:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "2119:31:9"
            },
            "returnParameters": {
              "id": 2204,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "2177:0:9"
            },
            "scope": 2386,
            "src": "2102:526:9",
            "stateMutability": "nonpayable",
            "superFunction": null,
            "visibility": "public"
          },
          {
            "body": {
              "id": 2276,
              "nodeType": "Block",
              "src": "2711:59:9",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "id": 2272,
                        "name": "id",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 2262,
                        "src": "2750:2:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "id": 2273,
                        "name": "destToken",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 2264,
                        "src": "2754:9:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        },
                        {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      ],
                      "expression": {
                        "argumentTypes": null,
                        "id": 2269,
                        "name": "manager",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 2009,
                        "src": "2718:7:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_contract$_ILendingManager_$9136",
                          "typeString": "contract ILendingManager"
                        }
                      },
                      "id": 2271,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "lValueRequested": false,
                      "memberName": "enableSwappedCollateral",
                      "nodeType": "MemberAccess",
                      "referencedDeclaration": 9079,
                      "src": "2718:31:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_external_nonpayable$_t_uint256_$_t_address_$returns$__$",
                        "typeString": "function (uint256,address) external"
                      }
                    },
                    "id": 2274,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "2718:46:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 2275,
                  "nodeType": "ExpressionStatement",
                  "src": "2718:46:9"
                }
              ]
            },
            "documentation": null,
            "id": 2277,
            "implemented": true,
            "kind": "function",
            "modifiers": [
              {
                "arguments": [],
                "id": 2267,
                "modifierName": {
                  "argumentTypes": null,
                  "id": 2266,
                  "name": "onlyValidRelayer",
                  "nodeType": "Identifier",
                  "overloadedDeclarations": [],
                  "referencedDeclaration": 2023,
                  "src": "2692:16:9",
                  "typeDescriptions": {
                    "typeIdentifier": "t_modifier$__$",
                    "typeString": "modifier ()"
                  }
                },
                "nodeType": "ModifierInvocation",
                "src": "2692:18:9"
              }
            ],
            "name": "enableKyberSwap",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 2265,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 2262,
                  "name": "id",
                  "nodeType": "VariableDeclaration",
                  "scope": 2277,
                  "src": "2657:7:9",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_uint256",
                    "typeString": "uint256"
                  },
                  "typeName": {
                    "id": 2261,
                    "name": "uint",
                    "nodeType": "ElementaryTypeName",
                    "src": "2657:4:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                },
                {
                  "constant": false,
                  "id": 2264,
                  "name": "destToken",
                  "nodeType": "VariableDeclaration",
                  "scope": 2277,
                  "src": "2666:17:9",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 2263,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "2666:7:9",
                    "stateMutability": "nonpayable",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "2656:28:9"
            },
            "returnParameters": {
              "id": 2268,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "2711:0:9"
            },
            "scope": 2386,
            "src": "2632:138:9",
            "stateMutability": "nonpayable",
            "superFunction": null,
            "visibility": "public"
          },
          {
            "body": {
              "id": 2319,
              "nodeType": "Block",
              "src": "2852:633:9",
              "statements": [
                {
                  "assignments": [
                    2287
                  ],
                  "declarations": [
                    {
                      "constant": false,
                      "id": 2287,
                      "name": "collateralReceived",
                      "nodeType": "VariableDeclaration",
                      "scope": 2319,
                      "src": "2859:26:9",
                      "stateVariable": false,
                      "storageLocation": "default",
                      "typeDescriptions": {
                        "typeIdentifier": "t_address",
                        "typeString": "address"
                      },
                      "typeName": {
                        "id": 2286,
                        "name": "address",
                        "nodeType": "ElementaryTypeName",
                        "src": "2859:7:9",
                        "stateMutability": "nonpayable",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      "value": null,
                      "visibility": "internal"
                    }
                  ],
                  "id": 2288,
                  "initialValue": null,
                  "nodeType": "VariableDeclarationStatement",
                  "src": "2859:26:9"
                },
                {
                  "assignments": [
                    2290
                  ],
                  "declarations": [
                    {
                      "constant": false,
                      "id": 2290,
                      "name": "collateralType",
                      "nodeType": "VariableDeclaration",
                      "scope": 2319,
                      "src": "2891:19:9",
                      "stateVariable": false,
                      "storageLocation": "default",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      },
                      "typeName": {
                        "id": 2289,
                        "name": "uint",
                        "nodeType": "ElementaryTypeName",
                        "src": "2891:4:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      "value": null,
                      "visibility": "internal"
                    }
                  ],
                  "id": 2291,
                  "initialValue": null,
                  "nodeType": "VariableDeclarationStatement",
                  "src": "2891:19:9"
                },
                {
                  "assignments": [
                    2293
                  ],
                  "declarations": [
                    {
                      "constant": false,
                      "id": 2293,
                      "name": "collateralDetails",
                      "nodeType": "VariableDeclaration",
                      "scope": 2319,
                      "src": "2916:30:9",
                      "stateVariable": false,
                      "storageLocation": "memory",
                      "typeDescriptions": {
                        "typeIdentifier": "t_bytes_memory_ptr",
                        "typeString": "bytes"
                      },
                      "typeName": {
                        "id": 2292,
                        "name": "bytes",
                        "nodeType": "ElementaryTypeName",
                        "src": "2916:5:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_bytes_storage_ptr",
                          "typeString": "bytes"
                        }
                      },
                      "value": null,
                      "visibility": "internal"
                    }
                  ],
                  "id": 2294,
                  "initialValue": null,
                  "nodeType": "VariableDeclarationStatement",
                  "src": "2916:30:9"
                },
                {
                  "assignments": [
                    2296
                  ],
                  "declarations": [
                    {
                      "constant": false,
                      "id": 2296,
                      "name": "ethAddress",
                      "nodeType": "VariableDeclaration",
                      "scope": 2319,
                      "src": "2953:18:9",
                      "stateVariable": false,
                      "storageLocation": "default",
                      "typeDescriptions": {
                        "typeIdentifier": "t_address",
                        "typeString": "address"
                      },
                      "typeName": {
                        "id": 2295,
                        "name": "address",
                        "nodeType": "ElementaryTypeName",
                        "src": "2953:7:9",
                        "stateMutability": "nonpayable",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      "value": null,
                      "visibility": "internal"
                    }
                  ],
                  "id": 2300,
                  "initialValue": {
                    "argumentTypes": null,
                    "arguments": [],
                    "expression": {
                      "argumentTypes": [],
                      "expression": {
                        "argumentTypes": null,
                        "id": 2297,
                        "name": "manager",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 2009,
                        "src": "2974:7:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_contract$_ILendingManager_$9136",
                          "typeString": "contract ILendingManager"
                        }
                      },
                      "id": 2298,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "lValueRequested": false,
                      "memberName": "getETHAddress",
                      "nodeType": "MemberAccess",
                      "referencedDeclaration": 9135,
                      "src": "2974:21:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_external_view$__$returns$_t_address_$",
                        "typeString": "function () view external returns (address)"
                      }
                    },
                    "id": 2299,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "2974:23:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "nodeType": "VariableDeclarationStatement",
                  "src": "2953:44:9"
                },
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 2309,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "components": [
                        {
                          "argumentTypes": null,
                          "id": 2301,
                          "name": "collateralReceived",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 2287,
                          "src": "3005:18:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        {
                          "argumentTypes": null,
                          "id": 2302,
                          "name": "collateralType",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 2290,
                          "src": "3025:14:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        },
                        {
                          "argumentTypes": null,
                          "id": 2303,
                          "name": "collateralDetails",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 2293,
                          "src": "3041:17:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_bytes_memory_ptr",
                            "typeString": "bytes memory"
                          }
                        }
                      ],
                      "id": 2304,
                      "isConstant": false,
                      "isInlineArray": false,
                      "isLValue": true,
                      "isPure": false,
                      "lValueRequested": true,
                      "nodeType": "TupleExpression",
                      "src": "3004:55:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_tuple$_t_address_$_t_uint256_$_t_bytes_memory_ptr_$",
                        "typeString": "tuple(address,uint256,bytes memory)"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "arguments": [
                        {
                          "argumentTypes": null,
                          "id": 2307,
                          "name": "id",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 2279,
                          "src": "3091:2:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        }
                      ],
                      "expression": {
                        "argumentTypes": [
                          {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        ],
                        "expression": {
                          "argumentTypes": null,
                          "id": 2305,
                          "name": "manager",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 2009,
                          "src": "3068:7:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_contract$_ILendingManager_$9136",
                            "typeString": "contract ILendingManager"
                          }
                        },
                        "id": 2306,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "memberName": "declareDefault",
                        "nodeType": "MemberAccess",
                        "referencedDeclaration": 9072,
                        "src": "3068:22:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_function_external_nonpayable$_t_uint256_$returns$_t_address_$_t_uint256_$_t_bytes_memory_ptr_$",
                          "typeString": "function (uint256) external returns (address,uint256,bytes memory)"
                        }
                      },
                      "id": 2308,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "kind": "functionCall",
                      "lValueRequested": false,
                      "names": [],
                      "nodeType": "FunctionCall",
                      "src": "3068:26:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_tuple$_t_address_$_t_uint256_$_t_bytes_memory_ptr_$",
                        "typeString": "tuple(address,uint256,bytes memory)"
                      }
                    },
                    "src": "3004:90:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 2310,
                  "nodeType": "ExpressionStatement",
                  "src": "3004:90:9"
                },
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 2317,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "baseExpression": {
                        "argumentTypes": null,
                        "baseExpression": {
                          "argumentTypes": null,
                          "id": 2311,
                          "name": "customerLoanTie",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 1999,
                          "src": "3442:15:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_mapping$_t_address_$_t_mapping$_t_uint256_$_t_bool_$_$",
                            "typeString": "mapping(address => mapping(uint256 => bool))"
                          }
                        },
                        "id": 2314,
                        "indexExpression": {
                          "argumentTypes": null,
                          "id": 2312,
                          "name": "customer",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 2281,
                          "src": "3458:8:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "isConstant": false,
                        "isLValue": true,
                        "isPure": false,
                        "lValueRequested": false,
                        "nodeType": "IndexAccess",
                        "src": "3442:25:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_mapping$_t_uint256_$_t_bool_$",
                          "typeString": "mapping(uint256 => bool)"
                        }
                      },
                      "id": 2315,
                      "indexExpression": {
                        "argumentTypes": null,
                        "id": 2313,
                        "name": "id",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 2279,
                        "src": "3468:2:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      "isConstant": false,
                      "isLValue": true,
                      "isPure": false,
                      "lValueRequested": true,
                      "nodeType": "IndexAccess",
                      "src": "3442:29:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_bool",
                        "typeString": "bool"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "hexValue": "66616c7365",
                      "id": 2316,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": true,
                      "kind": "bool",
                      "lValueRequested": false,
                      "nodeType": "Literal",
                      "src": "3474:5:9",
                      "subdenomination": null,
                      "typeDescriptions": {
                        "typeIdentifier": "t_bool",
                        "typeString": "bool"
                      },
                      "value": "false"
                    },
                    "src": "3442:37:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_bool",
                      "typeString": "bool"
                    }
                  },
                  "id": 2318,
                  "nodeType": "ExpressionStatement",
                  "src": "3442:37:9"
                }
              ]
            },
            "documentation": null,
            "id": 2320,
            "implemented": true,
            "kind": "function",
            "modifiers": [
              {
                "arguments": [],
                "id": 2284,
                "modifierName": {
                  "argumentTypes": null,
                  "id": 2283,
                  "name": "onlyValidRelayer",
                  "nodeType": "Identifier",
                  "overloadedDeclarations": [],
                  "referencedDeclaration": 2023,
                  "src": "2833:16:9",
                  "typeDescriptions": {
                    "typeIdentifier": "t_modifier$__$",
                    "typeString": "modifier ()"
                  }
                },
                "nodeType": "ModifierInvocation",
                "src": "2833:18:9"
              }
            ],
            "name": "seizeCollateral",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 2282,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 2279,
                  "name": "id",
                  "nodeType": "VariableDeclaration",
                  "scope": 2320,
                  "src": "2799:7:9",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_uint256",
                    "typeString": "uint256"
                  },
                  "typeName": {
                    "id": 2278,
                    "name": "uint",
                    "nodeType": "ElementaryTypeName",
                    "src": "2799:4:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                },
                {
                  "constant": false,
                  "id": 2281,
                  "name": "customer",
                  "nodeType": "VariableDeclaration",
                  "scope": 2320,
                  "src": "2808:16:9",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 2280,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "2808:7:9",
                    "stateMutability": "nonpayable",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "2798:27:9"
            },
            "returnParameters": {
              "id": 2285,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "2852:0:9"
            },
            "scope": 2386,
            "src": "2774:711:9",
            "stateMutability": "nonpayable",
            "superFunction": null,
            "visibility": "public"
          },
          {
            "body": {
              "id": 2384,
              "nodeType": "Block",
              "src": "3560:456:9",
              "statements": [
                {
                  "assignments": [
                    2328
                  ],
                  "declarations": [
                    {
                      "constant": false,
                      "id": 2328,
                      "name": "customer",
                      "nodeType": "VariableDeclaration",
                      "scope": 2384,
                      "src": "3567:16:9",
                      "stateVariable": false,
                      "storageLocation": "default",
                      "typeDescriptions": {
                        "typeIdentifier": "t_address",
                        "typeString": "address"
                      },
                      "typeName": {
                        "id": 2327,
                        "name": "address",
                        "nodeType": "ElementaryTypeName",
                        "src": "3567:7:9",
                        "stateMutability": "nonpayable",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      "value": null,
                      "visibility": "internal"
                    }
                  ],
                  "id": 2332,
                  "initialValue": {
                    "argumentTypes": null,
                    "baseExpression": {
                      "argumentTypes": null,
                      "id": 2329,
                      "name": "loanCustomerTie",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 2003,
                      "src": "3586:15:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_mapping$_t_uint256_$_t_address_$",
                        "typeString": "mapping(uint256 => address)"
                      }
                    },
                    "id": 2331,
                    "indexExpression": {
                      "argumentTypes": null,
                      "id": 2330,
                      "name": "id",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 2322,
                      "src": "3602:2:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "isConstant": false,
                    "isLValue": true,
                    "isPure": false,
                    "lValueRequested": false,
                    "nodeType": "IndexAccess",
                    "src": "3586:19:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "nodeType": "VariableDeclarationStatement",
                  "src": "3567:38:9"
                },
                {
                  "condition": {
                    "argumentTypes": null,
                    "commonType": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    },
                    "id": 2338,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftExpression": {
                      "argumentTypes": null,
                      "expression": {
                        "argumentTypes": null,
                        "id": 2333,
                        "name": "msg",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 11851,
                        "src": "3616:3:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_magic_message",
                          "typeString": "msg"
                        }
                      },
                      "id": 2334,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "lValueRequested": false,
                      "memberName": "value",
                      "nodeType": "MemberAccess",
                      "referencedDeclaration": null,
                      "src": "3616:9:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "nodeType": "BinaryOperation",
                    "operator": ">",
                    "rightExpression": {
                      "argumentTypes": null,
                      "baseExpression": {
                        "argumentTypes": null,
                        "id": 2335,
                        "name": "totalLentPerCustomer",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 2007,
                        "src": "3628:20:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                          "typeString": "mapping(address => uint256)"
                        }
                      },
                      "id": 2337,
                      "indexExpression": {
                        "argumentTypes": null,
                        "id": 2336,
                        "name": "customer",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 2328,
                        "src": "3649:8:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      "isConstant": false,
                      "isLValue": true,
                      "isPure": false,
                      "lValueRequested": false,
                      "nodeType": "IndexAccess",
                      "src": "3628:30:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "src": "3616:42:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_bool",
                      "typeString": "bool"
                    }
                  },
                  "falseBody": {
                    "id": 2376,
                    "nodeType": "Block",
                    "src": "3866:92:9",
                    "statements": [
                      {
                        "expression": {
                          "argumentTypes": null,
                          "id": 2374,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "leftHandSide": {
                            "argumentTypes": null,
                            "baseExpression": {
                              "argumentTypes": null,
                              "id": 2365,
                              "name": "totalLentPerCustomer",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 2007,
                              "src": "3875:20:9",
                              "typeDescriptions": {
                                "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                                "typeString": "mapping(address => uint256)"
                              }
                            },
                            "id": 2367,
                            "indexExpression": {
                              "argumentTypes": null,
                              "id": 2366,
                              "name": "customer",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 2328,
                              "src": "3896:8:9",
                              "typeDescriptions": {
                                "typeIdentifier": "t_address",
                                "typeString": "address"
                              }
                            },
                            "isConstant": false,
                            "isLValue": true,
                            "isPure": false,
                            "lValueRequested": true,
                            "nodeType": "IndexAccess",
                            "src": "3875:30:9",
                            "typeDescriptions": {
                              "typeIdentifier": "t_uint256",
                              "typeString": "uint256"
                            }
                          },
                          "nodeType": "Assignment",
                          "operator": "=",
                          "rightHandSide": {
                            "argumentTypes": null,
                            "commonType": {
                              "typeIdentifier": "t_uint256",
                              "typeString": "uint256"
                            },
                            "id": 2373,
                            "isConstant": false,
                            "isLValue": false,
                            "isPure": false,
                            "lValueRequested": false,
                            "leftExpression": {
                              "argumentTypes": null,
                              "baseExpression": {
                                "argumentTypes": null,
                                "id": 2368,
                                "name": "totalLentPerCustomer",
                                "nodeType": "Identifier",
                                "overloadedDeclarations": [],
                                "referencedDeclaration": 2007,
                                "src": "3908:20:9",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                                  "typeString": "mapping(address => uint256)"
                                }
                              },
                              "id": 2370,
                              "indexExpression": {
                                "argumentTypes": null,
                                "id": 2369,
                                "name": "customer",
                                "nodeType": "Identifier",
                                "overloadedDeclarations": [],
                                "referencedDeclaration": 2328,
                                "src": "3929:8:9",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_address",
                                  "typeString": "address"
                                }
                              },
                              "isConstant": false,
                              "isLValue": true,
                              "isPure": false,
                              "lValueRequested": false,
                              "nodeType": "IndexAccess",
                              "src": "3908:30:9",
                              "typeDescriptions": {
                                "typeIdentifier": "t_uint256",
                                "typeString": "uint256"
                              }
                            },
                            "nodeType": "BinaryOperation",
                            "operator": "-",
                            "rightExpression": {
                              "argumentTypes": null,
                              "expression": {
                                "argumentTypes": null,
                                "id": 2371,
                                "name": "msg",
                                "nodeType": "Identifier",
                                "overloadedDeclarations": [],
                                "referencedDeclaration": 11851,
                                "src": "3941:3:9",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_magic_message",
                                  "typeString": "msg"
                                }
                              },
                              "id": 2372,
                              "isConstant": false,
                              "isLValue": false,
                              "isPure": false,
                              "lValueRequested": false,
                              "memberName": "value",
                              "nodeType": "MemberAccess",
                              "referencedDeclaration": null,
                              "src": "3941:9:9",
                              "typeDescriptions": {
                                "typeIdentifier": "t_uint256",
                                "typeString": "uint256"
                              }
                            },
                            "src": "3908:42:9",
                            "typeDescriptions": {
                              "typeIdentifier": "t_uint256",
                              "typeString": "uint256"
                            }
                          },
                          "src": "3875:75:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        },
                        "id": 2375,
                        "nodeType": "ExpressionStatement",
                        "src": "3875:75:9"
                      }
                    ]
                  },
                  "id": 2377,
                  "nodeType": "IfStatement",
                  "src": "3612:346:9",
                  "trueBody": {
                    "id": 2364,
                    "nodeType": "Block",
                    "src": "3660:200:9",
                    "statements": [
                      {
                        "assignments": [
                          2340
                        ],
                        "declarations": [
                          {
                            "constant": false,
                            "id": 2340,
                            "name": "extraIncome",
                            "nodeType": "VariableDeclaration",
                            "scope": 2364,
                            "src": "3669:19:9",
                            "stateVariable": false,
                            "storageLocation": "default",
                            "typeDescriptions": {
                              "typeIdentifier": "t_uint256",
                              "typeString": "uint256"
                            },
                            "typeName": {
                              "id": 2339,
                              "name": "uint256",
                              "nodeType": "ElementaryTypeName",
                              "src": "3669:7:9",
                              "typeDescriptions": {
                                "typeIdentifier": "t_uint256",
                                "typeString": "uint256"
                              }
                            },
                            "value": null,
                            "visibility": "internal"
                          }
                        ],
                        "id": 2347,
                        "initialValue": {
                          "argumentTypes": null,
                          "commonType": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          },
                          "id": 2346,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "leftExpression": {
                            "argumentTypes": null,
                            "expression": {
                              "argumentTypes": null,
                              "id": 2341,
                              "name": "msg",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 11851,
                              "src": "3691:3:9",
                              "typeDescriptions": {
                                "typeIdentifier": "t_magic_message",
                                "typeString": "msg"
                              }
                            },
                            "id": 2342,
                            "isConstant": false,
                            "isLValue": false,
                            "isPure": false,
                            "lValueRequested": false,
                            "memberName": "value",
                            "nodeType": "MemberAccess",
                            "referencedDeclaration": null,
                            "src": "3691:9:9",
                            "typeDescriptions": {
                              "typeIdentifier": "t_uint256",
                              "typeString": "uint256"
                            }
                          },
                          "nodeType": "BinaryOperation",
                          "operator": "-",
                          "rightExpression": {
                            "argumentTypes": null,
                            "baseExpression": {
                              "argumentTypes": null,
                              "id": 2343,
                              "name": "totalLentPerCustomer",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 2007,
                              "src": "3703:20:9",
                              "typeDescriptions": {
                                "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                                "typeString": "mapping(address => uint256)"
                              }
                            },
                            "id": 2345,
                            "indexExpression": {
                              "argumentTypes": null,
                              "id": 2344,
                              "name": "customer",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 2328,
                              "src": "3724:8:9",
                              "typeDescriptions": {
                                "typeIdentifier": "t_address",
                                "typeString": "address"
                              }
                            },
                            "isConstant": false,
                            "isLValue": true,
                            "isPure": false,
                            "lValueRequested": false,
                            "nodeType": "IndexAccess",
                            "src": "3703:30:9",
                            "typeDescriptions": {
                              "typeIdentifier": "t_uint256",
                              "typeString": "uint256"
                            }
                          },
                          "src": "3691:42:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        },
                        "nodeType": "VariableDeclarationStatement",
                        "src": "3669:64:9"
                      },
                      {
                        "expression": {
                          "argumentTypes": null,
                          "id": 2352,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "leftHandSide": {
                            "argumentTypes": null,
                            "baseExpression": {
                              "argumentTypes": null,
                              "id": 2348,
                              "name": "totalLentPerCustomer",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 2007,
                              "src": "3742:20:9",
                              "typeDescriptions": {
                                "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                                "typeString": "mapping(address => uint256)"
                              }
                            },
                            "id": 2350,
                            "indexExpression": {
                              "argumentTypes": null,
                              "id": 2349,
                              "name": "customer",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 2328,
                              "src": "3763:8:9",
                              "typeDescriptions": {
                                "typeIdentifier": "t_address",
                                "typeString": "address"
                              }
                            },
                            "isConstant": false,
                            "isLValue": true,
                            "isPure": false,
                            "lValueRequested": true,
                            "nodeType": "IndexAccess",
                            "src": "3742:30:9",
                            "typeDescriptions": {
                              "typeIdentifier": "t_uint256",
                              "typeString": "uint256"
                            }
                          },
                          "nodeType": "Assignment",
                          "operator": "=",
                          "rightHandSide": {
                            "argumentTypes": null,
                            "hexValue": "30",
                            "id": 2351,
                            "isConstant": false,
                            "isLValue": false,
                            "isPure": true,
                            "kind": "number",
                            "lValueRequested": false,
                            "nodeType": "Literal",
                            "src": "3775:1:9",
                            "subdenomination": null,
                            "typeDescriptions": {
                              "typeIdentifier": "t_rational_0_by_1",
                              "typeString": "int_const 0"
                            },
                            "value": "0"
                          },
                          "src": "3742:34:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        },
                        "id": 2353,
                        "nodeType": "ExpressionStatement",
                        "src": "3742:34:9"
                      },
                      {
                        "expression": {
                          "argumentTypes": null,
                          "id": 2362,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "leftHandSide": {
                            "argumentTypes": null,
                            "baseExpression": {
                              "argumentTypes": null,
                              "id": 2354,
                              "name": "lendingBalances",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 1989,
                              "src": "3785:15:9",
                              "typeDescriptions": {
                                "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                                "typeString": "mapping(address => uint256)"
                              }
                            },
                            "id": 2356,
                            "indexExpression": {
                              "argumentTypes": null,
                              "id": 2355,
                              "name": "customer",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 2328,
                              "src": "3801:8:9",
                              "typeDescriptions": {
                                "typeIdentifier": "t_address",
                                "typeString": "address"
                              }
                            },
                            "isConstant": false,
                            "isLValue": true,
                            "isPure": false,
                            "lValueRequested": true,
                            "nodeType": "IndexAccess",
                            "src": "3785:25:9",
                            "typeDescriptions": {
                              "typeIdentifier": "t_uint256",
                              "typeString": "uint256"
                            }
                          },
                          "nodeType": "Assignment",
                          "operator": "=",
                          "rightHandSide": {
                            "argumentTypes": null,
                            "commonType": {
                              "typeIdentifier": "t_uint256",
                              "typeString": "uint256"
                            },
                            "id": 2361,
                            "isConstant": false,
                            "isLValue": false,
                            "isPure": false,
                            "lValueRequested": false,
                            "leftExpression": {
                              "argumentTypes": null,
                              "baseExpression": {
                                "argumentTypes": null,
                                "id": 2357,
                                "name": "lendingBalances",
                                "nodeType": "Identifier",
                                "overloadedDeclarations": [],
                                "referencedDeclaration": 1989,
                                "src": "3813:15:9",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                                  "typeString": "mapping(address => uint256)"
                                }
                              },
                              "id": 2359,
                              "indexExpression": {
                                "argumentTypes": null,
                                "id": 2358,
                                "name": "customer",
                                "nodeType": "Identifier",
                                "overloadedDeclarations": [],
                                "referencedDeclaration": 2328,
                                "src": "3829:8:9",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_address",
                                  "typeString": "address"
                                }
                              },
                              "isConstant": false,
                              "isLValue": true,
                              "isPure": false,
                              "lValueRequested": false,
                              "nodeType": "IndexAccess",
                              "src": "3813:25:9",
                              "typeDescriptions": {
                                "typeIdentifier": "t_uint256",
                                "typeString": "uint256"
                              }
                            },
                            "nodeType": "BinaryOperation",
                            "operator": "+",
                            "rightExpression": {
                              "argumentTypes": null,
                              "id": 2360,
                              "name": "extraIncome",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 2340,
                              "src": "3841:11:9",
                              "typeDescriptions": {
                                "typeIdentifier": "t_uint256",
                                "typeString": "uint256"
                              }
                            },
                            "src": "3813:39:9",
                            "typeDescriptions": {
                              "typeIdentifier": "t_uint256",
                              "typeString": "uint256"
                            }
                          },
                          "src": "3785:67:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        },
                        "id": 2363,
                        "nodeType": "ExpressionStatement",
                        "src": "3785:67:9"
                      }
                    ]
                  }
                },
                {
                  "eventCall": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "id": 2379,
                        "name": "customer",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 2328,
                        "src": "3990:8:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "expression": {
                          "argumentTypes": null,
                          "id": 2380,
                          "name": "msg",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 11851,
                          "src": "4000:3:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_magic_message",
                            "typeString": "msg"
                          }
                        },
                        "id": 2381,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "memberName": "value",
                        "nodeType": "MemberAccess",
                        "referencedDeclaration": null,
                        "src": "4000:9:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        },
                        {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      ],
                      "id": 2378,
                      "name": "RegisteredLoanIncome",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 9166,
                      "src": "3969:20:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_event_nonpayable$_t_address_$_t_uint256_$returns$__$",
                        "typeString": "function (address,uint256)"
                      }
                    },
                    "id": 2382,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "3969:41:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 2383,
                  "nodeType": "EmitStatement",
                  "src": "3964:46:9"
                }
              ]
            },
            "documentation": null,
            "id": 2385,
            "implemented": true,
            "kind": "function",
            "modifiers": [
              {
                "arguments": null,
                "id": 2325,
                "modifierName": {
                  "argumentTypes": null,
                  "id": 2324,
                  "name": "onlyLendingManager",
                  "nodeType": "Identifier",
                  "overloadedDeclarations": [],
                  "referencedDeclaration": 2037,
                  "src": "3541:18:9",
                  "typeDescriptions": {
                    "typeIdentifier": "t_modifier$__$",
                    "typeString": "modifier ()"
                  }
                },
                "nodeType": "ModifierInvocation",
                "src": "3541:18:9"
              }
            ],
            "name": "registerLoanIncome",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 2323,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 2322,
                  "name": "id",
                  "nodeType": "VariableDeclaration",
                  "scope": 2385,
                  "src": "3517:7:9",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_uint256",
                    "typeString": "uint256"
                  },
                  "typeName": {
                    "id": 2321,
                    "name": "uint",
                    "nodeType": "ElementaryTypeName",
                    "src": "3517:4:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "3516:9:9"
            },
            "returnParameters": {
              "id": 2326,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "3560:0:9"
            },
            "scope": 2386,
            "src": "3489:527:9",
            "stateMutability": "payable",
            "superFunction": 9171,
            "visibility": "public"
          }
        ],
        "scope": 2387,
        "src": "222:3797:9"
      }
    ],
    "src": "0:4020:9"
  },
  "legacyAST": {
    "absolutePath": "/home/stefan/Work/cyberlife-js/bank-protocol/contracts/defi/lending/LendingTreasury.sol",
    "exportedSymbols": {
      "LendingTreasury": [
        2386
      ]
    },
    "id": 2387,
    "nodeType": "SourceUnit",
    "nodes": [
      {
        "id": 1974,
        "literals": [
          "solidity",
          "0.5",
          ".3"
        ],
        "nodeType": "PragmaDirective",
        "src": "0:22:9"
      },
      {
        "absolutePath": "contracts/zeppelin/ownership/Ownable.sol",
        "file": "contracts/zeppelin/ownership/Ownable.sol",
        "id": 1975,
        "nodeType": "ImportDirective",
        "scope": 2387,
        "sourceUnit": 11837,
        "src": "24:50:9",
        "symbolAliases": [],
        "unitAlias": ""
      },
      {
        "absolutePath": "contracts/zeppelin/SafeMath.sol",
        "file": "contracts/zeppelin/SafeMath.sol",
        "id": 1976,
        "nodeType": "ImportDirective",
        "scope": 2387,
        "sourceUnit": 11751,
        "src": "75:41:9",
        "symbolAliases": [],
        "unitAlias": ""
      },
      {
        "absolutePath": "contracts/interfaces/ILendingManager.sol",
        "file": "contracts/interfaces/ILendingManager.sol",
        "id": 1977,
        "nodeType": "ImportDirective",
        "scope": 2387,
        "sourceUnit": 9137,
        "src": "118:50:9",
        "symbolAliases": [],
        "unitAlias": ""
      },
      {
        "absolutePath": "contracts/interfaces/ILendingTreasury.sol",
        "file": "contracts/interfaces/ILendingTreasury.sol",
        "id": 1978,
        "nodeType": "ImportDirective",
        "scope": 2387,
        "sourceUnit": 9173,
        "src": "169:51:9",
        "symbolAliases": [],
        "unitAlias": ""
      },
      {
        "baseContracts": [
          {
            "arguments": null,
            "baseName": {
              "contractScope": null,
              "id": 1979,
              "name": "ILendingTreasury",
              "nodeType": "UserDefinedTypeName",
              "referencedDeclaration": 9172,
              "src": "250:16:9",
              "typeDescriptions": {
                "typeIdentifier": "t_contract$_ILendingTreasury_$9172",
                "typeString": "contract ILendingTreasury"
              }
            },
            "id": 1980,
            "nodeType": "InheritanceSpecifier",
            "src": "250:16:9"
          },
          {
            "arguments": null,
            "baseName": {
              "contractScope": null,
              "id": 1981,
              "name": "Ownable",
              "nodeType": "UserDefinedTypeName",
              "referencedDeclaration": 11836,
              "src": "268:7:9",
              "typeDescriptions": {
                "typeIdentifier": "t_contract$_Ownable_$11836",
                "typeString": "contract Ownable"
              }
            },
            "id": 1982,
            "nodeType": "InheritanceSpecifier",
            "src": "268:7:9"
          }
        ],
        "contractDependencies": [
          9172,
          11836
        ],
        "contractKind": "contract",
        "documentation": null,
        "fullyImplemented": true,
        "id": 2386,
        "linearizedBaseContracts": [
          2386,
          11836,
          9172
        ],
        "name": "LendingTreasury",
        "nodeType": "ContractDefinition",
        "nodes": [
          {
            "id": 1985,
            "libraryName": {
              "contractScope": null,
              "id": 1983,
              "name": "SafeMath",
              "nodeType": "UserDefinedTypeName",
              "referencedDeclaration": 11750,
              "src": "287:8:9",
              "typeDescriptions": {
                "typeIdentifier": "t_contract$_SafeMath_$11750",
                "typeString": "library SafeMath"
              }
            },
            "nodeType": "UsingForDirective",
            "src": "281:27:9",
            "typeName": {
              "id": 1984,
              "name": "uint256",
              "nodeType": "ElementaryTypeName",
              "src": "300:7:9",
              "typeDescriptions": {
                "typeIdentifier": "t_uint256",
                "typeString": "uint256"
              }
            }
          },
          {
            "constant": false,
            "id": 1989,
            "name": "lendingBalances",
            "nodeType": "VariableDeclaration",
            "scope": 2386,
            "src": "312:43:9",
            "stateVariable": true,
            "storageLocation": "default",
            "typeDescriptions": {
              "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
              "typeString": "mapping(address => uint256)"
            },
            "typeName": {
              "id": 1988,
              "keyType": {
                "id": 1986,
                "name": "address",
                "nodeType": "ElementaryTypeName",
                "src": "320:7:9",
                "typeDescriptions": {
                  "typeIdentifier": "t_address",
                  "typeString": "address"
                }
              },
              "nodeType": "Mapping",
              "src": "312:27:9",
              "typeDescriptions": {
                "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                "typeString": "mapping(address => uint256)"
              },
              "valueType": {
                "id": 1987,
                "name": "uint256",
                "nodeType": "ElementaryTypeName",
                "src": "331:7:9",
                "typeDescriptions": {
                  "typeIdentifier": "t_uint256",
                  "typeString": "uint256"
                }
              }
            },
            "value": null,
            "visibility": "internal"
          },
          {
            "constant": false,
            "id": 1993,
            "name": "validRelayers",
            "nodeType": "VariableDeclaration",
            "scope": 2386,
            "src": "360:38:9",
            "stateVariable": true,
            "storageLocation": "default",
            "typeDescriptions": {
              "typeIdentifier": "t_mapping$_t_address_$_t_bool_$",
              "typeString": "mapping(address => bool)"
            },
            "typeName": {
              "id": 1992,
              "keyType": {
                "id": 1990,
                "name": "address",
                "nodeType": "ElementaryTypeName",
                "src": "368:7:9",
                "typeDescriptions": {
                  "typeIdentifier": "t_address",
                  "typeString": "address"
                }
              },
              "nodeType": "Mapping",
              "src": "360:24:9",
              "typeDescriptions": {
                "typeIdentifier": "t_mapping$_t_address_$_t_bool_$",
                "typeString": "mapping(address => bool)"
              },
              "valueType": {
                "id": 1991,
                "name": "bool",
                "nodeType": "ElementaryTypeName",
                "src": "379:4:9",
                "typeDescriptions": {
                  "typeIdentifier": "t_bool",
                  "typeString": "bool"
                }
              }
            },
            "value": null,
            "visibility": "internal"
          },
          {
            "constant": false,
            "id": 1999,
            "name": "customerLoanTie",
            "nodeType": "VariableDeclaration",
            "scope": 2386,
            "src": "403:57:9",
            "stateVariable": true,
            "storageLocation": "default",
            "typeDescriptions": {
              "typeIdentifier": "t_mapping$_t_address_$_t_mapping$_t_uint256_$_t_bool_$_$",
              "typeString": "mapping(address => mapping(uint256 => bool))"
            },
            "typeName": {
              "id": 1998,
              "keyType": {
                "id": 1994,
                "name": "address",
                "nodeType": "ElementaryTypeName",
                "src": "411:7:9",
                "typeDescriptions": {
                  "typeIdentifier": "t_address",
                  "typeString": "address"
                }
              },
              "nodeType": "Mapping",
              "src": "403:41:9",
              "typeDescriptions": {
                "typeIdentifier": "t_mapping$_t_address_$_t_mapping$_t_uint256_$_t_bool_$_$",
                "typeString": "mapping(address => mapping(uint256 => bool))"
              },
              "valueType": {
                "id": 1997,
                "keyType": {
                  "id": 1995,
                  "name": "uint",
                  "nodeType": "ElementaryTypeName",
                  "src": "430:4:9",
                  "typeDescriptions": {
                    "typeIdentifier": "t_uint256",
                    "typeString": "uint256"
                  }
                },
                "nodeType": "Mapping",
                "src": "422:21:9",
                "typeDescriptions": {
                  "typeIdentifier": "t_mapping$_t_uint256_$_t_bool_$",
                  "typeString": "mapping(uint256 => bool)"
                },
                "valueType": {
                  "id": 1996,
                  "name": "bool",
                  "nodeType": "ElementaryTypeName",
                  "src": "438:4:9",
                  "typeDescriptions": {
                    "typeIdentifier": "t_bool",
                    "typeString": "bool"
                  }
                }
              }
            },
            "value": null,
            "visibility": "internal"
          },
          {
            "constant": false,
            "id": 2003,
            "name": "loanCustomerTie",
            "nodeType": "VariableDeclaration",
            "scope": 2386,
            "src": "465:40:9",
            "stateVariable": true,
            "storageLocation": "default",
            "typeDescriptions": {
              "typeIdentifier": "t_mapping$_t_uint256_$_t_address_$",
              "typeString": "mapping(uint256 => address)"
            },
            "typeName": {
              "id": 2002,
              "keyType": {
                "id": 2000,
                "name": "uint",
                "nodeType": "ElementaryTypeName",
                "src": "473:4:9",
                "typeDescriptions": {
                  "typeIdentifier": "t_uint256",
                  "typeString": "uint256"
                }
              },
              "nodeType": "Mapping",
              "src": "465:24:9",
              "typeDescriptions": {
                "typeIdentifier": "t_mapping$_t_uint256_$_t_address_$",
                "typeString": "mapping(uint256 => address)"
              },
              "valueType": {
                "id": 2001,
                "name": "address",
                "nodeType": "ElementaryTypeName",
                "src": "481:7:9",
                "stateMutability": "nonpayable",
                "typeDescriptions": {
                  "typeIdentifier": "t_address",
                  "typeString": "address"
                }
              }
            },
            "value": null,
            "visibility": "internal"
          },
          {
            "constant": false,
            "id": 2007,
            "name": "totalLentPerCustomer",
            "nodeType": "VariableDeclaration",
            "scope": 2386,
            "src": "510:48:9",
            "stateVariable": true,
            "storageLocation": "default",
            "typeDescriptions": {
              "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
              "typeString": "mapping(address => uint256)"
            },
            "typeName": {
              "id": 2006,
              "keyType": {
                "id": 2004,
                "name": "address",
                "nodeType": "ElementaryTypeName",
                "src": "518:7:9",
                "typeDescriptions": {
                  "typeIdentifier": "t_address",
                  "typeString": "address"
                }
              },
              "nodeType": "Mapping",
              "src": "510:27:9",
              "typeDescriptions": {
                "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                "typeString": "mapping(address => uint256)"
              },
              "valueType": {
                "id": 2005,
                "name": "uint256",
                "nodeType": "ElementaryTypeName",
                "src": "529:7:9",
                "typeDescriptions": {
                  "typeIdentifier": "t_uint256",
                  "typeString": "uint256"
                }
              }
            },
            "value": null,
            "visibility": "internal"
          },
          {
            "constant": false,
            "id": 2009,
            "name": "manager",
            "nodeType": "VariableDeclaration",
            "scope": 2386,
            "src": "563:23:9",
            "stateVariable": true,
            "storageLocation": "default",
            "typeDescriptions": {
              "typeIdentifier": "t_contract$_ILendingManager_$9136",
              "typeString": "contract ILendingManager"
            },
            "typeName": {
              "contractScope": null,
              "id": 2008,
              "name": "ILendingManager",
              "nodeType": "UserDefinedTypeName",
              "referencedDeclaration": 9136,
              "src": "563:15:9",
              "typeDescriptions": {
                "typeIdentifier": "t_contract$_ILendingManager_$9136",
                "typeString": "contract ILendingManager"
              }
            },
            "value": null,
            "visibility": "internal"
          },
          {
            "body": {
              "id": 2022,
              "nodeType": "Block",
              "src": "619:101:9",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "commonType": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        },
                        "id": 2017,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "leftExpression": {
                          "argumentTypes": null,
                          "baseExpression": {
                            "argumentTypes": null,
                            "id": 2012,
                            "name": "validRelayers",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 1993,
                            "src": "634:13:9",
                            "typeDescriptions": {
                              "typeIdentifier": "t_mapping$_t_address_$_t_bool_$",
                              "typeString": "mapping(address => bool)"
                            }
                          },
                          "id": 2015,
                          "indexExpression": {
                            "argumentTypes": null,
                            "expression": {
                              "argumentTypes": null,
                              "id": 2013,
                              "name": "msg",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 11851,
                              "src": "648:3:9",
                              "typeDescriptions": {
                                "typeIdentifier": "t_magic_message",
                                "typeString": "msg"
                              }
                            },
                            "id": 2014,
                            "isConstant": false,
                            "isLValue": false,
                            "isPure": false,
                            "lValueRequested": false,
                            "memberName": "sender",
                            "nodeType": "MemberAccess",
                            "referencedDeclaration": null,
                            "src": "648:10:9",
                            "typeDescriptions": {
                              "typeIdentifier": "t_address_payable",
                              "typeString": "address payable"
                            }
                          },
                          "isConstant": false,
                          "isLValue": true,
                          "isPure": false,
                          "lValueRequested": false,
                          "nodeType": "IndexAccess",
                          "src": "634:25:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_bool",
                            "typeString": "bool"
                          }
                        },
                        "nodeType": "BinaryOperation",
                        "operator": "==",
                        "rightExpression": {
                          "argumentTypes": null,
                          "hexValue": "74727565",
                          "id": 2016,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": true,
                          "kind": "bool",
                          "lValueRequested": false,
                          "nodeType": "Literal",
                          "src": "663:4:9",
                          "subdenomination": null,
                          "typeDescriptions": {
                            "typeIdentifier": "t_bool",
                            "typeString": "bool"
                          },
                          "value": "true"
                        },
                        "src": "634:33:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "hexValue": "546869732073656e646572206973206e6f7420612076616c69642072656c61796572",
                        "id": 2018,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": true,
                        "kind": "string",
                        "lValueRequested": false,
                        "nodeType": "Literal",
                        "src": "669:36:9",
                        "subdenomination": null,
                        "typeDescriptions": {
                          "typeIdentifier": "t_stringliteral_011a7c50fa3a342edbf028d9772540c2f71073c1afb9f56c7a2cf31ca4b02e12",
                          "typeString": "literal_string \"This sender is not a valid relayer\""
                        },
                        "value": "This sender is not a valid relayer"
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        },
                        {
                          "typeIdentifier": "t_stringliteral_011a7c50fa3a342edbf028d9772540c2f71073c1afb9f56c7a2cf31ca4b02e12",
                          "typeString": "literal_string \"This sender is not a valid relayer\""
                        }
                      ],
                      "id": 2011,
                      "name": "require",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [
                        11854,
                        11855
                      ],
                      "referencedDeclaration": 11855,
                      "src": "626:7:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_require_pure$_t_bool_$_t_string_memory_ptr_$returns$__$",
                        "typeString": "function (bool,string memory) pure"
                      }
                    },
                    "id": 2019,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "626:80:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 2020,
                  "nodeType": "ExpressionStatement",
                  "src": "626:80:9"
                },
                {
                  "id": 2021,
                  "nodeType": "PlaceholderStatement",
                  "src": "713:1:9"
                }
              ]
            },
            "documentation": null,
            "id": 2023,
            "name": "onlyValidRelayer",
            "nodeType": "ModifierDefinition",
            "parameters": {
              "id": 2010,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "616:2:9"
            },
            "src": "591:129:9",
            "visibility": "internal"
          },
          {
            "body": {
              "id": 2036,
              "nodeType": "Block",
              "src": "754:101:9",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "commonType": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        },
                        "id": 2031,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "leftExpression": {
                          "argumentTypes": null,
                          "expression": {
                            "argumentTypes": null,
                            "id": 2026,
                            "name": "msg",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 11851,
                            "src": "769:3:9",
                            "typeDescriptions": {
                              "typeIdentifier": "t_magic_message",
                              "typeString": "msg"
                            }
                          },
                          "id": 2027,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "memberName": "sender",
                          "nodeType": "MemberAccess",
                          "referencedDeclaration": null,
                          "src": "769:10:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address_payable",
                            "typeString": "address payable"
                          }
                        },
                        "nodeType": "BinaryOperation",
                        "operator": "==",
                        "rightExpression": {
                          "argumentTypes": null,
                          "arguments": [
                            {
                              "argumentTypes": null,
                              "id": 2029,
                              "name": "manager",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 2009,
                              "src": "791:7:9",
                              "typeDescriptions": {
                                "typeIdentifier": "t_contract$_ILendingManager_$9136",
                                "typeString": "contract ILendingManager"
                              }
                            }
                          ],
                          "expression": {
                            "argumentTypes": [
                              {
                                "typeIdentifier": "t_contract$_ILendingManager_$9136",
                                "typeString": "contract ILendingManager"
                              }
                            ],
                            "id": 2028,
                            "isConstant": false,
                            "isLValue": false,
                            "isPure": true,
                            "lValueRequested": false,
                            "nodeType": "ElementaryTypeNameExpression",
                            "src": "783:7:9",
                            "typeDescriptions": {
                              "typeIdentifier": "t_type$_t_address_$",
                              "typeString": "type(address)"
                            },
                            "typeName": "address"
                          },
                          "id": 2030,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "kind": "typeConversion",
                          "lValueRequested": false,
                          "names": [],
                          "nodeType": "FunctionCall",
                          "src": "783:16:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "src": "769:30:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "hexValue": "5468652063616c6c6572206973206e6f7420746865206c656e64696e67206d616e61676572",
                        "id": 2032,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": true,
                        "kind": "string",
                        "lValueRequested": false,
                        "nodeType": "Literal",
                        "src": "801:39:9",
                        "subdenomination": null,
                        "typeDescriptions": {
                          "typeIdentifier": "t_stringliteral_27e3839b182de27b41afb4105c65813b427eb2bfea02e249df35c3534ae43682",
                          "typeString": "literal_string \"The caller is not the lending manager\""
                        },
                        "value": "The caller is not the lending manager"
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        },
                        {
                          "typeIdentifier": "t_stringliteral_27e3839b182de27b41afb4105c65813b427eb2bfea02e249df35c3534ae43682",
                          "typeString": "literal_string \"The caller is not the lending manager\""
                        }
                      ],
                      "id": 2025,
                      "name": "require",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [
                        11854,
                        11855
                      ],
                      "referencedDeclaration": 11855,
                      "src": "761:7:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_require_pure$_t_bool_$_t_string_memory_ptr_$returns$__$",
                        "typeString": "function (bool,string memory) pure"
                      }
                    },
                    "id": 2033,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "761:80:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 2034,
                  "nodeType": "ExpressionStatement",
                  "src": "761:80:9"
                },
                {
                  "id": 2035,
                  "nodeType": "PlaceholderStatement",
                  "src": "848:1:9"
                }
              ]
            },
            "documentation": null,
            "id": 2037,
            "name": "onlyLendingManager",
            "nodeType": "ModifierDefinition",
            "parameters": {
              "id": 2024,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "751:2:9"
            },
            "src": "724:131:9",
            "visibility": "internal"
          },
          {
            "body": {
              "id": 2040,
              "nodeType": "Block",
              "src": "880:2:9",
              "statements": []
            },
            "documentation": null,
            "id": 2041,
            "implemented": true,
            "kind": "constructor",
            "modifiers": [],
            "name": "",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 2038,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "870:2:9"
            },
            "returnParameters": {
              "id": 2039,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "880:0:9"
            },
            "scope": 2386,
            "src": "859:23:9",
            "stateMutability": "nonpayable",
            "superFunction": null,
            "visibility": "public"
          },
          {
            "body": {
              "id": 2073,
              "nodeType": "Block",
              "src": "914:197:9",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "commonType": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        },
                        "id": 2048,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "leftExpression": {
                          "argumentTypes": null,
                          "expression": {
                            "argumentTypes": null,
                            "id": 2045,
                            "name": "msg",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 11851,
                            "src": "929:3:9",
                            "typeDescriptions": {
                              "typeIdentifier": "t_magic_message",
                              "typeString": "msg"
                            }
                          },
                          "id": 2046,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "memberName": "value",
                          "nodeType": "MemberAccess",
                          "referencedDeclaration": null,
                          "src": "929:9:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        },
                        "nodeType": "BinaryOperation",
                        "operator": ">",
                        "rightExpression": {
                          "argumentTypes": null,
                          "hexValue": "30",
                          "id": 2047,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": true,
                          "kind": "number",
                          "lValueRequested": false,
                          "nodeType": "Literal",
                          "src": "941:1:9",
                          "subdenomination": null,
                          "typeDescriptions": {
                            "typeIdentifier": "t_rational_0_by_1",
                            "typeString": "int_const 0"
                          },
                          "value": "0"
                        },
                        "src": "929:13:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "hexValue": "596f75206e65656420746f2073656e64206d6f7265207468616e203020776569",
                        "id": 2049,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": true,
                        "kind": "string",
                        "lValueRequested": false,
                        "nodeType": "Literal",
                        "src": "944:34:9",
                        "subdenomination": null,
                        "typeDescriptions": {
                          "typeIdentifier": "t_stringliteral_3cbcdb32609fc2b20478afb11c0146a76c2025d17208fd6973e7f3f08d317dfb",
                          "typeString": "literal_string \"You need to send more than 0 wei\""
                        },
                        "value": "You need to send more than 0 wei"
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        },
                        {
                          "typeIdentifier": "t_stringliteral_3cbcdb32609fc2b20478afb11c0146a76c2025d17208fd6973e7f3f08d317dfb",
                          "typeString": "literal_string \"You need to send more than 0 wei\""
                        }
                      ],
                      "id": 2044,
                      "name": "require",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [
                        11854,
                        11855
                      ],
                      "referencedDeclaration": 11855,
                      "src": "921:7:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_require_pure$_t_bool_$_t_string_memory_ptr_$returns$__$",
                        "typeString": "function (bool,string memory) pure"
                      }
                    },
                    "id": 2050,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "921:58:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 2051,
                  "nodeType": "ExpressionStatement",
                  "src": "921:58:9"
                },
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 2064,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "baseExpression": {
                        "argumentTypes": null,
                        "id": 2052,
                        "name": "lendingBalances",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1989,
                        "src": "986:15:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                          "typeString": "mapping(address => uint256)"
                        }
                      },
                      "id": 2055,
                      "indexExpression": {
                        "argumentTypes": null,
                        "expression": {
                          "argumentTypes": null,
                          "id": 2053,
                          "name": "msg",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 11851,
                          "src": "1002:3:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_magic_message",
                            "typeString": "msg"
                          }
                        },
                        "id": 2054,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "memberName": "sender",
                        "nodeType": "MemberAccess",
                        "referencedDeclaration": null,
                        "src": "1002:10:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address_payable",
                          "typeString": "address payable"
                        }
                      },
                      "isConstant": false,
                      "isLValue": true,
                      "isPure": false,
                      "lValueRequested": true,
                      "nodeType": "IndexAccess",
                      "src": "986:27:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "arguments": [
                        {
                          "argumentTypes": null,
                          "expression": {
                            "argumentTypes": null,
                            "id": 2061,
                            "name": "msg",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 11851,
                            "src": "1048:3:9",
                            "typeDescriptions": {
                              "typeIdentifier": "t_magic_message",
                              "typeString": "msg"
                            }
                          },
                          "id": 2062,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "memberName": "value",
                          "nodeType": "MemberAccess",
                          "referencedDeclaration": null,
                          "src": "1048:9:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        }
                      ],
                      "expression": {
                        "argumentTypes": [
                          {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        ],
                        "expression": {
                          "argumentTypes": null,
                          "baseExpression": {
                            "argumentTypes": null,
                            "id": 2056,
                            "name": "lendingBalances",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 1989,
                            "src": "1016:15:9",
                            "typeDescriptions": {
                              "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                              "typeString": "mapping(address => uint256)"
                            }
                          },
                          "id": 2059,
                          "indexExpression": {
                            "argumentTypes": null,
                            "expression": {
                              "argumentTypes": null,
                              "id": 2057,
                              "name": "msg",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 11851,
                              "src": "1032:3:9",
                              "typeDescriptions": {
                                "typeIdentifier": "t_magic_message",
                                "typeString": "msg"
                              }
                            },
                            "id": 2058,
                            "isConstant": false,
                            "isLValue": false,
                            "isPure": false,
                            "lValueRequested": false,
                            "memberName": "sender",
                            "nodeType": "MemberAccess",
                            "referencedDeclaration": null,
                            "src": "1032:10:9",
                            "typeDescriptions": {
                              "typeIdentifier": "t_address_payable",
                              "typeString": "address payable"
                            }
                          },
                          "isConstant": false,
                          "isLValue": true,
                          "isPure": false,
                          "lValueRequested": false,
                          "nodeType": "IndexAccess",
                          "src": "1016:27:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        },
                        "id": 2060,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "memberName": "add",
                        "nodeType": "MemberAccess",
                        "referencedDeclaration": 11749,
                        "src": "1016:31:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_function_internal_pure$_t_uint256_$_t_uint256_$returns$_t_uint256_$bound_to$_t_uint256_$",
                          "typeString": "function (uint256,uint256) pure returns (uint256)"
                        }
                      },
                      "id": 2063,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "kind": "functionCall",
                      "lValueRequested": false,
                      "names": [],
                      "nodeType": "FunctionCall",
                      "src": "1016:42:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "src": "986:72:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "id": 2065,
                  "nodeType": "ExpressionStatement",
                  "src": "986:72:9"
                },
                {
                  "eventCall": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "expression": {
                          "argumentTypes": null,
                          "id": 2067,
                          "name": "msg",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 11851,
                          "src": "1083:3:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_magic_message",
                            "typeString": "msg"
                          }
                        },
                        "id": 2068,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "memberName": "sender",
                        "nodeType": "MemberAccess",
                        "referencedDeclaration": null,
                        "src": "1083:10:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address_payable",
                          "typeString": "address payable"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "expression": {
                          "argumentTypes": null,
                          "id": 2069,
                          "name": "msg",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 11851,
                          "src": "1095:3:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_magic_message",
                            "typeString": "msg"
                          }
                        },
                        "id": 2070,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "memberName": "value",
                        "nodeType": "MemberAccess",
                        "referencedDeclaration": null,
                        "src": "1095:9:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_address_payable",
                          "typeString": "address payable"
                        },
                        {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      ],
                      "id": 2066,
                      "name": "DepositedETH",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 9154,
                      "src": "1070:12:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_event_nonpayable$_t_address_$_t_uint256_$returns$__$",
                        "typeString": "function (address,uint256)"
                      }
                    },
                    "id": 2071,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "1070:35:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 2072,
                  "nodeType": "EmitStatement",
                  "src": "1065:40:9"
                }
              ]
            },
            "documentation": null,
            "id": 2074,
            "implemented": true,
            "kind": "fallback",
            "modifiers": [],
            "name": "",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 2042,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "894:2:9"
            },
            "returnParameters": {
              "id": 2043,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "914:0:9"
            },
            "scope": 2386,
            "src": "886:225:9",
            "stateMutability": "payable",
            "superFunction": null,
            "visibility": "external"
          },
          {
            "body": {
              "id": 2097,
              "nodeType": "Block",
              "src": "1183:120:9",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 2088,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "baseExpression": {
                        "argumentTypes": null,
                        "id": 2081,
                        "name": "validRelayers",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1993,
                        "src": "1190:13:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_mapping$_t_address_$_t_bool_$",
                          "typeString": "mapping(address => bool)"
                        }
                      },
                      "id": 2083,
                      "indexExpression": {
                        "argumentTypes": null,
                        "id": 2082,
                        "name": "relayer",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 2076,
                        "src": "1204:7:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      "isConstant": false,
                      "isLValue": true,
                      "isPure": false,
                      "lValueRequested": true,
                      "nodeType": "IndexAccess",
                      "src": "1190:22:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_bool",
                        "typeString": "bool"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "id": 2087,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "lValueRequested": false,
                      "nodeType": "UnaryOperation",
                      "operator": "!",
                      "prefix": true,
                      "src": "1215:23:9",
                      "subExpression": {
                        "argumentTypes": null,
                        "baseExpression": {
                          "argumentTypes": null,
                          "id": 2084,
                          "name": "validRelayers",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 1993,
                          "src": "1216:13:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_mapping$_t_address_$_t_bool_$",
                            "typeString": "mapping(address => bool)"
                          }
                        },
                        "id": 2086,
                        "indexExpression": {
                          "argumentTypes": null,
                          "id": 2085,
                          "name": "relayer",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 2076,
                          "src": "1230:7:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "isConstant": false,
                        "isLValue": true,
                        "isPure": false,
                        "lValueRequested": false,
                        "nodeType": "IndexAccess",
                        "src": "1216:22:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      },
                      "typeDescriptions": {
                        "typeIdentifier": "t_bool",
                        "typeString": "bool"
                      }
                    },
                    "src": "1190:48:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_bool",
                      "typeString": "bool"
                    }
                  },
                  "id": 2089,
                  "nodeType": "ExpressionStatement",
                  "src": "1190:48:9"
                },
                {
                  "eventCall": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "id": 2091,
                        "name": "relayer",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 2076,
                        "src": "1265:7:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "baseExpression": {
                          "argumentTypes": null,
                          "id": 2092,
                          "name": "validRelayers",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 1993,
                          "src": "1274:13:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_mapping$_t_address_$_t_bool_$",
                            "typeString": "mapping(address => bool)"
                          }
                        },
                        "id": 2094,
                        "indexExpression": {
                          "argumentTypes": null,
                          "id": 2093,
                          "name": "relayer",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 2076,
                          "src": "1288:7:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "isConstant": false,
                        "isLValue": true,
                        "isPure": false,
                        "lValueRequested": false,
                        "nodeType": "IndexAccess",
                        "src": "1274:22:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        },
                        {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      ],
                      "id": 2090,
                      "name": "ToggledRelayer",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 9144,
                      "src": "1250:14:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_event_nonpayable$_t_address_$_t_bool_$returns$__$",
                        "typeString": "function (address,bool)"
                      }
                    },
                    "id": 2095,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "1250:47:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 2096,
                  "nodeType": "EmitStatement",
                  "src": "1245:52:9"
                }
              ]
            },
            "documentation": null,
            "id": 2098,
            "implemented": true,
            "kind": "function",
            "modifiers": [
              {
                "arguments": null,
                "id": 2079,
                "modifierName": {
                  "argumentTypes": null,
                  "id": 2078,
                  "name": "onlyOwner",
                  "nodeType": "Identifier",
                  "overloadedDeclarations": [],
                  "referencedDeclaration": 11784,
                  "src": "1173:9:9",
                  "typeDescriptions": {
                    "typeIdentifier": "t_modifier$__$",
                    "typeString": "modifier ()"
                  }
                },
                "nodeType": "ModifierInvocation",
                "src": "1173:9:9"
              }
            ],
            "name": "toggleRelayer",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 2077,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 2076,
                  "name": "relayer",
                  "nodeType": "VariableDeclaration",
                  "scope": 2098,
                  "src": "1149:15:9",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 2075,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "1149:7:9",
                    "stateMutability": "nonpayable",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "1148:17:9"
            },
            "returnParameters": {
              "id": 2080,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "1183:0:9"
            },
            "scope": 2386,
            "src": "1126:177:9",
            "stateMutability": "nonpayable",
            "superFunction": null,
            "visibility": "public"
          },
          {
            "body": {
              "id": 2115,
              "nodeType": "Block",
              "src": "1366:84:9",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 2109,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "id": 2105,
                      "name": "manager",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 2009,
                      "src": "1373:7:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_contract$_ILendingManager_$9136",
                        "typeString": "contract ILendingManager"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "arguments": [
                        {
                          "argumentTypes": null,
                          "id": 2107,
                          "name": "_manager",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 2100,
                          "src": "1399:8:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        }
                      ],
                      "expression": {
                        "argumentTypes": [
                          {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        ],
                        "id": 2106,
                        "name": "ILendingManager",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 9136,
                        "src": "1383:15:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_type$_t_contract$_ILendingManager_$9136_$",
                          "typeString": "type(contract ILendingManager)"
                        }
                      },
                      "id": 2108,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "kind": "typeConversion",
                      "lValueRequested": false,
                      "names": [],
                      "nodeType": "FunctionCall",
                      "src": "1383:25:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_contract$_ILendingManager_$9136",
                        "typeString": "contract ILendingManager"
                      }
                    },
                    "src": "1373:35:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_contract$_ILendingManager_$9136",
                      "typeString": "contract ILendingManager"
                    }
                  },
                  "id": 2110,
                  "nodeType": "ExpressionStatement",
                  "src": "1373:35:9"
                },
                {
                  "eventCall": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "id": 2112,
                        "name": "_manager",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 2100,
                        "src": "1435:8:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      ],
                      "id": 2111,
                      "name": "SetLoanManager",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 9148,
                      "src": "1420:14:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_event_nonpayable$_t_address_$returns$__$",
                        "typeString": "function (address)"
                      }
                    },
                    "id": 2113,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "1420:24:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 2114,
                  "nodeType": "EmitStatement",
                  "src": "1415:29:9"
                }
              ]
            },
            "documentation": null,
            "id": 2116,
            "implemented": true,
            "kind": "function",
            "modifiers": [
              {
                "arguments": null,
                "id": 2103,
                "modifierName": {
                  "argumentTypes": null,
                  "id": 2102,
                  "name": "onlyOwner",
                  "nodeType": "Identifier",
                  "overloadedDeclarations": [],
                  "referencedDeclaration": 11784,
                  "src": "1356:9:9",
                  "typeDescriptions": {
                    "typeIdentifier": "t_modifier$__$",
                    "typeString": "modifier ()"
                  }
                },
                "nodeType": "ModifierInvocation",
                "src": "1356:9:9"
              }
            ],
            "name": "setLoanManager",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 2101,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 2100,
                  "name": "_manager",
                  "nodeType": "VariableDeclaration",
                  "scope": 2116,
                  "src": "1331:16:9",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 2099,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "1331:7:9",
                    "stateMutability": "nonpayable",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "1330:18:9"
            },
            "returnParameters": {
              "id": 2104,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "1366:0:9"
            },
            "scope": 2386,
            "src": "1307:143:9",
            "stateMutability": "nonpayable",
            "superFunction": null,
            "visibility": "public"
          },
          {
            "body": {
              "id": 2148,
              "nodeType": "Block",
              "src": "1498:208:9",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "commonType": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        },
                        "id": 2123,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "leftExpression": {
                          "argumentTypes": null,
                          "expression": {
                            "argumentTypes": null,
                            "id": 2120,
                            "name": "msg",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 11851,
                            "src": "1513:3:9",
                            "typeDescriptions": {
                              "typeIdentifier": "t_magic_message",
                              "typeString": "msg"
                            }
                          },
                          "id": 2121,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "memberName": "value",
                          "nodeType": "MemberAccess",
                          "referencedDeclaration": null,
                          "src": "1513:9:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        },
                        "nodeType": "BinaryOperation",
                        "operator": ">",
                        "rightExpression": {
                          "argumentTypes": null,
                          "hexValue": "30",
                          "id": 2122,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": true,
                          "kind": "number",
                          "lValueRequested": false,
                          "nodeType": "Literal",
                          "src": "1525:1:9",
                          "subdenomination": null,
                          "typeDescriptions": {
                            "typeIdentifier": "t_rational_0_by_1",
                            "typeString": "int_const 0"
                          },
                          "value": "0"
                        },
                        "src": "1513:13:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "hexValue": "596f75206e65656420746f2073656e64206120706f73697469766520616d6f756e74206f66206d6f6e6579",
                        "id": 2124,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": true,
                        "kind": "string",
                        "lValueRequested": false,
                        "nodeType": "Literal",
                        "src": "1528:45:9",
                        "subdenomination": null,
                        "typeDescriptions": {
                          "typeIdentifier": "t_stringliteral_d70d2a8c12d989eb36b3b63feb7ad989e7585e5e5f58a19a6d5ef0dd415c9dc4",
                          "typeString": "literal_string \"You need to send a positive amount of money\""
                        },
                        "value": "You need to send a positive amount of money"
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        },
                        {
                          "typeIdentifier": "t_stringliteral_d70d2a8c12d989eb36b3b63feb7ad989e7585e5e5f58a19a6d5ef0dd415c9dc4",
                          "typeString": "literal_string \"You need to send a positive amount of money\""
                        }
                      ],
                      "id": 2119,
                      "name": "require",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [
                        11854,
                        11855
                      ],
                      "referencedDeclaration": 11855,
                      "src": "1505:7:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_require_pure$_t_bool_$_t_string_memory_ptr_$returns$__$",
                        "typeString": "function (bool,string memory) pure"
                      }
                    },
                    "id": 2125,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "1505:69:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 2126,
                  "nodeType": "ExpressionStatement",
                  "src": "1505:69:9"
                },
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 2139,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "baseExpression": {
                        "argumentTypes": null,
                        "id": 2127,
                        "name": "lendingBalances",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1989,
                        "src": "1581:15:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                          "typeString": "mapping(address => uint256)"
                        }
                      },
                      "id": 2130,
                      "indexExpression": {
                        "argumentTypes": null,
                        "expression": {
                          "argumentTypes": null,
                          "id": 2128,
                          "name": "msg",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 11851,
                          "src": "1597:3:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_magic_message",
                            "typeString": "msg"
                          }
                        },
                        "id": 2129,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "memberName": "sender",
                        "nodeType": "MemberAccess",
                        "referencedDeclaration": null,
                        "src": "1597:10:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address_payable",
                          "typeString": "address payable"
                        }
                      },
                      "isConstant": false,
                      "isLValue": true,
                      "isPure": false,
                      "lValueRequested": true,
                      "nodeType": "IndexAccess",
                      "src": "1581:27:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "arguments": [
                        {
                          "argumentTypes": null,
                          "expression": {
                            "argumentTypes": null,
                            "id": 2136,
                            "name": "msg",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 11851,
                            "src": "1643:3:9",
                            "typeDescriptions": {
                              "typeIdentifier": "t_magic_message",
                              "typeString": "msg"
                            }
                          },
                          "id": 2137,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "memberName": "value",
                          "nodeType": "MemberAccess",
                          "referencedDeclaration": null,
                          "src": "1643:9:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        }
                      ],
                      "expression": {
                        "argumentTypes": [
                          {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        ],
                        "expression": {
                          "argumentTypes": null,
                          "baseExpression": {
                            "argumentTypes": null,
                            "id": 2131,
                            "name": "lendingBalances",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 1989,
                            "src": "1611:15:9",
                            "typeDescriptions": {
                              "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                              "typeString": "mapping(address => uint256)"
                            }
                          },
                          "id": 2134,
                          "indexExpression": {
                            "argumentTypes": null,
                            "expression": {
                              "argumentTypes": null,
                              "id": 2132,
                              "name": "msg",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 11851,
                              "src": "1627:3:9",
                              "typeDescriptions": {
                                "typeIdentifier": "t_magic_message",
                                "typeString": "msg"
                              }
                            },
                            "id": 2133,
                            "isConstant": false,
                            "isLValue": false,
                            "isPure": false,
                            "lValueRequested": false,
                            "memberName": "sender",
                            "nodeType": "MemberAccess",
                            "referencedDeclaration": null,
                            "src": "1627:10:9",
                            "typeDescriptions": {
                              "typeIdentifier": "t_address_payable",
                              "typeString": "address payable"
                            }
                          },
                          "isConstant": false,
                          "isLValue": true,
                          "isPure": false,
                          "lValueRequested": false,
                          "nodeType": "IndexAccess",
                          "src": "1611:27:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        },
                        "id": 2135,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "memberName": "add",
                        "nodeType": "MemberAccess",
                        "referencedDeclaration": 11749,
                        "src": "1611:31:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_function_internal_pure$_t_uint256_$_t_uint256_$returns$_t_uint256_$bound_to$_t_uint256_$",
                          "typeString": "function (uint256,uint256) pure returns (uint256)"
                        }
                      },
                      "id": 2138,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "kind": "functionCall",
                      "lValueRequested": false,
                      "names": [],
                      "nodeType": "FunctionCall",
                      "src": "1611:42:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "src": "1581:72:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "id": 2140,
                  "nodeType": "ExpressionStatement",
                  "src": "1581:72:9"
                },
                {
                  "eventCall": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "expression": {
                          "argumentTypes": null,
                          "id": 2142,
                          "name": "msg",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 11851,
                          "src": "1678:3:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_magic_message",
                            "typeString": "msg"
                          }
                        },
                        "id": 2143,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "memberName": "sender",
                        "nodeType": "MemberAccess",
                        "referencedDeclaration": null,
                        "src": "1678:10:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address_payable",
                          "typeString": "address payable"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "expression": {
                          "argumentTypes": null,
                          "id": 2144,
                          "name": "msg",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 11851,
                          "src": "1690:3:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_magic_message",
                            "typeString": "msg"
                          }
                        },
                        "id": 2145,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "memberName": "value",
                        "nodeType": "MemberAccess",
                        "referencedDeclaration": null,
                        "src": "1690:9:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_address_payable",
                          "typeString": "address payable"
                        },
                        {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      ],
                      "id": 2141,
                      "name": "DepositedETH",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 9154,
                      "src": "1665:12:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_event_nonpayable$_t_address_$_t_uint256_$returns$__$",
                        "typeString": "function (address,uint256)"
                      }
                    },
                    "id": 2146,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "1665:35:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 2147,
                  "nodeType": "EmitStatement",
                  "src": "1660:40:9"
                }
              ]
            },
            "documentation": null,
            "id": 2149,
            "implemented": true,
            "kind": "function",
            "modifiers": [],
            "name": "depositLendingETH",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 2117,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "1480:2:9"
            },
            "returnParameters": {
              "id": 2118,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "1498:0:9"
            },
            "scope": 2386,
            "src": "1454:252:9",
            "stateMutability": "payable",
            "superFunction": null,
            "visibility": "public"
          },
          {
            "body": {
              "id": 2195,
              "nodeType": "Block",
              "src": "1761:337:9",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "commonType": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        },
                        "id": 2165,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "leftExpression": {
                          "argumentTypes": null,
                          "id": 2155,
                          "name": "amount",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 2151,
                          "src": "1776:6:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        },
                        "nodeType": "BinaryOperation",
                        "operator": "<=",
                        "rightExpression": {
                          "argumentTypes": null,
                          "commonType": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          },
                          "id": 2164,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "leftExpression": {
                            "argumentTypes": null,
                            "baseExpression": {
                              "argumentTypes": null,
                              "id": 2156,
                              "name": "lendingBalances",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 1989,
                              "src": "1786:15:9",
                              "typeDescriptions": {
                                "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                                "typeString": "mapping(address => uint256)"
                              }
                            },
                            "id": 2159,
                            "indexExpression": {
                              "argumentTypes": null,
                              "expression": {
                                "argumentTypes": null,
                                "id": 2157,
                                "name": "msg",
                                "nodeType": "Identifier",
                                "overloadedDeclarations": [],
                                "referencedDeclaration": 11851,
                                "src": "1802:3:9",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_magic_message",
                                  "typeString": "msg"
                                }
                              },
                              "id": 2158,
                              "isConstant": false,
                              "isLValue": false,
                              "isPure": false,
                              "lValueRequested": false,
                              "memberName": "sender",
                              "nodeType": "MemberAccess",
                              "referencedDeclaration": null,
                              "src": "1802:10:9",
                              "typeDescriptions": {
                                "typeIdentifier": "t_address_payable",
                                "typeString": "address payable"
                              }
                            },
                            "isConstant": false,
                            "isLValue": true,
                            "isPure": false,
                            "lValueRequested": false,
                            "nodeType": "IndexAccess",
                            "src": "1786:27:9",
                            "typeDescriptions": {
                              "typeIdentifier": "t_uint256",
                              "typeString": "uint256"
                            }
                          },
                          "nodeType": "BinaryOperation",
                          "operator": "-",
                          "rightExpression": {
                            "argumentTypes": null,
                            "baseExpression": {
                              "argumentTypes": null,
                              "id": 2160,
                              "name": "totalLentPerCustomer",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 2007,
                              "src": "1816:20:9",
                              "typeDescriptions": {
                                "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                                "typeString": "mapping(address => uint256)"
                              }
                            },
                            "id": 2163,
                            "indexExpression": {
                              "argumentTypes": null,
                              "expression": {
                                "argumentTypes": null,
                                "id": 2161,
                                "name": "msg",
                                "nodeType": "Identifier",
                                "overloadedDeclarations": [],
                                "referencedDeclaration": 11851,
                                "src": "1837:3:9",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_magic_message",
                                  "typeString": "msg"
                                }
                              },
                              "id": 2162,
                              "isConstant": false,
                              "isLValue": false,
                              "isPure": false,
                              "lValueRequested": false,
                              "memberName": "sender",
                              "nodeType": "MemberAccess",
                              "referencedDeclaration": null,
                              "src": "1837:10:9",
                              "typeDescriptions": {
                                "typeIdentifier": "t_address_payable",
                                "typeString": "address payable"
                              }
                            },
                            "isConstant": false,
                            "isLValue": true,
                            "isPure": false,
                            "lValueRequested": false,
                            "nodeType": "IndexAccess",
                            "src": "1816:32:9",
                            "typeDescriptions": {
                              "typeIdentifier": "t_uint256",
                              "typeString": "uint256"
                            }
                          },
                          "src": "1786:62:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        },
                        "src": "1776:72:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "hexValue": "596f752063616e6e6f74207769746864726177206d6f7265207468616e20796f757220746f74616c2062616c616e6365206d696e757320776861742069732063757272656e746c79206265696e67206c656e74",
                        "id": 2166,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": true,
                        "kind": "string",
                        "lValueRequested": false,
                        "nodeType": "Literal",
                        "src": "1856:85:9",
                        "subdenomination": null,
                        "typeDescriptions": {
                          "typeIdentifier": "t_stringliteral_9b95227b372575c4d3540b25a2e007a20a3bdaffdc7e2a041b9c36ef3add4e50",
                          "typeString": "literal_string \"You cannot withdraw more than your total balance minus what is currently being lent\""
                        },
                        "value": "You cannot withdraw more than your total balance minus what is currently being lent"
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        },
                        {
                          "typeIdentifier": "t_stringliteral_9b95227b372575c4d3540b25a2e007a20a3bdaffdc7e2a041b9c36ef3add4e50",
                          "typeString": "literal_string \"You cannot withdraw more than your total balance minus what is currently being lent\""
                        }
                      ],
                      "id": 2154,
                      "name": "require",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [
                        11854,
                        11855
                      ],
                      "referencedDeclaration": 11855,
                      "src": "1768:7:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_require_pure$_t_bool_$_t_string_memory_ptr_$returns$__$",
                        "typeString": "function (bool,string memory) pure"
                      }
                    },
                    "id": 2167,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "1768:174:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 2168,
                  "nodeType": "ExpressionStatement",
                  "src": "1768:174:9"
                },
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 2179,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "baseExpression": {
                        "argumentTypes": null,
                        "id": 2169,
                        "name": "lendingBalances",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1989,
                        "src": "1949:15:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                          "typeString": "mapping(address => uint256)"
                        }
                      },
                      "id": 2172,
                      "indexExpression": {
                        "argumentTypes": null,
                        "expression": {
                          "argumentTypes": null,
                          "id": 2170,
                          "name": "msg",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 11851,
                          "src": "1965:3:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_magic_message",
                            "typeString": "msg"
                          }
                        },
                        "id": 2171,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "memberName": "sender",
                        "nodeType": "MemberAccess",
                        "referencedDeclaration": null,
                        "src": "1965:10:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address_payable",
                          "typeString": "address payable"
                        }
                      },
                      "isConstant": false,
                      "isLValue": true,
                      "isPure": false,
                      "lValueRequested": true,
                      "nodeType": "IndexAccess",
                      "src": "1949:27:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "commonType": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      },
                      "id": 2178,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "lValueRequested": false,
                      "leftExpression": {
                        "argumentTypes": null,
                        "baseExpression": {
                          "argumentTypes": null,
                          "id": 2173,
                          "name": "lendingBalances",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 1989,
                          "src": "1979:15:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                            "typeString": "mapping(address => uint256)"
                          }
                        },
                        "id": 2176,
                        "indexExpression": {
                          "argumentTypes": null,
                          "expression": {
                            "argumentTypes": null,
                            "id": 2174,
                            "name": "msg",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 11851,
                            "src": "1995:3:9",
                            "typeDescriptions": {
                              "typeIdentifier": "t_magic_message",
                              "typeString": "msg"
                            }
                          },
                          "id": 2175,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "memberName": "sender",
                          "nodeType": "MemberAccess",
                          "referencedDeclaration": null,
                          "src": "1995:10:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address_payable",
                            "typeString": "address payable"
                          }
                        },
                        "isConstant": false,
                        "isLValue": true,
                        "isPure": false,
                        "lValueRequested": false,
                        "nodeType": "IndexAccess",
                        "src": "1979:27:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      "nodeType": "BinaryOperation",
                      "operator": "-",
                      "rightExpression": {
                        "argumentTypes": null,
                        "id": 2177,
                        "name": "amount",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 2151,
                        "src": "2009:6:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      "src": "1979:36:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "src": "1949:66:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "id": 2180,
                  "nodeType": "ExpressionStatement",
                  "src": "1949:66:9"
                },
                {
                  "expression": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "id": 2186,
                        "name": "amount",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 2151,
                        "src": "2042:6:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      ],
                      "expression": {
                        "argumentTypes": null,
                        "expression": {
                          "argumentTypes": null,
                          "id": 2181,
                          "name": "msg",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 11851,
                          "src": "2022:3:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_magic_message",
                            "typeString": "msg"
                          }
                        },
                        "id": 2184,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "memberName": "sender",
                        "nodeType": "MemberAccess",
                        "referencedDeclaration": null,
                        "src": "2022:10:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address_payable",
                          "typeString": "address payable"
                        }
                      },
                      "id": 2185,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "lValueRequested": false,
                      "memberName": "transfer",
                      "nodeType": "MemberAccess",
                      "referencedDeclaration": null,
                      "src": "2022:19:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_transfer_nonpayable$_t_uint256_$returns$__$",
                        "typeString": "function (uint256)"
                      }
                    },
                    "id": 2187,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "2022:27:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 2188,
                  "nodeType": "ExpressionStatement",
                  "src": "2022:27:9"
                },
                {
                  "eventCall": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "expression": {
                          "argumentTypes": null,
                          "id": 2190,
                          "name": "msg",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 11851,
                          "src": "2073:3:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_magic_message",
                            "typeString": "msg"
                          }
                        },
                        "id": 2191,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "memberName": "sender",
                        "nodeType": "MemberAccess",
                        "referencedDeclaration": null,
                        "src": "2073:10:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address_payable",
                          "typeString": "address payable"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "id": 2192,
                        "name": "amount",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 2151,
                        "src": "2085:6:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_address_payable",
                          "typeString": "address payable"
                        },
                        {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      ],
                      "id": 2189,
                      "name": "WithdrewETH",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 9160,
                      "src": "2061:11:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_event_nonpayable$_t_address_$_t_uint256_$returns$__$",
                        "typeString": "function (address,uint256)"
                      }
                    },
                    "id": 2193,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "2061:31:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 2194,
                  "nodeType": "EmitStatement",
                  "src": "2056:36:9"
                }
              ]
            },
            "documentation": null,
            "id": 2196,
            "implemented": true,
            "kind": "function",
            "modifiers": [],
            "name": "withdrawLendingETH",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 2152,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 2151,
                  "name": "amount",
                  "nodeType": "VariableDeclaration",
                  "scope": 2196,
                  "src": "1738:14:9",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_uint256",
                    "typeString": "uint256"
                  },
                  "typeName": {
                    "id": 2150,
                    "name": "uint256",
                    "nodeType": "ElementaryTypeName",
                    "src": "1738:7:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "1737:16:9"
            },
            "returnParameters": {
              "id": 2153,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "1761:0:9"
            },
            "scope": 2386,
            "src": "1710:388:9",
            "stateMutability": "nonpayable",
            "superFunction": null,
            "visibility": "public"
          },
          {
            "body": {
              "id": 2259,
              "nodeType": "Block",
              "src": "2177:451:9",
              "statements": [
                {
                  "assignments": [
                    2206
                  ],
                  "declarations": [
                    {
                      "constant": false,
                      "id": 2206,
                      "name": "amountToLend",
                      "nodeType": "VariableDeclaration",
                      "scope": 2259,
                      "src": "2184:17:9",
                      "stateVariable": false,
                      "storageLocation": "default",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      },
                      "typeName": {
                        "id": 2205,
                        "name": "uint",
                        "nodeType": "ElementaryTypeName",
                        "src": "2184:4:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      "value": null,
                      "visibility": "internal"
                    }
                  ],
                  "id": 2211,
                  "initialValue": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "id": 2209,
                        "name": "loanId",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 2200,
                        "src": "2226:6:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      ],
                      "expression": {
                        "argumentTypes": null,
                        "id": 2207,
                        "name": "manager",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 2009,
                        "src": "2204:7:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_contract$_ILendingManager_$9136",
                          "typeString": "contract ILendingManager"
                        }
                      },
                      "id": 2208,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "lValueRequested": false,
                      "memberName": "getAmountLent",
                      "nodeType": "MemberAccess",
                      "referencedDeclaration": 9109,
                      "src": "2204:21:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_external_view$_t_uint256_$returns$_t_uint256_$",
                        "typeString": "function (uint256) view external returns (uint256)"
                      }
                    },
                    "id": 2210,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "2204:29:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "nodeType": "VariableDeclarationStatement",
                  "src": "2184:49:9"
                },
                {
                  "expression": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "commonType": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        },
                        "id": 2221,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "leftExpression": {
                          "argumentTypes": null,
                          "commonType": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          },
                          "id": 2217,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "leftExpression": {
                            "argumentTypes": null,
                            "baseExpression": {
                              "argumentTypes": null,
                              "id": 2213,
                              "name": "totalLentPerCustomer",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 2007,
                              "src": "2248:20:9",
                              "typeDescriptions": {
                                "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                                "typeString": "mapping(address => uint256)"
                              }
                            },
                            "id": 2215,
                            "indexExpression": {
                              "argumentTypes": null,
                              "id": 2214,
                              "name": "customer",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 2198,
                              "src": "2269:8:9",
                              "typeDescriptions": {
                                "typeIdentifier": "t_address",
                                "typeString": "address"
                              }
                            },
                            "isConstant": false,
                            "isLValue": true,
                            "isPure": false,
                            "lValueRequested": false,
                            "nodeType": "IndexAccess",
                            "src": "2248:30:9",
                            "typeDescriptions": {
                              "typeIdentifier": "t_uint256",
                              "typeString": "uint256"
                            }
                          },
                          "nodeType": "BinaryOperation",
                          "operator": "+",
                          "rightExpression": {
                            "argumentTypes": null,
                            "id": 2216,
                            "name": "amountToLend",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 2206,
                            "src": "2289:12:9",
                            "typeDescriptions": {
                              "typeIdentifier": "t_uint256",
                              "typeString": "uint256"
                            }
                          },
                          "src": "2248:53:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        },
                        "nodeType": "BinaryOperation",
                        "operator": "<=",
                        "rightExpression": {
                          "argumentTypes": null,
                          "baseExpression": {
                            "argumentTypes": null,
                            "id": 2218,
                            "name": "lendingBalances",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 1989,
                            "src": "2305:15:9",
                            "typeDescriptions": {
                              "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                              "typeString": "mapping(address => uint256)"
                            }
                          },
                          "id": 2220,
                          "indexExpression": {
                            "argumentTypes": null,
                            "id": 2219,
                            "name": "customer",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 2198,
                            "src": "2321:8:9",
                            "typeDescriptions": {
                              "typeIdentifier": "t_address",
                              "typeString": "address"
                            }
                          },
                          "isConstant": false,
                          "isLValue": true,
                          "isPure": false,
                          "lValueRequested": false,
                          "nodeType": "IndexAccess",
                          "src": "2305:25:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        },
                        "src": "2248:82:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "hexValue": "43616e6e6f74206c656e64206d6f7265207468616e2074686520637573746f6d657220617070726f76656420666f72206c656e64696e67",
                        "id": 2222,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": true,
                        "kind": "string",
                        "lValueRequested": false,
                        "nodeType": "Literal",
                        "src": "2340:57:9",
                        "subdenomination": null,
                        "typeDescriptions": {
                          "typeIdentifier": "t_stringliteral_dbcdd8a7afd244d1ef485b4244e6f588c584997d458f835376392c0228bb97c4",
                          "typeString": "literal_string \"Cannot lend more than the customer approved for lending\""
                        },
                        "value": "Cannot lend more than the customer approved for lending"
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        },
                        {
                          "typeIdentifier": "t_stringliteral_dbcdd8a7afd244d1ef485b4244e6f588c584997d458f835376392c0228bb97c4",
                          "typeString": "literal_string \"Cannot lend more than the customer approved for lending\""
                        }
                      ],
                      "id": 2212,
                      "name": "require",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [
                        11854,
                        11855
                      ],
                      "referencedDeclaration": 11855,
                      "src": "2240:7:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_require_pure$_t_bool_$_t_string_memory_ptr_$returns$__$",
                        "typeString": "function (bool,string memory) pure"
                      }
                    },
                    "id": 2223,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "2240:158:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 2224,
                  "nodeType": "ExpressionStatement",
                  "src": "2240:158:9"
                },
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 2233,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "baseExpression": {
                        "argumentTypes": null,
                        "id": 2225,
                        "name": "totalLentPerCustomer",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 2007,
                        "src": "2405:20:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                          "typeString": "mapping(address => uint256)"
                        }
                      },
                      "id": 2227,
                      "indexExpression": {
                        "argumentTypes": null,
                        "id": 2226,
                        "name": "customer",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 2198,
                        "src": "2426:8:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      "isConstant": false,
                      "isLValue": true,
                      "isPure": false,
                      "lValueRequested": true,
                      "nodeType": "IndexAccess",
                      "src": "2405:30:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "commonType": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      },
                      "id": 2232,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "lValueRequested": false,
                      "leftExpression": {
                        "argumentTypes": null,
                        "baseExpression": {
                          "argumentTypes": null,
                          "id": 2228,
                          "name": "totalLentPerCustomer",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 2007,
                          "src": "2438:20:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                            "typeString": "mapping(address => uint256)"
                          }
                        },
                        "id": 2230,
                        "indexExpression": {
                          "argumentTypes": null,
                          "id": 2229,
                          "name": "customer",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 2198,
                          "src": "2459:8:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "isConstant": false,
                        "isLValue": true,
                        "isPure": false,
                        "lValueRequested": false,
                        "nodeType": "IndexAccess",
                        "src": "2438:30:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      "nodeType": "BinaryOperation",
                      "operator": "+",
                      "rightExpression": {
                        "argumentTypes": null,
                        "id": 2231,
                        "name": "amountToLend",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 2206,
                        "src": "2471:12:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      "src": "2438:45:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "src": "2405:78:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "id": 2234,
                  "nodeType": "ExpressionStatement",
                  "src": "2405:78:9"
                },
                {
                  "expression": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "id": 2242,
                        "name": "loanId",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 2200,
                        "src": "2527:6:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      ],
                      "arguments": [
                        {
                          "argumentTypes": null,
                          "id": 2240,
                          "name": "amountToLend",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 2206,
                          "src": "2513:12:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        }
                      ],
                      "expression": {
                        "argumentTypes": [
                          {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        ],
                        "expression": {
                          "argumentTypes": null,
                          "expression": {
                            "argumentTypes": null,
                            "id": 2235,
                            "name": "manager",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 2009,
                            "src": "2490:7:9",
                            "typeDescriptions": {
                              "typeIdentifier": "t_contract$_ILendingManager_$9136",
                              "typeString": "contract ILendingManager"
                            }
                          },
                          "id": 2238,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "memberName": "fillLoan",
                          "nodeType": "MemberAccess",
                          "referencedDeclaration": 9054,
                          "src": "2490:16:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_function_external_payable$_t_uint256_$returns$__$",
                            "typeString": "function (uint256) payable external"
                          }
                        },
                        "id": 2239,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "memberName": "value",
                        "nodeType": "MemberAccess",
                        "referencedDeclaration": null,
                        "src": "2490:22:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_function_setvalue_nonpayable$_t_uint256_$returns$_t_function_external_payable$_t_uint256_$returns$__$value_$",
                          "typeString": "function (uint256) returns (function (uint256) payable external)"
                        }
                      },
                      "id": 2241,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "kind": "functionCall",
                      "lValueRequested": false,
                      "names": [],
                      "nodeType": "FunctionCall",
                      "src": "2490:36:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_external_payable$_t_uint256_$returns$__$value",
                        "typeString": "function (uint256) payable external"
                      }
                    },
                    "id": 2243,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "2490:44:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 2244,
                  "nodeType": "ExpressionStatement",
                  "src": "2490:44:9"
                },
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 2251,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "baseExpression": {
                        "argumentTypes": null,
                        "baseExpression": {
                          "argumentTypes": null,
                          "id": 2245,
                          "name": "customerLoanTie",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 1999,
                          "src": "2541:15:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_mapping$_t_address_$_t_mapping$_t_uint256_$_t_bool_$_$",
                            "typeString": "mapping(address => mapping(uint256 => bool))"
                          }
                        },
                        "id": 2248,
                        "indexExpression": {
                          "argumentTypes": null,
                          "id": 2246,
                          "name": "customer",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 2198,
                          "src": "2557:8:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "isConstant": false,
                        "isLValue": true,
                        "isPure": false,
                        "lValueRequested": false,
                        "nodeType": "IndexAccess",
                        "src": "2541:25:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_mapping$_t_uint256_$_t_bool_$",
                          "typeString": "mapping(uint256 => bool)"
                        }
                      },
                      "id": 2249,
                      "indexExpression": {
                        "argumentTypes": null,
                        "id": 2247,
                        "name": "loanId",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 2200,
                        "src": "2567:6:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      "isConstant": false,
                      "isLValue": true,
                      "isPure": false,
                      "lValueRequested": true,
                      "nodeType": "IndexAccess",
                      "src": "2541:33:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_bool",
                        "typeString": "bool"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "hexValue": "74727565",
                      "id": 2250,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": true,
                      "kind": "bool",
                      "lValueRequested": false,
                      "nodeType": "Literal",
                      "src": "2577:4:9",
                      "subdenomination": null,
                      "typeDescriptions": {
                        "typeIdentifier": "t_bool",
                        "typeString": "bool"
                      },
                      "value": "true"
                    },
                    "src": "2541:40:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_bool",
                      "typeString": "bool"
                    }
                  },
                  "id": 2252,
                  "nodeType": "ExpressionStatement",
                  "src": "2541:40:9"
                },
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 2257,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "baseExpression": {
                        "argumentTypes": null,
                        "id": 2253,
                        "name": "loanCustomerTie",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 2003,
                        "src": "2588:15:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_mapping$_t_uint256_$_t_address_$",
                          "typeString": "mapping(uint256 => address)"
                        }
                      },
                      "id": 2255,
                      "indexExpression": {
                        "argumentTypes": null,
                        "id": 2254,
                        "name": "loanId",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 2200,
                        "src": "2604:6:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      "isConstant": false,
                      "isLValue": true,
                      "isPure": false,
                      "lValueRequested": true,
                      "nodeType": "IndexAccess",
                      "src": "2588:23:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_address",
                        "typeString": "address"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "id": 2256,
                      "name": "customer",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 2198,
                      "src": "2614:8:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_address",
                        "typeString": "address"
                      }
                    },
                    "src": "2588:34:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "id": 2258,
                  "nodeType": "ExpressionStatement",
                  "src": "2588:34:9"
                }
              ]
            },
            "documentation": null,
            "id": 2260,
            "implemented": true,
            "kind": "function",
            "modifiers": [
              {
                "arguments": [],
                "id": 2203,
                "modifierName": {
                  "argumentTypes": null,
                  "id": 2202,
                  "name": "onlyValidRelayer",
                  "nodeType": "Identifier",
                  "overloadedDeclarations": [],
                  "referencedDeclaration": 2023,
                  "src": "2158:16:9",
                  "typeDescriptions": {
                    "typeIdentifier": "t_modifier$__$",
                    "typeString": "modifier ()"
                  }
                },
                "nodeType": "ModifierInvocation",
                "src": "2158:18:9"
              }
            ],
            "name": "fillLoan",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 2201,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 2198,
                  "name": "customer",
                  "nodeType": "VariableDeclaration",
                  "scope": 2260,
                  "src": "2120:16:9",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 2197,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "2120:7:9",
                    "stateMutability": "nonpayable",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                },
                {
                  "constant": false,
                  "id": 2200,
                  "name": "loanId",
                  "nodeType": "VariableDeclaration",
                  "scope": 2260,
                  "src": "2138:11:9",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_uint256",
                    "typeString": "uint256"
                  },
                  "typeName": {
                    "id": 2199,
                    "name": "uint",
                    "nodeType": "ElementaryTypeName",
                    "src": "2138:4:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "2119:31:9"
            },
            "returnParameters": {
              "id": 2204,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "2177:0:9"
            },
            "scope": 2386,
            "src": "2102:526:9",
            "stateMutability": "nonpayable",
            "superFunction": null,
            "visibility": "public"
          },
          {
            "body": {
              "id": 2276,
              "nodeType": "Block",
              "src": "2711:59:9",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "id": 2272,
                        "name": "id",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 2262,
                        "src": "2750:2:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "id": 2273,
                        "name": "destToken",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 2264,
                        "src": "2754:9:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        },
                        {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      ],
                      "expression": {
                        "argumentTypes": null,
                        "id": 2269,
                        "name": "manager",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 2009,
                        "src": "2718:7:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_contract$_ILendingManager_$9136",
                          "typeString": "contract ILendingManager"
                        }
                      },
                      "id": 2271,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "lValueRequested": false,
                      "memberName": "enableSwappedCollateral",
                      "nodeType": "MemberAccess",
                      "referencedDeclaration": 9079,
                      "src": "2718:31:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_external_nonpayable$_t_uint256_$_t_address_$returns$__$",
                        "typeString": "function (uint256,address) external"
                      }
                    },
                    "id": 2274,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "2718:46:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 2275,
                  "nodeType": "ExpressionStatement",
                  "src": "2718:46:9"
                }
              ]
            },
            "documentation": null,
            "id": 2277,
            "implemented": true,
            "kind": "function",
            "modifiers": [
              {
                "arguments": [],
                "id": 2267,
                "modifierName": {
                  "argumentTypes": null,
                  "id": 2266,
                  "name": "onlyValidRelayer",
                  "nodeType": "Identifier",
                  "overloadedDeclarations": [],
                  "referencedDeclaration": 2023,
                  "src": "2692:16:9",
                  "typeDescriptions": {
                    "typeIdentifier": "t_modifier$__$",
                    "typeString": "modifier ()"
                  }
                },
                "nodeType": "ModifierInvocation",
                "src": "2692:18:9"
              }
            ],
            "name": "enableKyberSwap",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 2265,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 2262,
                  "name": "id",
                  "nodeType": "VariableDeclaration",
                  "scope": 2277,
                  "src": "2657:7:9",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_uint256",
                    "typeString": "uint256"
                  },
                  "typeName": {
                    "id": 2261,
                    "name": "uint",
                    "nodeType": "ElementaryTypeName",
                    "src": "2657:4:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                },
                {
                  "constant": false,
                  "id": 2264,
                  "name": "destToken",
                  "nodeType": "VariableDeclaration",
                  "scope": 2277,
                  "src": "2666:17:9",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 2263,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "2666:7:9",
                    "stateMutability": "nonpayable",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "2656:28:9"
            },
            "returnParameters": {
              "id": 2268,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "2711:0:9"
            },
            "scope": 2386,
            "src": "2632:138:9",
            "stateMutability": "nonpayable",
            "superFunction": null,
            "visibility": "public"
          },
          {
            "body": {
              "id": 2319,
              "nodeType": "Block",
              "src": "2852:633:9",
              "statements": [
                {
                  "assignments": [
                    2287
                  ],
                  "declarations": [
                    {
                      "constant": false,
                      "id": 2287,
                      "name": "collateralReceived",
                      "nodeType": "VariableDeclaration",
                      "scope": 2319,
                      "src": "2859:26:9",
                      "stateVariable": false,
                      "storageLocation": "default",
                      "typeDescriptions": {
                        "typeIdentifier": "t_address",
                        "typeString": "address"
                      },
                      "typeName": {
                        "id": 2286,
                        "name": "address",
                        "nodeType": "ElementaryTypeName",
                        "src": "2859:7:9",
                        "stateMutability": "nonpayable",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      "value": null,
                      "visibility": "internal"
                    }
                  ],
                  "id": 2288,
                  "initialValue": null,
                  "nodeType": "VariableDeclarationStatement",
                  "src": "2859:26:9"
                },
                {
                  "assignments": [
                    2290
                  ],
                  "declarations": [
                    {
                      "constant": false,
                      "id": 2290,
                      "name": "collateralType",
                      "nodeType": "VariableDeclaration",
                      "scope": 2319,
                      "src": "2891:19:9",
                      "stateVariable": false,
                      "storageLocation": "default",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      },
                      "typeName": {
                        "id": 2289,
                        "name": "uint",
                        "nodeType": "ElementaryTypeName",
                        "src": "2891:4:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      "value": null,
                      "visibility": "internal"
                    }
                  ],
                  "id": 2291,
                  "initialValue": null,
                  "nodeType": "VariableDeclarationStatement",
                  "src": "2891:19:9"
                },
                {
                  "assignments": [
                    2293
                  ],
                  "declarations": [
                    {
                      "constant": false,
                      "id": 2293,
                      "name": "collateralDetails",
                      "nodeType": "VariableDeclaration",
                      "scope": 2319,
                      "src": "2916:30:9",
                      "stateVariable": false,
                      "storageLocation": "memory",
                      "typeDescriptions": {
                        "typeIdentifier": "t_bytes_memory_ptr",
                        "typeString": "bytes"
                      },
                      "typeName": {
                        "id": 2292,
                        "name": "bytes",
                        "nodeType": "ElementaryTypeName",
                        "src": "2916:5:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_bytes_storage_ptr",
                          "typeString": "bytes"
                        }
                      },
                      "value": null,
                      "visibility": "internal"
                    }
                  ],
                  "id": 2294,
                  "initialValue": null,
                  "nodeType": "VariableDeclarationStatement",
                  "src": "2916:30:9"
                },
                {
                  "assignments": [
                    2296
                  ],
                  "declarations": [
                    {
                      "constant": false,
                      "id": 2296,
                      "name": "ethAddress",
                      "nodeType": "VariableDeclaration",
                      "scope": 2319,
                      "src": "2953:18:9",
                      "stateVariable": false,
                      "storageLocation": "default",
                      "typeDescriptions": {
                        "typeIdentifier": "t_address",
                        "typeString": "address"
                      },
                      "typeName": {
                        "id": 2295,
                        "name": "address",
                        "nodeType": "ElementaryTypeName",
                        "src": "2953:7:9",
                        "stateMutability": "nonpayable",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      "value": null,
                      "visibility": "internal"
                    }
                  ],
                  "id": 2300,
                  "initialValue": {
                    "argumentTypes": null,
                    "arguments": [],
                    "expression": {
                      "argumentTypes": [],
                      "expression": {
                        "argumentTypes": null,
                        "id": 2297,
                        "name": "manager",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 2009,
                        "src": "2974:7:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_contract$_ILendingManager_$9136",
                          "typeString": "contract ILendingManager"
                        }
                      },
                      "id": 2298,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "lValueRequested": false,
                      "memberName": "getETHAddress",
                      "nodeType": "MemberAccess",
                      "referencedDeclaration": 9135,
                      "src": "2974:21:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_external_view$__$returns$_t_address_$",
                        "typeString": "function () view external returns (address)"
                      }
                    },
                    "id": 2299,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "2974:23:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "nodeType": "VariableDeclarationStatement",
                  "src": "2953:44:9"
                },
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 2309,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "components": [
                        {
                          "argumentTypes": null,
                          "id": 2301,
                          "name": "collateralReceived",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 2287,
                          "src": "3005:18:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        {
                          "argumentTypes": null,
                          "id": 2302,
                          "name": "collateralType",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 2290,
                          "src": "3025:14:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        },
                        {
                          "argumentTypes": null,
                          "id": 2303,
                          "name": "collateralDetails",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 2293,
                          "src": "3041:17:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_bytes_memory_ptr",
                            "typeString": "bytes memory"
                          }
                        }
                      ],
                      "id": 2304,
                      "isConstant": false,
                      "isInlineArray": false,
                      "isLValue": true,
                      "isPure": false,
                      "lValueRequested": true,
                      "nodeType": "TupleExpression",
                      "src": "3004:55:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_tuple$_t_address_$_t_uint256_$_t_bytes_memory_ptr_$",
                        "typeString": "tuple(address,uint256,bytes memory)"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "arguments": [
                        {
                          "argumentTypes": null,
                          "id": 2307,
                          "name": "id",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 2279,
                          "src": "3091:2:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        }
                      ],
                      "expression": {
                        "argumentTypes": [
                          {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        ],
                        "expression": {
                          "argumentTypes": null,
                          "id": 2305,
                          "name": "manager",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 2009,
                          "src": "3068:7:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_contract$_ILendingManager_$9136",
                            "typeString": "contract ILendingManager"
                          }
                        },
                        "id": 2306,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "memberName": "declareDefault",
                        "nodeType": "MemberAccess",
                        "referencedDeclaration": 9072,
                        "src": "3068:22:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_function_external_nonpayable$_t_uint256_$returns$_t_address_$_t_uint256_$_t_bytes_memory_ptr_$",
                          "typeString": "function (uint256) external returns (address,uint256,bytes memory)"
                        }
                      },
                      "id": 2308,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "kind": "functionCall",
                      "lValueRequested": false,
                      "names": [],
                      "nodeType": "FunctionCall",
                      "src": "3068:26:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_tuple$_t_address_$_t_uint256_$_t_bytes_memory_ptr_$",
                        "typeString": "tuple(address,uint256,bytes memory)"
                      }
                    },
                    "src": "3004:90:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 2310,
                  "nodeType": "ExpressionStatement",
                  "src": "3004:90:9"
                },
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 2317,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "baseExpression": {
                        "argumentTypes": null,
                        "baseExpression": {
                          "argumentTypes": null,
                          "id": 2311,
                          "name": "customerLoanTie",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 1999,
                          "src": "3442:15:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_mapping$_t_address_$_t_mapping$_t_uint256_$_t_bool_$_$",
                            "typeString": "mapping(address => mapping(uint256 => bool))"
                          }
                        },
                        "id": 2314,
                        "indexExpression": {
                          "argumentTypes": null,
                          "id": 2312,
                          "name": "customer",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 2281,
                          "src": "3458:8:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "isConstant": false,
                        "isLValue": true,
                        "isPure": false,
                        "lValueRequested": false,
                        "nodeType": "IndexAccess",
                        "src": "3442:25:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_mapping$_t_uint256_$_t_bool_$",
                          "typeString": "mapping(uint256 => bool)"
                        }
                      },
                      "id": 2315,
                      "indexExpression": {
                        "argumentTypes": null,
                        "id": 2313,
                        "name": "id",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 2279,
                        "src": "3468:2:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      "isConstant": false,
                      "isLValue": true,
                      "isPure": false,
                      "lValueRequested": true,
                      "nodeType": "IndexAccess",
                      "src": "3442:29:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_bool",
                        "typeString": "bool"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "hexValue": "66616c7365",
                      "id": 2316,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": true,
                      "kind": "bool",
                      "lValueRequested": false,
                      "nodeType": "Literal",
                      "src": "3474:5:9",
                      "subdenomination": null,
                      "typeDescriptions": {
                        "typeIdentifier": "t_bool",
                        "typeString": "bool"
                      },
                      "value": "false"
                    },
                    "src": "3442:37:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_bool",
                      "typeString": "bool"
                    }
                  },
                  "id": 2318,
                  "nodeType": "ExpressionStatement",
                  "src": "3442:37:9"
                }
              ]
            },
            "documentation": null,
            "id": 2320,
            "implemented": true,
            "kind": "function",
            "modifiers": [
              {
                "arguments": [],
                "id": 2284,
                "modifierName": {
                  "argumentTypes": null,
                  "id": 2283,
                  "name": "onlyValidRelayer",
                  "nodeType": "Identifier",
                  "overloadedDeclarations": [],
                  "referencedDeclaration": 2023,
                  "src": "2833:16:9",
                  "typeDescriptions": {
                    "typeIdentifier": "t_modifier$__$",
                    "typeString": "modifier ()"
                  }
                },
                "nodeType": "ModifierInvocation",
                "src": "2833:18:9"
              }
            ],
            "name": "seizeCollateral",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 2282,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 2279,
                  "name": "id",
                  "nodeType": "VariableDeclaration",
                  "scope": 2320,
                  "src": "2799:7:9",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_uint256",
                    "typeString": "uint256"
                  },
                  "typeName": {
                    "id": 2278,
                    "name": "uint",
                    "nodeType": "ElementaryTypeName",
                    "src": "2799:4:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                },
                {
                  "constant": false,
                  "id": 2281,
                  "name": "customer",
                  "nodeType": "VariableDeclaration",
                  "scope": 2320,
                  "src": "2808:16:9",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 2280,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "2808:7:9",
                    "stateMutability": "nonpayable",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "2798:27:9"
            },
            "returnParameters": {
              "id": 2285,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "2852:0:9"
            },
            "scope": 2386,
            "src": "2774:711:9",
            "stateMutability": "nonpayable",
            "superFunction": null,
            "visibility": "public"
          },
          {
            "body": {
              "id": 2384,
              "nodeType": "Block",
              "src": "3560:456:9",
              "statements": [
                {
                  "assignments": [
                    2328
                  ],
                  "declarations": [
                    {
                      "constant": false,
                      "id": 2328,
                      "name": "customer",
                      "nodeType": "VariableDeclaration",
                      "scope": 2384,
                      "src": "3567:16:9",
                      "stateVariable": false,
                      "storageLocation": "default",
                      "typeDescriptions": {
                        "typeIdentifier": "t_address",
                        "typeString": "address"
                      },
                      "typeName": {
                        "id": 2327,
                        "name": "address",
                        "nodeType": "ElementaryTypeName",
                        "src": "3567:7:9",
                        "stateMutability": "nonpayable",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      "value": null,
                      "visibility": "internal"
                    }
                  ],
                  "id": 2332,
                  "initialValue": {
                    "argumentTypes": null,
                    "baseExpression": {
                      "argumentTypes": null,
                      "id": 2329,
                      "name": "loanCustomerTie",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 2003,
                      "src": "3586:15:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_mapping$_t_uint256_$_t_address_$",
                        "typeString": "mapping(uint256 => address)"
                      }
                    },
                    "id": 2331,
                    "indexExpression": {
                      "argumentTypes": null,
                      "id": 2330,
                      "name": "id",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 2322,
                      "src": "3602:2:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "isConstant": false,
                    "isLValue": true,
                    "isPure": false,
                    "lValueRequested": false,
                    "nodeType": "IndexAccess",
                    "src": "3586:19:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "nodeType": "VariableDeclarationStatement",
                  "src": "3567:38:9"
                },
                {
                  "condition": {
                    "argumentTypes": null,
                    "commonType": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    },
                    "id": 2338,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftExpression": {
                      "argumentTypes": null,
                      "expression": {
                        "argumentTypes": null,
                        "id": 2333,
                        "name": "msg",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 11851,
                        "src": "3616:3:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_magic_message",
                          "typeString": "msg"
                        }
                      },
                      "id": 2334,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "lValueRequested": false,
                      "memberName": "value",
                      "nodeType": "MemberAccess",
                      "referencedDeclaration": null,
                      "src": "3616:9:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "nodeType": "BinaryOperation",
                    "operator": ">",
                    "rightExpression": {
                      "argumentTypes": null,
                      "baseExpression": {
                        "argumentTypes": null,
                        "id": 2335,
                        "name": "totalLentPerCustomer",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 2007,
                        "src": "3628:20:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                          "typeString": "mapping(address => uint256)"
                        }
                      },
                      "id": 2337,
                      "indexExpression": {
                        "argumentTypes": null,
                        "id": 2336,
                        "name": "customer",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 2328,
                        "src": "3649:8:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      "isConstant": false,
                      "isLValue": true,
                      "isPure": false,
                      "lValueRequested": false,
                      "nodeType": "IndexAccess",
                      "src": "3628:30:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "src": "3616:42:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_bool",
                      "typeString": "bool"
                    }
                  },
                  "falseBody": {
                    "id": 2376,
                    "nodeType": "Block",
                    "src": "3866:92:9",
                    "statements": [
                      {
                        "expression": {
                          "argumentTypes": null,
                          "id": 2374,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "leftHandSide": {
                            "argumentTypes": null,
                            "baseExpression": {
                              "argumentTypes": null,
                              "id": 2365,
                              "name": "totalLentPerCustomer",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 2007,
                              "src": "3875:20:9",
                              "typeDescriptions": {
                                "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                                "typeString": "mapping(address => uint256)"
                              }
                            },
                            "id": 2367,
                            "indexExpression": {
                              "argumentTypes": null,
                              "id": 2366,
                              "name": "customer",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 2328,
                              "src": "3896:8:9",
                              "typeDescriptions": {
                                "typeIdentifier": "t_address",
                                "typeString": "address"
                              }
                            },
                            "isConstant": false,
                            "isLValue": true,
                            "isPure": false,
                            "lValueRequested": true,
                            "nodeType": "IndexAccess",
                            "src": "3875:30:9",
                            "typeDescriptions": {
                              "typeIdentifier": "t_uint256",
                              "typeString": "uint256"
                            }
                          },
                          "nodeType": "Assignment",
                          "operator": "=",
                          "rightHandSide": {
                            "argumentTypes": null,
                            "commonType": {
                              "typeIdentifier": "t_uint256",
                              "typeString": "uint256"
                            },
                            "id": 2373,
                            "isConstant": false,
                            "isLValue": false,
                            "isPure": false,
                            "lValueRequested": false,
                            "leftExpression": {
                              "argumentTypes": null,
                              "baseExpression": {
                                "argumentTypes": null,
                                "id": 2368,
                                "name": "totalLentPerCustomer",
                                "nodeType": "Identifier",
                                "overloadedDeclarations": [],
                                "referencedDeclaration": 2007,
                                "src": "3908:20:9",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                                  "typeString": "mapping(address => uint256)"
                                }
                              },
                              "id": 2370,
                              "indexExpression": {
                                "argumentTypes": null,
                                "id": 2369,
                                "name": "customer",
                                "nodeType": "Identifier",
                                "overloadedDeclarations": [],
                                "referencedDeclaration": 2328,
                                "src": "3929:8:9",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_address",
                                  "typeString": "address"
                                }
                              },
                              "isConstant": false,
                              "isLValue": true,
                              "isPure": false,
                              "lValueRequested": false,
                              "nodeType": "IndexAccess",
                              "src": "3908:30:9",
                              "typeDescriptions": {
                                "typeIdentifier": "t_uint256",
                                "typeString": "uint256"
                              }
                            },
                            "nodeType": "BinaryOperation",
                            "operator": "-",
                            "rightExpression": {
                              "argumentTypes": null,
                              "expression": {
                                "argumentTypes": null,
                                "id": 2371,
                                "name": "msg",
                                "nodeType": "Identifier",
                                "overloadedDeclarations": [],
                                "referencedDeclaration": 11851,
                                "src": "3941:3:9",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_magic_message",
                                  "typeString": "msg"
                                }
                              },
                              "id": 2372,
                              "isConstant": false,
                              "isLValue": false,
                              "isPure": false,
                              "lValueRequested": false,
                              "memberName": "value",
                              "nodeType": "MemberAccess",
                              "referencedDeclaration": null,
                              "src": "3941:9:9",
                              "typeDescriptions": {
                                "typeIdentifier": "t_uint256",
                                "typeString": "uint256"
                              }
                            },
                            "src": "3908:42:9",
                            "typeDescriptions": {
                              "typeIdentifier": "t_uint256",
                              "typeString": "uint256"
                            }
                          },
                          "src": "3875:75:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        },
                        "id": 2375,
                        "nodeType": "ExpressionStatement",
                        "src": "3875:75:9"
                      }
                    ]
                  },
                  "id": 2377,
                  "nodeType": "IfStatement",
                  "src": "3612:346:9",
                  "trueBody": {
                    "id": 2364,
                    "nodeType": "Block",
                    "src": "3660:200:9",
                    "statements": [
                      {
                        "assignments": [
                          2340
                        ],
                        "declarations": [
                          {
                            "constant": false,
                            "id": 2340,
                            "name": "extraIncome",
                            "nodeType": "VariableDeclaration",
                            "scope": 2364,
                            "src": "3669:19:9",
                            "stateVariable": false,
                            "storageLocation": "default",
                            "typeDescriptions": {
                              "typeIdentifier": "t_uint256",
                              "typeString": "uint256"
                            },
                            "typeName": {
                              "id": 2339,
                              "name": "uint256",
                              "nodeType": "ElementaryTypeName",
                              "src": "3669:7:9",
                              "typeDescriptions": {
                                "typeIdentifier": "t_uint256",
                                "typeString": "uint256"
                              }
                            },
                            "value": null,
                            "visibility": "internal"
                          }
                        ],
                        "id": 2347,
                        "initialValue": {
                          "argumentTypes": null,
                          "commonType": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          },
                          "id": 2346,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "leftExpression": {
                            "argumentTypes": null,
                            "expression": {
                              "argumentTypes": null,
                              "id": 2341,
                              "name": "msg",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 11851,
                              "src": "3691:3:9",
                              "typeDescriptions": {
                                "typeIdentifier": "t_magic_message",
                                "typeString": "msg"
                              }
                            },
                            "id": 2342,
                            "isConstant": false,
                            "isLValue": false,
                            "isPure": false,
                            "lValueRequested": false,
                            "memberName": "value",
                            "nodeType": "MemberAccess",
                            "referencedDeclaration": null,
                            "src": "3691:9:9",
                            "typeDescriptions": {
                              "typeIdentifier": "t_uint256",
                              "typeString": "uint256"
                            }
                          },
                          "nodeType": "BinaryOperation",
                          "operator": "-",
                          "rightExpression": {
                            "argumentTypes": null,
                            "baseExpression": {
                              "argumentTypes": null,
                              "id": 2343,
                              "name": "totalLentPerCustomer",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 2007,
                              "src": "3703:20:9",
                              "typeDescriptions": {
                                "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                                "typeString": "mapping(address => uint256)"
                              }
                            },
                            "id": 2345,
                            "indexExpression": {
                              "argumentTypes": null,
                              "id": 2344,
                              "name": "customer",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 2328,
                              "src": "3724:8:9",
                              "typeDescriptions": {
                                "typeIdentifier": "t_address",
                                "typeString": "address"
                              }
                            },
                            "isConstant": false,
                            "isLValue": true,
                            "isPure": false,
                            "lValueRequested": false,
                            "nodeType": "IndexAccess",
                            "src": "3703:30:9",
                            "typeDescriptions": {
                              "typeIdentifier": "t_uint256",
                              "typeString": "uint256"
                            }
                          },
                          "src": "3691:42:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        },
                        "nodeType": "VariableDeclarationStatement",
                        "src": "3669:64:9"
                      },
                      {
                        "expression": {
                          "argumentTypes": null,
                          "id": 2352,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "leftHandSide": {
                            "argumentTypes": null,
                            "baseExpression": {
                              "argumentTypes": null,
                              "id": 2348,
                              "name": "totalLentPerCustomer",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 2007,
                              "src": "3742:20:9",
                              "typeDescriptions": {
                                "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                                "typeString": "mapping(address => uint256)"
                              }
                            },
                            "id": 2350,
                            "indexExpression": {
                              "argumentTypes": null,
                              "id": 2349,
                              "name": "customer",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 2328,
                              "src": "3763:8:9",
                              "typeDescriptions": {
                                "typeIdentifier": "t_address",
                                "typeString": "address"
                              }
                            },
                            "isConstant": false,
                            "isLValue": true,
                            "isPure": false,
                            "lValueRequested": true,
                            "nodeType": "IndexAccess",
                            "src": "3742:30:9",
                            "typeDescriptions": {
                              "typeIdentifier": "t_uint256",
                              "typeString": "uint256"
                            }
                          },
                          "nodeType": "Assignment",
                          "operator": "=",
                          "rightHandSide": {
                            "argumentTypes": null,
                            "hexValue": "30",
                            "id": 2351,
                            "isConstant": false,
                            "isLValue": false,
                            "isPure": true,
                            "kind": "number",
                            "lValueRequested": false,
                            "nodeType": "Literal",
                            "src": "3775:1:9",
                            "subdenomination": null,
                            "typeDescriptions": {
                              "typeIdentifier": "t_rational_0_by_1",
                              "typeString": "int_const 0"
                            },
                            "value": "0"
                          },
                          "src": "3742:34:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        },
                        "id": 2353,
                        "nodeType": "ExpressionStatement",
                        "src": "3742:34:9"
                      },
                      {
                        "expression": {
                          "argumentTypes": null,
                          "id": 2362,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "leftHandSide": {
                            "argumentTypes": null,
                            "baseExpression": {
                              "argumentTypes": null,
                              "id": 2354,
                              "name": "lendingBalances",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 1989,
                              "src": "3785:15:9",
                              "typeDescriptions": {
                                "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                                "typeString": "mapping(address => uint256)"
                              }
                            },
                            "id": 2356,
                            "indexExpression": {
                              "argumentTypes": null,
                              "id": 2355,
                              "name": "customer",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 2328,
                              "src": "3801:8:9",
                              "typeDescriptions": {
                                "typeIdentifier": "t_address",
                                "typeString": "address"
                              }
                            },
                            "isConstant": false,
                            "isLValue": true,
                            "isPure": false,
                            "lValueRequested": true,
                            "nodeType": "IndexAccess",
                            "src": "3785:25:9",
                            "typeDescriptions": {
                              "typeIdentifier": "t_uint256",
                              "typeString": "uint256"
                            }
                          },
                          "nodeType": "Assignment",
                          "operator": "=",
                          "rightHandSide": {
                            "argumentTypes": null,
                            "commonType": {
                              "typeIdentifier": "t_uint256",
                              "typeString": "uint256"
                            },
                            "id": 2361,
                            "isConstant": false,
                            "isLValue": false,
                            "isPure": false,
                            "lValueRequested": false,
                            "leftExpression": {
                              "argumentTypes": null,
                              "baseExpression": {
                                "argumentTypes": null,
                                "id": 2357,
                                "name": "lendingBalances",
                                "nodeType": "Identifier",
                                "overloadedDeclarations": [],
                                "referencedDeclaration": 1989,
                                "src": "3813:15:9",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                                  "typeString": "mapping(address => uint256)"
                                }
                              },
                              "id": 2359,
                              "indexExpression": {
                                "argumentTypes": null,
                                "id": 2358,
                                "name": "customer",
                                "nodeType": "Identifier",
                                "overloadedDeclarations": [],
                                "referencedDeclaration": 2328,
                                "src": "3829:8:9",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_address",
                                  "typeString": "address"
                                }
                              },
                              "isConstant": false,
                              "isLValue": true,
                              "isPure": false,
                              "lValueRequested": false,
                              "nodeType": "IndexAccess",
                              "src": "3813:25:9",
                              "typeDescriptions": {
                                "typeIdentifier": "t_uint256",
                                "typeString": "uint256"
                              }
                            },
                            "nodeType": "BinaryOperation",
                            "operator": "+",
                            "rightExpression": {
                              "argumentTypes": null,
                              "id": 2360,
                              "name": "extraIncome",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 2340,
                              "src": "3841:11:9",
                              "typeDescriptions": {
                                "typeIdentifier": "t_uint256",
                                "typeString": "uint256"
                              }
                            },
                            "src": "3813:39:9",
                            "typeDescriptions": {
                              "typeIdentifier": "t_uint256",
                              "typeString": "uint256"
                            }
                          },
                          "src": "3785:67:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        },
                        "id": 2363,
                        "nodeType": "ExpressionStatement",
                        "src": "3785:67:9"
                      }
                    ]
                  }
                },
                {
                  "eventCall": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "id": 2379,
                        "name": "customer",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 2328,
                        "src": "3990:8:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "expression": {
                          "argumentTypes": null,
                          "id": 2380,
                          "name": "msg",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 11851,
                          "src": "4000:3:9",
                          "typeDescriptions": {
                            "typeIdentifier": "t_magic_message",
                            "typeString": "msg"
                          }
                        },
                        "id": 2381,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "memberName": "value",
                        "nodeType": "MemberAccess",
                        "referencedDeclaration": null,
                        "src": "4000:9:9",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        },
                        {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      ],
                      "id": 2378,
                      "name": "RegisteredLoanIncome",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 9166,
                      "src": "3969:20:9",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_event_nonpayable$_t_address_$_t_uint256_$returns$__$",
                        "typeString": "function (address,uint256)"
                      }
                    },
                    "id": 2382,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "3969:41:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 2383,
                  "nodeType": "EmitStatement",
                  "src": "3964:46:9"
                }
              ]
            },
            "documentation": null,
            "id": 2385,
            "implemented": true,
            "kind": "function",
            "modifiers": [
              {
                "arguments": null,
                "id": 2325,
                "modifierName": {
                  "argumentTypes": null,
                  "id": 2324,
                  "name": "onlyLendingManager",
                  "nodeType": "Identifier",
                  "overloadedDeclarations": [],
                  "referencedDeclaration": 2037,
                  "src": "3541:18:9",
                  "typeDescriptions": {
                    "typeIdentifier": "t_modifier$__$",
                    "typeString": "modifier ()"
                  }
                },
                "nodeType": "ModifierInvocation",
                "src": "3541:18:9"
              }
            ],
            "name": "registerLoanIncome",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 2323,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 2322,
                  "name": "id",
                  "nodeType": "VariableDeclaration",
                  "scope": 2385,
                  "src": "3517:7:9",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_uint256",
                    "typeString": "uint256"
                  },
                  "typeName": {
                    "id": 2321,
                    "name": "uint",
                    "nodeType": "ElementaryTypeName",
                    "src": "3517:4:9",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "3516:9:9"
            },
            "returnParameters": {
              "id": 2326,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "3560:0:9"
            },
            "scope": 2386,
            "src": "3489:527:9",
            "stateMutability": "payable",
            "superFunction": 9171,
            "visibility": "public"
          }
        ],
        "scope": 2387,
        "src": "222:3797:9"
      }
    ],
    "src": "0:4020:9"
  },
  "compiler": {
    "name": "solc",
    "version": "0.5.3+commit.10d17f24.Emscripten.clang"
  },
  "networks": {
    "1551219753727": {
      "events": {
        "0xf8df31144d9c2f0f6b59d69b8b98abd5459d07f2742c4df920b25aae33c64820": {
          "anonymous": false,
          "inputs": [
            {
              "indexed": true,
              "name": "previousOwner",
              "type": "address"
            }
          ],
          "name": "OwnershipRenounced",
          "type": "event",
          "signature": "0xf8df31144d9c2f0f6b59d69b8b98abd5459d07f2742c4df920b25aae33c64820"
        },
        "0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0": {
          "anonymous": false,
          "inputs": [
            {
              "indexed": true,
              "name": "previousOwner",
              "type": "address"
            },
            {
              "indexed": true,
              "name": "newOwner",
              "type": "address"
            }
          ],
          "name": "OwnershipTransferred",
          "type": "event",
          "signature": "0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0"
        },
        "0x5d7c494485d85004eb95dd2894261ef8fc876bef99eec3e3577a6869bda29b6d": {
          "anonymous": false,
          "inputs": [
            {
              "indexed": false,
              "name": "relayer",
              "type": "address"
            },
            {
              "indexed": false,
              "name": "state",
              "type": "bool"
            }
          ],
          "name": "ToggledRelayer",
          "type": "event",
          "signature": "0x5d7c494485d85004eb95dd2894261ef8fc876bef99eec3e3577a6869bda29b6d"
        },
        "0xe001325effb580d8529fcf6c04532575ac739b436078041adae30263300be484": {
          "anonymous": false,
          "inputs": [
            {
              "indexed": false,
              "name": "manager",
              "type": "address"
            }
          ],
          "name": "SetLoanManager",
          "type": "event",
          "signature": "0xe001325effb580d8529fcf6c04532575ac739b436078041adae30263300be484"
        },
        "0x81ad7215cbec0a1d55a48f38b34525d2b677147ee7ceb872ac23ecca5ec966e5": {
          "anonymous": false,
          "inputs": [
            {
              "indexed": false,
              "name": "customer",
              "type": "address"
            },
            {
              "indexed": false,
              "name": "amount",
              "type": "uint256"
            }
          ],
          "name": "DepositedETH",
          "type": "event",
          "signature": "0x81ad7215cbec0a1d55a48f38b34525d2b677147ee7ceb872ac23ecca5ec966e5"
        },
        "0xaa21a47bc8f0df1a253be34a945b1d0a8f458e4da029c0ab59c1fee1e488c066": {
          "anonymous": false,
          "inputs": [
            {
              "indexed": false,
              "name": "customer",
              "type": "address"
            },
            {
              "indexed": false,
              "name": "amount",
              "type": "uint256"
            }
          ],
          "name": "WithdrewETH",
          "type": "event",
          "signature": "0xaa21a47bc8f0df1a253be34a945b1d0a8f458e4da029c0ab59c1fee1e488c066"
        },
        "0xa3e140a1160e0a4f93ae8cf2fe3e2df38a2ec3856f00a0f7bf4ff5e24d744797": {
          "anonymous": false,
          "inputs": [
            {
              "indexed": false,
              "name": "customer",
              "type": "address"
            },
            {
              "indexed": false,
              "name": "amount",
              "type": "uint256"
            }
          ],
          "name": "RegisteredLoanIncome",
          "type": "event",
          "signature": "0xa3e140a1160e0a4f93ae8cf2fe3e2df38a2ec3856f00a0f7bf4ff5e24d744797"
        }
      },
      "links": {},
      "address": "0xc8fE68643c1704DA5764e372a5840ae19643D45F",
      "transactionHash": "0xea63473711031abded378bbb77716ba792933aa4728e9bdbd31c5056afb649d9"
    },
    "1551235079185": {
      "events": {
        "0xf8df31144d9c2f0f6b59d69b8b98abd5459d07f2742c4df920b25aae33c64820": {
          "anonymous": false,
          "inputs": [
            {
              "indexed": true,
              "name": "previousOwner",
              "type": "address"
            }
          ],
          "name": "OwnershipRenounced",
          "type": "event",
          "signature": "0xf8df31144d9c2f0f6b59d69b8b98abd5459d07f2742c4df920b25aae33c64820"
        },
        "0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0": {
          "anonymous": false,
          "inputs": [
            {
              "indexed": true,
              "name": "previousOwner",
              "type": "address"
            },
            {
              "indexed": true,
              "name": "newOwner",
              "type": "address"
            }
          ],
          "name": "OwnershipTransferred",
          "type": "event",
          "signature": "0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0"
        },
        "0x5d7c494485d85004eb95dd2894261ef8fc876bef99eec3e3577a6869bda29b6d": {
          "anonymous": false,
          "inputs": [
            {
              "indexed": false,
              "name": "relayer",
              "type": "address"
            },
            {
              "indexed": false,
              "name": "state",
              "type": "bool"
            }
          ],
          "name": "ToggledRelayer",
          "type": "event",
          "signature": "0x5d7c494485d85004eb95dd2894261ef8fc876bef99eec3e3577a6869bda29b6d"
        },
        "0xe001325effb580d8529fcf6c04532575ac739b436078041adae30263300be484": {
          "anonymous": false,
          "inputs": [
            {
              "indexed": false,
              "name": "manager",
              "type": "address"
            }
          ],
          "name": "SetLoanManager",
          "type": "event",
          "signature": "0xe001325effb580d8529fcf6c04532575ac739b436078041adae30263300be484"
        },
        "0x81ad7215cbec0a1d55a48f38b34525d2b677147ee7ceb872ac23ecca5ec966e5": {
          "anonymous": false,
          "inputs": [
            {
              "indexed": false,
              "name": "customer",
              "type": "address"
            },
            {
              "indexed": false,
              "name": "amount",
              "type": "uint256"
            }
          ],
          "name": "DepositedETH",
          "type": "event",
          "signature": "0x81ad7215cbec0a1d55a48f38b34525d2b677147ee7ceb872ac23ecca5ec966e5"
        },
        "0xaa21a47bc8f0df1a253be34a945b1d0a8f458e4da029c0ab59c1fee1e488c066": {
          "anonymous": false,
          "inputs": [
            {
              "indexed": false,
              "name": "customer",
              "type": "address"
            },
            {
              "indexed": false,
              "name": "amount",
              "type": "uint256"
            }
          ],
          "name": "WithdrewETH",
          "type": "event",
          "signature": "0xaa21a47bc8f0df1a253be34a945b1d0a8f458e4da029c0ab59c1fee1e488c066"
        },
        "0xa3e140a1160e0a4f93ae8cf2fe3e2df38a2ec3856f00a0f7bf4ff5e24d744797": {
          "anonymous": false,
          "inputs": [
            {
              "indexed": false,
              "name": "customer",
              "type": "address"
            },
            {
              "indexed": false,
              "name": "amount",
              "type": "uint256"
            }
          ],
          "name": "RegisteredLoanIncome",
          "type": "event",
          "signature": "0xa3e140a1160e0a4f93ae8cf2fe3e2df38a2ec3856f00a0f7bf4ff5e24d744797"
        }
      },
      "links": {},
      "address": "0x9D1408c15D47baA11b8CB9A31855A1e1803A2483",
      "transactionHash": "0x68b56262d2912291cbc87ef45b71e64deb591127c81fef638cadfae037eed475"
    },
    "1551235556769": {
      "events": {
        "0xf8df31144d9c2f0f6b59d69b8b98abd5459d07f2742c4df920b25aae33c64820": {
          "anonymous": false,
          "inputs": [
            {
              "indexed": true,
              "name": "previousOwner",
              "type": "address"
            }
          ],
          "name": "OwnershipRenounced",
          "type": "event",
          "signature": "0xf8df31144d9c2f0f6b59d69b8b98abd5459d07f2742c4df920b25aae33c64820"
        },
        "0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0": {
          "anonymous": false,
          "inputs": [
            {
              "indexed": true,
              "name": "previousOwner",
              "type": "address"
            },
            {
              "indexed": true,
              "name": "newOwner",
              "type": "address"
            }
          ],
          "name": "OwnershipTransferred",
          "type": "event",
          "signature": "0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0"
        },
        "0x5d7c494485d85004eb95dd2894261ef8fc876bef99eec3e3577a6869bda29b6d": {
          "anonymous": false,
          "inputs": [
            {
              "indexed": false,
              "name": "relayer",
              "type": "address"
            },
            {
              "indexed": false,
              "name": "state",
              "type": "bool"
            }
          ],
          "name": "ToggledRelayer",
          "type": "event",
          "signature": "0x5d7c494485d85004eb95dd2894261ef8fc876bef99eec3e3577a6869bda29b6d"
        },
        "0xe001325effb580d8529fcf6c04532575ac739b436078041adae30263300be484": {
          "anonymous": false,
          "inputs": [
            {
              "indexed": false,
              "name": "manager",
              "type": "address"
            }
          ],
          "name": "SetLoanManager",
          "type": "event",
          "signature": "0xe001325effb580d8529fcf6c04532575ac739b436078041adae30263300be484"
        },
        "0x81ad7215cbec0a1d55a48f38b34525d2b677147ee7ceb872ac23ecca5ec966e5": {
          "anonymous": false,
          "inputs": [
            {
              "indexed": false,
              "name": "customer",
              "type": "address"
            },
            {
              "indexed": false,
              "name": "amount",
              "type": "uint256"
            }
          ],
          "name": "DepositedETH",
          "type": "event",
          "signature": "0x81ad7215cbec0a1d55a48f38b34525d2b677147ee7ceb872ac23ecca5ec966e5"
        },
        "0xaa21a47bc8f0df1a253be34a945b1d0a8f458e4da029c0ab59c1fee1e488c066": {
          "anonymous": false,
          "inputs": [
            {
              "indexed": false,
              "name": "customer",
              "type": "address"
            },
            {
              "indexed": false,
              "name": "amount",
              "type": "uint256"
            }
          ],
          "name": "WithdrewETH",
          "type": "event",
          "signature": "0xaa21a47bc8f0df1a253be34a945b1d0a8f458e4da029c0ab59c1fee1e488c066"
        },
        "0xa3e140a1160e0a4f93ae8cf2fe3e2df38a2ec3856f00a0f7bf4ff5e24d744797": {
          "anonymous": false,
          "inputs": [
            {
              "indexed": false,
              "name": "customer",
              "type": "address"
            },
            {
              "indexed": false,
              "name": "amount",
              "type": "uint256"
            }
          ],
          "name": "RegisteredLoanIncome",
          "type": "event",
          "signature": "0xa3e140a1160e0a4f93ae8cf2fe3e2df38a2ec3856f00a0f7bf4ff5e24d744797"
        }
      },
      "links": {},
      "address": "0xc8fE68643c1704DA5764e372a5840ae19643D45F",
      "transactionHash": "0xea63473711031abded378bbb77716ba792933aa4728e9bdbd31c5056afb649d9"
    },
    "1551267015172": {
      "events": {
        "0xf8df31144d9c2f0f6b59d69b8b98abd5459d07f2742c4df920b25aae33c64820": {
          "anonymous": false,
          "inputs": [
            {
              "indexed": true,
              "name": "previousOwner",
              "type": "address"
            }
          ],
          "name": "OwnershipRenounced",
          "type": "event",
          "signature": "0xf8df31144d9c2f0f6b59d69b8b98abd5459d07f2742c4df920b25aae33c64820"
        },
        "0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0": {
          "anonymous": false,
          "inputs": [
            {
              "indexed": true,
              "name": "previousOwner",
              "type": "address"
            },
            {
              "indexed": true,
              "name": "newOwner",
              "type": "address"
            }
          ],
          "name": "OwnershipTransferred",
          "type": "event",
          "signature": "0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0"
        },
        "0x5d7c494485d85004eb95dd2894261ef8fc876bef99eec3e3577a6869bda29b6d": {
          "anonymous": false,
          "inputs": [
            {
              "indexed": false,
              "name": "relayer",
              "type": "address"
            },
            {
              "indexed": false,
              "name": "state",
              "type": "bool"
            }
          ],
          "name": "ToggledRelayer",
          "type": "event",
          "signature": "0x5d7c494485d85004eb95dd2894261ef8fc876bef99eec3e3577a6869bda29b6d"
        },
        "0xe001325effb580d8529fcf6c04532575ac739b436078041adae30263300be484": {
          "anonymous": false,
          "inputs": [
            {
              "indexed": false,
              "name": "manager",
              "type": "address"
            }
          ],
          "name": "SetLoanManager",
          "type": "event",
          "signature": "0xe001325effb580d8529fcf6c04532575ac739b436078041adae30263300be484"
        },
        "0x81ad7215cbec0a1d55a48f38b34525d2b677147ee7ceb872ac23ecca5ec966e5": {
          "anonymous": false,
          "inputs": [
            {
              "indexed": false,
              "name": "customer",
              "type": "address"
            },
            {
              "indexed": false,
              "name": "amount",
              "type": "uint256"
            }
          ],
          "name": "DepositedETH",
          "type": "event",
          "signature": "0x81ad7215cbec0a1d55a48f38b34525d2b677147ee7ceb872ac23ecca5ec966e5"
        },
        "0xaa21a47bc8f0df1a253be34a945b1d0a8f458e4da029c0ab59c1fee1e488c066": {
          "anonymous": false,
          "inputs": [
            {
              "indexed": false,
              "name": "customer",
              "type": "address"
            },
            {
              "indexed": false,
              "name": "amount",
              "type": "uint256"
            }
          ],
          "name": "WithdrewETH",
          "type": "event",
          "signature": "0xaa21a47bc8f0df1a253be34a945b1d0a8f458e4da029c0ab59c1fee1e488c066"
        },
        "0xa3e140a1160e0a4f93ae8cf2fe3e2df38a2ec3856f00a0f7bf4ff5e24d744797": {
          "anonymous": false,
          "inputs": [
            {
              "indexed": false,
              "name": "customer",
              "type": "address"
            },
            {
              "indexed": false,
              "name": "amount",
              "type": "uint256"
            }
          ],
          "name": "RegisteredLoanIncome",
          "type": "event",
          "signature": "0xa3e140a1160e0a4f93ae8cf2fe3e2df38a2ec3856f00a0f7bf4ff5e24d744797"
        }
      },
      "links": {},
      "address": "0x026b59108F194C14916443421C74A53b98B4524B",
      "transactionHash": "0x688ba49accff9be2a5e90b02b977e7ed0af6d83d13629884fb049a247bdaf4ba"
    }
  },
  "schemaVersion": "3.0.2",
  "updatedAt": "2019-02-27T11:37:56.576Z",
  "devdoc": {
    "methods": {
      "renounceOwnership()": {
        "details": "Allows the current owner to relinquish control of the contract."
      },
      "transferOwnership(address)": {
        "details": "Allows the current owner to transfer control of the contract to a newOwner.",
        "params": {
          "_newOwner": "The address to transfer ownership to."
        }
      }
    }
  },
  "userdoc": {
    "methods": {
      "renounceOwnership()": {
        "notice": "Renouncing to ownership will leave the contract without an owner. It will not be possible to call the functions with the `onlyOwner` modifier anymore."
      }
    }
  }
}

module.exports = {

  lendingTreasury

}
