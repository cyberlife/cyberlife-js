var cashbackRelayer = require("./cashbackRelayer.js");
var localKyberRelayer = require("./localKyberRelayer.js");
var localLendingRelayer = require("./lendingRelayer.js");

var net = "local"

cashbackRelayer.startMonitoring(net, "websocket")
localKyberRelayer.start(net)
localLendingRelayer.watchLoans()
