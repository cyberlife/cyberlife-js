var houseABI = require('./abi/ClearingHouse.js');
var contractCaller = require('./crypto/contractCaller.js');
var contractsAddresses = require('./crypto/contractsAddresses.js');
var accounts = require('./crypto/accounts.js');
var utils = require('./crypto/utils.js');

var waitingToExecute = [];

async function startMonitoring(network, providerType) {

  var web3 = await contractCaller.getWeb3(network, providerType);

  var personalAddress = accounts.local[0];
  var privateKey = ""
  var houseAddress = contractsAddresses.localContracts[0];

  if (network == "thunder") {

    personalAddress = accounts.thunder[0];
    privateKey = accounts.thunderPrivate[0];

    houseAddress = contractsAddresses.thunderContracts[0]

  } else if (network == "rinkeby") {

    personalAddress = accounts.rinkeby[0];
    privateKey = accounts.rinkebyPrivate[0];
    houseAddress = contractsAddresses.rinkebyContracts[0]

  }

  var houseInstance = await utils.getContractInstance(network, "websocket", "house");

  console.log("CASHBACK-RELAYER: Started to listen for clearing house events")

  houseInstance.events.allEvents({
      fromBlock: 'latest'
  }, async (error, event) => {

    if (error) console.log(error);

    else {

      if (event.event == 'NewOrder') {

        if (event.returnValues.manualFundFill == false) {

          await contractCaller.processOrder(network, houseInstance,
                                            personalAddress, privateKey,
                                            event.returnValues.sender,
                                            event.returnValues.orderPosition);

        } else {

          waitingToExecute.push([

            event.returnValues.sender,
            event.returnValues.orderPosition

          ]);

        }

      } else if (event.event == 'ProcessedOrder') {

        console.log("CASHBACK-RELAYER: Processed the order with id " + event.returnValues.orderNumber)

      }

    }

  })

}

async function clearUnexecuted(_net) {

  //TODO

}

module.exports = {

  startMonitoring

}
