var utils = require("./crypto/utils.js");
var contractCaller = require("./crypto/contractCaller.js");
var cryptoPrices = require("./crypto/cryptoPrices.js");
var kyberTradingAPI = require("./crypto/kyberTradingAPI.js");
var accounts = require("./crypto/accounts.js");

var BigNumber = require('bignumber.js')

//ETH, KNC, MANA, ZILL, OMG

var kyberLocalTokens = [

  "0xeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee",
  "0xC3687e0278166dBB7EBcFD439fFdc87408442324",
  "0xE64709Bd8515982EEB0AC0B64D5c9aB1ba637b08",
  "0xb495ca8c0a5b60a501263579122944fB764C676f",
  "0x14bf65958be4f67DcA8864998CF5B33612A7294D"

];

var kyberMainTokens = [

  "0xeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee",
  "0xdd974d5c2e2928dea5f71b9825b8b646686bd200",
  "0x0f5d2fb29fb7d3cfee444a200298f468908cc942",
  "0xd26114cd6ee289accf82350c8d8487fedb8a0c07",
  "0x05f4a42e251f2d52b8ed15e9fedaacfcef1fad27"

];

var customers = [

  "0x6259ac218eed8caf47e26246d7e13c1df70165f2"

];

var latestPrices = [

  0,0,0,0,0

];

var minPriceChange = 0.01;
var toSwap = 1000;

async function start(network) {

  var rebalancerInstance =
    await utils.getContractInstance(network, "http", "rebalancer");

  var balances = await getCustomerBalances(network);

  cryptoPrices.getCryptoPrice(['ETH', 'KNC', 'MANA', 'ZIL', 'OMG'], "USD",
    async function(err, res) {

    if (err) {

      console.log("KYBER-REBALANCER: Could not get crypto prices. Wait more until you make another API request...");

      setTimeout(start, 5000, "local");

    } else {

      if (latestPrices[0] == 0) {

        console.log("KYBER-REBALANCER: Fetched prices for the first time")

        setLatestPrices(res);

        setTimeout(start, 5000, "local");

      } else {

        var swapped = await getSwappedCoins(res)

        var smallestChange = await getStableCoin(res);

        initiateSwappings(balances, network, swapped, smallestChange, rebalancerInstance);

        //Because of tx underpricing in ethereum when past txs are still in the mempool, we should avoid billing regularly in the demo

      //  billAllCustomers(network);

        setLatestPrices(res);

        setTimeout(start, 20000, "local");

      }

    }

  })

}

async function billAllCustomers(network) {

  var rebalancerData =
    await utils.getContractInstance(network, "http", "rebalancerData");

  for (var i = 0; i < customers.length; i++) {

    await contractCaller
    .billCustomer(network, rebalancerData, accounts.local[0],
                  accounts.local[0],
                  customers[i])

  }

}

async function setLatestPrices(pricesObj) {

  var prices = Object.values(pricesObj);

  for (var i = 0; i < prices.length; i++) {

    latestPrices[i] = prices[i].USD;

  }

}

async function getSwapType(smallestChange, targetToSwap) {

  if (targetToSwap == smallestChange) return "NOTHING";

  else if (targetToSwap > 0 && smallestChange == 0) return "TOKEN-ETH";

  else if (targetToSwap == 0 && smallestChange > 0) return "ETH-TOKEN";

  else if (targetToSwap > 0 && smallestChange > 0)  return "TOKEN-TOKEN";

}

async function getStableCoin(pricesObj) {

  var prices = Object.values(pricesObj);

  var stable = 0;
  var mainDifference = Math.abs(prices[0].USD - latestPrices[0]);

  for (var i = 1; i < prices.length; i++) {

    if (Math.abs(prices[i].USD - latestPrices[i]) < mainDifference) {

      mainDifference = Math.abs(prices[i].USD - latestPrices[i]);
      stable = i;

    }

  }

  return stable;

}

async function getCustomerBalances(network) {

  var balances = [];
  var currentBalances = [];

  var specificTokenBalance = 0;

  var rebalancerInstance =
  await utils.getContractInstance(network, "http", "rebalancer");

  for (var i = 0; i < customers.length; i++) {

    for (var j = 0; j < kyberLocalTokens.length; j++) {

      specificTokenBalance =
      await contractCaller
      .getCustomerBalance(network, rebalancerInstance, customers[0],
        customers[i], kyberLocalTokens[j]);

      currentBalances.push(specificTokenBalance);

    }

    balances.push(currentBalances.concat());

    currentBalances.length = 0;

    currentBalances = [];

  }

  return balances;

}

async function initiateSwappings(balances, network,
  swapped, smallestChange, rebalancerInstance) {

  var swapType = ''

  var j;

  var swapAmount = 0;

  var alreadyBeingSwapped = [];

  for (var m = 0; m < customers.length; m++) {

    alreadyBeingSwapped.push(0);

  }

  for (var i = 0; i < swapped.length; i++) {

    if (swapped[i] == 1) {

      swapType = await getSwapType(smallestChange, i);

      if (swapType == 'TOKEN-ETH') {

        for (j = 0; j < balances.length; j++) {

          if (balances[j][i] > 0 && alreadyBeingSwapped[j] == 0) {

            if (balances[j][i] > toSwap) {

              swapAmount = toSwap;

            } else {

              swapAmount = balances[j][i];

            }

            alreadyBeingSwapped[j] = 1;

            console.log("KYBER-REBALANCER: Performing a TOKEN-ETH swap for " + customers[j].toString())

            var destAmount = await getDestAmount(smallestChange, i, swapAmount);

            contractCaller
            .swapTokenToEther(network, rebalancerInstance,
                              accounts.local[0], accounts.local[0],
                              kyberLocalTokens[i], customers[j],
                              swapAmount, destAmount)

          }

        }

      } else if (swapType == "ETH-TOKEN") {

        for (j = 0; j < balances.length; j++) {

          if (balances[j][i] > 0 && alreadyBeingSwapped[j] == 0) {

            if (balances[j][i] > toSwap) {

              swapAmount = toSwap;

            } else {

              swapAmount = balances[j][i];

            }

            alreadyBeingSwapped[j] = 1;

            console.log("KYBER-REBALANCER: Performing an ETH-TOKEN swap for " + customers[j].toString())

            var destAmount = await getDestAmount(smallestChange, i, swapAmount);

            contractCaller
            .swapEtherToToken(network, rebalancerInstance,
                              accounts.local[0], accounts.local[0],
                              kyberLocalTokens[smallestChange], customers[j],
                              swapAmount, destAmount)

          }

        }

      } else if (swapType == "TOKEN-TOKEN") {

        for (j = 0; j < balances.length; j++) {

          if (balances[j][i] > 0 && alreadyBeingSwapped[j] == 0) {

            if (balances[j][i] > toSwap) {

              swapAmount = toSwap;

            } else {

              swapAmount = balances[j][i];

            }

            alreadyBeingSwapped[j] = 1;

            console.log("KYBER-REBALANCER: Performing a TOKEN-TOKEN swap for " + customers[j].toString())

            var destAmount = await getDestAmount(smallestChange, i, swapAmount);

            contractCaller
            .swapTokenToToken(network, rebalancerInstance,
                              accounts.local[0], accounts.local[0],
                              kyberLocalTokens[i], swapAmount, kyberLocalTokens[j],
                              destAmount, customers[j])

          }

        }

      }

    }

  }

}

async function getDestAmount(smallestChange, source, sourceAmount) {

  var destAmount = 0;

  if (source == 0) {

    destAmount = await kyberTradingAPI.getSellRates(kyberMainTokens[smallestChange], sourceAmount);

  } else if (smallestChange == 0) {

    destAmount = await kyberTradingAPI.getBuyRates(kyberMainTokens[source], sourceAmount);

  } else if (source != 0 && smallestChange != 0) {

    destAmount = "100000";

    return destAmount;

  }

  return (new BigNumber(destAmount * 10 ** 18)).toString();

}

async function getSwappedCoins(pricesObj) {

  var prices = Object.values(pricesObj);

  var swapped = [0,0,0,0,0];

  for (var i = 0; i < prices.length; i++) {

    if (Math.abs(prices[i].USD - latestPrices[i]) >
        latestPrices[i] * minPriceChange / 100) {

      swapped[i] = 1;

    }

  }

  return swapped;

}

module.exports = {

  start

}
