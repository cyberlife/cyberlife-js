pragma solidity 0.4.25;

contract IAgentsTreasury {

    event ChangedShardModifications(address _modifications);

    event ChangedDeploymentRequests(address _deployment);

    event Transfer(address indexed host, uint256 amount);

    event ChangedPaymentWindow(uint256 window);

    event ChangedMerkleUtils(address utils);

    event ChangedAgentManagement(address management);

    event RegisteredAgent(string name, bytes root, string ipfs, uint256 hosts);

    event UnregisteredAgent(string name);

    event AppliedModification(string name, bytes root, string ipfs);

    event Funded(address benefactor, string name);

    event EarnedMoney(address agent, uint256 amount);

    event WithdrewFunds(address who, string name);

    event SetHostManagement(address hostM);

    event GotPaid(address host, string name, bytes proof, uint256 timeWhenStartedHosting, uint256 secondsToHost,
                     uint256 pricePerSecond);

    //PUBLIC

    function setShardModifications(address _shardModifications) public;

    function setHostManagement(address hostM) public;

    function changeDeploymentRequestAddr(address _deploymentReq) public;

    function changePaymentWindow(uint256 window) public;

    function changeAgentManagement(address _newManagement) public;

    function registerAgent(string _name, bytes first_root, string ipfs, uint256 hostsNumber) public payable;

    function unregisterAgent(string _name) public;

    function applyModification(string _name, uint256[] intData, bytes _newRoot, string ipfsProof, address[] hosts,
                                uint256[] secondsToHost, uint256[] currentPrices) public payable;

    function fund(string _name) public payable;

    function addEarnedFunds(address toAgent) public payable;

    function withdrawFunds(string _name) public returns (uint256, uint256, uint256);



    function getPaid(string _name, bytes _proof, uint256 timeStartedHostingAgent, uint256 secondsToHost,
                     uint256 pricePerSecond) public returns (uint256, uint256, uint256, uint256);

    function getAgentManagementAddr() public view returns (address);

    function getPaymentWindow() public view returns (uint256);

    function agentAlreadyRegistered(string _name) public view returns (bool);

    function getAgentIntData(string _name) public view returns (uint256, uint256, uint256, uint256);

    function getAgentRoot(string _name) public view returns (uint256, bytes, string, uint256);

    function agentIsRegistered(string _name) public view returns (bool);

    function getEarnedFunds(address agent) public view returns (uint256);

    function getSecondsBilled(address host, string _name) public view returns (uint256);

    function getContribution(address agent, string _name) public view returns (uint256);

    function getSecondsToBill(address host, string _agent, uint256 secondsToHost) public view returns (uint256);

}
