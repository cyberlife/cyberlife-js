pragma solidity 0.4.25;

contract IAgentManagement {

    event CreatedNewAgent(address creator, uint256 positionInArray);

    event MadeAutonomous(address creator, uint256 position);

    event UpdatedCode(address caller, address creator, uint256 position, string codeLocation, bytes32 codeHash);

    event ChangedAgentContract(address caller, address creator, uint256 position, address newContract);

    event ChangedShardSize(uint256 newSize);


    function setEphemeral(address _ephemeral) public;

    function setTreasury(address _treasury) public;

    function changeMinimumShardSize(uint256 newShardSize) public;

    function newAgent(bool agentIsCreator,
        string _agent,
        uint256 position,
        string _name,
        string open_source,
        bytes32 code_hash,
        address newBotAddress) public;

    function deleteAgent(string _name, uint256 position) public;

    function makeFullyAutonomous(string _name, uint256 position) public;

    function updateCode(string _name, uint256 position, string codeSource, bytes32 codeHash) public;


    function getMinimumShardSize() public view returns (uint256);

    function getAgentControllingAgent(string _controlledAgent) public view returns (string);

    function getCreatorIsAgent(address creator, uint256 position) public view returns (bool);

    function getCreatorFromName(string name) public view returns (address);

    function getPositionFromName(string name) public view returns (uint256);

    function getAgentName(address creator, uint256 position) public view returns (string);

    function getAgentVersion(address creator, uint256 position) public view returns (uint256);

    function getAgentSourceLocation(address creator, uint256 position) public view returns (string);

    function getCurrentController(address creator, uint256 position) public view returns (address);

    function getAgentAddress(address creator, uint256 position) public view returns (address);

    function getAgentCodeHash(address creator, uint256 position) public view returns (bytes32);

    function getAgentAutonomy(address creator, uint256 position) public view returns (bool);

    function isNameTaken(string _name) public view returns (bool);

}
