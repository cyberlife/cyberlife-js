pragma solidity 0.4.25;

import "contracts/zeppelin/ERC20/MintableToken.sol";

//NEW: CHANGED CONTRACT NAME

contract WorkToken is MintableToken{

    address creator;

    string name;
    string symbol;

    uint256 decimals;

    constructor(string _name, string _symbol, uint256 _decimals) {

        name = _name;
        symbol = _symbol;
        decimals = _decimals;

    }

    function getName() public view returns (string) {

        return name;

    }

    function getSymbol() public view returns (string) {

        return symbol;

    }

    function getDecimals() public view returns (uint256) {

        return decimals;

    }

}
