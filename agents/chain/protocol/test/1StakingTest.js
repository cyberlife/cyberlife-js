const Staking = artifacts.require("contracts/security/Staking.sol")
const WorkToken = artifacts.require("contracts/token/WorkToken.sol")

const assertFail = require("./helpers/assertFail");

const increaseTime = require('./helpers/increaseTime');

contract('Staking', function(accounts) {

    let staking, work;

    beforeEach(async () => {

        work = await WorkToken.new("FUEL", "FUE", 18, {from: accounts[0]});

        staking = await Staking.new(work.address, 200000000000000000000, {from: accounts[0]});

        await work.mint(accounts[0], 1000000000000000000000000000, {from: accounts[0]});

        await work.mint(accounts[1], 1000000000000000000000000000, {from: accounts[0]});

        await work.mint(accounts[2], 1000000000000000000000000000, {from: accounts[0]});

        await work.approve(staking.address, 300000000000000000000, {from: accounts[1]});

        await work.approve(staking.address, 300000000000000000000, {from: accounts[2]});

    });

    it("Stake and unstake", async () => {

        await staking.stake({from: accounts[1]});

        await increaseTime.increaseTimeTo(web3.eth.getBlock(web3.eth.blockNumber).timestamp + increaseTime.duration.days(4));

        await staking.signalWithdraw({from: accounts[1]});

        await increaseTime.increaseTimeTo(web3.eth.getBlock(web3.eth.blockNumber).timestamp + increaseTime.duration.weeks(14));

        await staking.withdraw({from: accounts[1]});

    });

    it("Slash host", async () => {

        await staking.stake({from: accounts[2]});

        var stake = await staking.getAmountStaked(accounts[2])

        assert(stake == 200000000000000000000);

        await staking.slash("Bad host", accounts[2], 10);

        assert.equal(await staking.getAmountStaked(accounts[2]), 200000000000000000000 - 10)

    })

});
