const AgentManagement = artifacts.require("contracts/agents/AgentManagement.sol")
const AgentObligations = artifacts.require("contracts/agents/AgentObligations.sol")

contract('AgentObligations', function(accounts) {

  let management, obligations

  beforeEach(async () => {

    management = await AgentManagement.new(1, {from: accounts[0]})

    obligations = await AgentObligations.new(management.address, {from: accounts[0]})

    await management.newAgent(false, "", 0, "Simple Agent",
      "www.github.com/simple_agent", "codeHash", accounts[4], {from: accounts[0]});

  })

  it("Should add an obligation and change it", async () => {

    await obligations.changeObligation("0x0", "Simple Agent", 5, {from: accounts[0]})

    assert.equal(await obligations.getObligationsLength(accounts[4]), 1)

    var receiver = await obligations.getObligationReceiver(accounts[4], 0)

    var payment = await obligations.getObligationPayment(accounts[4], 0)

    assert.equal(receiver, accounts[0])

    assert.equal(payment, 5)

    assert.equal(5, await obligations.getTotalObligation(accounts[4]))

    await obligations.changeObligation("0x0", "Simple Agent", 50, {from: accounts[0]})

    assert.equal(await obligations.getObligationsLength(accounts[4]), 1)

    receiver = await obligations.getObligationReceiver(accounts[4], 0)

    payment = await obligations.getObligationPayment(accounts[4], 0)

    assert.equal(receiver, accounts[0])

    assert.equal(payment, 50)

    assert.equal(50, await obligations.getTotalObligation(accounts[4]))

  })

  it("Should delete obligation", async () => {

    await obligations.changeObligation("0x0", "Simple Agent", 5, {from: accounts[0]})

    await obligations.deleteObligation(accounts[4], 0, {from: accounts[0]})

    assert.equal(await obligations.getObligationsLength(accounts[4]), 0)

  })

  it("Should delete all obligations", async () => {

    await obligations.changeObligation("0x0", "Simple Agent", 5, {from: accounts[0]})

    await obligations.changeObligation("0x0", "Simple Agent", 5, {from: accounts[1]})

    assert.equal(await obligations.getObligationsLength(accounts[4]), 2)

    await obligations.deleteAllObligations(accounts[4])

    assert.equal(await obligations.getObligationsLength(accounts[4]), 0)

  })

  it("Create obligation for someone else", async () => {

    await obligations.changeObligation(accounts[1], "Simple Agent", 5, {from: accounts[0]})

    assert.equal(await obligations.getObligationsLength(accounts[4]), 1)

    var receiver = await obligations.getObligationReceiver(accounts[4], 0)

    var payment = await obligations.getObligationPayment(accounts[4], 0)

    assert.equal(receiver, accounts[1])

    assert.equal(payment, 5)

  })

})
