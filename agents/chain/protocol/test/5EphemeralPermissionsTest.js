const EphemeralPermissions = artifacts.require("contracts/agents/EphemeralPermissions.sol")
const HostManagement = artifacts.require("contracts/hosts/HostManagement.sol")
const Staking = artifacts.require("contracts/security/Staking.sol")
const WorkToken = artifacts.require("contracts/token/WorkToken.sol")

var sha3 = require('ethereumjs-util').sha3

const timeManipulation = require("./helpers/timeManipulation")

contract('EphemeralPermissions', function(accounts) {

  let permissions, hostManagement, staking, work

  let ID1 = '0000000000000000000000000000000000000000111111'

  beforeEach(async () => {

    work = await WorkToken.new("WORK", "WOR", 18, {from: accounts[0]})

    staking = await Staking.new(work.address, 200000000000000000000, {from: accounts[0]})

    await work.mint(accounts[0], 1000000000000000000000000000, {from: accounts[0]})

    await work.mint(accounts[1], 1000000000000000000000000000, {from: accounts[0]})

    await work.approve(staking.address, 300000000000000000000, {from: accounts[1]})


    hostManagement = await HostManagement.new(staking.address, staking.address, {from: accounts[0]});

    permissions = await EphemeralPermissions.new(30, hostManagement.address, {from: accounts[0]})


    await staking.stake({from: accounts[1]})

    await hostManagement.setID(ID1, {from: accounts[1]})

    await hostManagement.changeRent(1, {from: accounts[1]})

    await timeManipulation.advanceTimeAndBlock(50000);

    await hostManagement.broadcastActivity({from: accounts[1]})


    await permissions.toggleAllowedContract(accounts[2], {from: accounts[0]})

  })

  it("Propose permission and use it", async () => {

    var codeHash = '0x' + sha3("newHash").toString('hex')

    var createAgentParamsHash = '0x' + sha3("paramsHash").toString('hex')

    var executionProof = '0x' + sha3("executionProof").toString('hex')

    await permissions.proposePermission('Simple Agent', createAgentParamsHash,
      executionProof, accounts[2], "newAgent", "hostingProof", {from: accounts[1]})

    assert.equal(await permissions.getContractWhereUsed('Simple Agent', 0), accounts[2])

    assert.equal(await permissions.getHashOfParams('Simple Agent', 0), createAgentParamsHash)

    assert.equal(await permissions.getFunctionWhereUsed('Simple Agent', 0), "newAgent")

    assert.equal(await permissions.getHost('Simple Agent', 0), accounts[1])

    assert.equal(1, await permissions.getLatestNonce('Simple Agent'))

    assert.equal(await permissions.getTrustedExecutionProof('Simple Agent', 0), executionProof)

    assert.equal(true, await permissions.canUsePermission('Simple Agent', 0,
      accounts[2], 'newAgent', accounts[1], createAgentParamsHash))

    await permissions.usePermission('Simple Agent', 0, {from: accounts[2]})

    assert.equal(true, await permissions.getUsed('Simple Agent', 0))

  })

})
