const DeploymentRequests = artifacts.require("contracts/agents/DeploymentRequests.sol");
const Staking = artifacts.require("contracts/security/Staking.sol");
const AgentManagement = artifacts.require("contracts/agents/AgentManagement.sol");
const ShardModifications = artifacts.require("contracts/agents/ShardModifications.sol");
const AgentsTreasury = artifacts.require("contracts/agents/AgentsTreasury.sol");
const WorkToken = artifacts.require("contracts/token/WorkToken.sol");
const HostManagement = artifacts.require("contracts/hosts/HostManagement.sol");
var AssigningJobs = artifacts.require('contracts/hosts/AssigningJobs.sol');

const assertFail = require("./helpers/assertFail");

const increaseTime = require('./helpers/increaseTime');

const {sha3} = require('ethereumjs-util')
const crypto = require('crypto')
const MerkleTree = require('merkletreejs')

contract('Deployments, shard modifications and treasury tests', function(accounts) {

    let requests, staking, management, shardModif, pools, treasury, hostManagement, assigningJobs;

    let baseStake = 200000000000000000000;
    let withdrawal_time = 7;

    let paymentWindow = 100000;

    let batch_size = 3;
    let modifVoteThreshold = 2;
    let votingWindow = 60;

    let minimumExecutionTime = 30

    let defaultID = 'QaaaaaaaaaQaaaaaaaaaQaaaaaaaaaQaaaaaaaaaQaaaaa'

    beforeEach(async () => {

        fuel = await WorkToken.new("WORK", "WOR", 18, {from: accounts[0]});

        staking = await Staking.new(fuel.address, baseStake, {from: accounts[0]});

        management = await AgentManagement.new(3, {from: accounts[0]});

        shardModif = await ShardModifications.new(batch_size, modifVoteThreshold, votingWindow, {from: accounts[0]});

        treasury = await AgentsTreasury.new(paymentWindow, {from: accounts[0]});

        requests = await DeploymentRequests.new({from: accounts[0]});

        hostManagement = await HostManagement.new(staking.address, treasury.address, {from: accounts[0]});

        assigningJobs = await AssigningJobs.new(hostManagement.address, management.address, treasury.address, {from: accounts[0]});


        await requests.setTreasuryDeployment(treasury.address, {from: accounts[0]});

        await requests.setShardModifications(shardModif.address, {from: accounts[0]});

        await requests.changeAgentManagement(management.address, {from: accounts[0]});


        await shardModif.setTreasury(treasury.address, {from: accounts[0]});

        await shardModif.changeDeploymentRequestAddr(requests.address)


        await treasury.setShardModifications(shardModif.address)

        await treasury.changeDeploymentRequestAddr(requests.address)

        await treasury.changeAgentManagement(management.address)

        await treasury.setHostManagement(hostManagement.address, {from: accounts[0]})


        await fuel.mint(accounts[0], 1000000000000000000000000000, {from: accounts[0]});

        await fuel.mint(accounts[1], 1000000000000000000000000000, {from: accounts[0]});

        await fuel.mint(accounts[2], 1000000000000000000000000000, {from: accounts[0]});

        await fuel.mint(accounts[3], 1000000000000000000000000000, {from: accounts[0]});

        await fuel.approve(staking.address, 300000000000000000000, {from: accounts[1]});

        await fuel.approve(staking.address, 300000000000000000000, {from: accounts[2]});

        await fuel.approve(staking.address, 300000000000000000000, {from: accounts[3]});


        await staking.stake({from: accounts[1]});

        await staking.stake({from: accounts[2]});

        await staking.stake({from: accounts[3]});


        await increaseTime.increaseTimeTo(web3.eth.getBlock(web3.eth.blockNumber).timestamp + increaseTime.duration);


        await hostManagement.setLowerBound(100, {from: accounts[0]});

        await hostManagement.changeTimeBetweenChanges(100, {from: accounts[0]});


        await hostManagement.setID(defaultID, {from: accounts[1]})

        await hostManagement.setID(defaultID, {from: accounts[2]})

        await hostManagement.setID(defaultID, {from: accounts[3]})


        await hostManagement.changeRent(100, {from: accounts[1]});

        await hostManagement.changeRent(100, {from: accounts[2]});

        await hostManagement.changeRent(100, {from: accounts[3]});


        await hostManagement.broadcastActivity({from: accounts[1]});

        await hostManagement.broadcastActivity({from: accounts[2]});

        await hostManagement.broadcastActivity({from: accounts[3]});


        await management.newAgent(false, "", 0, "Simple Agent", "www.github.com/simple_agent", "codeHash", accounts[4], {from: accounts[0]});

        await management.setTreasury(treasury.address, {from: accounts[0]});


        await requests.changeStakeAddress(staking.address, {from: accounts[0]});

    });

    it("Push deploy and withdraw requests", async () => {

        let leaves = [accounts[1], accounts[2], accounts[3]];

        let deploymentTree = new MerkleTree(leaves.map(x => sha3(x)), sha3)

        let deploymentRoot = Buffer.from(deploymentTree.getRoot(), 'hex')

        let treasuryLeaves = generateTreasuryLeaves()

        let treasuryTree = new MerkleTree(treasuryLeaves.map(x => sha3(x)), sha3)

        let treasuryRoot = Buffer.from(treasuryTree.getRoot(), 'hex')

        var finalDeploy = '0x' + deploymentRoot.toString('hex')

        var finalTreasury = '0x' + treasuryRoot.toString('hex')

        await requests.deployAgent("Simple Agent", finalDeploy, "ipfs:asdfghjkhgfdsadfghjhgfds",
          finalTreasury, "ipfs:asdfghjkhgfdsadfghjhgfds", 4, 1000, {from: accounts[0]});

        await requests.withdrawAgent("Simple Agent", finalDeploy, "ipfs:asdfghjkhgfdsadfghjhgfds", 3);

    });

    it("Push deployment and respond to it", async () => {

      let leaves = [accounts[1], accounts[2], accounts[3]];

      let deploymentTree = new MerkleTree(leaves.map(x => sha3(x)), sha3)

      let deploymentRoot = Buffer.from(deploymentTree.getRoot(), 'hex')

      let treasuryLeaves = generateTreasuryLeaves()

      let treasuryTree = new MerkleTree(treasuryLeaves.map(x => sha3(x)), sha3)

      let treasuryRoot = Buffer.from(treasuryTree.getRoot(), 'hex')

      var finalDeploy = '0x' + deploymentRoot.toString('hex')

      var finalTreasury = '0x' + treasuryRoot.toString('hex')

      await requests.deployAgent("Simple Agent", finalDeploy, "ipfs:asdfghjkhgfdsadfghjhgfds",
        finalTreasury, "ipfs:asdfghjkhgfdsadfghjhgfds", 3, 1000, {from: accounts[0]});

      var deploymentIntData = await requests.getDeploymentIntData("Simple Agent", 0);

      assert.equal(deploymentIntData[0].toString(), 3)

      await requests.respond("Simple Agent", 0, "0x00000", {from: accounts[1]});

      await requests.respond("Simple Agent", 0, "0x00000", {from: accounts[2]});

      await requests.respond("Simple Agent", 0, "0x00000", {from: accounts[3]});

    })

    it("Propose modifications, vote them, register agent and manage agent funds", async () => {

      //PUSH DEPLOYMENT AND RESPOND TO IT

      let leaves = [accounts[1], accounts[2], accounts[3]];

      let deploymentTree = new MerkleTree(leaves.map(x => sha3(x)), sha3)

      let deploymentRoot = Buffer.from(deploymentTree.getRoot(), 'hex')

      let treasuryLeaves = generateTreasuryLeaves()

      let treasuryTree = new MerkleTree(treasuryLeaves.map(x => sha3(x)), sha3)

      let treasuryRoot = Buffer.from(treasuryTree.getRoot(), 'hex')

      var finalDeploy = '0x' + deploymentRoot.toString('hex')

      var finalTreasury = '0x' + treasuryRoot.toString('hex')

      await requests.deployAgent("Simple Agent", finalDeploy, "ipfs:asdfghjkhgfdsadfghjhgfds",
        finalTreasury, "ipfs:asdfghjkhgfdsadfghjhgfds", 3, 1000, {from: accounts[0]});

      await requests.respond("Simple Agent", 0, "0x00000", {from: accounts[1]});

      await requests.respond("Simple Agent", 0, "0x00000", {from: accounts[2]});

      await requests.respond("Simple Agent", 0, "0x00000", {from: accounts[3]});

      //PROPOSE

      var deadline1 = web3.eth.getBlock(web3.eth.blockNumber).timestamp + increaseTime.duration.minutes(10)

      await shardModif.proposeModification("Simple Agent", [accounts[1], accounts[2], accounts[3]],
                                           [600, 600, 600], [10, 10, 10],
                                           finalTreasury, "ipfs-1234",
                                           [0, 3, 1000]);

      await shardModif.voteModification("Simple Agent", true, {from: accounts[1]});

      await shardModif.voteModification("Simple Agent", true, {from: accounts[2]});

      await shardModif.voteModification("Simple Agent", true, {from: accounts[3]});

      await treasury.registerAgent("Simple Agent", finalTreasury, "ipfs:asdfghjkhgfdsadfghjhgfds", 3, {from: accounts[0], value: 1000});

      await increaseTime.increaseTimeTo(web3.eth.getBlock(web3.eth.blockNumber).timestamp + increaseTime.duration.minutes(1));

      await treasury.fund("Simple Agent", {from: accounts[1], value: 1000000000000000000});

      assert.equal(await treasury.agentAlreadyRegistered("Simple Agent"), true);

      var agentIntData = await treasury.getAgentIntData("Simple Agent");

      var agentRootData = await treasury.getAgentRoot("Simple Agent");

      assert.equal(agentIntData[0].toString(), 1000000000000001000);

      assert.equal(agentIntData[1].toString(), 1000000000000001000);

      assert.equal(agentRootData[1].toString(), finalTreasury);

      var time = web3.eth.getBlock(web3.eth.blockNumber).timestamp;

      var transferred = await treasury.getPaid.call("Simple Agent", "0x0000", 16534391,
        600, 10, {from: accounts[1]});

      await management.deleteAgent("Simple Agent", 0, {from: accounts[0]});

      agentIntData = await treasury.getAgentIntData("Simple Agent");

      assert(agentIntData[3] > 0);

      await increaseTime.increaseTimeTo(web3.eth.getBlock(web3.eth.blockNumber).timestamp
        + increaseTime.duration.seconds(paymentWindow + 1));

      var commitment = await treasury.getContribution(accounts[1], "Simple Agent");

      assert.equal(commitment.toString(), 1000000000000000000);

      var withdrawal = await treasury.withdrawFunds.call("Simple Agent", {from: accounts[1]})

    })

    it("Propose withdrawal modification, vote it and apply", async () => {

      let leaves = [accounts[1], accounts[2], accounts[3]];

      let deploymentTree = new MerkleTree(leaves.map(x => sha3(x)), sha3)

      let deploymentRoot = Buffer.from(deploymentTree.getRoot(), 'hex')

      let treasuryLeaves = generateTreasuryLeaves()

      let treasuryTree = new MerkleTree(treasuryLeaves.map(x => sha3(x)), sha3)

      let treasuryRoot = Buffer.from(treasuryTree.getRoot(), 'hex')

      var finalDeploy = '0x' + deploymentRoot.toString('hex')

      var finalTreasury = '0x' + treasuryRoot.toString('hex')

      await requests.deployAgent("Simple Agent", finalDeploy, "ipfs:asdfghjkhgfdsadfghjhgfds",
        finalTreasury, "ipfs:asdfghjkhgfdsadfghjhgfds", 3, 1000, {from: accounts[0]});

      await requests.respond("Simple Agent", 0, "0x00000", {from: accounts[1]});

      await requests.respond("Simple Agent", 0, "0x00000", {from: accounts[2]});

      await requests.respond("Simple Agent", 0, "0x00000", {from: accounts[3]});

      //PROPOSE

      var deadline1 = web3.eth.getBlock(web3.eth.blockNumber).timestamp + increaseTime.duration.minutes(1)

      await shardModif.proposeModification("Simple Agent", [accounts[1], accounts[2], accounts[3]],
                                           [600,600,600], [10, 10, 10],
                                           finalTreasury, "ipfs-1234",
                                           [0, 3, 1000]);

      await shardModif.voteModification("Simple Agent", true, {from: accounts[1]});

      await shardModif.voteModification("Simple Agent", true, {from: accounts[2]});

      await shardModif.voteModification("Simple Agent", true, {from: accounts[3]});

      await treasury.registerAgent("Simple Agent", finalTreasury, "ipfs:asdfghjkhgfdsadfghjhgfds", 3, {from: accounts[0], value: 1000});

      await increaseTime.increaseTimeTo(web3.eth.getBlock(web3.eth.blockNumber).timestamp + increaseTime.duration.minutes(10));

      await treasury.fund("Simple Agent", {from: accounts[1], value: 1000000000000000000});

      //WITHDRAW

      await requests.withdrawAgent("Simple Agent", finalTreasury, "ipfs:asdfghjkhgfdsadfghjhgfds", 1, {from: accounts[0]});

      await shardModif.proposeModification("Simple Agent", [accounts[1], accounts[2], accounts[3]],
                                           [600,600,600], [10, 10, 10],
                                           finalTreasury, "ipfs-12345678",
                                           [1, 3, 1000]);

      await shardModif.voteModification("Simple Agent", true, {from: accounts[1]});

      await shardModif.voteModification("Simple Agent", true, {from: accounts[2]});

      await shardModif.voteModification("Simple Agent", true, {from: accounts[3]});


      await shardModif.applyModification("Simple Agent", {from: accounts[0], value:0});

    })

    //Generate leaves for AgentsTreasury

    	function generateTreasuryLeaves() {

    		let leaves = []

        let present = web3.eth.getBlock(web3.eth.blockNumber).timestamp

        //ID, nr of seconds to host agent, rent per second, when was agent deployed

    		leaves.push(defaultID
                    + ' '
                    + '600'
                    + ' '
                    + '10'
                    + ' '
                    + '16534391');

        leaves.push(defaultID
                    + ' '
                    + '600'
                    + ' '
                    + '10'
                    + ' '
                    + '1653439');

        leaves.push(defaultID
                    + ' '
                    + '600'
                    + ' '
                    + '10'
                    + ' '
                    + '165343');

    		return leaves;

    }


});
