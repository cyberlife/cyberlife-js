const Staking = artifacts.require("contracts/security/Staking.sol");
const WorkToken = artifacts.require("contracts/token/WorkToken.sol");
const HostManagement = artifacts.require("contracts/hosts/HostManagement.sol")

const assertFail = require("./helpers/assertFail");

const increaseTime = require('./helpers/increaseTime');

const {sha3} = require('ethereumjs-util')
const crypto = require('crypto')
const MerkleTree = require('merkletreejs')

contract('HostManagement', function(accounts) {

    let staking, work, management;

    let ID = '0000000000000000000000000000000000000000111111'

    beforeEach(async () => {

        work = await WorkToken.new("WORK", "WOR", 18, {from: accounts[0]});

        staking = await Staking.new(work.address, 300000000000000000000, {from: accounts[0]});

        management = await HostManagement.new(staking.address, staking.address, {from: accounts[0]});

        await work.mint(accounts[0], 100000000000000000000000000000, {from: accounts[0]});

        await work.mint(accounts[1], 100000000000000000000000000000, {from: accounts[0]});

        await work.mint(accounts[2], 100000000000000000000000000000, {from: accounts[0]});

        await work.approve(staking.address, 400000000000000000000, {from: accounts[1]});

        await work.approve(staking.address, 400000000000000000000, {from: accounts[2]});

    });

    it("Set ID and change rent twice", async () => {

        await staking.stake({from: accounts[1]});

        await management.setID(ID, {from: accounts[1]});

        await management.changeRent(1, {from: accounts[1]});

        assert.equal(1, await management.hostPrice(ID));

        await increaseTime.increaseTimeTo(web3.eth.getBlock(web3.eth.blockNumber).timestamp + increaseTime.duration.days(2));

        await management.changeRent(10, {from: accounts[1]});

        assert.equal(10, await management.hostPrice(ID));

    });

    it("Check getter for eligibility of host to get requests", async () => {

        await staking.stake({from: accounts[1]});

        await management.setID(ID, {from: accounts[1]});

        await management.changeRent(1, {from: accounts[1]});

        await increaseTime.increaseTimeTo(web3.eth.getBlock(web3.eth.blockNumber).timestamp + increaseTime.duration.days(3));

        await management.broadcastActivity({from: accounts[1]});

        let id = await management.hostIDfromAddr(accounts[1])

        assert.equal(await management.canHostAgents(id), true);

    });

    it("Check if host is not eligible in case it is slashed", async () => {

        await staking.stake({from: accounts[1]});

        await management.setID(ID, {from: accounts[1]});

        await management.changeRent(1, {from: accounts[1]});

        await increaseTime.increaseTimeTo(web3.eth.getBlock(web3.eth.blockNumber).timestamp + increaseTime.duration.days(3));

        await management.broadcastActivity({from: accounts[1]});

        await staking.slash("Bad host", accounts[1], 10, {from: accounts[0]})

        let id = await management.hostIDfromAddr(accounts[1])

        assert.equal(await management.canHostAgents(id), false);

    })

    it("Check if host is eligible again after replenishing their stake", async () => {

        await staking.stake({from: accounts[1]});

        await management.setID(ID, {from: accounts[1]});

        await management.changeRent(1, {from: accounts[1]});

        await increaseTime.increaseTimeTo(web3.eth.getBlock(web3.eth.blockNumber).timestamp + increaseTime.duration.days(3));

        await management.broadcastActivity({from: accounts[1]});

        await staking.slash("Bad host", accounts[1], 10, {from: accounts[0]})

        await staking.replenishStake({from: accounts[1]})

        let id = await management.hostIDfromAddr(accounts[1])

        assert.equal(await management.canHostAgents(id), true);

    })

});
