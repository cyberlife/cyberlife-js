pragma solidity 0.4.25;

import "contracts/interfaces/IAgentObligations.sol";
import "contracts/interfaces/ICharityMarkets.sol";
import "contracts/interfaces/IAgentsTreasury.sol";
import "contracts/interfaces/IAgentManagement.sol";

import "contracts/interfaces/IArtBuying.sol";

//IN AGENT MANAGEMENT, ADD ADDRESSES OF CONTRACTS CONTROLLED BY AGENT

contract ArtBuying is IArtBuying {

  struct LatestPainting {

    string ipfsLocation;
    uint256 price;

  }

  struct Obligation {

    uint256 total;

  }

  struct MarketOrder {

    address charityAddr;

    uint256 charityPercentage;

    address[] targets;

    uint256[] shares;

    uint256 money;

  }

  IAgentObligations obligations;
  IAgentsTreasury treasury;
  IAgentManagement management;
  ICharityMarkets charityMarkets;

  MarketOrder[] marketOrders;
  Obligation[] unprocessedObligations;

  address[] receivers;
  uint256[] shares;

  string agentName;

  address agentCreator;

  uint256 minCharityPercentage = 1;

  uint256 currentOrder = 0;

  uint256 currentObligation = 0;

  uint256 agentPosition;

  uint256 agentCashback;

  uint256 marketCut;

  mapping(address => LatestPainting) browsedPaintings;

  mapping(string => bool) alreadyBought;

  mapping(address => LatestPainting[]) ownedPaintings;

  modifier onlyAgent() {

    address agentAddress = management.getAgentAddress(agentCreator, agentPosition);

    require(msg.sender == agentAddress);

    _;

  }

  modifier onlyCurrentController() {

    address controller = management.getCurrentController(agentCreator, agentPosition);

    require(msg.sender == controller);

    _;

  }

  modifier onlyAuthorized() {

    address agentAddress = management.getAgentAddress(agentCreator, agentPosition);

    require(msg.sender == agentCreator || msg.sender == agentAddress);

    _;

  }

  modifier afterSetup() {

    require(isContract(address(management)) == true);
    require(isContract(address(treasury)) == true);
    require(isContract(address(obligations)) == true);
    require(isContract(address(charityMarkets)) == true);

    _;

  }

  constructor(uint256 _agentCashback,
              uint256 _marketCut) public {

    require(_agentCashback < 100 - minCharityPercentage);
    require(_marketCut > 0 && _marketCut < 100);

    agentCashback = _agentCashback;

    marketCut = _marketCut;

  }

  function setAgentManagement(string _agentName, address _management) public {

    require(isContract(_management) == true);

    management = IAgentManagement(_management);

    bool nameTaken = management.isNameTaken(_agentName);

    require(nameTaken == true);

    agentCreator = management.getCreatorFromName(_agentName);

    agentPosition = management.getPositionFromName(_agentName);

    address controller = management.getCurrentController(agentCreator, agentPosition);

    require(msg.sender == controller);

    agentName = _agentName;

    emit SetAgentManagement(_agentName, _management);

  }

  function setTreasury(address _treasury) public onlyCurrentController {

    require(isContract(_treasury) == true);

    treasury = IAgentsTreasury(_treasury);

    emit SetTreasury(_treasury);

  }

  function setObligations(address _obligations) public onlyCurrentController {

    require(isContract(_obligations) == true);

    obligations = IAgentObligations(_obligations);

    emit SetObligations(_obligations);

  }

  function setCharityMarkets(address _charityMarkets) public onlyCurrentController {

    require(isContract(_charityMarkets) == true);

    charityMarkets = ICharityMarkets(_charityMarkets);

    emit SetCharityMarkets(_charityMarkets);

  }

  function changeAgentCashback(uint256 cashback) public onlyCurrentController {

    require(cashback < 100 - minCharityPercentage);

    agentCashback = cashback;

    emit ChangedAgentCashback(cashback);

  }

  function changeMarketCut(uint256 cut) public onlyCurrentController {

    require(cut > 0 && cut < 100);

    marketCut = cut;

    emit ChangedMarketCut(cut);

  }

  function showPainting(address potentialBuyer, string ipfsLocation, uint256 price)
    public //onlyAuthorized
    afterSetup {

    require(alreadyBought[ipfsLocation] == false, "A painting with this IPFS hash has already been sold");

    LatestPainting memory painting = LatestPainting(ipfsLocation, price);

    browsedPaintings[potentialBuyer] = painting;

  }

  function buyPainting(address charity) public payable afterSetup {

    require(msg.value == browsedPaintings[msg.sender].price,
        "Did not send the exact value of the painting");
    require(alreadyBought[browsedPaintings[msg.sender].ipfsLocation] == false,
        "A painting with this IPFS hash has already been sold");

    alreadyBought[browsedPaintings[msg.sender].ipfsLocation] = true;

    address agentAddress = management.getAgentAddress(agentCreator, agentPosition);

    require(agentAddress != address(0), "The agent must have an address");

    uint256 marketMoney = msg.value * marketCut / 100;

    uint256 obligationsMoney = msg.value - marketMoney;

    //Save market order to be processed later

    receivers.push(agentAddress);
    receivers.push(msg.sender);

    shares.push(agentCashback);
    shares.push(100 - minCharityPercentage - agentCashback);

    MarketOrder memory order =
      MarketOrder(charity, minCharityPercentage, receivers, shares, marketMoney);

    marketOrders.push(order);

    //Clear arrays

    delete(receivers[0]);
    delete(receivers[1]);

    delete(shares[0]);
    delete(shares[1]);

    receivers.length = receivers.length - 2;
    shares.length = shares.length - 2;

    //Save obligation to be processed later

    Obligation memory newObligation = Obligation(obligationsMoney);

    unprocessedObligations.push(newObligation);

    //Assign painting

    LatestPainting memory latest = browsedPaintings[msg.sender];

    ownedPaintings[msg.sender].push(latest);

    emit BoughtPainting(msg.sender, browsedPaintings[msg.sender].price,
        browsedPaintings[msg.sender].ipfsLocation, charity);

  }

  function processLatestObligation() public afterSetup {

    require(currentObligation < unprocessedObligations.length);

    address agentAddress = management.getAgentAddress(agentCreator, agentPosition);

    uint256 obligationsLength = obligations.getObligationsLength(agentAddress);

    uint256 obligationPay;

    uint256 forObligations = unprocessedObligations[currentObligation].total;

    currentObligation++;

    if (obligationsLength > 0) {

      address obligationReceiver;

      uint256 obligationPercentage;

      for (uint i = 0; i < obligationsLength; i++) {

        obligationReceiver = obligations.getObligationReceiver(agentAddress, i);

        obligationPercentage = obligations.getObligationPayment(agentAddress, i);

        obligationPay = forObligations * obligationPercentage / 100;

        obligationReceiver.transfer(obligationPay);

      }

    }

    delete(unprocessedObligations[currentObligation - 1]);

  }

  function processLatestOrder() public afterSetup {

    require(currentOrder < marketOrders.length, "All orders have been processed for now");

    currentOrder++;

    charityMarkets.donate.value(marketOrders[currentOrder - 1].money)
              (marketOrders[currentOrder - 1].charityAddr,
              marketOrders[currentOrder - 1].charityPercentage,
              marketOrders[currentOrder - 1].targets,
              marketOrders[currentOrder - 1].shares);

    delete(marketOrders[currentOrder - 1]);

  }

  //PRIVATE

  function isContract(address _addr) private returns (bool isContract) {

    uint32 size;

    assembly {

      size := extcodesize(_addr)

    }

    return (size > 0);

  }

  //GETTERS

  function getObligation(uint256 position) public view returns (uint256) {

    return unprocessedObligations[position].total;

  }

  function getOrderCharityAddress(uint256 position) public view returns (address) {

    return marketOrders[position].charityAddr;

  }

  function getOrderCharityCut(uint256 position) public view returns (uint256) {

    return marketOrders[position].charityPercentage;

  }

  function getOrderMoney(uint256 position) public view returns (uint256) {

    return marketOrders[position].money;

  }

  function getAgentName() public view returns (string) {

    return agentName;

  }

  function getMarketCut() public view returns (uint256) {

    return marketCut;

  }

  function getAgentCreator() public view returns (address) {

    return agentCreator;

  }

  function getAgentPositionInCreatorArray() public view returns (uint256) {

    return agentPosition;

  }

  function getAgentCashback() public view returns (uint256) {

    return agentCashback;

  }

  function getCurrentOrder() public view returns (uint256) {

    return currentOrder;

  }

  function getCurrentObligation() public view returns (uint256) {

    return currentObligation;

  }

  function getLatestPainting(address who) public view returns (string, uint256) {

    return (browsedPaintings[who].ipfsLocation, browsedPaintings[who].price);

  }

  function paintingIsBought(string painting) public view returns (bool) {

    return alreadyBought[painting];

  }

  function getOwnedPainting(address who, uint256 position) public view returns (string, uint256) {

    require(ownedPaintings[who].length > position);

    return (ownedPaintings[who][position].ipfsLocation, ownedPaintings[who][position].price);

  }

  function getOwnedPaintingsNumber(address who) public view returns (uint256) {

    return ownedPaintings[who].length;

  }

}
