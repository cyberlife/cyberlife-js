pragma solidity 0.4.25;

import "contracts/zeppelin/ownership/Ownable.sol";
import "contracts/zeppelin/SafeMath.sol";

import "contracts/base/charity/interfaces/IBondingCurveVault.sol";
import "contracts/base/charity/token/Token.sol";
import "contracts/base/charity/FractionalExponents.sol";

import "contracts/base/charity/interfaces/ILogic.sol";

contract Logic is ILogic, Ownable {

    using SafeMath for uint256;

    struct Charity {

      address token;
      address vault;

    }

    mapping(address => Charity) charityData;

    // Contract that handles fractional exponents
    address public exponentContract;

    // Minimum ETH balance for valid bonding curve
    uint256 public minEth;

    modifier minimumBondingBalance(address charity) {

        require(charityData[charity].vault.balance >= minEth,
            "Not enough ETH in bonding vault contract");

        _;

    }

    /**
     * @dev No fallback is allowed, use sponsor() to fund the Bonding Curve
     */
    function () public payable {
        revert();
    }

    function sponsor(address charity) public payable {

        require(charityData[charity].vault != address(0), "Vault is missing");
        charityData[charity].vault.transfer(msg.value);

        emit Sponsored(charity, msg.sender, msg.value);

    }

    function award(address charity, address _account, uint256 _amount) public {

        require(charityData[charity].token != address(0), "The token must be set");

        Token newToken = Token(charityData[charity].token);

        newToken.mintToken(_account, _amount);

        emit Awarded(charity, _account, _amount);

    }

    /**
    * @dev sell function for selling tokens to bonding curve
    */
    function sell(address charity, uint256 _amount) public
      minimumBondingBalance(charity) returns (bool) {

        Token tkn = Token(charityData[charity].token);
        IBondingCurveVault vault = IBondingCurveVault(charityData[charity].vault);

        uint256 tokenBalanceOfSender = tkn.balanceOf(msg.sender);
        require(_amount > 0 && tokenBalanceOfSender >= _amount, "Amount needs to be > 0 and tokenBalance >= amount to sell");

        // calculate sell return
        (uint256 price, uint256 amountOfEth) =
          calculateReturn(charity, _amount, tokenBalanceOfSender);

        // burn tokens
        tkn.burn(msg.sender, _amount);

        // sendEth to msg.sender from bonding curve
        vault.sendEth(amountOfEth, msg.sender);

        emit Sold(charity, _amount, amountOfEth);

    }

    /**
     * @dev calculate how much ETH should be returned for a certain amount of tokens
     * @notice using version 2.1 of Khana formula - see documentation for more details
     * @notice the first returned value (finalPrice) includes the 10^18 multiplier.
    */
    function calculateReturn(address _charity, uint256 _sellAmount, uint256 _tokenBalance) public view
        returns (uint256 finalPrice, uint256 redeemableEth) {

        require(exponentContract != address(0), "exponentContract must be set to valid address");
        require(_tokenBalance >= _sellAmount, "User trying to sell more than they have");

        Token tkn = Token(charityData[_charity].token);

        uint256 tokenSupply = tkn.totalSupply();
        uint256 ethInVault = charityData[_charity].vault.balance;

        // For EVM accuracy
        uint256 multiplier = 10**18;

        if (coolDownPeriod(msg.sender) <= 0) {

            // a = (Sp.10^8)
            uint256 portionE8 = (_tokenBalance.mul(10**8).div(tokenSupply));

            // b = a^1/10
            (uint256 exponentResult, uint8 precision) = FractionalExponents(exponentContract).power(portionE8, 1, 1, 10);

            // b/8 * (funds backing curve / token supply)
            uint256 interimPrice = (exponentResult.div(8)).mul(ethInVault.mul(multiplier).div(tokenSupply)).div(multiplier);

            // get final price (with multiplier)
            finalPrice = (interimPrice.mul(multiplier)).div(2**uint256(precision));

            // redeemable ETH (without multiplier)
            redeemableEth = finalPrice.mul(_sellAmount).div(multiplier);
            return (finalPrice, redeemableEth);

        } else {

            return (0,0);

        }
    }

    /**
    * @dev Owner can withdraw the remaining ETH balance as long as no minted tokens left
    */
    function sweepVault(address charity) public onlyOwner {

        Token tkn = Token(charityData[charity].token);
        IBondingCurveVault vault = IBondingCurveVault(charityData[charity].vault);

        require(tkn.totalSupply() == 0, 'Sweep available only if no minted tokens left');
        require(charityData[charity].vault.balance > 0, 'Vault is empty');
        vault.sendEth(charityData[charity].vault.balance, msg.sender);

        emit SweptVault(charity);

    }

    function coolDownPeriod(address _tokenHolder) public view returns (uint256) {
        // something like today - (day of buying + 7 days)
        // todo when minting tokens
        return 0;
    }

    /**
    * @dev Set both the 'logicContract' and 'bondingVault' to different contract addresses in 1 tx
    */
    function setTokenAndBondingVault(address charity, address _tokenContract,
        address _bondingVaultContract) internal {

        Charity memory newCharity = Charity(_tokenContract, _bondingVaultContract);

        charityData[charity] = newCharity;

        emit SetCharityData(charity, _tokenContract, _bondingVaultContract);

    }

    function deleteCharityData(address _charity) internal {

        delete(charityData[_charity]);

        emit DeletedCharityData(_charity);

    }

    /**
    * @dev Set the 'exponentContract' which calculates fractional exponents
    */
    function setExponentContract(address _exponentContract) public onlyOwner {

        address oldContract = exponentContract;
        exponentContract = _exponentContract;

    }

    /**
    * @dev Set the 'minEth' amount
    */
    function setMinEth(uint256 _minEth) public onlyOwner {

        uint256 oldAmount = minEth;
        minEth = _minEth;

        emit SetMinEth(_minEth);

    }

    //GETTERS

    function getTokenSupply(address charity) public view returns (uint256) {

        require(charityData[charity].token != address(0), "This charity does not have a token set");

        Token tkn = Token(charityData[charity].token);

        return tkn.totalSupply();

    }

    function getTokenContract(address charity) public view returns (address) {

        return charityData[charity].token;

    }

    function getVaultContract(address charity) public view returns (address) {

        return charityData[charity].vault;

    }

}
