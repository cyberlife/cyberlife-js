pragma solidity 0.4.25;

contract IBondingCurveVault {

  function sendEth(uint256 _amount, address _account) public;

  function setLogicContract(address _logicContract) public;

  function () public payable;

}
