pragma solidity 0.4.25;

contract ILogic {

  event Sponsored(address charity, address indexed sender, uint256 amount);

  event Awarded(address charity, address _account, uint256 _amount);

  event Sold(address charity, uint256 _amount, uint256 ethBack);

  event SweptVault(address charity);

  event SetCharityData(address charity, address _tokenContract, address _bondingVaultContract);

  event DeletedCharityData(address _charity);

  event SetMinEth(uint256 _minEth);


  function () public payable;

  function sponsor(address charity) public payable;

  function award(address charity, address _account, uint256 _amount) public;

  function sell(address charity, uint256 _amount) public returns (bool);

  function calculateReturn(address _charity, uint256 _sellAmount, uint256 _tokenBalance) public view
      returns (uint256 finalPrice, uint256 redeemableEth);

  function sweepVault(address charity) public;

  function coolDownPeriod(address _tokenHolder) public view returns (uint256);

  function setTokenAndBondingVault(address charity, address _tokenContract,
      address _bondingVaultContract) internal;

  function deleteCharityData(address _charity) internal;

  function setExponentContract(address _exponentContract) public;

  function setMinEth(uint256 _minEth) public;


  function getTokenSupply(address charity) public view returns (uint256);

  function getTokenContract(address charity) public view returns (address);

  function getVaultContract(address charity) public view returns (address);

}
