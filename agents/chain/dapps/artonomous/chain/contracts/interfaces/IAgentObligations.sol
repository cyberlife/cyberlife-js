pragma solidity 0.4.25;

contract IAgentObligations {

  function changeObligation(address receiver, string agentName, uint256 newEarnings) public;

  function deleteAllObligations(address agent) public;

  function deleteObligation(address agentAddress, uint256 obligationPosition) public;


  function getObligationsLength(address agent) public view returns (uint256);

  function getTotalObligation(address agent) public view returns (uint256);

  function getObligationReceiver(address agent, uint256 position) public view returns (address);

  function getObligationPayment(address agent, uint256 position) public view returns (uint256);

}
