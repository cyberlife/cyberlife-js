pragma solidity 0.4.25;

contract ICharityMarkets {

  event Donated(address targetCharity, uint256 charityPercentage);

  event SendToCharity(address charity, uint256 _amount);

  event ToggleCharity(string ipfsCharities, address _charityAddress,
                      address token, address vault);


  function donate(address targetCharity, uint256 charityPercentage,
                  address[] targets, uint256[] paymentShares)
                  public payable;

  function sendToCharity(address charity, uint256 _amount) internal;

  function setCharityAddress(string ipfsCharities, address _charityAddress,
                             address token, address vault) public;

}
