var GeoPattern = require('geopattern');
var base = require('cyber-base')

function agentFunc(paramTypes) {

  this.paramTypes = paramTypes

}

var agentDictionary = {

  "createArt": new agentFunc([])

}

//------------------------------------------------------------------

var GENERATORS = [
	'concentricCircles',
	'diamonds',
	'hexagons',
	'mosaicSquares',
	'nestedSquares',
	'octogons',
	'overlappingCircles',
	'overlappingRings',
	'plaid',
	'plusSigns',
	'sineWaves',
	'squares',
	'tessellation',
	'triangles',
	'xes'
];

var ASSET_DIR = './assets'

function createArt(jobId, agentName, caller, host, paramsArr) {

  var sanitize = await base.checkAgentFunction(jobId, agentName, caller,
                                               host, agentDictionary["createArt"].paramTypes,
                                               paramsArr);

  if (!sanitize) return

  var hashOrPattern = Math.floor(Math.random() * 2)

  var secretText = makeText( Math.floor(Math.random() * 10000000 + 100000) )
  var price = Math.floor(Math.random() * 1000000 + 1000)
  var pattern
  var url

  if (hashOrPattern > 0) {

    pattern = GeoPattern.generate(secretText)

    url = pattern.toDataUrl()

  } else {

    var randomGenerator = Math.floor(Math.random() * GENERATORS.length)

    pattern = GeoPattern.generate(secretText, { generator: GENERATORS[randomGenerator].toString() })

    url = pattern.toDataUrl()

  }

  var bufferedURL = Buffer.from(url.toString(), 'utf8')

  await base.postDatatoIPFS(bufferedURL, async function(err, res) {

  //  await base.dialNode( host, caller, [jobId.toString() + " -- " + res[0].hash.toString('utf8')] )

  })

}

function makeText(randLength) {

  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < randLength; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;

}

module.exports = {

  createArt

}
