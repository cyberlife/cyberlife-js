var chain = require('../chainInteraction')
var ipfsCalls = require('./ipfsInteraction')

var sha3 = require('ethereumjs-util').sha3
const async = require('async')
var utils = require('./utils')
const BigNumber = require('bignumber.js');
const MerkleTree = require('merkletreejs')
const crypto = require('crypto')

async function fundAgent(pubKey, name, funds) {

  var web3 = chain.web3

  var personalFunds = await web3.eth.getBalance(pubKey)

  if (personalFunds >= funds)
    chain.fundAgent(pubKey, name, funds)

  else console.log("You don't have that much money in your account")

}

async function deleteAgent(pbk, name) {

  var agentExists = await chain.getNameAvailability(name)

  if (agentExists == true) {

    await chain.deleteAgent(pbk, name)

  } else console.log("The agent with the name '" + name + "' does not exist")

}

async function getCurrentAgentShard(name, callback) {

  var canGetShard = await chain.agentIsRegistered(name)

  if (canGetShard != undefined && canGetShard == true) {

    var ipfsAddress = await chain.getAgentRoot(name)

    ipfsCalls.getIPFSData(ipfsAddress[2].toString(), async function(err, sh) {

      if (err != undefined && err != null) return callback(err, undefined)

      return callback(undefined, sh[0].content)

    })

  } else {

    return callback(undefined, undefined)

  }

}

async function getHostPrices(stakedIds) {

  var hostPrices = []

  for (var i = 0; i < stakedIds.length; i++) {

    var price = await chain.getHostRent(stakedIds[i].id.toB58String())

    hostPrices.push(new BigNumber(price))

  }

  return hostPrices

}

async function changeHostsTrees(pubKey, name, hostsIds, changeType) {

  var hostAddr = ''
  var agentsTree = ''
  var i
  var currentId

  console.log('Changing hosts trees...')

  for (i = 0; i < hostsIds.length; i++) {

    if (changeType == 0) currentId = hostsIds[i].id.toB58String()
    else if (changeType == 1) currentId = hostsIds[i]

    hostAddr = await chain.getHostAddrFromID(currentId)

    agentsTree = await chain.getAgentsTree(hostAddr)

    if ( agentsTree[0] == undefined || agentsTree[0] == NaN
        || agentsTree[0].length == 0
      ) {

      if (changeType == 0) {

        initAgentTree(pubKey, name, hostAddr, currentId)

      }

    } else {

      updateAgentTree(pubKey, name, hostAddr, currentId, agentsTree[0], changeType)

    }

  }

}

async function initAgentTree(pubKey, name, hostAddr, currentId) {

  await ipfsCalls.postDatatoIPFS(Buffer.from(name, 'utf8'), async function(err, postResult) {

    await chain.setAgentsTree(pubKey, hostAddr, postResult[0].hash.toString(), sha3(name), 1)

    var tree = await chain.getAgentsTree(hostAddr)

  })

}

async function updateAgentTree(pubKey, name, hostAddr, currentId, treeIpfs, changeType) {

  //BUGS: agents are not deleted from hosts trees

  await ipfsCalls.getIPFSData(treeIpfs, async function(err, leaves) {

    var splitLeaves = leaves[0].content.toString().split('\n')

    var agentsNumber = await chain.getAgentsCount(currentId)

    if (changeType == 0) {splitLeaves.push(name); agentsNumber++}

    else {splitLeaves = await utils.deleteElementFromArray(name,
        splitLeaves, "string"); agentsNumber--}

    if (agentsNumber == 0) {

      await chain.setAgentsTree(pubKey, hostAddr, "", [], 0)

      return

    } else if (agentsNumber > 0) {

      var hashedLeaves = await utils.getHashedLeaves(splitLeaves)

      const tree = new MerkleTree(hashedLeaves, sha3)

      var bufferedLeaves

      for (i = 0; i < splitLeaves.length; i++) {

        if (i == 0) bufferedLeaves = Buffer.from(name + '\n', 'utf8')

        else if (i > 0 && i < splitLeaves.length - 1)
          bufferedLeaves = bufferedLeaves.concat(Buffer.from(name + '\n', 'utf8'))

        else bufferedLeaves = bufferedLeaves.concat(Buffer.from(name, 'utf8'))

      }

      await ipfsCalls.postDatatoIPFS(bufferedLeaves, function(err, postRes) {

        chain.setAgentsTree(pubKey, hostAddr, postRes[0].hash.toString(),
          tree.getRoot(), agentsNumber)

      })

    }

  })

}

module.exports = {

  fundAgent,
  deleteAgent,
  getCurrentAgentShard,
  getHostPrices,
  changeHostsTrees

}
