var rimraf = require('rimraf');
var chain = require('../chainInteraction')
var fs = require('fs')
const async = require('async')
var shell = require('shelljs')
var sha3 = require('ethereumjs-util').sha3
const MerkleTree = require('merkletreejs')
const crypto = require('crypto')
const BigNumber = require('bignumber.js')

var ipfsUtils = require('../general/ipfsInteraction')
var utils = require('../general/utils')
var network = require('../general/networkHelpers')

var chainCalls = require('../general/deployerChainCalls')

var web3 = chain.web3
var responseWindow = 5
var migrationCheckTime = 10
var protocolName = '/cyberlife'
var pingDelay = 10000

async function execute(personalNode, agentsLocation, agentName, executedFileName,
  funcToExecute, jobId, callerId, paramsAddr, peerList) {

  try {

    var caller

    var i = 0

    for (i = 0; i < peerList.length; i++) {

      if (peerList[i].id.toB58String() == callerId) {caller = peerList[i]; break}

    }

    if (agentName == undefined && caller != undefined) {

      await network.dialNode(personalNode, protocolName, caller, [jobId.toString() +  " -- the agent name was not defined"])

      return

    }

    if (executedFileName == undefined && caller != undefined) {

      await network.dialNode(personalNode, protocolName, caller, [jobId.toString() + " -- the executedFileName was not defined"])

      return

    }

    if (funcToExecute == undefined && caller != undefined) {

      await network.dialNode(personalNode, protocolName, caller, [jobId.toString() + " -- the function to be executed was not defined"])

      return

    }

    var fullPath = agentsLocation + agentName + '/' + executedFileName

    await ipfsUtils.getIPFSData(paramsAddr, async function(err, data) {

      var params = JSON.parse(data[0].content.toString('utf8'))

      //TODO: check if agent is in your tree
      //TODO: if it is, check that it is installed; if not, slash myself and announce caller
      //TODO: check that agent is not migrating from this host and that it is not being migrated now to this host

      if (funcToExecute == 'no_specific_function') {

        //Quick and dirty solution to use npx babel-node for Dai.js
        if (agentName == 'Lending') {

          shell.exec('npx babel-node ' + fullPath + ' ' + '80' + ' ' + '0.1', {silent:false}, async function(code, stdout, stderr) {

            if (stderr != undefined && stderr != '\n' && stderr != '') {

              await network.dialNode(personalNode, protocolName, caller,
                [jobId.toString() + " -- " + stderr])

            }

          })

        } else {

          shell.exec('node ' + fullPath, {silent:true}, async function(code, stdout, stderr) {

            if (stderr != undefined && stderr != '\n' && stderr != '') {

              await network.dialNode(personalNode, protocolName, caller,
                [jobId.toString() + " -- " + stderr])

            }

          })

        }

      } else {

        try {

          var agent = require(fullPath)

          if (params == "[]") {

            agent[funcToExecute](jobId, agentName, caller, personalNode, [])

          } else {

            agent[funcToExecute](jobId, agentName, caller, personalNode, params)

          }

        } catch (err) {

          await network.dialNode(personalNode, protocolName, caller,
            [jobId.toString() + " -- " + err])

        }

      }

    })

  } catch(hostErr) {

    await network.dialNode(personalNode, protocolName, caller,
      [jobId.toString() + " -- " + hostErr.toString()])

  }

}

async function logSecondsHosted(pbk, personalNode, callback) {

  var personalAddress = await chain.getHostAddrFromID(personalNode.peerInfo.id.toB58String())

  var hostedAgents = await chain.getAgentsTree(personalAddress)

  await ipfsUtils.getIPFSData(hostedAgents[0], async function(agentsErr, leaves) {

    if (agentsErr != undefined) {return callback(1, undefined)}

    var agents = leaves[0].content.toString('utf8').split('\n')

    if (agents.length == 0) return callback(undefined, 1)

    var secondsToAdd = await chain.getStartOfWork(pbk)

    for (var i = 0; i < agents.length; i++) {

      var isBeingMigrated =
        await chain.getChangingTree(agents[i].toString())

      var hostChangingTree = await chain.getHostChangingTree(agents[i])

      if (isBeingMigrated && pbk == hostChangingTree) continue

      var currentTime = new BigNumber(Math.floor(Date.now() / 1000))

      await chain.registerSeconds(pbk, pbk, agents[i], currentTime - secondsToAdd)

    }

    return callback(undefined, 1)

  })

}

async function getPaid(pbk, id, agents_location, agentName) {

  await chainCalls.getCurrentAgentShard(agentName, async function(shardErr, shard) {

    if (shardErr != undefined) {console.log(shardErr); return}

    var leaves = await utils.getCurrentShardLeaves(shard)

    var foundMyself = false
    var splitLeaf

    for(var i = 0; i < leaves[1].length; i++) {

      splitLeaf = leaves[1][i].split(' ')

      if (splitLeaf[0] == id) {foundMyself = true; break}

    }

    if (!foundMyself) {

      console.log("You are not hosting agent " + agentName)

      return

    }

    var proof = await chain.stringToBytes32("someProof")

    await chain.getPaid(pbk, agentName, proof, splitLeaf[3] - splitLeaf[1],
      splitLeaf[1], splitLeaf[2])

    var secondsBilled = await chain.getSecondsBilled(pbk, agentName)

    if (secondsBilled >= splitLeaf[1]) {

      //TODO: delete agent from personal tree and myself from the agent's tree

      uninstall_agent(pbk, agents_location, agentName)

    }

  })

}

async function install_agent(pbk, personalNode, agents_location, agent_name) {

  var agentPath = agents_location.toString() + agent_name.toString()

  if (fs.existsSync(agentPath)) {

    await chain.updateAgentVersion(pbk, agent_name)

    return

  }

  //Check if agent is registered

  var agentStatus = await chain.agentIsRegistered(agent_name)

  if (agentStatus == false) return

  //Get data about agent

  var creator = await chain.getCreatorFromName(agent_name)

  var position = await chain.getPositionFromName(agent_name)

  var version = await chain.getAgentVersion(creator, position)

  var agent_source_code = await chain.getAgentSource(creator, position)

  var source_hash = await chain.getAgentCodeHash(creator, position)

  //TODO: check if agent is in your tree

  //Check if you are in the agent's merkle tree

  var agentTree = await chain.getAgentRoot(agent_name)

  ipfsUtils.getIPFSData(agentTree[2], async function(billingErr, agentBillings) {

    var leaves = agentBillings[0].content.toString('utf8').split('\n')

    var foundMyself = false

    for (var j = 0; j < leaves.length; j++) {

      if (leaves[j].split(' ')[0] == personalNode.peerInfo.id.toB58String()) { foundMyself = true; break }

    }

    if (foundMyself == false) return

    utils.executeCommand('git clone ' + agent_source_code.toString() + ' ' + agentPath, async function(err, install) {

        if (err != undefined && err != '') { return }

        if (fs.existsSync(agentPath + '/installation.txt')) {

          var commands = await chain.readFile(agentPath + '/installation.txt')

          if (commands == undefined || commands.length == 0) return

          var instruction

          for (var i = 0; i < commands.length; i++) {

            if (commands[i].toString().includes('_')) {

              var splitCommand = commands[i].toString().split('_')

              if (splitCommand.length > 2) return

              instruction = await utils.prepareInstruction(splitCommand, agentPath)

              if (instruction != undefined) await utils.executeCommand(instruction, undefined)

            } else {

              instruction = await utils.prepareInstruction(commands[i].toString(), agentPath)

              if (instruction != undefined) await utils.executeCommand(instruction, undefined)

              }

            }

            await utils.linkResource(agentPath, agent_name, "cyber-base")

            console.log("Installed " + agent_name.toString())

          } else {

            await utils.linkResource(agentPath, agent_name, "cyber-base")

            console.log("Installed " + agent_name.toString())

          }

      })

    })

}

async function uninstall_agent(pbk, agents_location, name) {

  var agentPath = agents_location.toString() + name.toString()

  if (!fs.existsSync(agentPath)) {

    return

  }

  await utils.unlinkResources(agents_location, name)

  //TODO: check that you are deleted from the agent's tree

  rimraf(agentPath, async function () {

    await chain.clearAgent(pbk, name)

  })

}

async function migrateAgents(peerList, personalNode, agentsLocation) {

  var toBeModified = peerList.concat()

  var auxPeers = await utils.getRegisteredHosts(toBeModified)

  if (auxPeers.length == 0) return

  var lowestRent = await chain.getHostRent(personalNode.peerInfo.id.toB58String())

  //Get agents you host

  var personalAddress = await chain.getHostAddrFromID(personalNode.peerInfo.id.toB58String())

  var hostedAgents = await chain.getAgentsTree(personalAddress)

  ipfsUtils.getIPFSData(hostedAgents[0], async function(agentsErr, leaves) {

    if (agentsErr != undefined) {return}

    var agents = leaves[0].content.toString('utf8').split('\n')

    if (agents.length == 0) return

    var hostsForAgent = []

    var migrateTo = []

    var peersRents = []

    var i, j, p

    for (i = 0; i < auxPeers.length; i++) {

      peersRents.push( await chain.getHostRent(auxPeers[i].id.toB58String()) )

    }

    //Get hosts for each agent

    for (j = 0; j < agents.length; j++) {

      var agentMerkleData = await chain.getAgentRoot(agents[j].replace("'", ""))

      ipfsUtils.getIPFSData(agentMerkleData[2], async function(agentTreeErr, agentTree) {

        if (agentTreeErr != undefined) {return}

        var hosts = agentTree[0].content.toString('utf8').split('\n')

        var hostsArr = []

        for (i = 0; i < hosts.length; i++) {

          hostsArr.push(hosts[i].split(' ')[0])

        }

        hostsForAgent.push(hostsArr)

        if (j == agents.length) {

          for (i = 0; i < agents.length; i++) {

            migrateTo.push([undefined, lowestRent])

          }

          for (i = 0; i < agents.length; i++) {

            for (p = 0; p < peersRents.length; p++) {

              if (new BigNumber(migrateTo[i][1]).isGreaterThan(new BigNumber(peersRents[p]))
                  && peersRents[p] > 0) {

                var found = false

                for (var m = 0; m < hostsForAgent[i].length; m++) {

                  if (auxPeers[p].id.toB58String() == hostsForAgent[i][m]) {found = true; break}

                }

                if (found == false) {

                  migrateTo[i] = [auxPeers[p], peersRents[p]]

                }

              }

            }

          }

          for (i = 0; i < migrateTo.length; i++) {

            if (migrateTo[i][0] != undefined) {

              var isBeingMigrated =
                await chain.getChangingTree(agents[i].replace("'", "").toString())

              var hostAddress = await chain.getHostAddrFromID(migrateTo[i][0].id.toB58String())

              var hostIsChanging = await chain.getIsChangingTree(hostAddress)

              if (isBeingMigrated || hostIsChanging) {

                continue

              } else {

                prepareAgentTransfer(migrateTo[i], agents[i].replace("'", "").toString(),
                                     personalNode, personalAddress, peerList, agentsLocation)

              }

            }

          }


        }

      })

    }

  })

}

async function prepareAgentTransfer(migrationData, agentName, personalNode, personalAddress, peerList, agentsLocation) {

  //DELETE FROM LOCAL HOST

  var agentRootData = await chain.getAgentRoot(agentName)

  await ipfsUtils.getIPFSData(agentRootData[2], async function(err, billingData) {

    var billings = billingData[0].content.toString('utf8').split('\n')

    if (billings.length == 1) return

    //Announce that we're changing the agent's tree

    var treeIsChanging =
      await chain.getChangingTree(agentName)

    if (treeIsChanging == true) return

    await chain.changingAgentTree(personalAddress, agentName)

    var decomposedBillings = await utils.decomposeBillings(billings)

    var endTimeCurrentHost = 0

    for (var i = 0; i < decomposedBillings[3].length; i++) {

      if (decomposedBillings[0][i] == personalNode.peerInfo.id.toB58String()) {

        endTimeCurrentHost = decomposedBillings[3][i]
        break

      }

    }

    console.log("Deleting local host from " + agentName + "'s tree...")

    var hostsToDelete = [personalNode.peerInfo.id.toB58String()]

    var remainingLeaves = await network.getWithdrawalLeaves(hostsToDelete, billings)

    var hashedWithdrawalLeaves = await utils.getHashedLeaves(remainingLeaves[0])

    var bufferedWithdrawalLeaves = await utils.stringArrToBuffArr(remainingLeaves[0])

    const billingsTree = new MerkleTree(hashedWithdrawalLeaves, sha3)

    var billingsRoot = await billingsTree.getRoot()

    var billingsIPFS = await ipfsUtils.createIPFSData(bufferedWithdrawalLeaves)

    var hostsAddresses = await utils.idsToAddresses(hostsToDelete)

    ipfsUtils.postDatatoIPFS(billingsIPFS, async function(errBillings, billingsHash) {

      if (errBillings != null) {console.log(errBillings); return}

      if (billingsHash == NaN || billingsHash == undefined) {

        console.log("Could not store billing data");

        return

      }

      var totalRemainingHosts = parseInt(agentRootData[3].toString()) - 1

      await chain.withdrawAgent(personalAddress, agentName, billingsRoot,
          billingsHash[0].hash.toString(), totalRemainingHosts)

      await chain.proposeModification(personalAddress, agentName, hostsAddresses,
                                      remainingLeaves[1], remainingLeaves[2],
                                      billingsRoot, billingsHash[0].hash.toString(),
                                      [1, totalRemainingHosts, 0])

      var modificationIntData = await chain.getModificationIntData(agentName)

      var voteWindow = await chain.getVotingWindow()

      var voteThreshold = await chain.getVoteThreshold()

      var requestPosition = await chain.getWithdrawalsLength(agentName)

      var dialData = await utils.createJSON(['type', 'name'], ['string', 'string'],
        ["shard_modif", agentName])

      //Contact peers and ask them to vote the modification

      console.log("Contacting peers in order to vote on the " + agentName + "'s migration")

      for (i = 0; i < peerList.length; i++) {

        var idToBytes = await chain.stringToBytes32(peerList[i].id.toB58String())

        var peerIsHost = await chain.hostCanInteract(idToBytes)

        if (peerIsHost == false) continue

        await network.dialNode(personalNode, protocolName, peerList[i], [dialData])

      }

      console.log('Waiting for votes on the migration...')

      waitForDeletionConfirmation(personalAddress, personalNode, protocolName, agentName,
                                  new BigNumber(modificationIntData[0]).plus(voteWindow),
                                  voteThreshold, migrationData, hostsToDelete,
                                  0, agentsLocation, endTimeCurrentHost,
                                  peerList, migrationData[0])

    })

  })

}

async function update_hosted_agents(pbk, agents_location, personalNode) {

  //TODO: get money from all agents you host and delete agents that you host but stopped paying enough

  var personalAddress = await chain.getHostAddrFromID(personalNode.peerInfo.id.toB58String())

  var agentsTree = await chain.getAgentsTree(personalAddress)

  var i

  var agents

  await ipfsUtils.getIPFSData(agentsTree[0], async function(err1, agentsHosted) {

    if (err1 != undefined) {

      uninstallAllUnhostedAgents(agents_location, [])
      return

    }

    agents = agentsHosted[0].content.toString().split('\n')

    if (agents.length == 0) return

    for (i = 0; i < agents.length; i++) {

      var versionHosted = await chain.getHostAgentVersion(personalAddress, agents[i].replace("'", ""))

      var agentCreator = await chain.getCreatorFromName(agents[i].replace("'", ""))

      var agentPosition = await chain.getPositionFromName(agents[i].replace("'", ""))

      var latestVersion = await chain.getAgentVersion(agentCreator, agentPosition)

      var agentLocation = (agents_location + agents[i].replace("'", "") ).toString()

      if ( !fs.existsSync(agentLocation) ) {

        console.log("Installing " + agents[i].replace("'", "") + " which is missing")

        await install_agent(pbk, personalNode, agents_location, agents[i].replace("'", ""))

      } else if (versionHosted != latestVersion) {

        console.log("Updating " + agents[i].replace("'", ""))

        await uninstall_agent(pbk, agents_location, agents[i].replace("'", ""))

        await install_agent(pbk, personalNode, agents_location, agents[i].replace("'", ""))

      }

    }

    uninstallAllUnhostedAgents(agents_location, agents)

  })

}

async function uninstallAllUnhostedAgents(agents_location, installed_agents) {

  try {

    var agents = installed_agents

    var agentFound = false

    var allHostedAgents = fs.readdirSync(agents_location.toString())

    //Naive

    for (var i = 0; i < allHostedAgents.length; i++) {

      agentFound = false

      for (var j = 0; j < agents.length; j++) {

        if (agents[j] == allHostedAgents[i]) {agentFound = true; break}

      }

      if (agentFound == false) {

        rimraf(agents_location.toString() + allHostedAgents[i].replace("'", ""), async function () {



        })

      }

    }

  } catch(error) {


  }

}

//NOT EXPORTED

async function waitForDeletionConfirmation(pubKey, personalNode, protocolName,
                                           name, untilWhen, threshold,
                                           migrationData, hostsToDelete, money,
                                           agentsLocation, endTimeCurrentHost,
                                           peerList, transferTarget) {

  var i

  var modifIntData = await chain.getModificationIntData(name)

  if (modifIntData[1] != 1) {

    await chain.finishedChangingTree(pubKey, name)

  }

  if (modifIntData[3] >= threshold) {

    transferAgent(pubKey, name, migrationData,
                  hostsToDelete, personalNode, agentsLocation,
                  endTimeCurrentHost, peerList, transferTarget)

  } else {

    var currentDate = new BigNumber(Math.floor(Date.now() / 1000))

    if (currentDate < untilWhen) {

      setTimeout(waitForDeletionConfirmation, pingDelay, pubKey,
                 personalNode, protocolName, name,
                 untilWhen, threshold, migrationData,
                 hostsToDelete, money, agentsLocation,
                 endTimeCurrentHost, peerList, transferTarget)

    } else {

      console.log("The shard modification for " + name + " did not get enough acceptance votes" + "\n")

    }

  }

}

async function transferAgent(pubKey, name, migrationData,
                             hostsToDelete, personalNode, agentsLocation,
                             endTimeCurrentHost, peerList, transferTarget) {

  var personalRent = await chain.getHostRent(personalNode.peerInfo.id.toB58String())

  var otherHostRent = await chain.getHostRent(migrationData[0].id.toB58String())

  var idToBytes = await chain.stringToBytes32(migrationData[0].id.toB58String())

  var active = await chain.hostCanInteract(idToBytes)

  if (parseFloat(otherHostRent) >= parseFloat(personalRent) || active == false) {

      console.log("Abandon " + name + "'s migration, the receiving host is no longer eligible")
      return

  }

  await chainCalls.changeHostsTrees(pubKey, name, hostsToDelete, 1)

  await chain.applyModification(pubKey, name, 0)

  var currentDate = new BigNumber(Math.floor(Date.now() / 1000))

  var remainingTime = endTimeCurrentHost - currentDate

  uninstall_agent(pubKey, agentsLocation, name)

  await chainCalls.getCurrentAgentShard(name, async function(shardErr, shard) {

    var currentLeaves = await utils.getCurrentShardLeaves(shard)

    var hostsLeaves = await utils.getHostsLeaves([migrationData[0]])

    var transferLeaf = migrationData[0].id.toB58String() + " " +
                       remainingTime + " " +
                       otherHostRent + " " +
                       endTimeCurrentHost

    var billingLeaves = await utils.getHashedLeaves([transferLeaf])

    var auxCurrentLeaves = currentLeaves.concat()

    auxCurrentLeaves[0] = await utils.getHashedLeaves(auxCurrentLeaves[0])
    auxCurrentLeaves[1] = await utils.getHashedLeaves(auxCurrentLeaves[1])

    hostsLeaves = auxCurrentLeaves[0].concat(hostsLeaves)
    billingLeaves = auxCurrentLeaves[1].concat(billingLeaves)

    //Create merkle trees and get roots

    const hostsTree = new MerkleTree(hostsLeaves, sha3)

    var hostsRoot = await hostsTree.getRoot()

    const billingTree = new MerkleTree(billingLeaves, sha3)

    var billingRoot = await billingTree.getRoot()

    //Post IDs and billings to IPFS

    var hostsIPFS = await ipfsUtils.createIPFSData(hostsLeaves)

    var bufferedBillings = await utils.stringArrToBuffArr(currentLeaves[1].concat([transferLeaf]))

    var billingsIPFS = await ipfsUtils.createIPFSData(bufferedBillings)

    ipfsUtils.postDatatoIPFS(hostsIPFS, function(errHosts, hostsHash) {

      if (errHosts != null) {console.log(errHosts); return}

      if (hostsHash == NaN || hostsHash == undefined) {console.log("Could not store hosts data"); return}

      ipfsUtils.postDatatoIPFS(billingsIPFS, async function(errBillings, billingsHash) {

        if (errBillings != null) {console.log(errBillings); return}

        if (billingsHash == NaN || billingsHash == undefined) {console.log("Could not store billing data"); return}

        //Call deployment function from SC

        await chain.deployAgent( pubKey, name, hostsRoot,
          hostsHash[0].hash.toString(), billingRoot, billingsHash[0].hash.toString(),
          1, 1 )

        //Contact host and ask them to respond to request

        var requestPosition = await chain.getDeploymentsLength(name)
        var dialResponse = 0

        var dialData = await utils.createJSON(['type', 'name', 'position'], ['string', 'string', 'int'],
          ["deployment_response", name, requestPosition - 1])

        dialResponse = network.dialNode(personalNode, protocolName, migrationData[0], [dialData])

        if (dialResponse != undefined && dialResponse != NaN && dialResponse == -1) {

          console.log("Could not contact a host. Aborting deployment")
          return

        }

        var requestIntData = await chain.getDeploymentIntData(name, requestPosition - 1)

        checkTransferConfirmations(pubKey, personalNode, protocolName,
                                  name, requestPosition - 1, requestIntData[1],
                                  billingRoot, billingsHash[0].hash.toString(),
                                  currentLeaves[1].concat([transferLeaf]),
                                  (currentLeaves[1].concat([transferLeaf])).length,
                                  peerList, 1, transferTarget)

      })

    })

  })

}

async function checkTransferConfirmations(pubKey, personalNode, protocolName,
                                         name, position, requestCreationTime,
                                         billingRoot, billingIPFS, billingsArr,
                                         totalHostsNr, hostsPeerIDs, money,
                                         transferTarget) {

  var enoughResponses = await chain.hasEnoughResponses(name, position)

  if (enoughResponses == true) {

    finalizeTransfer(pubKey, personalNode, protocolName,
                     name, position, 0,
                     billingRoot, billingIPFS, billingsArr,
                     totalHostsNr, hostsPeerIDs, money.toString(),
                     transferTarget)

  } else {

    var currentDate = new BigNumber(Math.floor(Date.now() / 1000))

    var responsesWindow = new BigNumber(60)

    if (currentDate.minus(requestCreationTime).isLessThan(responsesWindow)) {

      setTimeout(checkTransferConfirmations, pingDelay, pubKey,
                 personalNode, protocolName, name,
                 position, requestCreationTime, billingRoot,
                 billingIPFS, billingsArr, totalHostsNr,
                 hostsPeerIDs, money, transferTarget)

    } else {

      console.log("Failed request. Did not get enough responses\n")

    }

  }

}

async function finalizeTransfer(pubKey, personalNode, protocolName,
                                name, position, modifType,
                                billingRoot, billingIPFS, billingsArr,
                                totalHostsNr, hostsPeerIDs, money,
                                transferTarget) {

  var decomposedBillings = await utils.decomposeBillings(billingsArr)

  var hostsAddrs = await utils.idsToAddresses(decomposedBillings[0])

  await chain.proposeModification(pubKey, name, hostsAddrs,
                                  decomposedBillings[1], decomposedBillings[2],
                                  billingRoot, billingIPFS, [modifType, totalHostsNr, money])

  var modificationIntData = await chain.getModificationIntData(name)

  var voteWindow = await chain.getVotingWindow()

  var voteThreshold = await chain.getVoteThreshold()

  var dialData = await utils.createJSON(['type', 'name'], ['string', 'string'],
    ["shard_modif", name])

  for (var i = 0; i < hostsPeerIDs.length; i++) {

    var idToBytes = await chain.stringToBytes32(hostsPeerIDs[i].id.toB58String())

    var peerIsHost = await chain.hostCanInteract(idToBytes)

    if (peerIsHost == false) continue

    await network.dialNode(personalNode, protocolName, hostsPeerIDs[i], [dialData])

  }

  applyTransfer(pubKey, personalNode, protocolName, name,
                new BigNumber(modificationIntData[0]).plus(voteWindow),
                voteThreshold, transferTarget, money)

}

async function applyTransfer(pubKey, personalNode, protocolName, name, untilWhen,
    threshold, hostsToDelete, money) {

  var hostInstruction, i

  var modifIntData = await chain.getModificationIntData(name)

  if (modifIntData[3] >= threshold) {

    await chainCalls.changeHostsTrees(pubKey, name, hostsToDelete, 0)

    await chain.applyModification(pubKey, name, money)

    //Send signal to install the agent

    hostInstruction = await utils.createJSON(['type', 'name'], ['string', 'string'],
      ["install_agent", name])

    network.dialNode(personalNode, protocolName, hostsToDelete, [hostInstruction])

    await chain.finishedChangingTree(pubKey, name)

  } else {

    var currentDate = new BigNumber(Math.floor(Date.now() / 1000))

    if (currentDate < untilWhen) {

      setTimeout(applyTransfer, pingDelay, pubKey, personalNode, protocolName, name,
          untilWhen, threshold, hostsToDelete, money)

    } else {

      console.log("The transfer for " + name + " did not get enough acceptance votes" + "\n")

    }

  }

}

module.exports = {

  execute,
  logSecondsHosted,
  getPaid,
  install_agent,
  uninstall_agent,
  migrateAgents,
  update_hosted_agents,
  uninstallAllUnhostedAgents

}
