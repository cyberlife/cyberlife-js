var chain = require('../chainInteraction')
var manager = require('./hostAgentManager')
const async = require('async')

async function executeAction(pbk, consoleOutput) {

    var action = JSON.parse(consoleOutput)

    if (action.type == 'broadcastActivity') {

      await chain.broadcastActivity(pbk)

    }

    if (action.type == 'stopActivity') {

      //TODO: test functions

      var permission = await chain.getShutdownPermission(pbk)

      var currentTime = new BigNumber(Math.floor(Date.now() / 1000))

      if (currentTime >= permission) {

        await manager.logSecondsHosted(pbk, personalNode, async function(err, result) {

          if (err != undefined) {console.log("Could not stop activity"); return}

          await chain.shutdown(pbk)

        })

      } else {

        console.log("You need to wait at least until " + permission.toString() + " to shutdown")

      }

    }

    if (action.type == 'changeRent') {

      await chain.changeRent(action.price, pbk);

    }

    if (action.type == 'getPaid') {

      await manager.getPaid(pbk, personalNode.peerInfo.id.toB58String(),
        agents_location, action.name)

    }

    if (action.type == 'manageStakeWithdrawal') {

      if (action.action == 'signal') {

        await chain.signalStakeWithdrawal(pbk)

      } else if (action.action == 'withdraw') {

        await chain.withdrawStake(pbk)

      }

    }

    if (action.type == 'setID') {

      await chain.setHostID(action.id, pbk)

    }

    if (action.type == 'stake') {

      await chain.stake(pbk, privKey)

    }

    if (action.type == 'install') {

      await manager.install_agent(pbk, personalNode, agents_location, action.name)

    }

}

module.exports = {

  executeAction

}
