var botVersioning = {
  "contractName": "BotVersioning",
  "abi": [
    {
      "inputs": [
        {
          "name": "management",
          "type": "address"
        },
        {
          "name": "_staking",
          "type": "address"
        }
      ],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "constructor"
    },
    {
      "anonymous": false,
      "inputs": [
        {
          "indexed": false,
          "name": "host",
          "type": "address"
        },
        {
          "indexed": false,
          "name": "bot",
          "type": "string"
        }
      ],
      "name": "ChangedBotVersion",
      "type": "event"
    },
    {
      "anonymous": false,
      "inputs": [
        {
          "indexed": false,
          "name": "host",
          "type": "address"
        },
        {
          "indexed": false,
          "name": "bot",
          "type": "string"
        }
      ],
      "name": "ClearedBot",
      "type": "event"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "_newOwner",
          "type": "address"
        }
      ],
      "name": "changeOwner",
      "outputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "_management",
          "type": "address"
        }
      ],
      "name": "setManagement",
      "outputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "_staking",
          "type": "address"
        }
      ],
      "name": "setStaking",
      "outputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "_bot",
          "type": "string"
        }
      ],
      "name": "updateBotVersion",
      "outputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "_bot",
          "type": "string"
        }
      ],
      "name": "clearBot",
      "outputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "constant": true,
      "inputs": [
        {
          "name": "host",
          "type": "address"
        },
        {
          "name": "bot",
          "type": "string"
        }
      ],
      "name": "getHostBotVersion",
      "outputs": [
        {
          "name": "",
          "type": "uint256"
        }
      ],
      "payable": false,
      "stateMutability": "view",
      "type": "function"
    }
  ],
  "bytecode": "0x608060405234801561001057600080fd5b50604051604080610a2083398101604052805160209091015160018054600160a060020a0319908116331790915560028054600160a060020a03948516908316179055600380549390921692169190911790556109ae806100726000396000f3006080604052600436106100775763ffffffff7c01000000000000000000000000000000000000000000000000000000006000350416633408f4f9811461007c5780636bfeced1146100f55780638ff3909914610150578063a6f9dae114610171578063b13af76714610192578063d4a22bde146101eb575b600080fd5b34801561008857600080fd5b5060408051602060046024803582810135601f81018590048502860185019096528585526100e3958335600160a060020a031695369560449491939091019190819084018382808284375094975061020c9650505050505050565b60408051918252519081900360200190f35b34801561010157600080fd5b506040805160206004803580820135601f810184900484028501840190955284845261014e94369492936024939284019190819084018382808284375094975061028a9650505050505050565b005b34801561015c57600080fd5b5061014e600160a060020a036004351661043f565b34801561017d57600080fd5b5061014e600160a060020a036004351661049a565b34801561019e57600080fd5b506040805160206004803580820135601f810184900484028501840190955284845261014e9436949293602493928401919081908401838280828437509497506104e09650505050505050565b3480156101f757600080fd5b5061014e600160a060020a0360043516610927565b600160a060020a038216600090815260208181526040808320905184519192859282918401908083835b602083106102555780518252601f199092019160209182019101610236565b51815160209384036101000a600019018019909216911617905292019485525060405193849003019092205495945050505050565b600354604080517fc1f159700000000000000000000000000000000000000000000000000000000081523360048201529051600160a060020a039092169163c1f15970916024808201926020929091908290030181600087803b1580156102f057600080fd5b505af1158015610304573d6000803e3d6000fd5b505050506040513d602081101561031a57600080fd5b5051151560011461032a57600080fd5b33600090815260208181526040808320905184519192859282918401908083835b6020831061036a5780518252601f19909201916020918201910161034b565b51815160209384036101000a6000190180199092169116179052920194855250604080519485900382018520959095553380855284820186815287519686019690965286517f5389f90260ff8e090dd9c384485ddbdcc61c087c6f7ffcc2b44ec7bdc303b7619691958895509350909160608401919085019080838360005b838110156104015781810151838201526020016103e9565b50505050905090810190601f16801561042e5780820380516001836020036101000a031916815260200191505b50935050505060405180910390a150565b600154600160a060020a0316331461045657600080fd5b600160a060020a038116151561046b57600080fd5b6003805473ffffffffffffffffffffffffffffffffffffffff1916600160a060020a0392909216919091179055565b600154600160a060020a031633146104b157600080fd5b6001805473ffffffffffffffffffffffffffffffffffffffff1916600160a060020a0392909216919091179055565b600354604080517fc1f15970000000000000000000000000000000000000000000000000000000008152336004820152905160009283928392600160a060020a039092169163c1f159709160248082019260209290919082900301818787803b15801561054c57600080fd5b505af1158015610560573d6000803e3d6000fd5b505050506040513d602081101561057657600080fd5b5051151560011461058657600080fd5b6002546040517ffd4eb5b0000000000000000000000000000000000000000000000000000000008152602060048201818152875160248401528751600160a060020a039094169363fd4eb5b093899383926044909201919085019080838360005b838110156105ff5781810151838201526020016105e7565b50505050905090810190601f16801561062c5780820380516001836020036101000a031916815260200191505b5092505050602060405180830381600087803b15801561064b57600080fd5b505af115801561065f573d6000803e3d6000fd5b505050506040513d602081101561067557600080fd5b50516002546040517f13910305000000000000000000000000000000000000000000000000000000008152602060048201818152885160248401528851949750600160a060020a03909316936313910305938993909283926044909201919085019080838360005b838110156106f55781810151838201526020016106dd565b50505050905090810190601f1680156107225780820380516001836020036101000a031916815260200191505b5092505050602060405180830381600087803b15801561074157600080fd5b505af1158015610755573d6000803e3d6000fd5b505050506040513d602081101561076b57600080fd5b5051600254604080517fc09ab91a000000000000000000000000000000000000000000000000000000008152600160a060020a03878116600483015260248201859052915193955091169163c09ab91a916044808201926020929091908290030181600087803b1580156107de57600080fd5b505af11580156107f2573d6000803e3d6000fd5b505050506040513d602081101561080857600080fd5b5051336000908152602081815260409182902091518751939450849388928291908401908083835b6020831061084f5780518252601f199092019160209182019101610830565b51815160209384036101000a600019018019909216911617905292019485525060408051948590038201852095909555338085528482018681528a519686019690965289517fef186ac341b48563e93c27ff0e8225a2cc27b5e01c85e6979273e31457ce7bff9691958b95509350909160608401919085019080838360005b838110156108e65781810151838201526020016108ce565b50505050905090810190601f1680156109135780820380516001836020036101000a031916815260200191505b50935050505060405180910390a150505050565b600154600160a060020a0316331461093e57600080fd5b600160a060020a038116151561095357600080fd5b6002805473ffffffffffffffffffffffffffffffffffffffff1916600160a060020a03929092169190911790555600a165627a7a7230582017626d3fecd2a460b2cc7d67b4bcb0dba772e24db3a2c8a9310136288f22acbe0029",
  "deployedBytecode": "0x6080604052600436106100775763ffffffff7c01000000000000000000000000000000000000000000000000000000006000350416633408f4f9811461007c5780636bfeced1146100f55780638ff3909914610150578063a6f9dae114610171578063b13af76714610192578063d4a22bde146101eb575b600080fd5b34801561008857600080fd5b5060408051602060046024803582810135601f81018590048502860185019096528585526100e3958335600160a060020a031695369560449491939091019190819084018382808284375094975061020c9650505050505050565b60408051918252519081900360200190f35b34801561010157600080fd5b506040805160206004803580820135601f810184900484028501840190955284845261014e94369492936024939284019190819084018382808284375094975061028a9650505050505050565b005b34801561015c57600080fd5b5061014e600160a060020a036004351661043f565b34801561017d57600080fd5b5061014e600160a060020a036004351661049a565b34801561019e57600080fd5b506040805160206004803580820135601f810184900484028501840190955284845261014e9436949293602493928401919081908401838280828437509497506104e09650505050505050565b3480156101f757600080fd5b5061014e600160a060020a0360043516610927565b600160a060020a038216600090815260208181526040808320905184519192859282918401908083835b602083106102555780518252601f199092019160209182019101610236565b51815160209384036101000a600019018019909216911617905292019485525060405193849003019092205495945050505050565b600354604080517fc1f159700000000000000000000000000000000000000000000000000000000081523360048201529051600160a060020a039092169163c1f15970916024808201926020929091908290030181600087803b1580156102f057600080fd5b505af1158015610304573d6000803e3d6000fd5b505050506040513d602081101561031a57600080fd5b5051151560011461032a57600080fd5b33600090815260208181526040808320905184519192859282918401908083835b6020831061036a5780518252601f19909201916020918201910161034b565b51815160209384036101000a6000190180199092169116179052920194855250604080519485900382018520959095553380855284820186815287519686019690965286517f5389f90260ff8e090dd9c384485ddbdcc61c087c6f7ffcc2b44ec7bdc303b7619691958895509350909160608401919085019080838360005b838110156104015781810151838201526020016103e9565b50505050905090810190601f16801561042e5780820380516001836020036101000a031916815260200191505b50935050505060405180910390a150565b600154600160a060020a0316331461045657600080fd5b600160a060020a038116151561046b57600080fd5b6003805473ffffffffffffffffffffffffffffffffffffffff1916600160a060020a0392909216919091179055565b600154600160a060020a031633146104b157600080fd5b6001805473ffffffffffffffffffffffffffffffffffffffff1916600160a060020a0392909216919091179055565b600354604080517fc1f15970000000000000000000000000000000000000000000000000000000008152336004820152905160009283928392600160a060020a039092169163c1f159709160248082019260209290919082900301818787803b15801561054c57600080fd5b505af1158015610560573d6000803e3d6000fd5b505050506040513d602081101561057657600080fd5b5051151560011461058657600080fd5b6002546040517ffd4eb5b0000000000000000000000000000000000000000000000000000000008152602060048201818152875160248401528751600160a060020a039094169363fd4eb5b093899383926044909201919085019080838360005b838110156105ff5781810151838201526020016105e7565b50505050905090810190601f16801561062c5780820380516001836020036101000a031916815260200191505b5092505050602060405180830381600087803b15801561064b57600080fd5b505af115801561065f573d6000803e3d6000fd5b505050506040513d602081101561067557600080fd5b50516002546040517f13910305000000000000000000000000000000000000000000000000000000008152602060048201818152885160248401528851949750600160a060020a03909316936313910305938993909283926044909201919085019080838360005b838110156106f55781810151838201526020016106dd565b50505050905090810190601f1680156107225780820380516001836020036101000a031916815260200191505b5092505050602060405180830381600087803b15801561074157600080fd5b505af1158015610755573d6000803e3d6000fd5b505050506040513d602081101561076b57600080fd5b5051600254604080517fc09ab91a000000000000000000000000000000000000000000000000000000008152600160a060020a03878116600483015260248201859052915193955091169163c09ab91a916044808201926020929091908290030181600087803b1580156107de57600080fd5b505af11580156107f2573d6000803e3d6000fd5b505050506040513d602081101561080857600080fd5b5051336000908152602081815260409182902091518751939450849388928291908401908083835b6020831061084f5780518252601f199092019160209182019101610830565b51815160209384036101000a600019018019909216911617905292019485525060408051948590038201852095909555338085528482018681528a519686019690965289517fef186ac341b48563e93c27ff0e8225a2cc27b5e01c85e6979273e31457ce7bff9691958b95509350909160608401919085019080838360005b838110156108e65781810151838201526020016108ce565b50505050905090810190601f1680156109135780820380516001836020036101000a031916815260200191505b50935050505060405180910390a150505050565b600154600160a060020a0316331461093e57600080fd5b600160a060020a038116151561095357600080fd5b6002805473ffffffffffffffffffffffffffffffffffffffff1916600160a060020a03929092169190911790555600a165627a7a7230582017626d3fecd2a460b2cc7d67b4bcb0dba772e24db3a2c8a9310136288f22acbe0029",
  "sourceMap": "172:1587:7:-;;;538:169;8:9:-1;5:2;;;30:1;27;20:12;5:2;538:169:7;;;;;;;;;;;;;;;;;;;595:5;:18;;-1:-1:-1;;;;;;595:18:7;;;603:10;595:18;;;;620:15;:46;;-1:-1:-1;;;;;620:46:7;;;;;;;;;673:7;:28;;;;;;;;;;;;;;172:1587;;;;;;",
  "deployedSourceMap": "172:1587:7:-;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1631:125;;8:9:-1;5:2;;;30:1;27;20:12;5:2;-1:-1;1631:125:7;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;-1:-1:-1;;;;;1631:125:7;;;;;;;;;;;;;;;;;;;;;;;-1:-1:-1;1631:125:7;;-1:-1:-1;1631:125:7;;-1:-1:-1;;;;;;;1631:125:7;;;;;;;;;;;;;;;;;1475:139;;8:9:-1;5:2;;;30:1;27;20:12;5:2;-1:-1;1475:139:7;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;-1:-1:-1;1475:139:7;;-1:-1:-1;1475:139:7;;-1:-1:-1;;;;;;;1475:139:7;;;961:134;;8:9:-1;5:2;;;30:1;27;20:12;5:2;-1:-1;961:134:7;-1:-1:-1;;;;;961:134:7;;;;;711:80;;8:9:-1;5:2;;;30:1;27;20:12;5:2;-1:-1;711:80:7;-1:-1:-1;;;;;711:80:7;;;;;1099:372;;8:9:-1;5:2;;;30:1;27;20:12;5:2;-1:-1;1099:372:7;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;-1:-1:-1;1099:372:7;;-1:-1:-1;1099:372:7;;-1:-1:-1;;;;;;;1099:372:7;795:162;;8:9:-1;5:2;;;30:1;27;20:12;5:2;-1:-1;795:162:7;-1:-1:-1;;;;;795:162:7;;;;;1631:125;-1:-1:-1;;;;;1728:17:7;;1705:7;1728:17;;;;;;;;;;;:22;;;;:17;;1746:3;;1728:22;;;;;;;;36:153:-1;66:2;58:11;;36:153;;176:10;;164:23;;-1:-1;;139:12;;;;98:2;89:12;;;;114;36:153;;;299:10;344;;263:2;259:12;;;254:3;250:22;-1:-1;;246:30;311:9;;295:26;;;340:21;;377:20;365:33;;1728:22:7;;;;;-1:-1:-1;1728:22:7;;;;;;;;;;;;1631:125;-1:-1:-1;;;;;1631:125:7:o;1475:139::-;397:7;;:40;;;;;;426:10;397:40;;;;;;-1:-1:-1;;;;;397:7:7;;;;:28;;:40;;;;;;;;;;;;;;;:7;;:40;;;5:2:-1;;;;30:1;27;20:12;5:2;397:40:7;;;;8:9:-1;5:2;;;45:16;42:1;39;24:38;77:16;74:1;67:27;5:2;397:40:7;;;;;;;13:2:-1;8:3;5:11;2:2;;;29:1;26;19:12;2:2;-1:-1;397:40:7;:48;;441:4;397:48;389:57;;;;;;1547:10;1567:1;1535:23;;;;;;;;;;;:29;;;;:23;;1559:4;;1535:29;;;;;;;;36:153:-1;66:2;58:11;;36:153;;176:10;;164:23;;-1:-1;;139:12;;;;98:2;89:12;;;;114;36:153;;;299:10;344;;263:2;259:12;;;254:3;250:22;-1:-1;;246:30;311:9;;295:26;;;340:21;;377:20;365:33;;1535:29:7;;;;;-1:-1:-1;1535:29:7;;;;;;;;;;;:33;;;;1591:10;1580:28;;;;;;;;;;;;;;;;;;;;;;1591:10;;1580:28;;-1:-1:-1;1535:29:7;-1:-1:-1;1580:28:7;;;;;;;;;;;;;-1:-1:-1;8:100;33:3;30:1;27:10;8:100;;;90:11;;;84:18;71:11;;;64:39;52:2;45:10;8:100;;;12:14;1580:28:7;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1475:139;:::o;961:134::-;514:5;;-1:-1:-1;;;;;514:5:7;500:10;:19;492:28;;;;;;-1:-1:-1;;;;;1031:22:7;;;;1023:31;;;;;;1061:7;:28;;-1:-1:-1;;1061:28:7;-1:-1:-1;;;;;1061:28:7;;;;;;;;;;961:134::o;711:80::-;514:5;;-1:-1:-1;;;;;514:5:7;500:10;:19;492:28;;;;;;768:5;:17;;-1:-1:-1;;768:17:7;-1:-1:-1;;;;;768:17:7;;;;;;;;;;711:80::o;1099:372::-;397:7;;:40;;;;;;426:10;397:40;;;;;;1167:18;;;;;;-1:-1:-1;;;;;397:7:7;;;;:28;;:40;;;;;;;;;;;;;;;1167:18;397:7;:40;;;5:2:-1;;;;30:1;27;20:12;5:2;397:40:7;;;;8:9:-1;5:2;;;45:16;42:1;39;24:38;77:16;74:1;67:27;5:2;397:40:7;;;;;;;13:2:-1;8:3;5:11;2:2;;;29:1;26;19:12;2:2;-1:-1;397:40:7;:48;;441:4;397:48;389:57;;;;;;1188:15;;:40;;;;;;;;;;;;;;;;;;;;-1:-1:-1;;;;;1188:15:7;;;;:34;;1223:4;;1188:40;;;;;;;;;;;;;;:15;8:100:-1;33:3;30:1;27:10;8:100;;;90:11;;;84:18;71:11;;;64:39;52:2;45:10;8:100;;;12:14;1188:40:7;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;8:9:-1;5:2;;;30:1;27;20:12;5:2;1188:40:7;;;;8:9:-1;5:2;;;45:16;42:1;39;24:38;77:16;74:1;67:27;5:2;1188:40:7;;;;;;;13:2:-1;8:3;5:11;2:2;;;29:1;26;19:12;2:2;-1:-1;1188:40:7;1253:15;;:41;;;;;1188:40;1253:41;;;;;;;;;;;;;;1188:40;;-1:-1:-1;;;;;;1253:15:7;;;;:35;;1289:4;;1253:41;;;;;;;;;;;;;;;;:15;8:100:-1;33:3;30:1;27:10;8:100;;;90:11;;;84:18;71:11;;;64:39;52:2;45:10;8:100;;;12:14;1253:41:7;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;8:9:-1;5:2;;;30:1;27;20:12;5:2;1253:41:7;;;;8:9:-1;5:2;;;45:16;42:1;39;24:38;77:16;74:1;67:27;5:2;1253:41:7;;;;;;;13:2:-1;8:3;5:11;2:2;;;29:1;26;19:12;2:2;-1:-1;1253:41:7;1319:15;;:53;;;;;;-1:-1:-1;;;;;1319:53:7;;;;;;;;;;;;;;;1253:41;;-1:-1:-1;1319:15:7;;;:31;;:53;;;;;1253:41;;1319:53;;;;;;;;:15;;:53;;;5:2:-1;;;;30:1;27;20:12;5:2;1319:53:7;;;;8:9:-1;5:2;;;45:16;42:1;39;24:38;77:16;74:1;67:27;5:2;1319:53:7;;;;;;;13:2:-1;8:3;5:11;2:2;;;29:1;26;19:12;2:2;-1:-1;1319:53:7;1391:10;1379:11;:23;;;1319:53;1379:23;;;;;;;;:29;;;;1319:53;;-1:-1:-1;1319:53:7;;1403:4;;1379:29;;;;;;;;;36:153:-1;66:2;58:11;;36:153;;176:10;;164:23;;-1:-1;;139:12;;;;98:2;89:12;;;;114;36:153;;;299:10;344;;263:2;259:12;;;254:3;250:22;-1:-1;;246:30;311:9;;295:26;;;340:21;;377:20;365:33;;1379:29:7;;;;;-1:-1:-1;1379:29:7;;;;;;;;;;;:39;;;;1448:10;1430:35;;;;;;;;;;;;;;;;;;;;;;1448:10;;1430:35;;-1:-1:-1;1379:29:7;-1:-1:-1;1430:35:7;;;;;;;;;;;;;-1:-1:-1;8:100;33:3;30:1;27:10;8:100;;;90:11;;;84:18;71:11;;;64:39;52:2;45:10;8:100;;;12:14;1430:35:7;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1099:372;;;;:::o;795:162::-;514:5;;-1:-1:-1;;;;;514:5:7;500:10;:19;492:28;;;;;;-1:-1:-1;;;;;871:25:7;;;;863:34;;;;;;904:15;:47;;-1:-1:-1;;904:47:7;-1:-1:-1;;;;;904:47:7;;;;;;;;;;795:162::o",
  "source": "pragma solidity 0.4.24;\n\nimport \"contracts/interfaces/IAgentManagement.sol\";\nimport \"contracts/interfaces/IStaking.sol\";\nimport \"contracts/interfaces/IBotVersioning.sol\";\n\ncontract BotVersioning is IBotVersioning {\n\n  mapping(address => mapping(string => uint256)) botVersions;\n\n  address owner;\n\n  IAgentManagement agentManagement;\n  IStaking staking;\n\n  modifier onlyStakedHost() {\n\n    require(staking.getIsCurrentlyStaked(msg.sender) == true);\n\n    _;\n\n  }\n\n  modifier onlyOwner() {\n\n    require(msg.sender == owner);\n\n    _;\n\n  }\n\n  constructor(address management, address _staking) {\n\n    owner = msg.sender;\n\n    agentManagement = IAgentManagement(management);\n\n    staking = IStaking(_staking);\n\n  }\n\n  function changeOwner(address _newOwner) onlyOwner {\n\n    owner = _newOwner;\n\n  }\n\n  function setManagement(address _management) public onlyOwner {\n\n    require(_management != address(0));\n\n    agentManagement = IAgentManagement(_management);\n\n  }\n\n  function setStaking(address _staking) public onlyOwner {\n\n    require(_staking != address(0));\n\n    staking = IStaking(_staking);\n\n  }\n\n  function updateBotVersion(string _bot) public onlyStakedHost {\n\n    address botCreator = agentManagement.getCreatorFromName(_bot);\n    uint256 position = agentManagement.getPositionFromName(_bot);\n\n    uint256 version = agentManagement.getAgentVersion(botCreator, position);\n\n    botVersions[msg.sender][_bot] = version;\n\n    emit ChangedBotVersion(msg.sender, _bot);\n\n  }\n\n  function clearBot(string _bot) public onlyStakedHost {\n\n    botVersions[msg.sender][_bot] = 0;\n\n    emit ClearedBot(msg.sender, _bot);\n\n  }\n\n  //GETTERS\n\n  function getHostBotVersion(address host, string bot) public view returns (uint256) {\n\n    return botVersions[host][bot];\n\n  }\n\n}\n",
  "sourcePath": "/home/stefan/Work/Cyberlife/chain/protocol/contracts/hosts/BotVersioning.sol",
  "ast": {
    "absolutePath": "/home/stefan/Work/Cyberlife/chain/protocol/contracts/hosts/BotVersioning.sol",
    "exportedSymbols": {
      "BotVersioning": [
        5232
      ]
    },
    "id": 5233,
    "nodeType": "SourceUnit",
    "nodes": [
      {
        "id": 5024,
        "literals": [
          "solidity",
          "0.4",
          ".24"
        ],
        "nodeType": "PragmaDirective",
        "src": "0:23:7"
      },
      {
        "absolutePath": "contracts/interfaces/IAgentManagement.sol",
        "file": "contracts/interfaces/IAgentManagement.sol",
        "id": 5025,
        "nodeType": "ImportDirective",
        "scope": 5233,
        "sourceUnit": 10417,
        "src": "25:51:7",
        "symbolAliases": [],
        "unitAlias": ""
      },
      {
        "absolutePath": "contracts/interfaces/IStaking.sol",
        "file": "contracts/interfaces/IStaking.sol",
        "id": 5026,
        "nodeType": "ImportDirective",
        "scope": 5233,
        "sourceUnit": 11770,
        "src": "77:43:7",
        "symbolAliases": [],
        "unitAlias": ""
      },
      {
        "absolutePath": "contracts/interfaces/IBotVersioning.sol",
        "file": "contracts/interfaces/IBotVersioning.sol",
        "id": 5027,
        "nodeType": "ImportDirective",
        "scope": 5233,
        "sourceUnit": 10800,
        "src": "121:49:7",
        "symbolAliases": [],
        "unitAlias": ""
      },
      {
        "baseContracts": [
          {
            "arguments": null,
            "baseName": {
              "contractScope": null,
              "id": 5028,
              "name": "IBotVersioning",
              "nodeType": "UserDefinedTypeName",
              "referencedDeclaration": 10799,
              "src": "198:14:7",
              "typeDescriptions": {
                "typeIdentifier": "t_contract$_IBotVersioning_$10799",
                "typeString": "contract IBotVersioning"
              }
            },
            "id": 5029,
            "nodeType": "InheritanceSpecifier",
            "src": "198:14:7"
          }
        ],
        "contractDependencies": [
          10799
        ],
        "contractKind": "contract",
        "documentation": null,
        "fullyImplemented": true,
        "id": 5232,
        "linearizedBaseContracts": [
          5232,
          10799
        ],
        "name": "BotVersioning",
        "nodeType": "ContractDefinition",
        "nodes": [
          {
            "constant": false,
            "id": 5035,
            "name": "botVersions",
            "nodeType": "VariableDeclaration",
            "scope": 5232,
            "src": "218:58:7",
            "stateVariable": true,
            "storageLocation": "default",
            "typeDescriptions": {
              "typeIdentifier": "t_mapping$_t_address_$_t_mapping$_t_string_memory_$_t_uint256_$_$",
              "typeString": "mapping(address => mapping(string => uint256))"
            },
            "typeName": {
              "id": 5034,
              "keyType": {
                "id": 5030,
                "name": "address",
                "nodeType": "ElementaryTypeName",
                "src": "226:7:7",
                "typeDescriptions": {
                  "typeIdentifier": "t_address",
                  "typeString": "address"
                }
              },
              "nodeType": "Mapping",
              "src": "218:46:7",
              "typeDescriptions": {
                "typeIdentifier": "t_mapping$_t_address_$_t_mapping$_t_string_memory_$_t_uint256_$_$",
                "typeString": "mapping(address => mapping(string => uint256))"
              },
              "valueType": {
                "id": 5033,
                "keyType": {
                  "id": 5031,
                  "name": "string",
                  "nodeType": "ElementaryTypeName",
                  "src": "245:6:7",
                  "typeDescriptions": {
                    "typeIdentifier": "t_string_storage_ptr",
                    "typeString": "string"
                  }
                },
                "nodeType": "Mapping",
                "src": "237:26:7",
                "typeDescriptions": {
                  "typeIdentifier": "t_mapping$_t_string_memory_$_t_uint256_$",
                  "typeString": "mapping(string => uint256)"
                },
                "valueType": {
                  "id": 5032,
                  "name": "uint256",
                  "nodeType": "ElementaryTypeName",
                  "src": "255:7:7",
                  "typeDescriptions": {
                    "typeIdentifier": "t_uint256",
                    "typeString": "uint256"
                  }
                }
              }
            },
            "value": null,
            "visibility": "internal"
          },
          {
            "constant": false,
            "id": 5037,
            "name": "owner",
            "nodeType": "VariableDeclaration",
            "scope": 5232,
            "src": "281:13:7",
            "stateVariable": true,
            "storageLocation": "default",
            "typeDescriptions": {
              "typeIdentifier": "t_address",
              "typeString": "address"
            },
            "typeName": {
              "id": 5036,
              "name": "address",
              "nodeType": "ElementaryTypeName",
              "src": "281:7:7",
              "typeDescriptions": {
                "typeIdentifier": "t_address",
                "typeString": "address"
              }
            },
            "value": null,
            "visibility": "internal"
          },
          {
            "constant": false,
            "id": 5039,
            "name": "agentManagement",
            "nodeType": "VariableDeclaration",
            "scope": 5232,
            "src": "299:32:7",
            "stateVariable": true,
            "storageLocation": "default",
            "typeDescriptions": {
              "typeIdentifier": "t_contract$_IAgentManagement_$10416",
              "typeString": "contract IAgentManagement"
            },
            "typeName": {
              "contractScope": null,
              "id": 5038,
              "name": "IAgentManagement",
              "nodeType": "UserDefinedTypeName",
              "referencedDeclaration": 10416,
              "src": "299:16:7",
              "typeDescriptions": {
                "typeIdentifier": "t_contract$_IAgentManagement_$10416",
                "typeString": "contract IAgentManagement"
              }
            },
            "value": null,
            "visibility": "internal"
          },
          {
            "constant": false,
            "id": 5041,
            "name": "staking",
            "nodeType": "VariableDeclaration",
            "scope": 5232,
            "src": "335:16:7",
            "stateVariable": true,
            "storageLocation": "default",
            "typeDescriptions": {
              "typeIdentifier": "t_contract$_IStaking_$11769",
              "typeString": "contract IStaking"
            },
            "typeName": {
              "contractScope": null,
              "id": 5040,
              "name": "IStaking",
              "nodeType": "UserDefinedTypeName",
              "referencedDeclaration": 11769,
              "src": "335:8:7",
              "typeDescriptions": {
                "typeIdentifier": "t_contract$_IStaking_$11769",
                "typeString": "contract IStaking"
              }
            },
            "value": null,
            "visibility": "internal"
          },
          {
            "body": {
              "id": 5054,
              "nodeType": "Block",
              "src": "382:78:7",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "commonType": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        },
                        "id": 5050,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "leftExpression": {
                          "argumentTypes": null,
                          "arguments": [
                            {
                              "argumentTypes": null,
                              "expression": {
                                "argumentTypes": null,
                                "id": 5046,
                                "name": "msg",
                                "nodeType": "Identifier",
                                "overloadedDeclarations": [],
                                "referencedDeclaration": 12634,
                                "src": "426:3:7",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_magic_message",
                                  "typeString": "msg"
                                }
                              },
                              "id": 5047,
                              "isConstant": false,
                              "isLValue": false,
                              "isPure": false,
                              "lValueRequested": false,
                              "memberName": "sender",
                              "nodeType": "MemberAccess",
                              "referencedDeclaration": null,
                              "src": "426:10:7",
                              "typeDescriptions": {
                                "typeIdentifier": "t_address",
                                "typeString": "address"
                              }
                            }
                          ],
                          "expression": {
                            "argumentTypes": [
                              {
                                "typeIdentifier": "t_address",
                                "typeString": "address"
                              }
                            ],
                            "expression": {
                              "argumentTypes": null,
                              "id": 5044,
                              "name": "staking",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 5041,
                              "src": "397:7:7",
                              "typeDescriptions": {
                                "typeIdentifier": "t_contract$_IStaking_$11769",
                                "typeString": "contract IStaking"
                              }
                            },
                            "id": 5045,
                            "isConstant": false,
                            "isLValue": false,
                            "isPure": false,
                            "lValueRequested": false,
                            "memberName": "getIsCurrentlyStaked",
                            "nodeType": "MemberAccess",
                            "referencedDeclaration": 11744,
                            "src": "397:28:7",
                            "typeDescriptions": {
                              "typeIdentifier": "t_function_external_view$_t_address_$returns$_t_bool_$",
                              "typeString": "function (address) view external returns (bool)"
                            }
                          },
                          "id": 5048,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "kind": "functionCall",
                          "lValueRequested": false,
                          "names": [],
                          "nodeType": "FunctionCall",
                          "src": "397:40:7",
                          "typeDescriptions": {
                            "typeIdentifier": "t_bool",
                            "typeString": "bool"
                          }
                        },
                        "nodeType": "BinaryOperation",
                        "operator": "==",
                        "rightExpression": {
                          "argumentTypes": null,
                          "hexValue": "74727565",
                          "id": 5049,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": true,
                          "kind": "bool",
                          "lValueRequested": false,
                          "nodeType": "Literal",
                          "src": "441:4:7",
                          "subdenomination": null,
                          "typeDescriptions": {
                            "typeIdentifier": "t_bool",
                            "typeString": "bool"
                          },
                          "value": "true"
                        },
                        "src": "397:48:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      ],
                      "id": 5043,
                      "name": "require",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [
                        12637,
                        12638
                      ],
                      "referencedDeclaration": 12637,
                      "src": "389:7:7",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_require_pure$_t_bool_$returns$__$",
                        "typeString": "function (bool) pure"
                      }
                    },
                    "id": 5051,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "389:57:7",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 5052,
                  "nodeType": "ExpressionStatement",
                  "src": "389:57:7"
                },
                {
                  "id": 5053,
                  "nodeType": "PlaceholderStatement",
                  "src": "453:1:7"
                }
              ]
            },
            "documentation": null,
            "id": 5055,
            "name": "onlyStakedHost",
            "nodeType": "ModifierDefinition",
            "parameters": {
              "id": 5042,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "379:2:7"
            },
            "src": "356:104:7",
            "visibility": "internal"
          },
          {
            "body": {
              "id": 5065,
              "nodeType": "Block",
              "src": "485:49:7",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "commonType": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        },
                        "id": 5061,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "leftExpression": {
                          "argumentTypes": null,
                          "expression": {
                            "argumentTypes": null,
                            "id": 5058,
                            "name": "msg",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 12634,
                            "src": "500:3:7",
                            "typeDescriptions": {
                              "typeIdentifier": "t_magic_message",
                              "typeString": "msg"
                            }
                          },
                          "id": 5059,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "memberName": "sender",
                          "nodeType": "MemberAccess",
                          "referencedDeclaration": null,
                          "src": "500:10:7",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "nodeType": "BinaryOperation",
                        "operator": "==",
                        "rightExpression": {
                          "argumentTypes": null,
                          "id": 5060,
                          "name": "owner",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 5037,
                          "src": "514:5:7",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "src": "500:19:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      ],
                      "id": 5057,
                      "name": "require",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [
                        12637,
                        12638
                      ],
                      "referencedDeclaration": 12637,
                      "src": "492:7:7",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_require_pure$_t_bool_$returns$__$",
                        "typeString": "function (bool) pure"
                      }
                    },
                    "id": 5062,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "492:28:7",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 5063,
                  "nodeType": "ExpressionStatement",
                  "src": "492:28:7"
                },
                {
                  "id": 5064,
                  "nodeType": "PlaceholderStatement",
                  "src": "527:1:7"
                }
              ]
            },
            "documentation": null,
            "id": 5066,
            "name": "onlyOwner",
            "nodeType": "ModifierDefinition",
            "parameters": {
              "id": 5056,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "482:2:7"
            },
            "src": "464:70:7",
            "visibility": "internal"
          },
          {
            "body": {
              "id": 5090,
              "nodeType": "Block",
              "src": "588:119:7",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 5076,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "id": 5073,
                      "name": "owner",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 5037,
                      "src": "595:5:7",
                      "typeDescriptions": {
                        "typeIdentifier": "t_address",
                        "typeString": "address"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "expression": {
                        "argumentTypes": null,
                        "id": 5074,
                        "name": "msg",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 12634,
                        "src": "603:3:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_magic_message",
                          "typeString": "msg"
                        }
                      },
                      "id": 5075,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "lValueRequested": false,
                      "memberName": "sender",
                      "nodeType": "MemberAccess",
                      "referencedDeclaration": null,
                      "src": "603:10:7",
                      "typeDescriptions": {
                        "typeIdentifier": "t_address",
                        "typeString": "address"
                      }
                    },
                    "src": "595:18:7",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "id": 5077,
                  "nodeType": "ExpressionStatement",
                  "src": "595:18:7"
                },
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 5082,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "id": 5078,
                      "name": "agentManagement",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 5039,
                      "src": "620:15:7",
                      "typeDescriptions": {
                        "typeIdentifier": "t_contract$_IAgentManagement_$10416",
                        "typeString": "contract IAgentManagement"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "arguments": [
                        {
                          "argumentTypes": null,
                          "id": 5080,
                          "name": "management",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 5068,
                          "src": "655:10:7",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        }
                      ],
                      "expression": {
                        "argumentTypes": [
                          {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        ],
                        "id": 5079,
                        "name": "IAgentManagement",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 10416,
                        "src": "638:16:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_type$_t_contract$_IAgentManagement_$10416_$",
                          "typeString": "type(contract IAgentManagement)"
                        }
                      },
                      "id": 5081,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "kind": "typeConversion",
                      "lValueRequested": false,
                      "names": [],
                      "nodeType": "FunctionCall",
                      "src": "638:28:7",
                      "typeDescriptions": {
                        "typeIdentifier": "t_contract$_IAgentManagement_$10416",
                        "typeString": "contract IAgentManagement"
                      }
                    },
                    "src": "620:46:7",
                    "typeDescriptions": {
                      "typeIdentifier": "t_contract$_IAgentManagement_$10416",
                      "typeString": "contract IAgentManagement"
                    }
                  },
                  "id": 5083,
                  "nodeType": "ExpressionStatement",
                  "src": "620:46:7"
                },
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 5088,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "id": 5084,
                      "name": "staking",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 5041,
                      "src": "673:7:7",
                      "typeDescriptions": {
                        "typeIdentifier": "t_contract$_IStaking_$11769",
                        "typeString": "contract IStaking"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "arguments": [
                        {
                          "argumentTypes": null,
                          "id": 5086,
                          "name": "_staking",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 5070,
                          "src": "692:8:7",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        }
                      ],
                      "expression": {
                        "argumentTypes": [
                          {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        ],
                        "id": 5085,
                        "name": "IStaking",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 11769,
                        "src": "683:8:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_type$_t_contract$_IStaking_$11769_$",
                          "typeString": "type(contract IStaking)"
                        }
                      },
                      "id": 5087,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "kind": "typeConversion",
                      "lValueRequested": false,
                      "names": [],
                      "nodeType": "FunctionCall",
                      "src": "683:18:7",
                      "typeDescriptions": {
                        "typeIdentifier": "t_contract$_IStaking_$11769",
                        "typeString": "contract IStaking"
                      }
                    },
                    "src": "673:28:7",
                    "typeDescriptions": {
                      "typeIdentifier": "t_contract$_IStaking_$11769",
                      "typeString": "contract IStaking"
                    }
                  },
                  "id": 5089,
                  "nodeType": "ExpressionStatement",
                  "src": "673:28:7"
                }
              ]
            },
            "documentation": null,
            "id": 5091,
            "implemented": true,
            "isConstructor": true,
            "isDeclaredConst": false,
            "modifiers": [],
            "name": "",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 5071,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 5068,
                  "name": "management",
                  "nodeType": "VariableDeclaration",
                  "scope": 5091,
                  "src": "550:18:7",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 5067,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "550:7:7",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                },
                {
                  "constant": false,
                  "id": 5070,
                  "name": "_staking",
                  "nodeType": "VariableDeclaration",
                  "scope": 5091,
                  "src": "570:16:7",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 5069,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "570:7:7",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "549:38:7"
            },
            "payable": false,
            "returnParameters": {
              "id": 5072,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "588:0:7"
            },
            "scope": 5232,
            "src": "538:169:7",
            "stateMutability": "nonpayable",
            "superFunction": null,
            "visibility": "public"
          },
          {
            "body": {
              "id": 5102,
              "nodeType": "Block",
              "src": "761:30:7",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 5100,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "id": 5098,
                      "name": "owner",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 5037,
                      "src": "768:5:7",
                      "typeDescriptions": {
                        "typeIdentifier": "t_address",
                        "typeString": "address"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "id": 5099,
                      "name": "_newOwner",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 5093,
                      "src": "776:9:7",
                      "typeDescriptions": {
                        "typeIdentifier": "t_address",
                        "typeString": "address"
                      }
                    },
                    "src": "768:17:7",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "id": 5101,
                  "nodeType": "ExpressionStatement",
                  "src": "768:17:7"
                }
              ]
            },
            "documentation": null,
            "id": 5103,
            "implemented": true,
            "isConstructor": false,
            "isDeclaredConst": false,
            "modifiers": [
              {
                "arguments": null,
                "id": 5096,
                "modifierName": {
                  "argumentTypes": null,
                  "id": 5095,
                  "name": "onlyOwner",
                  "nodeType": "Identifier",
                  "overloadedDeclarations": [],
                  "referencedDeclaration": 5066,
                  "src": "751:9:7",
                  "typeDescriptions": {
                    "typeIdentifier": "t_modifier$__$",
                    "typeString": "modifier ()"
                  }
                },
                "nodeType": "ModifierInvocation",
                "src": "751:9:7"
              }
            ],
            "name": "changeOwner",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 5094,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 5093,
                  "name": "_newOwner",
                  "nodeType": "VariableDeclaration",
                  "scope": 5103,
                  "src": "732:17:7",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 5092,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "732:7:7",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "731:19:7"
            },
            "payable": false,
            "returnParameters": {
              "id": 5097,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "761:0:7"
            },
            "scope": 5232,
            "src": "711:80:7",
            "stateMutability": "nonpayable",
            "superFunction": null,
            "visibility": "public"
          },
          {
            "body": {
              "id": 5124,
              "nodeType": "Block",
              "src": "856:101:7",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "commonType": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        },
                        "id": 5115,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "leftExpression": {
                          "argumentTypes": null,
                          "id": 5111,
                          "name": "_management",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 5105,
                          "src": "871:11:7",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "nodeType": "BinaryOperation",
                        "operator": "!=",
                        "rightExpression": {
                          "argumentTypes": null,
                          "arguments": [
                            {
                              "argumentTypes": null,
                              "hexValue": "30",
                              "id": 5113,
                              "isConstant": false,
                              "isLValue": false,
                              "isPure": true,
                              "kind": "number",
                              "lValueRequested": false,
                              "nodeType": "Literal",
                              "src": "894:1:7",
                              "subdenomination": null,
                              "typeDescriptions": {
                                "typeIdentifier": "t_rational_0_by_1",
                                "typeString": "int_const 0"
                              },
                              "value": "0"
                            }
                          ],
                          "expression": {
                            "argumentTypes": [
                              {
                                "typeIdentifier": "t_rational_0_by_1",
                                "typeString": "int_const 0"
                              }
                            ],
                            "id": 5112,
                            "isConstant": false,
                            "isLValue": false,
                            "isPure": true,
                            "lValueRequested": false,
                            "nodeType": "ElementaryTypeNameExpression",
                            "src": "886:7:7",
                            "typeDescriptions": {
                              "typeIdentifier": "t_type$_t_address_$",
                              "typeString": "type(address)"
                            },
                            "typeName": "address"
                          },
                          "id": 5114,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": true,
                          "kind": "typeConversion",
                          "lValueRequested": false,
                          "names": [],
                          "nodeType": "FunctionCall",
                          "src": "886:10:7",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "src": "871:25:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      ],
                      "id": 5110,
                      "name": "require",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [
                        12637,
                        12638
                      ],
                      "referencedDeclaration": 12637,
                      "src": "863:7:7",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_require_pure$_t_bool_$returns$__$",
                        "typeString": "function (bool) pure"
                      }
                    },
                    "id": 5116,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "863:34:7",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 5117,
                  "nodeType": "ExpressionStatement",
                  "src": "863:34:7"
                },
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 5122,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "id": 5118,
                      "name": "agentManagement",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 5039,
                      "src": "904:15:7",
                      "typeDescriptions": {
                        "typeIdentifier": "t_contract$_IAgentManagement_$10416",
                        "typeString": "contract IAgentManagement"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "arguments": [
                        {
                          "argumentTypes": null,
                          "id": 5120,
                          "name": "_management",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 5105,
                          "src": "939:11:7",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        }
                      ],
                      "expression": {
                        "argumentTypes": [
                          {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        ],
                        "id": 5119,
                        "name": "IAgentManagement",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 10416,
                        "src": "922:16:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_type$_t_contract$_IAgentManagement_$10416_$",
                          "typeString": "type(contract IAgentManagement)"
                        }
                      },
                      "id": 5121,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "kind": "typeConversion",
                      "lValueRequested": false,
                      "names": [],
                      "nodeType": "FunctionCall",
                      "src": "922:29:7",
                      "typeDescriptions": {
                        "typeIdentifier": "t_contract$_IAgentManagement_$10416",
                        "typeString": "contract IAgentManagement"
                      }
                    },
                    "src": "904:47:7",
                    "typeDescriptions": {
                      "typeIdentifier": "t_contract$_IAgentManagement_$10416",
                      "typeString": "contract IAgentManagement"
                    }
                  },
                  "id": 5123,
                  "nodeType": "ExpressionStatement",
                  "src": "904:47:7"
                }
              ]
            },
            "documentation": null,
            "id": 5125,
            "implemented": true,
            "isConstructor": false,
            "isDeclaredConst": false,
            "modifiers": [
              {
                "arguments": null,
                "id": 5108,
                "modifierName": {
                  "argumentTypes": null,
                  "id": 5107,
                  "name": "onlyOwner",
                  "nodeType": "Identifier",
                  "overloadedDeclarations": [],
                  "referencedDeclaration": 5066,
                  "src": "846:9:7",
                  "typeDescriptions": {
                    "typeIdentifier": "t_modifier$__$",
                    "typeString": "modifier ()"
                  }
                },
                "nodeType": "ModifierInvocation",
                "src": "846:9:7"
              }
            ],
            "name": "setManagement",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 5106,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 5105,
                  "name": "_management",
                  "nodeType": "VariableDeclaration",
                  "scope": 5125,
                  "src": "818:19:7",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 5104,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "818:7:7",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "817:21:7"
            },
            "payable": false,
            "returnParameters": {
              "id": 5109,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "856:0:7"
            },
            "scope": 5232,
            "src": "795:162:7",
            "stateMutability": "nonpayable",
            "superFunction": 10774,
            "visibility": "public"
          },
          {
            "body": {
              "id": 5146,
              "nodeType": "Block",
              "src": "1016:79:7",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "commonType": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        },
                        "id": 5137,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "leftExpression": {
                          "argumentTypes": null,
                          "id": 5133,
                          "name": "_staking",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 5127,
                          "src": "1031:8:7",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "nodeType": "BinaryOperation",
                        "operator": "!=",
                        "rightExpression": {
                          "argumentTypes": null,
                          "arguments": [
                            {
                              "argumentTypes": null,
                              "hexValue": "30",
                              "id": 5135,
                              "isConstant": false,
                              "isLValue": false,
                              "isPure": true,
                              "kind": "number",
                              "lValueRequested": false,
                              "nodeType": "Literal",
                              "src": "1051:1:7",
                              "subdenomination": null,
                              "typeDescriptions": {
                                "typeIdentifier": "t_rational_0_by_1",
                                "typeString": "int_const 0"
                              },
                              "value": "0"
                            }
                          ],
                          "expression": {
                            "argumentTypes": [
                              {
                                "typeIdentifier": "t_rational_0_by_1",
                                "typeString": "int_const 0"
                              }
                            ],
                            "id": 5134,
                            "isConstant": false,
                            "isLValue": false,
                            "isPure": true,
                            "lValueRequested": false,
                            "nodeType": "ElementaryTypeNameExpression",
                            "src": "1043:7:7",
                            "typeDescriptions": {
                              "typeIdentifier": "t_type$_t_address_$",
                              "typeString": "type(address)"
                            },
                            "typeName": "address"
                          },
                          "id": 5136,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": true,
                          "kind": "typeConversion",
                          "lValueRequested": false,
                          "names": [],
                          "nodeType": "FunctionCall",
                          "src": "1043:10:7",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "src": "1031:22:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      ],
                      "id": 5132,
                      "name": "require",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [
                        12637,
                        12638
                      ],
                      "referencedDeclaration": 12637,
                      "src": "1023:7:7",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_require_pure$_t_bool_$returns$__$",
                        "typeString": "function (bool) pure"
                      }
                    },
                    "id": 5138,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "1023:31:7",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 5139,
                  "nodeType": "ExpressionStatement",
                  "src": "1023:31:7"
                },
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 5144,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "id": 5140,
                      "name": "staking",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 5041,
                      "src": "1061:7:7",
                      "typeDescriptions": {
                        "typeIdentifier": "t_contract$_IStaking_$11769",
                        "typeString": "contract IStaking"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "arguments": [
                        {
                          "argumentTypes": null,
                          "id": 5142,
                          "name": "_staking",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 5127,
                          "src": "1080:8:7",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        }
                      ],
                      "expression": {
                        "argumentTypes": [
                          {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        ],
                        "id": 5141,
                        "name": "IStaking",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 11769,
                        "src": "1071:8:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_type$_t_contract$_IStaking_$11769_$",
                          "typeString": "type(contract IStaking)"
                        }
                      },
                      "id": 5143,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "kind": "typeConversion",
                      "lValueRequested": false,
                      "names": [],
                      "nodeType": "FunctionCall",
                      "src": "1071:18:7",
                      "typeDescriptions": {
                        "typeIdentifier": "t_contract$_IStaking_$11769",
                        "typeString": "contract IStaking"
                      }
                    },
                    "src": "1061:28:7",
                    "typeDescriptions": {
                      "typeIdentifier": "t_contract$_IStaking_$11769",
                      "typeString": "contract IStaking"
                    }
                  },
                  "id": 5145,
                  "nodeType": "ExpressionStatement",
                  "src": "1061:28:7"
                }
              ]
            },
            "documentation": null,
            "id": 5147,
            "implemented": true,
            "isConstructor": false,
            "isDeclaredConst": false,
            "modifiers": [
              {
                "arguments": null,
                "id": 5130,
                "modifierName": {
                  "argumentTypes": null,
                  "id": 5129,
                  "name": "onlyOwner",
                  "nodeType": "Identifier",
                  "overloadedDeclarations": [],
                  "referencedDeclaration": 5066,
                  "src": "1006:9:7",
                  "typeDescriptions": {
                    "typeIdentifier": "t_modifier$__$",
                    "typeString": "modifier ()"
                  }
                },
                "nodeType": "ModifierInvocation",
                "src": "1006:9:7"
              }
            ],
            "name": "setStaking",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 5128,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 5127,
                  "name": "_staking",
                  "nodeType": "VariableDeclaration",
                  "scope": 5147,
                  "src": "981:16:7",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 5126,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "981:7:7",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "980:18:7"
            },
            "payable": false,
            "returnParameters": {
              "id": 5131,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "1016:0:7"
            },
            "scope": 5232,
            "src": "961:134:7",
            "stateMutability": "nonpayable",
            "superFunction": 10779,
            "visibility": "public"
          },
          {
            "body": {
              "id": 5191,
              "nodeType": "Block",
              "src": "1160:311:7",
              "statements": [
                {
                  "assignments": [
                    5155
                  ],
                  "declarations": [
                    {
                      "constant": false,
                      "id": 5155,
                      "name": "botCreator",
                      "nodeType": "VariableDeclaration",
                      "scope": 5192,
                      "src": "1167:18:7",
                      "stateVariable": false,
                      "storageLocation": "default",
                      "typeDescriptions": {
                        "typeIdentifier": "t_address",
                        "typeString": "address"
                      },
                      "typeName": {
                        "id": 5154,
                        "name": "address",
                        "nodeType": "ElementaryTypeName",
                        "src": "1167:7:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      "value": null,
                      "visibility": "internal"
                    }
                  ],
                  "id": 5160,
                  "initialValue": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "id": 5158,
                        "name": "_bot",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 5149,
                        "src": "1223:4:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_string_memory_ptr",
                          "typeString": "string memory"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_string_memory_ptr",
                          "typeString": "string memory"
                        }
                      ],
                      "expression": {
                        "argumentTypes": null,
                        "id": 5156,
                        "name": "agentManagement",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 5039,
                        "src": "1188:15:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_contract$_IAgentManagement_$10416",
                          "typeString": "contract IAgentManagement"
                        }
                      },
                      "id": 5157,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "lValueRequested": false,
                      "memberName": "getCreatorFromName",
                      "nodeType": "MemberAccess",
                      "referencedDeclaration": 10338,
                      "src": "1188:34:7",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_external_view$_t_string_memory_ptr_$returns$_t_address_$",
                        "typeString": "function (string memory) view external returns (address)"
                      }
                    },
                    "id": 5159,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "1188:40:7",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "nodeType": "VariableDeclarationStatement",
                  "src": "1167:61:7"
                },
                {
                  "assignments": [
                    5162
                  ],
                  "declarations": [
                    {
                      "constant": false,
                      "id": 5162,
                      "name": "position",
                      "nodeType": "VariableDeclaration",
                      "scope": 5192,
                      "src": "1234:16:7",
                      "stateVariable": false,
                      "storageLocation": "default",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      },
                      "typeName": {
                        "id": 5161,
                        "name": "uint256",
                        "nodeType": "ElementaryTypeName",
                        "src": "1234:7:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      "value": null,
                      "visibility": "internal"
                    }
                  ],
                  "id": 5167,
                  "initialValue": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "id": 5165,
                        "name": "_bot",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 5149,
                        "src": "1289:4:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_string_memory_ptr",
                          "typeString": "string memory"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_string_memory_ptr",
                          "typeString": "string memory"
                        }
                      ],
                      "expression": {
                        "argumentTypes": null,
                        "id": 5163,
                        "name": "agentManagement",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 5039,
                        "src": "1253:15:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_contract$_IAgentManagement_$10416",
                          "typeString": "contract IAgentManagement"
                        }
                      },
                      "id": 5164,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "lValueRequested": false,
                      "memberName": "getPositionFromName",
                      "nodeType": "MemberAccess",
                      "referencedDeclaration": 10345,
                      "src": "1253:35:7",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_external_view$_t_string_memory_ptr_$returns$_t_uint256_$",
                        "typeString": "function (string memory) view external returns (uint256)"
                      }
                    },
                    "id": 5166,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "1253:41:7",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "nodeType": "VariableDeclarationStatement",
                  "src": "1234:60:7"
                },
                {
                  "assignments": [
                    5169
                  ],
                  "declarations": [
                    {
                      "constant": false,
                      "id": 5169,
                      "name": "version",
                      "nodeType": "VariableDeclaration",
                      "scope": 5192,
                      "src": "1301:15:7",
                      "stateVariable": false,
                      "storageLocation": "default",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      },
                      "typeName": {
                        "id": 5168,
                        "name": "uint256",
                        "nodeType": "ElementaryTypeName",
                        "src": "1301:7:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      "value": null,
                      "visibility": "internal"
                    }
                  ],
                  "id": 5175,
                  "initialValue": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "id": 5172,
                        "name": "botCreator",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 5155,
                        "src": "1351:10:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "id": 5173,
                        "name": "position",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 5162,
                        "src": "1363:8:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        },
                        {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      ],
                      "expression": {
                        "argumentTypes": null,
                        "id": 5170,
                        "name": "agentManagement",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 5039,
                        "src": "1319:15:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_contract$_IAgentManagement_$10416",
                          "typeString": "contract IAgentManagement"
                        }
                      },
                      "id": 5171,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "lValueRequested": false,
                      "memberName": "getAgentVersion",
                      "nodeType": "MemberAccess",
                      "referencedDeclaration": 10363,
                      "src": "1319:31:7",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_external_view$_t_address_$_t_uint256_$returns$_t_uint256_$",
                        "typeString": "function (address,uint256) view external returns (uint256)"
                      }
                    },
                    "id": 5174,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "1319:53:7",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "nodeType": "VariableDeclarationStatement",
                  "src": "1301:71:7"
                },
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 5183,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "baseExpression": {
                        "argumentTypes": null,
                        "baseExpression": {
                          "argumentTypes": null,
                          "id": 5176,
                          "name": "botVersions",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 5035,
                          "src": "1379:11:7",
                          "typeDescriptions": {
                            "typeIdentifier": "t_mapping$_t_address_$_t_mapping$_t_string_memory_$_t_uint256_$_$",
                            "typeString": "mapping(address => mapping(string memory => uint256))"
                          }
                        },
                        "id": 5180,
                        "indexExpression": {
                          "argumentTypes": null,
                          "expression": {
                            "argumentTypes": null,
                            "id": 5177,
                            "name": "msg",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 12634,
                            "src": "1391:3:7",
                            "typeDescriptions": {
                              "typeIdentifier": "t_magic_message",
                              "typeString": "msg"
                            }
                          },
                          "id": 5178,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "memberName": "sender",
                          "nodeType": "MemberAccess",
                          "referencedDeclaration": null,
                          "src": "1391:10:7",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "isConstant": false,
                        "isLValue": true,
                        "isPure": false,
                        "lValueRequested": false,
                        "nodeType": "IndexAccess",
                        "src": "1379:23:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_mapping$_t_string_memory_$_t_uint256_$",
                          "typeString": "mapping(string memory => uint256)"
                        }
                      },
                      "id": 5181,
                      "indexExpression": {
                        "argumentTypes": null,
                        "id": 5179,
                        "name": "_bot",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 5149,
                        "src": "1403:4:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_string_memory_ptr",
                          "typeString": "string memory"
                        }
                      },
                      "isConstant": false,
                      "isLValue": true,
                      "isPure": false,
                      "lValueRequested": true,
                      "nodeType": "IndexAccess",
                      "src": "1379:29:7",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "id": 5182,
                      "name": "version",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 5169,
                      "src": "1411:7:7",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "src": "1379:39:7",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "id": 5184,
                  "nodeType": "ExpressionStatement",
                  "src": "1379:39:7"
                },
                {
                  "eventCall": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "expression": {
                          "argumentTypes": null,
                          "id": 5186,
                          "name": "msg",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 12634,
                          "src": "1448:3:7",
                          "typeDescriptions": {
                            "typeIdentifier": "t_magic_message",
                            "typeString": "msg"
                          }
                        },
                        "id": 5187,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "memberName": "sender",
                        "nodeType": "MemberAccess",
                        "referencedDeclaration": null,
                        "src": "1448:10:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "id": 5188,
                        "name": "_bot",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 5149,
                        "src": "1460:4:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_string_memory_ptr",
                          "typeString": "string memory"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        },
                        {
                          "typeIdentifier": "t_string_memory_ptr",
                          "typeString": "string memory"
                        }
                      ],
                      "id": 5185,
                      "name": "ChangedBotVersion",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 10763,
                      "src": "1430:17:7",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_event_nonpayable$_t_address_$_t_string_memory_ptr_$returns$__$",
                        "typeString": "function (address,string memory)"
                      }
                    },
                    "id": 5189,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "1430:35:7",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 5190,
                  "nodeType": "EmitStatement",
                  "src": "1425:40:7"
                }
              ]
            },
            "documentation": null,
            "id": 5192,
            "implemented": true,
            "isConstructor": false,
            "isDeclaredConst": false,
            "modifiers": [
              {
                "arguments": null,
                "id": 5152,
                "modifierName": {
                  "argumentTypes": null,
                  "id": 5151,
                  "name": "onlyStakedHost",
                  "nodeType": "Identifier",
                  "overloadedDeclarations": [],
                  "referencedDeclaration": 5055,
                  "src": "1145:14:7",
                  "typeDescriptions": {
                    "typeIdentifier": "t_modifier$__$",
                    "typeString": "modifier ()"
                  }
                },
                "nodeType": "ModifierInvocation",
                "src": "1145:14:7"
              }
            ],
            "name": "updateBotVersion",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 5150,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 5149,
                  "name": "_bot",
                  "nodeType": "VariableDeclaration",
                  "scope": 5192,
                  "src": "1125:11:7",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_string_memory_ptr",
                    "typeString": "string"
                  },
                  "typeName": {
                    "id": 5148,
                    "name": "string",
                    "nodeType": "ElementaryTypeName",
                    "src": "1125:6:7",
                    "typeDescriptions": {
                      "typeIdentifier": "t_string_storage_ptr",
                      "typeString": "string"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "1124:13:7"
            },
            "payable": false,
            "returnParameters": {
              "id": 5153,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "1160:0:7"
            },
            "scope": 5232,
            "src": "1099:372:7",
            "stateMutability": "nonpayable",
            "superFunction": 10784,
            "visibility": "public"
          },
          {
            "body": {
              "id": 5214,
              "nodeType": "Block",
              "src": "1528:86:7",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 5206,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "baseExpression": {
                        "argumentTypes": null,
                        "baseExpression": {
                          "argumentTypes": null,
                          "id": 5199,
                          "name": "botVersions",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 5035,
                          "src": "1535:11:7",
                          "typeDescriptions": {
                            "typeIdentifier": "t_mapping$_t_address_$_t_mapping$_t_string_memory_$_t_uint256_$_$",
                            "typeString": "mapping(address => mapping(string memory => uint256))"
                          }
                        },
                        "id": 5203,
                        "indexExpression": {
                          "argumentTypes": null,
                          "expression": {
                            "argumentTypes": null,
                            "id": 5200,
                            "name": "msg",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 12634,
                            "src": "1547:3:7",
                            "typeDescriptions": {
                              "typeIdentifier": "t_magic_message",
                              "typeString": "msg"
                            }
                          },
                          "id": 5201,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "memberName": "sender",
                          "nodeType": "MemberAccess",
                          "referencedDeclaration": null,
                          "src": "1547:10:7",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "isConstant": false,
                        "isLValue": true,
                        "isPure": false,
                        "lValueRequested": false,
                        "nodeType": "IndexAccess",
                        "src": "1535:23:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_mapping$_t_string_memory_$_t_uint256_$",
                          "typeString": "mapping(string memory => uint256)"
                        }
                      },
                      "id": 5204,
                      "indexExpression": {
                        "argumentTypes": null,
                        "id": 5202,
                        "name": "_bot",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 5194,
                        "src": "1559:4:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_string_memory_ptr",
                          "typeString": "string memory"
                        }
                      },
                      "isConstant": false,
                      "isLValue": true,
                      "isPure": false,
                      "lValueRequested": true,
                      "nodeType": "IndexAccess",
                      "src": "1535:29:7",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "hexValue": "30",
                      "id": 5205,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": true,
                      "kind": "number",
                      "lValueRequested": false,
                      "nodeType": "Literal",
                      "src": "1567:1:7",
                      "subdenomination": null,
                      "typeDescriptions": {
                        "typeIdentifier": "t_rational_0_by_1",
                        "typeString": "int_const 0"
                      },
                      "value": "0"
                    },
                    "src": "1535:33:7",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "id": 5207,
                  "nodeType": "ExpressionStatement",
                  "src": "1535:33:7"
                },
                {
                  "eventCall": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "expression": {
                          "argumentTypes": null,
                          "id": 5209,
                          "name": "msg",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 12634,
                          "src": "1591:3:7",
                          "typeDescriptions": {
                            "typeIdentifier": "t_magic_message",
                            "typeString": "msg"
                          }
                        },
                        "id": 5210,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "memberName": "sender",
                        "nodeType": "MemberAccess",
                        "referencedDeclaration": null,
                        "src": "1591:10:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "id": 5211,
                        "name": "_bot",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 5194,
                        "src": "1603:4:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_string_memory_ptr",
                          "typeString": "string memory"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        },
                        {
                          "typeIdentifier": "t_string_memory_ptr",
                          "typeString": "string memory"
                        }
                      ],
                      "id": 5208,
                      "name": "ClearedBot",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 10769,
                      "src": "1580:10:7",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_event_nonpayable$_t_address_$_t_string_memory_ptr_$returns$__$",
                        "typeString": "function (address,string memory)"
                      }
                    },
                    "id": 5212,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "1580:28:7",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 5213,
                  "nodeType": "EmitStatement",
                  "src": "1575:33:7"
                }
              ]
            },
            "documentation": null,
            "id": 5215,
            "implemented": true,
            "isConstructor": false,
            "isDeclaredConst": false,
            "modifiers": [
              {
                "arguments": null,
                "id": 5197,
                "modifierName": {
                  "argumentTypes": null,
                  "id": 5196,
                  "name": "onlyStakedHost",
                  "nodeType": "Identifier",
                  "overloadedDeclarations": [],
                  "referencedDeclaration": 5055,
                  "src": "1513:14:7",
                  "typeDescriptions": {
                    "typeIdentifier": "t_modifier$__$",
                    "typeString": "modifier ()"
                  }
                },
                "nodeType": "ModifierInvocation",
                "src": "1513:14:7"
              }
            ],
            "name": "clearBot",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 5195,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 5194,
                  "name": "_bot",
                  "nodeType": "VariableDeclaration",
                  "scope": 5215,
                  "src": "1493:11:7",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_string_memory_ptr",
                    "typeString": "string"
                  },
                  "typeName": {
                    "id": 5193,
                    "name": "string",
                    "nodeType": "ElementaryTypeName",
                    "src": "1493:6:7",
                    "typeDescriptions": {
                      "typeIdentifier": "t_string_storage_ptr",
                      "typeString": "string"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "1492:13:7"
            },
            "payable": false,
            "returnParameters": {
              "id": 5198,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "1528:0:7"
            },
            "scope": 5232,
            "src": "1475:139:7",
            "stateMutability": "nonpayable",
            "superFunction": 10789,
            "visibility": "public"
          },
          {
            "body": {
              "id": 5230,
              "nodeType": "Block",
              "src": "1714:42:7",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "baseExpression": {
                      "argumentTypes": null,
                      "baseExpression": {
                        "argumentTypes": null,
                        "id": 5224,
                        "name": "botVersions",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 5035,
                        "src": "1728:11:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_mapping$_t_address_$_t_mapping$_t_string_memory_$_t_uint256_$_$",
                          "typeString": "mapping(address => mapping(string memory => uint256))"
                        }
                      },
                      "id": 5226,
                      "indexExpression": {
                        "argumentTypes": null,
                        "id": 5225,
                        "name": "host",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 5217,
                        "src": "1740:4:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      "isConstant": false,
                      "isLValue": true,
                      "isPure": false,
                      "lValueRequested": false,
                      "nodeType": "IndexAccess",
                      "src": "1728:17:7",
                      "typeDescriptions": {
                        "typeIdentifier": "t_mapping$_t_string_memory_$_t_uint256_$",
                        "typeString": "mapping(string memory => uint256)"
                      }
                    },
                    "id": 5228,
                    "indexExpression": {
                      "argumentTypes": null,
                      "id": 5227,
                      "name": "bot",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 5219,
                      "src": "1746:3:7",
                      "typeDescriptions": {
                        "typeIdentifier": "t_string_memory_ptr",
                        "typeString": "string memory"
                      }
                    },
                    "isConstant": false,
                    "isLValue": true,
                    "isPure": false,
                    "lValueRequested": false,
                    "nodeType": "IndexAccess",
                    "src": "1728:22:7",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "functionReturnParameters": 5223,
                  "id": 5229,
                  "nodeType": "Return",
                  "src": "1721:29:7"
                }
              ]
            },
            "documentation": null,
            "id": 5231,
            "implemented": true,
            "isConstructor": false,
            "isDeclaredConst": true,
            "modifiers": [],
            "name": "getHostBotVersion",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 5220,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 5217,
                  "name": "host",
                  "nodeType": "VariableDeclaration",
                  "scope": 5231,
                  "src": "1658:12:7",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 5216,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "1658:7:7",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                },
                {
                  "constant": false,
                  "id": 5219,
                  "name": "bot",
                  "nodeType": "VariableDeclaration",
                  "scope": 5231,
                  "src": "1672:10:7",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_string_memory_ptr",
                    "typeString": "string"
                  },
                  "typeName": {
                    "id": 5218,
                    "name": "string",
                    "nodeType": "ElementaryTypeName",
                    "src": "1672:6:7",
                    "typeDescriptions": {
                      "typeIdentifier": "t_string_storage_ptr",
                      "typeString": "string"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "1657:26:7"
            },
            "payable": false,
            "returnParameters": {
              "id": 5223,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 5222,
                  "name": "",
                  "nodeType": "VariableDeclaration",
                  "scope": 5231,
                  "src": "1705:7:7",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_uint256",
                    "typeString": "uint256"
                  },
                  "typeName": {
                    "id": 5221,
                    "name": "uint256",
                    "nodeType": "ElementaryTypeName",
                    "src": "1705:7:7",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "1704:9:7"
            },
            "scope": 5232,
            "src": "1631:125:7",
            "stateMutability": "view",
            "superFunction": 10798,
            "visibility": "public"
          }
        ],
        "scope": 5233,
        "src": "172:1587:7"
      }
    ],
    "src": "0:1760:7"
  },
  "legacyAST": {
    "absolutePath": "/home/stefan/Work/Cyberlife/chain/protocol/contracts/hosts/BotVersioning.sol",
    "exportedSymbols": {
      "BotVersioning": [
        5232
      ]
    },
    "id": 5233,
    "nodeType": "SourceUnit",
    "nodes": [
      {
        "id": 5024,
        "literals": [
          "solidity",
          "0.4",
          ".24"
        ],
        "nodeType": "PragmaDirective",
        "src": "0:23:7"
      },
      {
        "absolutePath": "contracts/interfaces/IAgentManagement.sol",
        "file": "contracts/interfaces/IAgentManagement.sol",
        "id": 5025,
        "nodeType": "ImportDirective",
        "scope": 5233,
        "sourceUnit": 10417,
        "src": "25:51:7",
        "symbolAliases": [],
        "unitAlias": ""
      },
      {
        "absolutePath": "contracts/interfaces/IStaking.sol",
        "file": "contracts/interfaces/IStaking.sol",
        "id": 5026,
        "nodeType": "ImportDirective",
        "scope": 5233,
        "sourceUnit": 11770,
        "src": "77:43:7",
        "symbolAliases": [],
        "unitAlias": ""
      },
      {
        "absolutePath": "contracts/interfaces/IBotVersioning.sol",
        "file": "contracts/interfaces/IBotVersioning.sol",
        "id": 5027,
        "nodeType": "ImportDirective",
        "scope": 5233,
        "sourceUnit": 10800,
        "src": "121:49:7",
        "symbolAliases": [],
        "unitAlias": ""
      },
      {
        "baseContracts": [
          {
            "arguments": null,
            "baseName": {
              "contractScope": null,
              "id": 5028,
              "name": "IBotVersioning",
              "nodeType": "UserDefinedTypeName",
              "referencedDeclaration": 10799,
              "src": "198:14:7",
              "typeDescriptions": {
                "typeIdentifier": "t_contract$_IBotVersioning_$10799",
                "typeString": "contract IBotVersioning"
              }
            },
            "id": 5029,
            "nodeType": "InheritanceSpecifier",
            "src": "198:14:7"
          }
        ],
        "contractDependencies": [
          10799
        ],
        "contractKind": "contract",
        "documentation": null,
        "fullyImplemented": true,
        "id": 5232,
        "linearizedBaseContracts": [
          5232,
          10799
        ],
        "name": "BotVersioning",
        "nodeType": "ContractDefinition",
        "nodes": [
          {
            "constant": false,
            "id": 5035,
            "name": "botVersions",
            "nodeType": "VariableDeclaration",
            "scope": 5232,
            "src": "218:58:7",
            "stateVariable": true,
            "storageLocation": "default",
            "typeDescriptions": {
              "typeIdentifier": "t_mapping$_t_address_$_t_mapping$_t_string_memory_$_t_uint256_$_$",
              "typeString": "mapping(address => mapping(string => uint256))"
            },
            "typeName": {
              "id": 5034,
              "keyType": {
                "id": 5030,
                "name": "address",
                "nodeType": "ElementaryTypeName",
                "src": "226:7:7",
                "typeDescriptions": {
                  "typeIdentifier": "t_address",
                  "typeString": "address"
                }
              },
              "nodeType": "Mapping",
              "src": "218:46:7",
              "typeDescriptions": {
                "typeIdentifier": "t_mapping$_t_address_$_t_mapping$_t_string_memory_$_t_uint256_$_$",
                "typeString": "mapping(address => mapping(string => uint256))"
              },
              "valueType": {
                "id": 5033,
                "keyType": {
                  "id": 5031,
                  "name": "string",
                  "nodeType": "ElementaryTypeName",
                  "src": "245:6:7",
                  "typeDescriptions": {
                    "typeIdentifier": "t_string_storage_ptr",
                    "typeString": "string"
                  }
                },
                "nodeType": "Mapping",
                "src": "237:26:7",
                "typeDescriptions": {
                  "typeIdentifier": "t_mapping$_t_string_memory_$_t_uint256_$",
                  "typeString": "mapping(string => uint256)"
                },
                "valueType": {
                  "id": 5032,
                  "name": "uint256",
                  "nodeType": "ElementaryTypeName",
                  "src": "255:7:7",
                  "typeDescriptions": {
                    "typeIdentifier": "t_uint256",
                    "typeString": "uint256"
                  }
                }
              }
            },
            "value": null,
            "visibility": "internal"
          },
          {
            "constant": false,
            "id": 5037,
            "name": "owner",
            "nodeType": "VariableDeclaration",
            "scope": 5232,
            "src": "281:13:7",
            "stateVariable": true,
            "storageLocation": "default",
            "typeDescriptions": {
              "typeIdentifier": "t_address",
              "typeString": "address"
            },
            "typeName": {
              "id": 5036,
              "name": "address",
              "nodeType": "ElementaryTypeName",
              "src": "281:7:7",
              "typeDescriptions": {
                "typeIdentifier": "t_address",
                "typeString": "address"
              }
            },
            "value": null,
            "visibility": "internal"
          },
          {
            "constant": false,
            "id": 5039,
            "name": "agentManagement",
            "nodeType": "VariableDeclaration",
            "scope": 5232,
            "src": "299:32:7",
            "stateVariable": true,
            "storageLocation": "default",
            "typeDescriptions": {
              "typeIdentifier": "t_contract$_IAgentManagement_$10416",
              "typeString": "contract IAgentManagement"
            },
            "typeName": {
              "contractScope": null,
              "id": 5038,
              "name": "IAgentManagement",
              "nodeType": "UserDefinedTypeName",
              "referencedDeclaration": 10416,
              "src": "299:16:7",
              "typeDescriptions": {
                "typeIdentifier": "t_contract$_IAgentManagement_$10416",
                "typeString": "contract IAgentManagement"
              }
            },
            "value": null,
            "visibility": "internal"
          },
          {
            "constant": false,
            "id": 5041,
            "name": "staking",
            "nodeType": "VariableDeclaration",
            "scope": 5232,
            "src": "335:16:7",
            "stateVariable": true,
            "storageLocation": "default",
            "typeDescriptions": {
              "typeIdentifier": "t_contract$_IStaking_$11769",
              "typeString": "contract IStaking"
            },
            "typeName": {
              "contractScope": null,
              "id": 5040,
              "name": "IStaking",
              "nodeType": "UserDefinedTypeName",
              "referencedDeclaration": 11769,
              "src": "335:8:7",
              "typeDescriptions": {
                "typeIdentifier": "t_contract$_IStaking_$11769",
                "typeString": "contract IStaking"
              }
            },
            "value": null,
            "visibility": "internal"
          },
          {
            "body": {
              "id": 5054,
              "nodeType": "Block",
              "src": "382:78:7",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "commonType": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        },
                        "id": 5050,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "leftExpression": {
                          "argumentTypes": null,
                          "arguments": [
                            {
                              "argumentTypes": null,
                              "expression": {
                                "argumentTypes": null,
                                "id": 5046,
                                "name": "msg",
                                "nodeType": "Identifier",
                                "overloadedDeclarations": [],
                                "referencedDeclaration": 12634,
                                "src": "426:3:7",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_magic_message",
                                  "typeString": "msg"
                                }
                              },
                              "id": 5047,
                              "isConstant": false,
                              "isLValue": false,
                              "isPure": false,
                              "lValueRequested": false,
                              "memberName": "sender",
                              "nodeType": "MemberAccess",
                              "referencedDeclaration": null,
                              "src": "426:10:7",
                              "typeDescriptions": {
                                "typeIdentifier": "t_address",
                                "typeString": "address"
                              }
                            }
                          ],
                          "expression": {
                            "argumentTypes": [
                              {
                                "typeIdentifier": "t_address",
                                "typeString": "address"
                              }
                            ],
                            "expression": {
                              "argumentTypes": null,
                              "id": 5044,
                              "name": "staking",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 5041,
                              "src": "397:7:7",
                              "typeDescriptions": {
                                "typeIdentifier": "t_contract$_IStaking_$11769",
                                "typeString": "contract IStaking"
                              }
                            },
                            "id": 5045,
                            "isConstant": false,
                            "isLValue": false,
                            "isPure": false,
                            "lValueRequested": false,
                            "memberName": "getIsCurrentlyStaked",
                            "nodeType": "MemberAccess",
                            "referencedDeclaration": 11744,
                            "src": "397:28:7",
                            "typeDescriptions": {
                              "typeIdentifier": "t_function_external_view$_t_address_$returns$_t_bool_$",
                              "typeString": "function (address) view external returns (bool)"
                            }
                          },
                          "id": 5048,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "kind": "functionCall",
                          "lValueRequested": false,
                          "names": [],
                          "nodeType": "FunctionCall",
                          "src": "397:40:7",
                          "typeDescriptions": {
                            "typeIdentifier": "t_bool",
                            "typeString": "bool"
                          }
                        },
                        "nodeType": "BinaryOperation",
                        "operator": "==",
                        "rightExpression": {
                          "argumentTypes": null,
                          "hexValue": "74727565",
                          "id": 5049,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": true,
                          "kind": "bool",
                          "lValueRequested": false,
                          "nodeType": "Literal",
                          "src": "441:4:7",
                          "subdenomination": null,
                          "typeDescriptions": {
                            "typeIdentifier": "t_bool",
                            "typeString": "bool"
                          },
                          "value": "true"
                        },
                        "src": "397:48:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      ],
                      "id": 5043,
                      "name": "require",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [
                        12637,
                        12638
                      ],
                      "referencedDeclaration": 12637,
                      "src": "389:7:7",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_require_pure$_t_bool_$returns$__$",
                        "typeString": "function (bool) pure"
                      }
                    },
                    "id": 5051,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "389:57:7",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 5052,
                  "nodeType": "ExpressionStatement",
                  "src": "389:57:7"
                },
                {
                  "id": 5053,
                  "nodeType": "PlaceholderStatement",
                  "src": "453:1:7"
                }
              ]
            },
            "documentation": null,
            "id": 5055,
            "name": "onlyStakedHost",
            "nodeType": "ModifierDefinition",
            "parameters": {
              "id": 5042,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "379:2:7"
            },
            "src": "356:104:7",
            "visibility": "internal"
          },
          {
            "body": {
              "id": 5065,
              "nodeType": "Block",
              "src": "485:49:7",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "commonType": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        },
                        "id": 5061,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "leftExpression": {
                          "argumentTypes": null,
                          "expression": {
                            "argumentTypes": null,
                            "id": 5058,
                            "name": "msg",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 12634,
                            "src": "500:3:7",
                            "typeDescriptions": {
                              "typeIdentifier": "t_magic_message",
                              "typeString": "msg"
                            }
                          },
                          "id": 5059,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "memberName": "sender",
                          "nodeType": "MemberAccess",
                          "referencedDeclaration": null,
                          "src": "500:10:7",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "nodeType": "BinaryOperation",
                        "operator": "==",
                        "rightExpression": {
                          "argumentTypes": null,
                          "id": 5060,
                          "name": "owner",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 5037,
                          "src": "514:5:7",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "src": "500:19:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      ],
                      "id": 5057,
                      "name": "require",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [
                        12637,
                        12638
                      ],
                      "referencedDeclaration": 12637,
                      "src": "492:7:7",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_require_pure$_t_bool_$returns$__$",
                        "typeString": "function (bool) pure"
                      }
                    },
                    "id": 5062,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "492:28:7",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 5063,
                  "nodeType": "ExpressionStatement",
                  "src": "492:28:7"
                },
                {
                  "id": 5064,
                  "nodeType": "PlaceholderStatement",
                  "src": "527:1:7"
                }
              ]
            },
            "documentation": null,
            "id": 5066,
            "name": "onlyOwner",
            "nodeType": "ModifierDefinition",
            "parameters": {
              "id": 5056,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "482:2:7"
            },
            "src": "464:70:7",
            "visibility": "internal"
          },
          {
            "body": {
              "id": 5090,
              "nodeType": "Block",
              "src": "588:119:7",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 5076,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "id": 5073,
                      "name": "owner",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 5037,
                      "src": "595:5:7",
                      "typeDescriptions": {
                        "typeIdentifier": "t_address",
                        "typeString": "address"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "expression": {
                        "argumentTypes": null,
                        "id": 5074,
                        "name": "msg",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 12634,
                        "src": "603:3:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_magic_message",
                          "typeString": "msg"
                        }
                      },
                      "id": 5075,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "lValueRequested": false,
                      "memberName": "sender",
                      "nodeType": "MemberAccess",
                      "referencedDeclaration": null,
                      "src": "603:10:7",
                      "typeDescriptions": {
                        "typeIdentifier": "t_address",
                        "typeString": "address"
                      }
                    },
                    "src": "595:18:7",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "id": 5077,
                  "nodeType": "ExpressionStatement",
                  "src": "595:18:7"
                },
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 5082,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "id": 5078,
                      "name": "agentManagement",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 5039,
                      "src": "620:15:7",
                      "typeDescriptions": {
                        "typeIdentifier": "t_contract$_IAgentManagement_$10416",
                        "typeString": "contract IAgentManagement"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "arguments": [
                        {
                          "argumentTypes": null,
                          "id": 5080,
                          "name": "management",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 5068,
                          "src": "655:10:7",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        }
                      ],
                      "expression": {
                        "argumentTypes": [
                          {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        ],
                        "id": 5079,
                        "name": "IAgentManagement",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 10416,
                        "src": "638:16:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_type$_t_contract$_IAgentManagement_$10416_$",
                          "typeString": "type(contract IAgentManagement)"
                        }
                      },
                      "id": 5081,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "kind": "typeConversion",
                      "lValueRequested": false,
                      "names": [],
                      "nodeType": "FunctionCall",
                      "src": "638:28:7",
                      "typeDescriptions": {
                        "typeIdentifier": "t_contract$_IAgentManagement_$10416",
                        "typeString": "contract IAgentManagement"
                      }
                    },
                    "src": "620:46:7",
                    "typeDescriptions": {
                      "typeIdentifier": "t_contract$_IAgentManagement_$10416",
                      "typeString": "contract IAgentManagement"
                    }
                  },
                  "id": 5083,
                  "nodeType": "ExpressionStatement",
                  "src": "620:46:7"
                },
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 5088,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "id": 5084,
                      "name": "staking",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 5041,
                      "src": "673:7:7",
                      "typeDescriptions": {
                        "typeIdentifier": "t_contract$_IStaking_$11769",
                        "typeString": "contract IStaking"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "arguments": [
                        {
                          "argumentTypes": null,
                          "id": 5086,
                          "name": "_staking",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 5070,
                          "src": "692:8:7",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        }
                      ],
                      "expression": {
                        "argumentTypes": [
                          {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        ],
                        "id": 5085,
                        "name": "IStaking",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 11769,
                        "src": "683:8:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_type$_t_contract$_IStaking_$11769_$",
                          "typeString": "type(contract IStaking)"
                        }
                      },
                      "id": 5087,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "kind": "typeConversion",
                      "lValueRequested": false,
                      "names": [],
                      "nodeType": "FunctionCall",
                      "src": "683:18:7",
                      "typeDescriptions": {
                        "typeIdentifier": "t_contract$_IStaking_$11769",
                        "typeString": "contract IStaking"
                      }
                    },
                    "src": "673:28:7",
                    "typeDescriptions": {
                      "typeIdentifier": "t_contract$_IStaking_$11769",
                      "typeString": "contract IStaking"
                    }
                  },
                  "id": 5089,
                  "nodeType": "ExpressionStatement",
                  "src": "673:28:7"
                }
              ]
            },
            "documentation": null,
            "id": 5091,
            "implemented": true,
            "isConstructor": true,
            "isDeclaredConst": false,
            "modifiers": [],
            "name": "",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 5071,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 5068,
                  "name": "management",
                  "nodeType": "VariableDeclaration",
                  "scope": 5091,
                  "src": "550:18:7",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 5067,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "550:7:7",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                },
                {
                  "constant": false,
                  "id": 5070,
                  "name": "_staking",
                  "nodeType": "VariableDeclaration",
                  "scope": 5091,
                  "src": "570:16:7",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 5069,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "570:7:7",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "549:38:7"
            },
            "payable": false,
            "returnParameters": {
              "id": 5072,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "588:0:7"
            },
            "scope": 5232,
            "src": "538:169:7",
            "stateMutability": "nonpayable",
            "superFunction": null,
            "visibility": "public"
          },
          {
            "body": {
              "id": 5102,
              "nodeType": "Block",
              "src": "761:30:7",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 5100,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "id": 5098,
                      "name": "owner",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 5037,
                      "src": "768:5:7",
                      "typeDescriptions": {
                        "typeIdentifier": "t_address",
                        "typeString": "address"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "id": 5099,
                      "name": "_newOwner",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 5093,
                      "src": "776:9:7",
                      "typeDescriptions": {
                        "typeIdentifier": "t_address",
                        "typeString": "address"
                      }
                    },
                    "src": "768:17:7",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "id": 5101,
                  "nodeType": "ExpressionStatement",
                  "src": "768:17:7"
                }
              ]
            },
            "documentation": null,
            "id": 5103,
            "implemented": true,
            "isConstructor": false,
            "isDeclaredConst": false,
            "modifiers": [
              {
                "arguments": null,
                "id": 5096,
                "modifierName": {
                  "argumentTypes": null,
                  "id": 5095,
                  "name": "onlyOwner",
                  "nodeType": "Identifier",
                  "overloadedDeclarations": [],
                  "referencedDeclaration": 5066,
                  "src": "751:9:7",
                  "typeDescriptions": {
                    "typeIdentifier": "t_modifier$__$",
                    "typeString": "modifier ()"
                  }
                },
                "nodeType": "ModifierInvocation",
                "src": "751:9:7"
              }
            ],
            "name": "changeOwner",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 5094,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 5093,
                  "name": "_newOwner",
                  "nodeType": "VariableDeclaration",
                  "scope": 5103,
                  "src": "732:17:7",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 5092,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "732:7:7",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "731:19:7"
            },
            "payable": false,
            "returnParameters": {
              "id": 5097,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "761:0:7"
            },
            "scope": 5232,
            "src": "711:80:7",
            "stateMutability": "nonpayable",
            "superFunction": null,
            "visibility": "public"
          },
          {
            "body": {
              "id": 5124,
              "nodeType": "Block",
              "src": "856:101:7",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "commonType": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        },
                        "id": 5115,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "leftExpression": {
                          "argumentTypes": null,
                          "id": 5111,
                          "name": "_management",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 5105,
                          "src": "871:11:7",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "nodeType": "BinaryOperation",
                        "operator": "!=",
                        "rightExpression": {
                          "argumentTypes": null,
                          "arguments": [
                            {
                              "argumentTypes": null,
                              "hexValue": "30",
                              "id": 5113,
                              "isConstant": false,
                              "isLValue": false,
                              "isPure": true,
                              "kind": "number",
                              "lValueRequested": false,
                              "nodeType": "Literal",
                              "src": "894:1:7",
                              "subdenomination": null,
                              "typeDescriptions": {
                                "typeIdentifier": "t_rational_0_by_1",
                                "typeString": "int_const 0"
                              },
                              "value": "0"
                            }
                          ],
                          "expression": {
                            "argumentTypes": [
                              {
                                "typeIdentifier": "t_rational_0_by_1",
                                "typeString": "int_const 0"
                              }
                            ],
                            "id": 5112,
                            "isConstant": false,
                            "isLValue": false,
                            "isPure": true,
                            "lValueRequested": false,
                            "nodeType": "ElementaryTypeNameExpression",
                            "src": "886:7:7",
                            "typeDescriptions": {
                              "typeIdentifier": "t_type$_t_address_$",
                              "typeString": "type(address)"
                            },
                            "typeName": "address"
                          },
                          "id": 5114,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": true,
                          "kind": "typeConversion",
                          "lValueRequested": false,
                          "names": [],
                          "nodeType": "FunctionCall",
                          "src": "886:10:7",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "src": "871:25:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      ],
                      "id": 5110,
                      "name": "require",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [
                        12637,
                        12638
                      ],
                      "referencedDeclaration": 12637,
                      "src": "863:7:7",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_require_pure$_t_bool_$returns$__$",
                        "typeString": "function (bool) pure"
                      }
                    },
                    "id": 5116,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "863:34:7",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 5117,
                  "nodeType": "ExpressionStatement",
                  "src": "863:34:7"
                },
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 5122,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "id": 5118,
                      "name": "agentManagement",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 5039,
                      "src": "904:15:7",
                      "typeDescriptions": {
                        "typeIdentifier": "t_contract$_IAgentManagement_$10416",
                        "typeString": "contract IAgentManagement"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "arguments": [
                        {
                          "argumentTypes": null,
                          "id": 5120,
                          "name": "_management",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 5105,
                          "src": "939:11:7",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        }
                      ],
                      "expression": {
                        "argumentTypes": [
                          {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        ],
                        "id": 5119,
                        "name": "IAgentManagement",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 10416,
                        "src": "922:16:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_type$_t_contract$_IAgentManagement_$10416_$",
                          "typeString": "type(contract IAgentManagement)"
                        }
                      },
                      "id": 5121,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "kind": "typeConversion",
                      "lValueRequested": false,
                      "names": [],
                      "nodeType": "FunctionCall",
                      "src": "922:29:7",
                      "typeDescriptions": {
                        "typeIdentifier": "t_contract$_IAgentManagement_$10416",
                        "typeString": "contract IAgentManagement"
                      }
                    },
                    "src": "904:47:7",
                    "typeDescriptions": {
                      "typeIdentifier": "t_contract$_IAgentManagement_$10416",
                      "typeString": "contract IAgentManagement"
                    }
                  },
                  "id": 5123,
                  "nodeType": "ExpressionStatement",
                  "src": "904:47:7"
                }
              ]
            },
            "documentation": null,
            "id": 5125,
            "implemented": true,
            "isConstructor": false,
            "isDeclaredConst": false,
            "modifiers": [
              {
                "arguments": null,
                "id": 5108,
                "modifierName": {
                  "argumentTypes": null,
                  "id": 5107,
                  "name": "onlyOwner",
                  "nodeType": "Identifier",
                  "overloadedDeclarations": [],
                  "referencedDeclaration": 5066,
                  "src": "846:9:7",
                  "typeDescriptions": {
                    "typeIdentifier": "t_modifier$__$",
                    "typeString": "modifier ()"
                  }
                },
                "nodeType": "ModifierInvocation",
                "src": "846:9:7"
              }
            ],
            "name": "setManagement",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 5106,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 5105,
                  "name": "_management",
                  "nodeType": "VariableDeclaration",
                  "scope": 5125,
                  "src": "818:19:7",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 5104,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "818:7:7",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "817:21:7"
            },
            "payable": false,
            "returnParameters": {
              "id": 5109,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "856:0:7"
            },
            "scope": 5232,
            "src": "795:162:7",
            "stateMutability": "nonpayable",
            "superFunction": 10774,
            "visibility": "public"
          },
          {
            "body": {
              "id": 5146,
              "nodeType": "Block",
              "src": "1016:79:7",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "commonType": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        },
                        "id": 5137,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "leftExpression": {
                          "argumentTypes": null,
                          "id": 5133,
                          "name": "_staking",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 5127,
                          "src": "1031:8:7",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "nodeType": "BinaryOperation",
                        "operator": "!=",
                        "rightExpression": {
                          "argumentTypes": null,
                          "arguments": [
                            {
                              "argumentTypes": null,
                              "hexValue": "30",
                              "id": 5135,
                              "isConstant": false,
                              "isLValue": false,
                              "isPure": true,
                              "kind": "number",
                              "lValueRequested": false,
                              "nodeType": "Literal",
                              "src": "1051:1:7",
                              "subdenomination": null,
                              "typeDescriptions": {
                                "typeIdentifier": "t_rational_0_by_1",
                                "typeString": "int_const 0"
                              },
                              "value": "0"
                            }
                          ],
                          "expression": {
                            "argumentTypes": [
                              {
                                "typeIdentifier": "t_rational_0_by_1",
                                "typeString": "int_const 0"
                              }
                            ],
                            "id": 5134,
                            "isConstant": false,
                            "isLValue": false,
                            "isPure": true,
                            "lValueRequested": false,
                            "nodeType": "ElementaryTypeNameExpression",
                            "src": "1043:7:7",
                            "typeDescriptions": {
                              "typeIdentifier": "t_type$_t_address_$",
                              "typeString": "type(address)"
                            },
                            "typeName": "address"
                          },
                          "id": 5136,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": true,
                          "kind": "typeConversion",
                          "lValueRequested": false,
                          "names": [],
                          "nodeType": "FunctionCall",
                          "src": "1043:10:7",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "src": "1031:22:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      ],
                      "id": 5132,
                      "name": "require",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [
                        12637,
                        12638
                      ],
                      "referencedDeclaration": 12637,
                      "src": "1023:7:7",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_require_pure$_t_bool_$returns$__$",
                        "typeString": "function (bool) pure"
                      }
                    },
                    "id": 5138,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "1023:31:7",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 5139,
                  "nodeType": "ExpressionStatement",
                  "src": "1023:31:7"
                },
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 5144,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "id": 5140,
                      "name": "staking",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 5041,
                      "src": "1061:7:7",
                      "typeDescriptions": {
                        "typeIdentifier": "t_contract$_IStaking_$11769",
                        "typeString": "contract IStaking"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "arguments": [
                        {
                          "argumentTypes": null,
                          "id": 5142,
                          "name": "_staking",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 5127,
                          "src": "1080:8:7",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        }
                      ],
                      "expression": {
                        "argumentTypes": [
                          {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        ],
                        "id": 5141,
                        "name": "IStaking",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 11769,
                        "src": "1071:8:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_type$_t_contract$_IStaking_$11769_$",
                          "typeString": "type(contract IStaking)"
                        }
                      },
                      "id": 5143,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "kind": "typeConversion",
                      "lValueRequested": false,
                      "names": [],
                      "nodeType": "FunctionCall",
                      "src": "1071:18:7",
                      "typeDescriptions": {
                        "typeIdentifier": "t_contract$_IStaking_$11769",
                        "typeString": "contract IStaking"
                      }
                    },
                    "src": "1061:28:7",
                    "typeDescriptions": {
                      "typeIdentifier": "t_contract$_IStaking_$11769",
                      "typeString": "contract IStaking"
                    }
                  },
                  "id": 5145,
                  "nodeType": "ExpressionStatement",
                  "src": "1061:28:7"
                }
              ]
            },
            "documentation": null,
            "id": 5147,
            "implemented": true,
            "isConstructor": false,
            "isDeclaredConst": false,
            "modifiers": [
              {
                "arguments": null,
                "id": 5130,
                "modifierName": {
                  "argumentTypes": null,
                  "id": 5129,
                  "name": "onlyOwner",
                  "nodeType": "Identifier",
                  "overloadedDeclarations": [],
                  "referencedDeclaration": 5066,
                  "src": "1006:9:7",
                  "typeDescriptions": {
                    "typeIdentifier": "t_modifier$__$",
                    "typeString": "modifier ()"
                  }
                },
                "nodeType": "ModifierInvocation",
                "src": "1006:9:7"
              }
            ],
            "name": "setStaking",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 5128,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 5127,
                  "name": "_staking",
                  "nodeType": "VariableDeclaration",
                  "scope": 5147,
                  "src": "981:16:7",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 5126,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "981:7:7",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "980:18:7"
            },
            "payable": false,
            "returnParameters": {
              "id": 5131,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "1016:0:7"
            },
            "scope": 5232,
            "src": "961:134:7",
            "stateMutability": "nonpayable",
            "superFunction": 10779,
            "visibility": "public"
          },
          {
            "body": {
              "id": 5191,
              "nodeType": "Block",
              "src": "1160:311:7",
              "statements": [
                {
                  "assignments": [
                    5155
                  ],
                  "declarations": [
                    {
                      "constant": false,
                      "id": 5155,
                      "name": "botCreator",
                      "nodeType": "VariableDeclaration",
                      "scope": 5192,
                      "src": "1167:18:7",
                      "stateVariable": false,
                      "storageLocation": "default",
                      "typeDescriptions": {
                        "typeIdentifier": "t_address",
                        "typeString": "address"
                      },
                      "typeName": {
                        "id": 5154,
                        "name": "address",
                        "nodeType": "ElementaryTypeName",
                        "src": "1167:7:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      "value": null,
                      "visibility": "internal"
                    }
                  ],
                  "id": 5160,
                  "initialValue": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "id": 5158,
                        "name": "_bot",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 5149,
                        "src": "1223:4:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_string_memory_ptr",
                          "typeString": "string memory"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_string_memory_ptr",
                          "typeString": "string memory"
                        }
                      ],
                      "expression": {
                        "argumentTypes": null,
                        "id": 5156,
                        "name": "agentManagement",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 5039,
                        "src": "1188:15:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_contract$_IAgentManagement_$10416",
                          "typeString": "contract IAgentManagement"
                        }
                      },
                      "id": 5157,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "lValueRequested": false,
                      "memberName": "getCreatorFromName",
                      "nodeType": "MemberAccess",
                      "referencedDeclaration": 10338,
                      "src": "1188:34:7",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_external_view$_t_string_memory_ptr_$returns$_t_address_$",
                        "typeString": "function (string memory) view external returns (address)"
                      }
                    },
                    "id": 5159,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "1188:40:7",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "nodeType": "VariableDeclarationStatement",
                  "src": "1167:61:7"
                },
                {
                  "assignments": [
                    5162
                  ],
                  "declarations": [
                    {
                      "constant": false,
                      "id": 5162,
                      "name": "position",
                      "nodeType": "VariableDeclaration",
                      "scope": 5192,
                      "src": "1234:16:7",
                      "stateVariable": false,
                      "storageLocation": "default",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      },
                      "typeName": {
                        "id": 5161,
                        "name": "uint256",
                        "nodeType": "ElementaryTypeName",
                        "src": "1234:7:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      "value": null,
                      "visibility": "internal"
                    }
                  ],
                  "id": 5167,
                  "initialValue": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "id": 5165,
                        "name": "_bot",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 5149,
                        "src": "1289:4:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_string_memory_ptr",
                          "typeString": "string memory"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_string_memory_ptr",
                          "typeString": "string memory"
                        }
                      ],
                      "expression": {
                        "argumentTypes": null,
                        "id": 5163,
                        "name": "agentManagement",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 5039,
                        "src": "1253:15:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_contract$_IAgentManagement_$10416",
                          "typeString": "contract IAgentManagement"
                        }
                      },
                      "id": 5164,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "lValueRequested": false,
                      "memberName": "getPositionFromName",
                      "nodeType": "MemberAccess",
                      "referencedDeclaration": 10345,
                      "src": "1253:35:7",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_external_view$_t_string_memory_ptr_$returns$_t_uint256_$",
                        "typeString": "function (string memory) view external returns (uint256)"
                      }
                    },
                    "id": 5166,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "1253:41:7",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "nodeType": "VariableDeclarationStatement",
                  "src": "1234:60:7"
                },
                {
                  "assignments": [
                    5169
                  ],
                  "declarations": [
                    {
                      "constant": false,
                      "id": 5169,
                      "name": "version",
                      "nodeType": "VariableDeclaration",
                      "scope": 5192,
                      "src": "1301:15:7",
                      "stateVariable": false,
                      "storageLocation": "default",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      },
                      "typeName": {
                        "id": 5168,
                        "name": "uint256",
                        "nodeType": "ElementaryTypeName",
                        "src": "1301:7:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      "value": null,
                      "visibility": "internal"
                    }
                  ],
                  "id": 5175,
                  "initialValue": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "id": 5172,
                        "name": "botCreator",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 5155,
                        "src": "1351:10:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "id": 5173,
                        "name": "position",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 5162,
                        "src": "1363:8:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        },
                        {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      ],
                      "expression": {
                        "argumentTypes": null,
                        "id": 5170,
                        "name": "agentManagement",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 5039,
                        "src": "1319:15:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_contract$_IAgentManagement_$10416",
                          "typeString": "contract IAgentManagement"
                        }
                      },
                      "id": 5171,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "lValueRequested": false,
                      "memberName": "getAgentVersion",
                      "nodeType": "MemberAccess",
                      "referencedDeclaration": 10363,
                      "src": "1319:31:7",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_external_view$_t_address_$_t_uint256_$returns$_t_uint256_$",
                        "typeString": "function (address,uint256) view external returns (uint256)"
                      }
                    },
                    "id": 5174,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "1319:53:7",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "nodeType": "VariableDeclarationStatement",
                  "src": "1301:71:7"
                },
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 5183,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "baseExpression": {
                        "argumentTypes": null,
                        "baseExpression": {
                          "argumentTypes": null,
                          "id": 5176,
                          "name": "botVersions",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 5035,
                          "src": "1379:11:7",
                          "typeDescriptions": {
                            "typeIdentifier": "t_mapping$_t_address_$_t_mapping$_t_string_memory_$_t_uint256_$_$",
                            "typeString": "mapping(address => mapping(string memory => uint256))"
                          }
                        },
                        "id": 5180,
                        "indexExpression": {
                          "argumentTypes": null,
                          "expression": {
                            "argumentTypes": null,
                            "id": 5177,
                            "name": "msg",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 12634,
                            "src": "1391:3:7",
                            "typeDescriptions": {
                              "typeIdentifier": "t_magic_message",
                              "typeString": "msg"
                            }
                          },
                          "id": 5178,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "memberName": "sender",
                          "nodeType": "MemberAccess",
                          "referencedDeclaration": null,
                          "src": "1391:10:7",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "isConstant": false,
                        "isLValue": true,
                        "isPure": false,
                        "lValueRequested": false,
                        "nodeType": "IndexAccess",
                        "src": "1379:23:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_mapping$_t_string_memory_$_t_uint256_$",
                          "typeString": "mapping(string memory => uint256)"
                        }
                      },
                      "id": 5181,
                      "indexExpression": {
                        "argumentTypes": null,
                        "id": 5179,
                        "name": "_bot",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 5149,
                        "src": "1403:4:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_string_memory_ptr",
                          "typeString": "string memory"
                        }
                      },
                      "isConstant": false,
                      "isLValue": true,
                      "isPure": false,
                      "lValueRequested": true,
                      "nodeType": "IndexAccess",
                      "src": "1379:29:7",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "id": 5182,
                      "name": "version",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 5169,
                      "src": "1411:7:7",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "src": "1379:39:7",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "id": 5184,
                  "nodeType": "ExpressionStatement",
                  "src": "1379:39:7"
                },
                {
                  "eventCall": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "expression": {
                          "argumentTypes": null,
                          "id": 5186,
                          "name": "msg",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 12634,
                          "src": "1448:3:7",
                          "typeDescriptions": {
                            "typeIdentifier": "t_magic_message",
                            "typeString": "msg"
                          }
                        },
                        "id": 5187,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "memberName": "sender",
                        "nodeType": "MemberAccess",
                        "referencedDeclaration": null,
                        "src": "1448:10:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "id": 5188,
                        "name": "_bot",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 5149,
                        "src": "1460:4:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_string_memory_ptr",
                          "typeString": "string memory"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        },
                        {
                          "typeIdentifier": "t_string_memory_ptr",
                          "typeString": "string memory"
                        }
                      ],
                      "id": 5185,
                      "name": "ChangedBotVersion",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 10763,
                      "src": "1430:17:7",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_event_nonpayable$_t_address_$_t_string_memory_ptr_$returns$__$",
                        "typeString": "function (address,string memory)"
                      }
                    },
                    "id": 5189,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "1430:35:7",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 5190,
                  "nodeType": "EmitStatement",
                  "src": "1425:40:7"
                }
              ]
            },
            "documentation": null,
            "id": 5192,
            "implemented": true,
            "isConstructor": false,
            "isDeclaredConst": false,
            "modifiers": [
              {
                "arguments": null,
                "id": 5152,
                "modifierName": {
                  "argumentTypes": null,
                  "id": 5151,
                  "name": "onlyStakedHost",
                  "nodeType": "Identifier",
                  "overloadedDeclarations": [],
                  "referencedDeclaration": 5055,
                  "src": "1145:14:7",
                  "typeDescriptions": {
                    "typeIdentifier": "t_modifier$__$",
                    "typeString": "modifier ()"
                  }
                },
                "nodeType": "ModifierInvocation",
                "src": "1145:14:7"
              }
            ],
            "name": "updateBotVersion",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 5150,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 5149,
                  "name": "_bot",
                  "nodeType": "VariableDeclaration",
                  "scope": 5192,
                  "src": "1125:11:7",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_string_memory_ptr",
                    "typeString": "string"
                  },
                  "typeName": {
                    "id": 5148,
                    "name": "string",
                    "nodeType": "ElementaryTypeName",
                    "src": "1125:6:7",
                    "typeDescriptions": {
                      "typeIdentifier": "t_string_storage_ptr",
                      "typeString": "string"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "1124:13:7"
            },
            "payable": false,
            "returnParameters": {
              "id": 5153,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "1160:0:7"
            },
            "scope": 5232,
            "src": "1099:372:7",
            "stateMutability": "nonpayable",
            "superFunction": 10784,
            "visibility": "public"
          },
          {
            "body": {
              "id": 5214,
              "nodeType": "Block",
              "src": "1528:86:7",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 5206,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "baseExpression": {
                        "argumentTypes": null,
                        "baseExpression": {
                          "argumentTypes": null,
                          "id": 5199,
                          "name": "botVersions",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 5035,
                          "src": "1535:11:7",
                          "typeDescriptions": {
                            "typeIdentifier": "t_mapping$_t_address_$_t_mapping$_t_string_memory_$_t_uint256_$_$",
                            "typeString": "mapping(address => mapping(string memory => uint256))"
                          }
                        },
                        "id": 5203,
                        "indexExpression": {
                          "argumentTypes": null,
                          "expression": {
                            "argumentTypes": null,
                            "id": 5200,
                            "name": "msg",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 12634,
                            "src": "1547:3:7",
                            "typeDescriptions": {
                              "typeIdentifier": "t_magic_message",
                              "typeString": "msg"
                            }
                          },
                          "id": 5201,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "memberName": "sender",
                          "nodeType": "MemberAccess",
                          "referencedDeclaration": null,
                          "src": "1547:10:7",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "isConstant": false,
                        "isLValue": true,
                        "isPure": false,
                        "lValueRequested": false,
                        "nodeType": "IndexAccess",
                        "src": "1535:23:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_mapping$_t_string_memory_$_t_uint256_$",
                          "typeString": "mapping(string memory => uint256)"
                        }
                      },
                      "id": 5204,
                      "indexExpression": {
                        "argumentTypes": null,
                        "id": 5202,
                        "name": "_bot",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 5194,
                        "src": "1559:4:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_string_memory_ptr",
                          "typeString": "string memory"
                        }
                      },
                      "isConstant": false,
                      "isLValue": true,
                      "isPure": false,
                      "lValueRequested": true,
                      "nodeType": "IndexAccess",
                      "src": "1535:29:7",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "hexValue": "30",
                      "id": 5205,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": true,
                      "kind": "number",
                      "lValueRequested": false,
                      "nodeType": "Literal",
                      "src": "1567:1:7",
                      "subdenomination": null,
                      "typeDescriptions": {
                        "typeIdentifier": "t_rational_0_by_1",
                        "typeString": "int_const 0"
                      },
                      "value": "0"
                    },
                    "src": "1535:33:7",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "id": 5207,
                  "nodeType": "ExpressionStatement",
                  "src": "1535:33:7"
                },
                {
                  "eventCall": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "expression": {
                          "argumentTypes": null,
                          "id": 5209,
                          "name": "msg",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 12634,
                          "src": "1591:3:7",
                          "typeDescriptions": {
                            "typeIdentifier": "t_magic_message",
                            "typeString": "msg"
                          }
                        },
                        "id": 5210,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "memberName": "sender",
                        "nodeType": "MemberAccess",
                        "referencedDeclaration": null,
                        "src": "1591:10:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "id": 5211,
                        "name": "_bot",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 5194,
                        "src": "1603:4:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_string_memory_ptr",
                          "typeString": "string memory"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        },
                        {
                          "typeIdentifier": "t_string_memory_ptr",
                          "typeString": "string memory"
                        }
                      ],
                      "id": 5208,
                      "name": "ClearedBot",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 10769,
                      "src": "1580:10:7",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_event_nonpayable$_t_address_$_t_string_memory_ptr_$returns$__$",
                        "typeString": "function (address,string memory)"
                      }
                    },
                    "id": 5212,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "1580:28:7",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 5213,
                  "nodeType": "EmitStatement",
                  "src": "1575:33:7"
                }
              ]
            },
            "documentation": null,
            "id": 5215,
            "implemented": true,
            "isConstructor": false,
            "isDeclaredConst": false,
            "modifiers": [
              {
                "arguments": null,
                "id": 5197,
                "modifierName": {
                  "argumentTypes": null,
                  "id": 5196,
                  "name": "onlyStakedHost",
                  "nodeType": "Identifier",
                  "overloadedDeclarations": [],
                  "referencedDeclaration": 5055,
                  "src": "1513:14:7",
                  "typeDescriptions": {
                    "typeIdentifier": "t_modifier$__$",
                    "typeString": "modifier ()"
                  }
                },
                "nodeType": "ModifierInvocation",
                "src": "1513:14:7"
              }
            ],
            "name": "clearBot",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 5195,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 5194,
                  "name": "_bot",
                  "nodeType": "VariableDeclaration",
                  "scope": 5215,
                  "src": "1493:11:7",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_string_memory_ptr",
                    "typeString": "string"
                  },
                  "typeName": {
                    "id": 5193,
                    "name": "string",
                    "nodeType": "ElementaryTypeName",
                    "src": "1493:6:7",
                    "typeDescriptions": {
                      "typeIdentifier": "t_string_storage_ptr",
                      "typeString": "string"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "1492:13:7"
            },
            "payable": false,
            "returnParameters": {
              "id": 5198,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "1528:0:7"
            },
            "scope": 5232,
            "src": "1475:139:7",
            "stateMutability": "nonpayable",
            "superFunction": 10789,
            "visibility": "public"
          },
          {
            "body": {
              "id": 5230,
              "nodeType": "Block",
              "src": "1714:42:7",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "baseExpression": {
                      "argumentTypes": null,
                      "baseExpression": {
                        "argumentTypes": null,
                        "id": 5224,
                        "name": "botVersions",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 5035,
                        "src": "1728:11:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_mapping$_t_address_$_t_mapping$_t_string_memory_$_t_uint256_$_$",
                          "typeString": "mapping(address => mapping(string memory => uint256))"
                        }
                      },
                      "id": 5226,
                      "indexExpression": {
                        "argumentTypes": null,
                        "id": 5225,
                        "name": "host",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 5217,
                        "src": "1740:4:7",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      "isConstant": false,
                      "isLValue": true,
                      "isPure": false,
                      "lValueRequested": false,
                      "nodeType": "IndexAccess",
                      "src": "1728:17:7",
                      "typeDescriptions": {
                        "typeIdentifier": "t_mapping$_t_string_memory_$_t_uint256_$",
                        "typeString": "mapping(string memory => uint256)"
                      }
                    },
                    "id": 5228,
                    "indexExpression": {
                      "argumentTypes": null,
                      "id": 5227,
                      "name": "bot",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 5219,
                      "src": "1746:3:7",
                      "typeDescriptions": {
                        "typeIdentifier": "t_string_memory_ptr",
                        "typeString": "string memory"
                      }
                    },
                    "isConstant": false,
                    "isLValue": true,
                    "isPure": false,
                    "lValueRequested": false,
                    "nodeType": "IndexAccess",
                    "src": "1728:22:7",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "functionReturnParameters": 5223,
                  "id": 5229,
                  "nodeType": "Return",
                  "src": "1721:29:7"
                }
              ]
            },
            "documentation": null,
            "id": 5231,
            "implemented": true,
            "isConstructor": false,
            "isDeclaredConst": true,
            "modifiers": [],
            "name": "getHostBotVersion",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 5220,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 5217,
                  "name": "host",
                  "nodeType": "VariableDeclaration",
                  "scope": 5231,
                  "src": "1658:12:7",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 5216,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "1658:7:7",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                },
                {
                  "constant": false,
                  "id": 5219,
                  "name": "bot",
                  "nodeType": "VariableDeclaration",
                  "scope": 5231,
                  "src": "1672:10:7",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_string_memory_ptr",
                    "typeString": "string"
                  },
                  "typeName": {
                    "id": 5218,
                    "name": "string",
                    "nodeType": "ElementaryTypeName",
                    "src": "1672:6:7",
                    "typeDescriptions": {
                      "typeIdentifier": "t_string_storage_ptr",
                      "typeString": "string"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "1657:26:7"
            },
            "payable": false,
            "returnParameters": {
              "id": 5223,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 5222,
                  "name": "",
                  "nodeType": "VariableDeclaration",
                  "scope": 5231,
                  "src": "1705:7:7",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_uint256",
                    "typeString": "uint256"
                  },
                  "typeName": {
                    "id": 5221,
                    "name": "uint256",
                    "nodeType": "ElementaryTypeName",
                    "src": "1705:7:7",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "1704:9:7"
            },
            "scope": 5232,
            "src": "1631:125:7",
            "stateMutability": "view",
            "superFunction": 10798,
            "visibility": "public"
          }
        ],
        "scope": 5233,
        "src": "172:1587:7"
      }
    ],
    "src": "0:1760:7"
  },
  "compiler": {
    "name": "solc",
    "version": "0.4.24+commit.e67f0147.Emscripten.clang"
  },
  "networks": {
    "1545517246387": {
      "events": {},
      "links": {},
      "address": "0x485b590faef411a1a99d11a02dbb4755ed85b220",
      "transactionHash": "0x05f87ffb6c02c31697842d6668abe97659b88a560a0f2b6387954e33576dd7d8"
    },
    "1545586710519": {
      "events": {},
      "links": {},
      "address": "0x4eb87c820706c164330170d463ca6835b8966657",
      "transactionHash": "0x4b394a22664d2ceda89464c5128f8a8bdc6295ddbf974c96173df95d6b3a57d9"
    }
  },
  "schemaVersion": "2.0.1",
  "updatedAt": "2018-12-23T17:38:48.866Z"
}

module.exports = {

  botVersioning

}
