const {Command, flags} = require('@oclif/command')
var ethUtils = require('ethereumjs-util')

class stake extends Command {

  async run() {

    const {flags} = this.parse(stake);

    this.log(createJSON('stake'));

  }

}

stake.description = `Stake in order to participate as a host in Cyberlife`

stake.flags = {
  version: flags.version({char: 'v'}),
  help: flags.help({char: 'h'})
}

function createJSON(type) {

  return '{"type":' + '"' + type.toString()
         + '"}';

}

module.exports = stake
