const {Command, flags} = require('@oclif/command')
var ethUtils = require('ethereumjs-util')

class install extends Command {

  async run() {

    const {flags} = this.parse(install);

    if (flags.name == undefined) this.log("Please provide a name for the agent")

    else this.log(createJSON("install", flags.name));

  }

}

install.description = `Install a agent`

install.flags = {
  version: flags.version({char: 'v'}),
  help: flags.help({char: 'h'}),
  name: flags.string({char: 'n', description: "Name of the agent"})
}

function createJSON(type, name) {

  return '{"type":' + '"' + type.toString()
         + '", "name":' + '"' + name.toString()
         + '"}';

}

module.exports = install
