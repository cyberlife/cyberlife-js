hostcli
=======

CLI to setup a host node and manage the storage and execution of autonomous agents

[![oclif](https://img.shields.io/badge/cli-oclif-brightgreen.svg)](https://oclif.io)
[![Version](https://img.shields.io/npm/v/hostcli.svg)](https://npmjs.org/package/hostcli)
[![Downloads/week](https://img.shields.io/npm/dw/hostcli.svg)](https://npmjs.org/package/hostcli)
[![License](https://img.shields.io/npm/l/hostcli.svg)](https://github.com/stefanionescu/hostcli/blob/master/package.json)

<!-- toc -->
* [Usage](#usage)
* [Commands](#commands)
<!-- tocstop -->
# Usage
<!-- usage -->
```sh-session
$ npm install -g hostcli
$ host COMMAND
running command...
$ host (-v|--version|version)
hostcli/1.0.0 linux-x64 node-v8.10.0
$ host --help [COMMAND]
USAGE
  $ host COMMAND
...
```
<!-- usagestop -->
# Commands
<!-- commands -->
* [`host hello`](#host-hello)
* [`host help [COMMAND]`](#host-help-command)

## `host hello`

Describe the command here

```
USAGE
  $ host hello

OPTIONS
  -n, --name=name  name to print

DESCRIPTION
  ...
  Extra documentation goes here
```

_See code: [src/commands/hello.js](https://github.com/stefanionescu/hostcli/blob/v1.0.0/src/commands/hello.js)_

## `host help [COMMAND]`

display help for host

```
USAGE
  $ host help [COMMAND]

ARGUMENTS
  COMMAND  command to show help for

OPTIONS
  --all  see all commands in CLI
```

_See code: [@oclif/plugin-help](https://github.com/oclif/plugin-help/blob/v2.1.2/src/commands/help.ts)_
<!-- commandsstop -->
