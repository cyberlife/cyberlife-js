const {Command, flags} = require('@oclif/command')

class fund extends Command {

  async run() {

    const {flags} = this.parse(fund);

    if (flags.name == undefined
        || flags.funds == undefined) this.log("You need to provide all important flags");

    else if (flags.funds <= 0) this.log("You need to provide a positive number for 'funds'")

    else this.log(createJSON("fund", flags.name, flags.funds));

  }

}

fund.description = `Give an agent money to survive`

fund.flags = {
  version: flags.version({char: 'v'}),
  help: flags.help({char: 'h'}),
  name: flags.string({char: 'n', description: 'Name of agent'}),
  funds: flags.string({char: 'f', description: 'How much money denominated in WEI is used to fund the agent'})
}

function createJSON(action_type, name, funds) {

  return '{"type":' + '"' + action_type.toString()
         + '", "name":' + '"' + name.toString()
         + '", "funds":' + '"' + funds.tostring()
         + '"}';

}

module.exports = fund
