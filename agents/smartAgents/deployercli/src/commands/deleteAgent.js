const {Command, flags} = require('@oclif/command')

class deleteAgent extends Command {

  async run() {

    const {flags} = this.parse(deleteAgent);

    if (flags.name == undefined) this.log("You need to provide a name for an agent");

    else this.log(createJSON("deleteAgent", flags.name));

  }

}

deleteAgent.description = `Delete an agent`

deleteAgent.flags = {
  version: flags.version({char: 'v'}),
  help: flags.help({char: 'h'}),
  name: flags.string({char: 'n', description: 'Name of the agent'}),
}

function createJSON(action_type, name, funds) {

  return '{"type":' + '"' + action_type.toString()
         + '", "name":' + '"' + name.toString()
         + '"}';

}

module.exports = deleteAgent
