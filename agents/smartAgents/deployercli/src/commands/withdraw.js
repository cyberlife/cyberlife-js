const {Command, flags} = require('@oclif/command')

class withdraw extends Command {

  async run() {

    const {flags} = this.parse(withdraw)

    if (flags.name == undefined) this.log("You need to provide a name for a agent")

    else if (flags.hostsNumber == undefined) this.log("Please provide the number of hosts that the agent will exit")

    else this.log(createJSON("withdraw", flags.name, flags.hostsNumber))

  }

}

withdraw.description = `Withdraw an agent from hosts`

withdraw.flags = {
  version: flags.version({char: 'v'}),
  help: flags.help({char: 'h'}),
  name: flags.string({char: 'n', description: 'Name of the agent'}),
  hostsNumber: flags.string({char: 'o', description: 'From how many hosts you want the agent to withdraw'})
}

function createJSON(action_type, name, hostsNumber) {

  return '{"type":' + '"' + action_type.toString()
         + '", "name":' + '"' + name.toString()
         + '", "hostsNumber":' + hostsNumber.toString()
         + '}';

}

module.exports = withdraw
