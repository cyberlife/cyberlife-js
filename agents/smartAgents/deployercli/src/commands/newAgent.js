const {Command, flags} = require('@oclif/command')

class newAgent extends Command {

  async run() {

    const {flags} = this.parse(newAgent)

    if (flags.name == undefined
        || flags.source == undefined
        || flags.source_hash == undefined) {this.log("You need to provide all important variables");return;}

    if (flags.name.length == 0) {this.log("You need to provide a name for the agent")}

    else if (flags.source.length == 0) {this.log("You need to provide the Github url of the agent")}

    else if (flags.source_hash.length == 0) {this.log("You need to provide a hash of the code")}

    else this.log(createJSON("newAgent", flags.name, flags.source, flags.source_hash));

  }

}

newAgent.description = `Register a new agent`

newAgent.flags = {
  version: flags.version({char: 'v'}),
  help: flags.help({char: 'h'}),
  name: flags.string({char: 'n', description: 'Name of the agent'}),
  source: flags.string({char: 's', description: 'Github URL of the agent'}),
  source_hash: flags.string({char: 'a', description: 'The hash of the code'})
}

function createJSON(action_type, name, source, source_hash, execution) {

  var json = '{"type":"' + action_type.toString()
              + '", "name":"' + name.toString()
              + '", "source":"' + source.toString()
              + '", "source_hash":"' + source_hash.toString()
              + '"}'

  return json;

}

module.exports = newAgent
