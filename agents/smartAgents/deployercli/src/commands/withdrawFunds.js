const {Command, flags} = require('@oclif/command')

class withdrawFunds extends Command {

  async run() {

    const {flags} = this.parse(withdrawFunds);

    if (flags.name == undefined) this.log("You need to provide a name for an agent");

    else this.log(createJSON("withdrawFunds", flags.name));

  }

}

withdrawFunds.description = `Withdraw funds deposited for an agent`

withdrawFunds.flags = {
  version: flags.version({char: 'v'}),
  help: flags.help({char: 'h'}),
  name: flags.string({char: 'n', description: 'Name of the agent'}),
}

function createJSON(action_type, name) {

  return '{"type":' + '"' + action_type.toString()
         + '", "name":' + '"' + name.toString()
         + '"}';

}

module.exports = withdrawFunds
