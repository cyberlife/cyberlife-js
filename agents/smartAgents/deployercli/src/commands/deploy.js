const {Command, flags} = require('@oclif/command')

class deploy extends Command {

  async run() {

    const {flags} = this.parse(deploy);

    if (flags.name == undefined) this.log("You need to provide a name for an agent");

    else if (flags.responsesNumber == undefined) this.log("You need to provide a number of hosts where you want the agent deployed")

    else if (flags.time == undefined) this.log("Please specify how many seconds you want the agent to stay on the hosts")

    else this.log(createJSON("deploy", flags.name, flags.time, flags.responsesNumber));

  }

}

deploy.description = `Deploy agent on more hosts`

deploy.flags = {
  version: flags.version({char: 'v'}),
  help: flags.help({char: 'h'}),
  name: flags.string({char: 'n', description: 'Name of agent'}),
  time: flags.string({char: 't', description: 'How much time do you want the agent to stay on the new hosts'}),
  responsesNumber: flags.string({char: 'r', description: 'On how many hosts you want the agent to be deployed'})
}

function createJSON(action_type, name, time, responsesNumber) {

  return '{"type":' + '"' + action_type.toString()
         + '", "name":' + '"' + name.toString()
         + '", "time":' + '"' + time.toString()
         + '", "responsesNumber":' + responsesNumber.toString()
         + '}';

}

module.exports = deploy
