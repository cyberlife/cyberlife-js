const {Command, flags} = require('@oclif/command')

class grant_autonomy extends Command {

  async run() {

    const {flags} = this.parse(grant_autonomy);

    if (flags.name == undefined || flags.name.length == 0) this.log("You need to provide an agent name");

    else if (flags.position == undefined || flags.position < 0) this.log("Please input a valid position for the agent")

    else this.log(createJSON("autonomy", flags.name, flags.position));

  }

}

grant_autonomy.description = `Make the agent fully autonomous (nobody can control it anymore)`

grant_autonomy.flags = {
  version: flags.version({char: 'v'}),
  help: flags.help({char: 'h'}),
  name: flags.string({char: 'n', description: 'Name of agent'}),
  position: flags.string({char: 'p', description: 'Position of the agent in the array of the creator'})
}

function createJSON(action_type, name, position) {

  return '{"type":"' + action_type
          + '", "name":"' + name
          + '", "position":"' + position
          + '"}';

}

module.exports = grant_autonomy
