pragma solidity 0.5.3;

import "contracts/interfaces/IClearingHouse.sol";
import "contracts/interfaces/IPartenered.sol";
import "contracts/interfaces/ICashbackLib.sol";

import "contracts/zeppelin/ownership/Ownable.sol";
import "contracts/zeppelin/ContractDetector.sol";

contract CashbackLib is ICashbackLib, Ownable, ContractDetector {

  IClearingHouse house;
  IPartenered partners;

  constructor() public {}

  function setHouse(address _house) public onlyOwner {

    require(isContract(_house) == true, "The address must be from a contract");

    house = IClearingHouse(_house);

    emit SetHouse(_house);

  }

  function setPartners(address _partners) public onlyOwner {

    require(isContract(_partners) == true, "The address must be from a contract");

    partners = IPartenered(_partners);

    emit SetPartners(_partners);

  }

  //UTILS

  function serializeOrder(uint256 _charityCut,
                          address[] memory receivers,
                          uint256[] memory shares,
                          uint256 money) public view returns (bytes memory) {

    return (abi.encode(_charityCut, receivers, shares, money));

  }

  function computeTwoPartyOrder(address sender,
                                address company,
                                uint256 totalMoney
                              ) public view returns (bytes memory, uint256) {

    uint256 companyCashback = partners.getCompanyCashbackAmount(company);

    uint256 marketCut = partners.getCompanyMarketCut(company);

    uint256 minCharityPercentage = house.getMinimumCharityCut();

    uint256 marketMoney = totalMoney * marketCut / 100;

    bytes memory serialized;

    if (companyCashback > 0) {

      address[] memory twoParties = new address[](2);
      uint256[] memory twoShares = new uint256[](2);

      twoParties[0] = company;
      twoParties[1] = sender;

      twoShares[0] = companyCashback;
      twoShares[1] = 100 - minCharityPercentage - companyCashback;

      serialized = serializeOrder(minCharityPercentage, twoParties,
                                  twoShares, marketMoney);

    } else {

      address[] memory oneParty = new address[](1);
      uint256[] memory oneShare = new uint256[](1);

      oneParty[0] = sender;

      oneShare[0] = 100 - minCharityPercentage;

      serialized = serializeOrder(minCharityPercentage, oneParty,
                                  oneShare, marketMoney);

    }

    return (serialized, marketMoney);

  }

  function correctOrder(bytes memory data, uint256 money) public view returns (bool) {

    uint256 _charityCut;
    address[] memory receivers;
    uint256[] memory shares;
    uint256 _money;

    (_charityCut, receivers, shares, _money)
        = abi.decode(data,(uint256, address[], uint256[], uint256));

    uint256 minCharityPercentage = house.getMinimumCharityCut();

    return (

      money == _money &&
      receivers.length == shares.length &&
      _charityCut >= minCharityPercentage &&
      true == checkSharesValidity(_charityCut, shares)

    );

  }

  function checkSharesValidity(uint256 charity, uint256[] memory shares)
    public view returns (bool) {

    uint256 total = charity;

    bool noNullShare = true;

    for (uint i = 0; i < shares.length; i++) {

      total += shares[i];

      if (shares[i] == 0) {noNullShare = false; break;}

    }

    return (total == 100 && noNullShare);

  }

}
