pragma solidity 0.5.3;

import "contracts/zeppelin/ownership/Ownable.sol";
import "contracts/zeppelin/SafeMath.sol";
import "contracts/zeppelin/ContractDetector.sol";

import "contracts/interfaces/ITreasury.sol";

contract Treasury is Ownable, ITreasury, ContractDetector {

  using SafeMath for uint256;

  address cashbackMarkets;

  mapping(address => uint256) ethBalances;

  modifier onlyMarkets() {

    require(msg.sender == cashbackMarkets, "The sender is not the markets contract");
    _;

  }

  constructor() public {}

  function() external payable {

    revert();

  }

  function setMarkets(address _markets) public onlyOwner {

    require(isContract(_markets) == true, "The param specified is not a contract address");
    cashbackMarkets = _markets;

    emit SetMarkets(_markets);

  }

  function fundTarget(address target) public payable onlyMarkets() {

    ethBalances[target] = ethBalances[target].add(msg.value);

    emit FundedTarget(target, msg.value);

  }

  //GETTERS

  function getTreasuryBalance(address target) public view returns (uint256) {

    return ethBalances[target];

  }

}
