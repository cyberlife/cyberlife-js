pragma solidity 0.5.3;

import "contracts/shop/CashbackUpgrade.sol";
import "contracts/shop/ECDSA.sol";

import "contracts/zeppelin/ownership/Ownable.sol";

contract Shop is CashbackUpgrade, Ownable {

  using ECDSA for string;

  mapping(address => bytes[]) shopOrders;

  constructor() CashbackUpgrade() public {}

  function buySomething(address customer, string memory encryptedOrder,
                        uint8 v, bytes32 r, bytes32 s) public payable {

    require(msg.value > 0, "You need to send a positive amount of money");

    require(encryptedOrder.verifyString(v, r, s) == owner,
      "The owner of the contract did not sign the order");

    bytes memory convertedOrder = bytes(encryptedOrder);

    shopOrders[customer].push(convertedOrder);

    this.createCashbackOrder.value(msg.value)(customer);

  }

}
