pragma solidity 0.5.3;

contract ICashbackLib {

  event SetHouse(address house);

  event SetPartners(address partners);


  function setHouse(address _house) public;

  function setPartners(address _partners) public;


  function serializeOrder(uint256 _charityCut,
                          address[] memory receivers,
                          uint256[] memory shares,
                          uint256 money) public view returns (bytes memory);

  function computeTwoPartyOrder(address sender,
                                address company,
                                uint256 totalMoney)
                                public view returns (bytes memory, uint256);

  function correctOrder(bytes memory data, uint256 money) public view returns (bool);

  function checkSharesValidity(uint256 charity, uint256[] memory shares) public view returns (bool);

}
