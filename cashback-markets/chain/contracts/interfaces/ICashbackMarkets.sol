pragma solidity 0.5.3;

contract ICashbackMarkets {

  event SetClearingHouse(address _house);

  event ExecutedOrder(address company);

  event SendToCharity(address charity, uint256 _amount);

  event ToggleCharity(string ipfsCharities, address _charityAddress);

  event SetPreferences(address preferences);


  function executeOrder(address company, address payable targetCharity,
                        uint256 charityPercentage,
                        address[] memory targets, uint256[] memory paymentShares)
                        public payable;

  function distributePayments(uint256 totalPayment,
                              uint256 cashbackAllocation,
                              address company,
                              address[] memory targets,
                              uint256[] memory paymentShares)
                              internal;

  function sendToCharity(address payable charity, uint256 _amount) internal;

  function setCashbackPreferences(address _preferences) public;

  function toggleCharityAddress(string memory ipfsCharities, address payable _charityAddress)
    public;

  function isCharityAvailable(address _charity) public view returns (bool);

}
