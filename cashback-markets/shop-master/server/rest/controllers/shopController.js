var gateway = require('../crypto/chain/chainGateway.js');

exports.shop_post_order = async (req, res, next) => {

  try {

    gateway.checkout(req.body.fiat,
                     req.body.order,
                     req.body.customer,
                     req.body.business,
                     req.body.network,
                     function(err, chainResult) {

      if (err) {

        res.status(500).json({

          error: JSON.stringify(err)

        });

      }

      res.status(200).json({

        bondedFiat: chainResult[0].toString(),
        treasuryFiat: chainResult[1].toString()

      });

    });

  } catch(err) {

    res.status(500).json({

      error: JSON.stringify(err)

    });

  }

}
