const express = require('express');
const router = express.Router();

const ShopController = require('../controllers/shopController');

//Handle incoming requests

router.post('/', ShopController.shop_post_order);

module.exports = router;
